using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;
using System.Reflection.Emit;

namespace Scythe
{

    public delegate bool AddCustomizableMenuDelegate(string menuidstr, string menuname, string kbdsecname, bool addtomainmenu);

    public delegate bool AddExtensionsMainMenuDelegate();

    public delegate IntPtr AddMediaItemToTrackDelegate(IntPtr tr);

    public delegate int AddProjectMarkerDelegate(IntPtr proj, bool isrgn, double pos, double rgnend, string name, int wantidx);

    public delegate int AddProjectMarker2Delegate(IntPtr proj, bool isrgn, double pos, double rgnend, string name, int wantidx, int color);

    public delegate IntPtr AddTakeToMediaItemDelegate(IntPtr item);

    public delegate bool AddTempoTimeSigMarkerDelegate(IntPtr proj, double timepos, double bpm, int timesig_num, int timesig_denom, bool lineartempochange);

    public delegate void adjustZoomDelegate(double amt, int forceset, bool doupd, int centermode);

    public delegate bool AnyTrackSoloDelegate(IntPtr proj);

    public delegate void APITestDelegate();

    public delegate bool ApplyNudgeDelegate(IntPtr project, int nudgeflag, int nudgewhat, int nudgeunits, double value, bool reverse, int copies);

    public delegate int Audio_IsPreBufferDelegate();

    public delegate int Audio_IsRunningDelegate();

    public delegate int Audio_RegHardwareHookDelegate(bool isAdd, IntPtr reg);

    public delegate bool AudioAccessorValidateStateDelegate(IntPtr accessor);

    public delegate void BypassFxAllTracksDelegate(int bypass);

    public delegate int CalculatePeaksDelegate(IntPtr srcBlock, IntPtr pksBlock);

    public delegate int CalculatePeaksFloatSrcPtrDelegate(IntPtr srcBlock, IntPtr pksBlock);

    public delegate void ClearAllRecArmedDelegate();

    public delegate void ClearPeakCacheDelegate();

    public delegate int CountActionShortcutsDelegate(IntPtr section, int cmdID);

    public delegate int CountMediaItemsDelegate(IntPtr proj);

    public delegate int CountProjectMarkersDelegate(IntPtr proj, ref int num_markersOut, ref int num_regionsOut);

    public delegate int CountSelectedMediaItemsDelegate(IntPtr proj);

    public delegate int CountSelectedTracksDelegate(IntPtr proj);

    public delegate int CountTakesDelegate(IntPtr item);

    public delegate int CountTCPFXParmsDelegate(IntPtr project, IntPtr track);

    public delegate int CountTempoTimeSigMarkersDelegate(IntPtr proj);

    public delegate int CountTrackEnvelopesDelegate(IntPtr track);

    public delegate int CountTrackMediaItemsDelegate(IntPtr track);

    public delegate int CountTracksDelegate(IntPtr proj);

    public delegate IntPtr CreateLocalOscHandlerDelegate(IntPtr obj, IntPtr callback);

    public delegate IntPtr CreateMIDIInputDelegate(int dev);

    public delegate IntPtr CreateMIDIOutputDelegate(int dev, bool streamMode, ref int msoffset100);

    public delegate IntPtr CreateNewMIDIItemInProjDelegate(IntPtr track, double starttime, double endtime);

    public delegate IntPtr CreateTakeAudioAccessorDelegate(IntPtr take);

    public delegate IntPtr CreateTrackAudioAccessorDelegate(IntPtr track);

    public delegate void CSurf_FlushUndoDelegate(bool force);

    public delegate bool CSurf_GetTouchStateDelegate(IntPtr trackid, int isPan);

    public delegate void CSurf_GoEndDelegate();

    public delegate void CSurf_GoStartDelegate();

    public delegate int CSurf_NumTracksDelegate(bool mcpView);

    public delegate void CSurf_OnArrowDelegate(int whichdir, bool wantzoom);

    public delegate void CSurf_OnFwdDelegate(int seekplay);

    public delegate bool CSurf_OnFXChangeDelegate(IntPtr trackid, int en);

    public delegate int CSurf_OnInputMonitorChangeDelegate(IntPtr trackid, int monitor);

    public delegate int CSurf_OnInputMonitorChangeExDelegate(IntPtr trackid, int monitor, bool allowgang);

    public delegate bool CSurf_OnMuteChangeDelegate(IntPtr trackid, int mute);

    public delegate bool CSurf_OnMuteChangeExDelegate(IntPtr trackid, int mute, bool allowgang);

    public delegate void CSurf_OnOscControlMessageDelegate(string msg, IntPtr arg);

    public delegate double CSurf_OnPanChangeDelegate(IntPtr trackid, double pan, bool relative);

    public delegate double CSurf_OnPanChangeExDelegate(IntPtr trackid, double pan, bool relative, bool allowGang);

    public delegate void CSurf_OnPauseDelegate();

    public delegate void CSurf_OnPlayDelegate();

    public delegate void CSurf_OnPlayRateChangeDelegate(double playrate);

    public delegate bool CSurf_OnRecArmChangeDelegate(IntPtr trackid, int recarm);

    public delegate bool CSurf_OnRecArmChangeExDelegate(IntPtr trackid, int recarm, bool allowgang);

    public delegate void CSurf_OnRecordDelegate();

    public delegate double CSurf_OnRecvPanChangeDelegate(IntPtr trackid, int recv_index, double pan, bool relative);

    public delegate double CSurf_OnRecvVolumeChangeDelegate(IntPtr trackid, int recv_index, double volume, bool relative);

    public delegate void CSurf_OnRewDelegate(int seekplay);

    public delegate void CSurf_OnRewFwdDelegate(int seekplay, int dir);

    public delegate void CSurf_OnScrollDelegate(int xdir, int ydir);

    public delegate bool CSurf_OnSelectedChangeDelegate(IntPtr trackid, int selected);

    public delegate double CSurf_OnSendPanChangeDelegate(IntPtr trackid, int send_index, double pan, bool relative);

    public delegate double CSurf_OnSendVolumeChangeDelegate(IntPtr trackid, int send_index, double volume, bool relative);

    public delegate bool CSurf_OnSoloChangeDelegate(IntPtr trackid, int solo);

    public delegate bool CSurf_OnSoloChangeExDelegate(IntPtr trackid, int solo, bool allowgang);

    public delegate void CSurf_OnStopDelegate();

    public delegate void CSurf_OnTempoChangeDelegate(double bpm);

    public delegate void CSurf_OnTrackSelectionDelegate(IntPtr trackid);

    public delegate double CSurf_OnVolumeChangeDelegate(IntPtr trackid, double volume, bool relative);

    public delegate double CSurf_OnVolumeChangeExDelegate(IntPtr trackid, double volume, bool relative, bool allowGang);

    public delegate double CSurf_OnWidthChangeDelegate(IntPtr trackid, double width, bool relative);

    public delegate double CSurf_OnWidthChangeExDelegate(IntPtr trackid, double width, bool relative, bool allowGang);

    public delegate void CSurf_OnZoomDelegate(int xdir, int ydir);

    public delegate void CSurf_ResetAllCachedVolPanStatesDelegate();

    public delegate void CSurf_ScrubAmtDelegate(double amt);

    public delegate void CSurf_SetAutoModeDelegate(int mode, IntPtr ignoresurf);

    public delegate void CSurf_SetPlayStateDelegate(bool play, bool pause, bool rec, IntPtr ignoresurf);

    public delegate void CSurf_SetRepeatStateDelegate(bool rep, IntPtr ignoresurf);

    public delegate void CSurf_SetSurfaceMuteDelegate(IntPtr trackid, bool mute, IntPtr ignoresurf);

    public delegate void CSurf_SetSurfacePanDelegate(IntPtr trackid, double pan, IntPtr ignoresurf);

    public delegate void CSurf_SetSurfaceRecArmDelegate(IntPtr trackid, bool recarm, IntPtr ignoresurf);

    public delegate void CSurf_SetSurfaceSelectedDelegate(IntPtr trackid, bool selected, IntPtr ignoresurf);

    public delegate void CSurf_SetSurfaceSoloDelegate(IntPtr trackid, bool solo, IntPtr ignoresurf);

    public delegate void CSurf_SetSurfaceVolumeDelegate(IntPtr trackid, double volume, IntPtr ignoresurf);

    public delegate void CSurf_SetTrackListChangeDelegate();

    public delegate IntPtr CSurf_TrackFromIDDelegate(int idx, bool mcpView);

    public delegate int CSurf_TrackToIDDelegate(IntPtr track, bool mcpView);

    public delegate double DB2SLIDERDelegate(double x);

    public delegate bool DeleteActionShortcutDelegate(IntPtr section, int cmdID, int shortcutidx);

    public delegate void DeleteExtStateDelegate(string section, string key, bool persist);

    public delegate bool DeleteProjectMarkerDelegate(IntPtr proj, int markrgnindexnumber, bool isrgn);

    public delegate bool DeleteProjectMarkerByIndexDelegate(IntPtr proj, int markrgnidx);

    public delegate int DeleteTakeStretchMarkersDelegate(IntPtr take, int idx, ref int countInOptional);

    public delegate void DeleteTrackDelegate(IntPtr tr);

    public delegate bool DeleteTrackMediaItemDelegate(IntPtr tr, IntPtr it);

    public delegate void DestroyAudioAccessorDelegate(IntPtr accessor);

    public delegate void DestroyLocalOscHandlerDelegate(IntPtr local_osc_handler);

    public delegate bool DoActionShortcutDialogDelegate(IntPtr hwnd, IntPtr section, int cmdID, int shortcutidx);

    public delegate void Dock_UpdateDockIDDelegate(string ident_str, int whichDock);

    public delegate int DockIsChildOfDockDelegate(IntPtr hwnd, ref bool isFloatingDockerOut);

    public delegate void DockWindowActivateDelegate(IntPtr hwnd);

    public delegate void DockWindowAddDelegate(IntPtr hwnd, string name, int pos, bool allowShow);

    public delegate void DockWindowAddExDelegate(IntPtr hwnd, string name, string identstr, bool allowShow);

    public delegate void DockWindowRefreshDelegate();

    public delegate void DockWindowRefreshForHWNDDelegate(IntPtr hwnd);

    public delegate void DockWindowRemoveDelegate(IntPtr hwnd);

    public delegate bool DuplicateCustomizableMenuDelegate(IntPtr srcmenu, IntPtr destmenu);

    public delegate void EnsureNotCompletelyOffscreenDelegate(IntPtr rOut);

    public delegate bool EnumPitchShiftModesDelegate(int mode, IntPtr strOut);

    public delegate IntPtr EnumPitchShiftSubModesDelegate(int mode, int submode);

    public delegate int EnumProjectMarkersDelegate(int idx, ref bool isrgnOut, ref double posOut, ref double rgnendOut, IntPtr nameOut, ref int markrgnindexnumberOut);

    public delegate int EnumProjectMarkers2Delegate(IntPtr proj, int idx, ref bool isrgnOut, ref double posOut, ref double rgnendOut, IntPtr nameOut, ref int markrgnindexnumberOut);

    public delegate int EnumProjectMarkers3Delegate(IntPtr proj, int idx, ref bool isrgnOut, ref double posOut, ref double rgnendOut, IntPtr nameOut, ref int markrgnindexnumberOut, ref int colorOut);

    public delegate IntPtr EnumProjectsDelegate(int idx, IntPtr projfn, int projfn_sz);

    public delegate IntPtr EnumRegionRenderMatrixDelegate(IntPtr proj, int regionindex, int rendertrack);

    public delegate bool EnumTrackMIDIProgramNamesDelegate(int track, int programNumber, IntPtr programName, int programName_sz);

    public delegate bool EnumTrackMIDIProgramNamesExDelegate(IntPtr proj, IntPtr track, int programNumber, IntPtr programName, int programName_sz);

    public delegate bool file_existsDelegate(string path);

    public delegate void format_timestrDelegate(double tpos, IntPtr buf, int buf_sz);

    public delegate void format_timestr_lenDelegate(double tpos, IntPtr buf, int buf_sz, double offset, int modeoverride);

    public delegate void format_timestr_posDelegate(double tpos, IntPtr buf, int buf_sz, int modeoverride);

    public delegate void FreeHeapPtrDelegate(IntPtr ptr);

    public delegate void genGuidDelegate(IntPtr g);

    public delegate IntPtr get_config_varDelegate(string name, ref int szOut);

    public delegate IntPtr get_ini_fileDelegate();

    public delegate IntPtr get_midi_config_varDelegate(string name, ref int szOut);

    public delegate bool GetActionShortcutDescDelegate(IntPtr section, int cmdID, int shortcutidx, IntPtr desc, int desclen);

    public delegate IntPtr GetActiveTakeDelegate(IntPtr item);

    public delegate IntPtr GetAppVersionDelegate();

    public delegate double GetAudioAccessorEndTimeDelegate(IntPtr accessor);

    public delegate void GetAudioAccessorHashDelegate(IntPtr accessor, IntPtr hashNeed128);

    public delegate int GetAudioAccessorSamplesDelegate(IntPtr accessor, int samplerate, int numchannels, double starttime_sec, int numsamplesperchannel, ref double samplebuffer);

    public delegate double GetAudioAccessorStartTimeDelegate(IntPtr accessor);

    public delegate IntPtr GetColorThemeDelegate(int idx, int defval);

    public delegate IntPtr GetColorThemeStructDelegate(ref int szOut);

    public delegate int GetConfigWantsDockDelegate(string ident_str);

    public delegate IntPtr GetContextMenuDelegate(int idx);

    public delegate IntPtr GetCurrentProjectInLoadSaveDelegate();

    public delegate int GetCursorContextDelegate();

    public delegate double GetCursorPositionDelegate();

    public delegate double GetCursorPositionExDelegate(IntPtr proj);

    public delegate int GetDisplayedMediaItemColorDelegate(IntPtr item);

    public delegate int GetDisplayedMediaItemColor2Delegate(IntPtr item, IntPtr take);

    public delegate bool GetEnvelopeNameDelegate(IntPtr env, IntPtr buf, int buf_sz);

    public delegate IntPtr GetExePathDelegate();

    public delegate IntPtr GetExtStateDelegate(string section, string key);

    public delegate bool GetFocusedFXDelegate(ref int tracknumberOut, ref int itemnumberOut, ref int fxnumberOut);

    public delegate int GetFreeDiskSpaceForRecordPathDelegate(IntPtr proj, int pathidx);

    public delegate double GetHZoomLevelDelegate();

    public delegate IntPtr GetIconThemePointerDelegate(string name);

    public delegate IntPtr GetIconThemeStructDelegate(ref int szOut);

    public delegate IntPtr GetInputChannelNameDelegate(int channelIndex);

    public delegate void GetInputOutputLatencyDelegate(ref int inputlatencyOut, ref int outputLatencyOut);

    public delegate double GetItemEditingTime2Delegate(IntPtr which_itemOut, ref int flagsOut);

    public delegate IntPtr GetItemProjectContextDelegate(IntPtr item);

    public delegate void GetLastMarkerAndCurRegionDelegate(IntPtr proj, double time, ref int markeridxOut, ref int regionidxOut);

    public delegate bool GetLastTouchedFXDelegate(ref int tracknumberOut, ref int fxnumberOut, ref int paramnumberOut);

    public delegate IntPtr GetLastTouchedTrackDelegate();

    public delegate IntPtr GetMainHwndDelegate();

    public delegate int GetMasterMuteSoloFlagsDelegate();

    public delegate IntPtr GetMasterTrackDelegate(IntPtr proj);

    public delegate int GetMasterTrackVisibilityDelegate();

    public delegate int GetMaxMidiInputsDelegate();

    public delegate int GetMaxMidiOutputsDelegate();

    public delegate IntPtr GetMediaItemDelegate(IntPtr proj, int itemidx);

    public delegate IntPtr GetMediaItem_TrackDelegate(IntPtr item);

    public delegate double GetMediaItemInfo_ValueDelegate(IntPtr item, string parmname);

    public delegate int GetMediaItemNumTakesDelegate(IntPtr item);

    public delegate IntPtr GetMediaItemTakeDelegate(IntPtr item, int tk);

    public delegate IntPtr GetMediaItemTake_ItemDelegate(IntPtr take);

    public delegate IntPtr GetMediaItemTake_SourceDelegate(IntPtr take);

    public delegate IntPtr GetMediaItemTake_TrackDelegate(IntPtr take);

    public delegate IntPtr GetMediaItemTakeByGUIDDelegate(IntPtr project, IntPtr guid);

    public delegate double GetMediaItemTakeInfo_ValueDelegate(IntPtr take, string parmname);

    public delegate IntPtr GetMediaItemTrackDelegate(IntPtr item);

    public delegate void GetMediaSourceFileNameDelegate(IntPtr source, IntPtr filenamebuf, int filenamebuf_sz);

    public delegate int GetMediaSourceNumChannelsDelegate(IntPtr source);

    public delegate int GetMediaSourceSampleRateDelegate(IntPtr source);

    public delegate void GetMediaSourceTypeDelegate(IntPtr source, IntPtr typebuf, int typebuf_sz);

    public delegate double GetMediaTrackInfo_ValueDelegate(IntPtr tr, string parmname);

    public delegate IntPtr GetMidiInputDelegate(int idx);

    public delegate bool GetMIDIInputNameDelegate(int dev, IntPtr nameout, int nameout_sz);

    public delegate IntPtr GetMidiOutputDelegate(int idx);

    public delegate bool GetMIDIOutputNameDelegate(int dev, IntPtr nameout, int nameout_sz);

    public delegate IntPtr GetMixerScrollDelegate();

    public delegate void GetMouseModifierDelegate(string context, int modifier_flag, IntPtr action, int action_sz);

    public delegate int GetNumAudioInputsDelegate();

    public delegate int GetNumAudioOutputsDelegate();

    public delegate int GetNumMIDIInputsDelegate();

    public delegate int GetNumMIDIOutputsDelegate();

    public delegate int GetNumTracksDelegate();

    public delegate IntPtr GetOutputChannelNameDelegate(int channelIndex);

    public delegate double GetOutputLatencyDelegate();

    public delegate IntPtr GetParentTrackDelegate(IntPtr track);

    public delegate void GetPeakFileNameDelegate(string fn, IntPtr buf, int buf_sz);

    public delegate void GetPeakFileNameExDelegate(string fn, IntPtr buf, int buf_sz, bool forWrite);

    public delegate void GetPeakFileNameEx2Delegate(string fn, IntPtr buf, int buf_sz, bool forWrite, string peaksfileextension);

    public delegate IntPtr GetPeaksBitmapDelegate(IntPtr pks, double maxamp, int w, int h, IntPtr bmp);

    public delegate double GetPlayPositionDelegate();

    public delegate double GetPlayPosition2Delegate();

    public delegate double GetPlayPosition2ExDelegate(IntPtr proj);

    public delegate double GetPlayPositionExDelegate(IntPtr proj);

    public delegate int GetPlayStateDelegate();

    public delegate int GetPlayStateExDelegate(IntPtr proj);

    public delegate void GetProjectPathDelegate(IntPtr buf, int buf_sz);

    public delegate void GetProjectPathExDelegate(IntPtr proj, IntPtr buf, int buf_sz);

    public delegate int GetProjectStateChangeCountDelegate(IntPtr proj);

    public delegate void GetProjectTimeSignatureDelegate(ref double bpmOut, ref double bpiOut);

    public delegate void GetProjectTimeSignature2Delegate(IntPtr proj, ref double bpmOut, ref double bpiOut);

    public delegate IntPtr GetResourcePathDelegate();

    public delegate IntPtr GetSelectedMediaItemDelegate(IntPtr proj, int selitem);

    public delegate IntPtr GetSelectedTrackDelegate(IntPtr proj, int seltrackidx);

    public delegate IntPtr GetSelectedTrackEnvelopeDelegate(IntPtr proj);

    public delegate void GetSet_ArrangeView2Delegate(IntPtr proj, bool isSet, int screen_x_start, int screen_x_end, ref double start_timeOut, ref double end_timeOut);

    public delegate void GetSet_LoopTimeRangeDelegate(bool isSet, bool isLoop, ref double startOut, ref double endOut, bool allowautoseek);

    public delegate void GetSet_LoopTimeRange2Delegate(IntPtr proj, bool isSet, bool isLoop, ref double startOut, ref double endOut, bool allowautoseek);

    public delegate bool GetSetEnvelopeStateDelegate(IntPtr env, IntPtr str, int str_sz);

    public delegate bool GetSetEnvelopeState2Delegate(IntPtr env, IntPtr str, int str_sz, bool isundo);

    public delegate bool GetSetItemStateDelegate(IntPtr item, IntPtr str, int str_sz);

    public delegate bool GetSetItemState2Delegate(IntPtr item, IntPtr str, int str_sz, bool isundo);

    public delegate IntPtr GetSetMediaItemInfoDelegate(IntPtr item, string parmname, IntPtr setNewValue);

    public delegate IntPtr GetSetMediaItemTakeInfoDelegate(IntPtr tk, string parmname, IntPtr setNewValue);

    public delegate bool GetSetMediaItemTakeInfo_StringDelegate(IntPtr tk, string parmname, IntPtr stringNeedBig, bool setnewvalue);

    public delegate IntPtr GetSetMediaTrackInfoDelegate(IntPtr tr, string parmname, IntPtr setNewVanue);

    public delegate bool GetSetMediaTrackInfo_StringDelegate(IntPtr tr, string parmname, IntPtr stringNeedBig, bool setnewvalue);

    public delegate IntPtr GetSetObjectStateDelegate(IntPtr obj, string str);

    public delegate IntPtr GetSetObjectState2Delegate(IntPtr obj, string str, bool isundo);

    public delegate int GetSetRepeatDelegate(int val);

    public delegate int GetSetRepeatExDelegate(IntPtr proj, int val);

    public delegate IntPtr GetSetTrackMIDISupportFileDelegate(IntPtr proj, IntPtr track, int which, string filename);

    public delegate IntPtr GetSetTrackSendInfoDelegate(IntPtr tr, int category, int sendidx, string parmname, IntPtr setNewValue);

    public delegate bool GetSetTrackStateDelegate(IntPtr track, IntPtr str, int str_sz);

    public delegate bool GetSetTrackState2Delegate(IntPtr track, IntPtr str, int str_sz, bool isundo);

    public delegate IntPtr GetSubProjectFromSourceDelegate(IntPtr src);

    public delegate IntPtr GetTakeDelegate(IntPtr item, int takeidx);

    public delegate IntPtr GetTakeEnvelopeByNameDelegate(IntPtr take, string envname);

    public delegate IntPtr GetTakeNameDelegate(IntPtr take);

    public delegate int GetTakeNumStretchMarkersDelegate(IntPtr take);

    public delegate int GetTakeStretchMarkerDelegate(IntPtr take, int idx, ref double posOut, ref double srcposOutOptional);

    public delegate bool GetTCPFXParmDelegate(IntPtr project, IntPtr track, int index, ref int fxindexOut, ref int parmidxOut);

    public delegate bool GetTempoMatchPlayRateDelegate(IntPtr source, double srcscale, double position, double mult, ref double rateOut, ref double targetlenOut);

    public delegate bool GetTempoTimeSigMarkerDelegate(IntPtr proj, int ptidx, ref double timeposOut, ref int measureposOut, ref double beatposOut, ref double bpmOut, ref int timesig_numOut, ref int timesig_denomOut, ref bool lineartempoOut);

    public delegate int GetToggleCommandStateDelegate(int command_id);

    public delegate int GetToggleCommandState2Delegate(IntPtr section, int cmdID);

    public delegate int GetToggleCommandStateThroughHooksDelegate(IntPtr sec, int command_id);

    public delegate IntPtr GetTooltipWindowDelegate();

    public delegate IntPtr GetTrackDelegate(IntPtr proj, int trackidx);

    public delegate int GetTrackAutomationModeDelegate(IntPtr tr);

    public delegate int GetTrackColorDelegate(IntPtr track);

    public delegate IntPtr GetTrackEnvelopeDelegate(IntPtr track, int envidx);

    public delegate IntPtr GetTrackEnvelopeByNameDelegate(IntPtr track, string envname);

    public delegate IntPtr GetTrackGUIDDelegate(IntPtr tr);

    public delegate IntPtr GetTrackInfoDelegate(IntPtr track, ref int flags);

    public delegate IntPtr GetTrackMediaItemDelegate(IntPtr tr, int itemidx);

    public delegate IntPtr GetTrackMIDINoteNameDelegate(int track, int note, int chan);

    public delegate IntPtr GetTrackMIDINoteNameExDelegate(IntPtr proj, IntPtr track, int note, int chan);

    public delegate void GetTrackMIDINoteRangeDelegate(IntPtr proj, IntPtr track, ref int note_loOut, ref int note_hiOut);

    public delegate int GetTrackNumMediaItemsDelegate(IntPtr tr);

    public delegate int GetTrackNumSendsDelegate(IntPtr tr, int category);

    public delegate bool GetTrackReceiveNameDelegate(IntPtr track, int recv_index, IntPtr buf, int buf_sz);

    public delegate bool GetTrackReceiveUIMuteDelegate(IntPtr track, int recv_index, ref bool muteOut);

    public delegate bool GetTrackReceiveUIVolPanDelegate(IntPtr track, int recv_index, ref double volumeOut, ref double panOut);

    public delegate bool GetTrackSendNameDelegate(IntPtr track, int send_index, IntPtr buf, int buf_sz);

    public delegate bool GetTrackSendUIMuteDelegate(IntPtr track, int send_index, ref bool muteOut);

    public delegate bool GetTrackSendUIVolPanDelegate(IntPtr track, int send_index, ref double volumeOut, ref double panOut);

    public delegate IntPtr GetTrackStateDelegate(IntPtr track, ref int flagsOut);

    public delegate bool GetTrackUIMuteDelegate(IntPtr track, ref bool muteOut);

    public delegate bool GetTrackUIPanDelegate(IntPtr track, ref double pan1Out, ref double pan2Out, ref int panmodeOut);

    public delegate bool GetTrackUIVolPanDelegate(IntPtr track, ref double volumeOut, ref double panOut);

    public delegate bool GetUserFileNameForReadDelegate(IntPtr filenameNeed4096, string title, string defext);

    public delegate bool GetUserInputsDelegate(string title, int num_inputs, string captions_csv, IntPtr retvals_csv, int retvals_csv_sz);

    public delegate void GoToMarkerDelegate(IntPtr proj, int marker_index, bool use_timeline_order);

    public delegate void GoToRegionDelegate(IntPtr proj, int region_index, bool use_timeline_order);

    public delegate int GR_SelectColorDelegate(IntPtr hwnd, ref int colorOut);

    public delegate int GSC_mainwndDelegate(int t);

    public delegate void guidToStringDelegate(IntPtr g, IntPtr destNeed64);

    public delegate bool HasExtStateDelegate(string section, string key);

    public delegate IntPtr HasTrackMIDIProgramsDelegate(int track);

    public delegate IntPtr HasTrackMIDIProgramsExDelegate(IntPtr proj, IntPtr track);

    public delegate void Help_SetDelegate(string helpstring, bool is_temporary_help);

    public delegate void HiresPeaksFromSourceDelegate(IntPtr src, IntPtr block);

    public delegate void image_resolve_fnDelegate(string in_, IntPtr out_, int out_sz);

    public delegate int InsertMediaDelegate(string file, int mode);

    public delegate int InsertMediaSectionDelegate(string file, int mode, double startpct, double endpct, double pitchshift);

    public delegate void InsertTrackAtIndexDelegate(int idx, bool wantDefaults);

    public delegate int IsInRealTimeAudioDelegate();

    public delegate bool IsItemTakeActiveForPlaybackDelegate(IntPtr item, IntPtr take);

    public delegate bool IsMediaExtensionDelegate(string ext, bool wantOthers);

    public delegate bool IsMediaItemSelectedDelegate(IntPtr item);

    public delegate bool IsTrackSelectedDelegate(IntPtr track);

    public delegate bool IsTrackVisibleDelegate(IntPtr track, bool mixer);

    public delegate int kbd_enumerateActionsDelegate(IntPtr section, int idx, IntPtr nameOut);

    public delegate void kbd_formatKeyNameDelegate(IntPtr ac, IntPtr s);

    public delegate void kbd_getCommandNameDelegate(int cmd, IntPtr s, IntPtr section);

    public delegate IntPtr kbd_getTextFromCmdDelegate(int cmd, IntPtr section);

    public delegate int KBD_OnMainActionExDelegate(int cmd, int val, int valhw, int relmode, IntPtr hwnd, IntPtr proj);

    public delegate void kbd_OnMidiEventDelegate(IntPtr evt, int dev_index);

    public delegate void kbd_OnMidiListDelegate(IntPtr list, int dev_index);

    public delegate void kbd_ProcessActionsMenuDelegate(IntPtr menu, IntPtr section);

    public delegate bool kbd_processMidiEventActionExDelegate(IntPtr evt, IntPtr section, IntPtr hwndCtx);

    public delegate void kbd_reprocessMenuDelegate(IntPtr menu, IntPtr section);

    public delegate bool kbd_RunCommandThroughHooksDelegate(IntPtr section, ref int actionCommandID, ref int val, ref int valhw, ref int relmode, IntPtr hwnd);

    public delegate int kbd_translateAcceleratorDelegate(IntPtr hwnd, IntPtr msg, IntPtr section);

    public delegate bool kbd_translateMouseDelegate(IntPtr winmsg, IntPtr midimsg);

    public delegate bool Loop_OnArrowDelegate(IntPtr project, int direction);

    public delegate void Main_OnCommandDelegate(int command, int flag);

    public delegate void Main_OnCommandExDelegate(int command, int flag, IntPtr proj);

    public delegate void Main_openProjectDelegate(string name);

    public delegate void Main_UpdateLoopInfoDelegate(int ignoremask);

    public delegate void MarkProjectDirtyDelegate(IntPtr proj);

    public delegate void MarkTrackItemsDirtyDelegate(IntPtr track, IntPtr item);

    public delegate double Master_GetPlayRateDelegate(IntPtr project);

    public delegate double Master_GetPlayRateAtTimeDelegate(double time_s, IntPtr proj);

    public delegate double Master_GetTempoDelegate();

    public delegate double Master_NormalizePlayRateDelegate(double playrate, bool isnormalized);

    public delegate double Master_NormalizeTempoDelegate(double bpm, bool isnormalized);

    public delegate int MBDelegate(string msg, string title, int type);

    public delegate int MediaItemDescendsFromTrackDelegate(IntPtr item, IntPtr track);

    public delegate int MIDI_CountEvtsDelegate(IntPtr take, ref int notecntOut, ref int ccevtcntOut, ref int textsyxevtcntOut);

    public delegate bool MIDI_DeleteCCDelegate(IntPtr take, int ccidx);

    public delegate bool MIDI_DeleteEvtDelegate(IntPtr take, int evtidx);

    public delegate bool MIDI_DeleteNoteDelegate(IntPtr take, int noteidx);

    public delegate bool MIDI_DeleteTextSysexEvtDelegate(IntPtr take, int textsyxevtidx);

    public delegate int MIDI_EnumSelCCDelegate(IntPtr take, int ccidx);

    public delegate int MIDI_EnumSelEvtsDelegate(IntPtr take, int evtidx);

    public delegate int MIDI_EnumSelNotesDelegate(IntPtr take, int noteidx);

    public delegate int MIDI_EnumSelTextSysexEvtsDelegate(IntPtr take, int textsyxidx);

    public delegate IntPtr MIDI_eventlist_CreateDelegate();

    public delegate void MIDI_eventlist_DestroyDelegate(IntPtr evtlist);

    public delegate bool MIDI_GetCCDelegate(IntPtr take, int ccidx, ref bool selectedOut, ref bool mutedOut, ref double ppqposOut, ref int chanmsgOut, ref int chanOut, ref int msg2Out, ref int msg3Out);

    public delegate bool MIDI_GetEvtDelegate(IntPtr take, int evtidx, ref bool selectedOut, ref bool mutedOut, ref double ppqposOut, IntPtr msg, ref int msg_sz);

    public delegate bool MIDI_GetNoteDelegate(IntPtr take, int noteidx, ref bool selectedOut, ref bool mutedOut, ref double startppqposOut, ref double endppqposOut, ref int chanOut, ref int pitchOut, ref int velOut);

    public delegate double MIDI_GetPPQPos_EndOfMeasureDelegate(IntPtr take, double ppqpos);

    public delegate double MIDI_GetPPQPos_StartOfMeasureDelegate(IntPtr take, double ppqpos);

    public delegate double MIDI_GetPPQPosFromProjTimeDelegate(IntPtr take, double projtime);

    public delegate double MIDI_GetProjTimeFromPPQPosDelegate(IntPtr take, double ppqpos);

    public delegate bool MIDI_GetTextSysexEvtDelegate(IntPtr take, int textsyxevtidx, ref bool selectedOutOptional, ref bool mutedOutOptional, ref double ppqposOutOptional, ref int typeOutOptional, IntPtr msgOptional, ref int msgOptional_sz);

    public delegate bool MIDI_InsertCCDelegate(IntPtr take, bool selected, bool muted, double ppqpos, int chanmsg, int chan, int msg2, int msg3);

    public delegate bool MIDI_InsertEvtDelegate(IntPtr take, bool selected, bool muted, double ppqpos, string msg, int msg_sz);

    public delegate bool MIDI_InsertNoteDelegate(IntPtr take, bool selected, bool muted, double startppqpos, double endppqpos, int chan, int pitch, int vel);

    public delegate bool MIDI_InsertTextSysexEvtDelegate(IntPtr take, bool selected, bool muted, double ppqpos, int type, string msg, int msg_sz);

    public delegate void midi_reinitDelegate();

    public delegate bool MIDI_SetCCDelegate(IntPtr take, int ccidx, ref bool selectedInOptional, ref bool mutedInOptional, ref double ppqposInOptional, ref int chanmsgInOptional, ref int chanInOptional, ref int msg2InOptional, ref int msg3InOptional);

    public delegate bool MIDI_SetEvtDelegate(IntPtr take, int evtidx, ref bool selectedInOptional, ref bool mutedInOptional, ref double ppqposInOptional, string msgOptional, int msgOptional_sz);

    public delegate bool MIDI_SetNoteDelegate(IntPtr take, int noteidx, ref bool selectedInOptional, ref bool mutedInOptional, ref double startppqposInOptional, ref double endppqposInOptional, ref int chanInOptional, ref int pitchInOptional, ref int velInOptional);

    public delegate bool MIDI_SetTextSysexEvtDelegate(IntPtr take, int textsyxevtidx, ref bool selectedInOptional, ref bool mutedInOptional, ref double ppqposInOptional, ref int typeInOptional, string msgOptional, int msgOptional_sz);

    public delegate IntPtr MIDIEditor_GetActiveDelegate();

    public delegate int MIDIEditor_GetModeDelegate(IntPtr midieditor);

    public delegate int MIDIEditor_GetSetting_intDelegate(IntPtr midieditor, string setting_desc);

    public delegate bool MIDIEditor_GetSetting_strDelegate(IntPtr midieditor, string setting_desc, IntPtr buf, int buf_sz);

    public delegate IntPtr MIDIEditor_GetTakeDelegate(IntPtr midieditor);

    public delegate bool MIDIEditor_LastFocused_OnCommandDelegate(int command_id, bool islistviewcommand);

    public delegate bool MIDIEditor_OnCommandDelegate(IntPtr midieditor, int command_id);

    public delegate void mkpanstrDelegate(IntPtr strNeed64, double pan);

    public delegate void mkvolpanstrDelegate(IntPtr strNeed64, double vol, double pan);

    public delegate void mkvolstrDelegate(IntPtr strNeed64, double vol);

    public delegate void MoveEditCursorDelegate(double adjamt, bool dosel);

    public delegate bool MoveMediaItemToTrackDelegate(IntPtr item, IntPtr desttr);

    public delegate void MuteAllTracksDelegate(bool mute);

    public delegate void my_getViewportDelegate(IntPtr r, IntPtr sr, bool wantWorkArea);

    public delegate int NamedCommandLookupDelegate(string command_name);

    public delegate void OnPauseButtonDelegate();

    public delegate void OnPauseButtonExDelegate(IntPtr proj);

    public delegate void OnPlayButtonDelegate();

    public delegate void OnPlayButtonExDelegate(IntPtr proj);

    public delegate void OnStopButtonDelegate();

    public delegate void OnStopButtonExDelegate(IntPtr proj);

    public delegate void OscLocalMessageToHostDelegate(string message, ref double valueInOptional);

    public delegate double parse_timestrDelegate(string buf);

    public delegate double parse_timestr_lenDelegate(string buf, double offset, int modeoverride);

    public delegate double parse_timestr_posDelegate(string buf, int modeoverride);

    public delegate double parsepanstrDelegate(string str);

    public delegate IntPtr PCM_Sink_CreateDelegate(string filename, string cfg, int cfg_sz, int nch, int srate, bool buildpeaks);

    public delegate IntPtr PCM_Sink_CreateExDelegate(IntPtr proj, string filename, string cfg, int cfg_sz, int nch, int srate, bool buildpeaks);

    public delegate IntPtr PCM_Sink_CreateMIDIFileDelegate(string filename, string cfg, int cfg_sz, double bpm, int div);

    public delegate IntPtr PCM_Sink_CreateMIDIFileExDelegate(IntPtr proj, string filename, string cfg, int cfg_sz, double bpm, int div);

    public delegate uint PCM_Sink_EnumDelegate(int idx, IntPtr descstrOut);

    public delegate IntPtr PCM_Sink_GetExtensionDelegate(string data, int data_sz);

    public delegate IntPtr PCM_Sink_ShowConfigDelegate(string cfg, int cfg_sz, IntPtr hwndParent);

    public delegate IntPtr PCM_Source_CreateFromFileDelegate(string filename);

    public delegate IntPtr PCM_Source_CreateFromFileExDelegate(string filename, bool forcenoMidiImp);

    public delegate IntPtr PCM_Source_CreateFromSimpleDelegate(IntPtr dec, string fn);

    public delegate IntPtr PCM_Source_CreateFromTypeDelegate(string sourcetype);

    public delegate bool PCM_Source_GetSectionInfoDelegate(IntPtr src, ref double offsOut, ref double lenOut, ref bool revOut);

    public delegate IntPtr PeakBuild_CreateDelegate(IntPtr src, string fn, int srate, int nch);

    public delegate IntPtr PeakGet_CreateDelegate(string fn, int srate, int nch);

    public delegate int PlayPreviewDelegate(IntPtr preview);

    public delegate int PlayPreviewExDelegate(IntPtr preview, int bufflags, double MSI);

    public delegate int PlayTrackPreviewDelegate(IntPtr preview);

    public delegate int PlayTrackPreview2Delegate(IntPtr proj, IntPtr preview);

    public delegate int PlayTrackPreview2ExDelegate(IntPtr proj, IntPtr preview, int flags, double msi);

    public delegate IntPtr plugin_getapiDelegate(string name);

    public delegate IntPtr plugin_getFilterListDelegate();

    public delegate IntPtr plugin_getImportableProjectFilterListDelegate();

    public delegate int plugin_registerDelegate(string name, IntPtr infostruct);

    public delegate void PluginWantsAlwaysRunFxDelegate(int amt);

    public delegate void PreventUIRefreshDelegate(int prevent_count);

    public delegate IntPtr projectconfig_var_addrDelegate(IntPtr proj, int idx);

    public delegate int projectconfig_var_getoffsDelegate(string name, ref int szOut);

    public delegate IntPtr ReaperGetPitchShiftAPIDelegate(int version);

    public delegate void ReaScriptErrorDelegate(string errmsg);

    public delegate int RecursiveCreateDirectoryDelegate(string path, uint ignored);

    public delegate void RefreshToolbarDelegate(int command_id);

    public delegate void relative_fnDelegate(string in_, IntPtr out_, int out_sz);

    public delegate bool RenderFileSectionDelegate(string source_filename, string target_filename, double start_percent, double end_percent, double playrate);

    public delegate IntPtr Resample_EnumModesDelegate(int mode);

    public delegate IntPtr Resampler_CreateDelegate();

    public delegate void resolve_fnDelegate(string in_, IntPtr out_, int out_sz);

    public delegate void resolve_fn2Delegate(string in_, IntPtr out_, int out_sz, string checkSubDirOptional);

    public delegate void screenset_registerDelegate(IntPtr id, IntPtr callbackFunc, IntPtr param);

    public delegate void screenset_registerNewDelegate(IntPtr id, IntPtr callbackFunc, IntPtr param);

    public delegate void screenset_unregisterDelegate(IntPtr id);

    public delegate void screenset_unregisterByParamDelegate(IntPtr param);

    public delegate IntPtr SectionFromUniqueIDDelegate(int uniqueID);

    public delegate void SelectAllMediaItemsDelegate(IntPtr proj, bool selected);

    public delegate void SelectProjectInstanceDelegate(IntPtr proj);

    public delegate void SendLocalOscMessageDelegate(IntPtr local_osc_handler, string msg, int msglen);

    public delegate void SetActiveTakeDelegate(IntPtr take);

    public delegate void SetAutomationModeDelegate(int mode, bool onlySel);

    public delegate void SetCurrentBPMDelegate(IntPtr __proj, double bpm, bool wantUndo);

    public delegate void SetEditCurPosDelegate(double time, bool moveview, bool seekplay);

    public delegate void SetEditCurPos2Delegate(IntPtr proj, double time, bool moveview, bool seekplay);

    public delegate void SetExtStateDelegate(string section, string key, string value, bool persist);

    public delegate int SetMasterTrackVisibilityDelegate(int flag);

    public delegate bool SetMediaItemInfo_ValueDelegate(IntPtr item, string parmname, double newvalue);

    public delegate bool SetMediaItemLengthDelegate(IntPtr item, double length, bool refreshUI);

    public delegate bool SetMediaItemPositionDelegate(IntPtr item, double position, bool refreshUI);

    public delegate void SetMediaItemSelectedDelegate(IntPtr item, bool selected);

    public delegate bool SetMediaItemTakeInfo_ValueDelegate(IntPtr take, string parmname, double newvalue);

    public delegate bool SetMediaTrackInfo_ValueDelegate(IntPtr tr, string parmname, double newvalue);

    public delegate IntPtr SetMixerScrollDelegate(IntPtr leftmosttrack);

    public delegate void SetMouseModifierDelegate(string context, int modifier_flag, string action);

    public delegate void SetOnlyTrackSelectedDelegate(IntPtr track);

    public delegate bool SetProjectMarkerDelegate(int markrgnindexnumber, bool isrgn, double pos, double rgnend, string name);

    public delegate bool SetProjectMarker2Delegate(IntPtr proj, int markrgnindexnumber, bool isrgn, double pos, double rgnend, string name);

    public delegate bool SetProjectMarker3Delegate(IntPtr proj, int markrgnindexnumber, bool isrgn, double pos, double rgnend, string name, int color);

    public delegate bool SetProjectMarkerByIndexDelegate(IntPtr proj, int markrgnidx, bool isrgn, double pos, double rgnend, int IDnumber, string name, int color);

    public delegate void SetRegionRenderMatrixDelegate(IntPtr proj, int regionindex, IntPtr track, int addorremove);

    public delegate void SetRenderLastErrorDelegate(string errorstr);

    public delegate int SetTakeStretchMarkerDelegate(IntPtr take, int idx, double pos, ref double srcposInOptional);

    public delegate bool SetTempoTimeSigMarkerDelegate(IntPtr proj, int ptidx, double timepos, int measurepos, double beatpos, double bpm, int timesig_num, int timesig_denom, bool lineartempo);

    public delegate void SetTrackAutomationModeDelegate(IntPtr tr, int mode);

    public delegate void SetTrackColorDelegate(IntPtr track, int color);

    public delegate bool SetTrackMIDINoteNameDelegate(int track, int note, int chan, string name);

    public delegate bool SetTrackMIDINoteNameExDelegate(IntPtr proj, IntPtr track, int note, int chan, string name);

    public delegate void SetTrackSelectedDelegate(IntPtr track, bool selected);

    public delegate bool SetTrackSendUIPanDelegate(IntPtr track, int send_idx, double pan, int isend);

    public delegate bool SetTrackSendUIVolDelegate(IntPtr track, int send_idx, double vol, int isend);

    public delegate void ShowActionListDelegate(IntPtr caller, IntPtr callerWnd);

    public delegate void ShowConsoleMsgDelegate(string msg);

    public delegate int ShowMessageBoxDelegate(string msg, string title, int type);

    public delegate double SLIDER2DBDelegate(double y);

    public delegate double SnapToGridDelegate(IntPtr project, double time_pos);

    public delegate void SoloAllTracksDelegate(int solo);

    public delegate IntPtr SplitMediaItemDelegate(IntPtr item, double position);

    public delegate int StopPreviewDelegate(IntPtr preview);

    public delegate int StopTrackPreviewDelegate(IntPtr preview);

    public delegate int StopTrackPreview2Delegate(IntPtr proj, IntPtr preview);

    public delegate void stringToGuidDelegate(string str, IntPtr g);

    public delegate void StuffMIDIMessageDelegate(int mode, int msg1, int msg2, int msg3);

    public delegate double TimeMap2_beatsToTimeDelegate(IntPtr proj, double tpos, ref int measuresOutOptional);

    public delegate double TimeMap2_GetDividedBpmAtTimeDelegate(IntPtr proj, double time);

    public delegate double TimeMap2_GetNextChangeTimeDelegate(IntPtr proj, double time);

    public delegate double TimeMap2_QNToTimeDelegate(IntPtr proj, double qn);

    public delegate double TimeMap2_timeToBeatsDelegate(IntPtr proj, double tpos, ref int measuresOutOptional, ref int cmlOutOptional, ref double fullbeatsOutOptional, ref int cdenomOutOptional);

    public delegate double TimeMap2_timeToQNDelegate(IntPtr proj, double tpos);

    public delegate double TimeMap_GetDividedBpmAtTimeDelegate(double time);

    public delegate void TimeMap_GetTimeSigAtTimeDelegate(IntPtr proj, double time, ref int timesig_numOut, ref int timesig_denomOut, ref double tempoOut);

    public delegate double TimeMap_QNToTimeDelegate(double qn);

    public delegate double TimeMap_QNToTime_absDelegate(IntPtr proj, double qn);

    public delegate double TimeMap_timeToQNDelegate(double qn);

    public delegate double TimeMap_timeToQN_absDelegate(IntPtr proj, double tpos);

    public delegate bool ToggleTrackSendUIMuteDelegate(IntPtr track, int send_idx);

    public delegate double Track_GetPeakHoldDBDelegate(IntPtr track, int channel, bool clear);

    public delegate double Track_GetPeakInfoDelegate(IntPtr track, int channel);

    public delegate void TrackCtl_SetToolTipDelegate(string fmt, int xpos, int ypos, bool topmost);

    public delegate bool TrackFX_EndParamEditDelegate(IntPtr track, int fx, int param);

    public delegate bool TrackFX_FormatParamValueDelegate(IntPtr track, int fx, int param, double val, IntPtr buf, int buf_sz);

    public delegate bool TrackFX_FormatParamValueNormalizedDelegate(IntPtr track, int fx, int param, double value, IntPtr buf, int buf_sz);

    public delegate int TrackFX_GetByNameDelegate(IntPtr track, string fxname, bool instantiate);

    public delegate int TrackFX_GetChainVisibleDelegate(IntPtr track);

    public delegate int TrackFX_GetCountDelegate(IntPtr track);

    public delegate bool TrackFX_GetEnabledDelegate(IntPtr track, int fx);

    public delegate int TrackFX_GetEQDelegate(IntPtr track, bool instantiate);

    public delegate bool TrackFX_GetEQBandEnabledDelegate(IntPtr track, int fxidx, int bandtype, int bandidx);

    public delegate bool TrackFX_GetEQParamDelegate(IntPtr track, int fxidx, int paramidx, ref int bandtypeOut, ref int bandidxOut, ref int paramtypeOut, ref double normvalOut);

    public delegate IntPtr TrackFX_GetFloatingWindowDelegate(IntPtr track, int index);

    public delegate bool TrackFX_GetFormattedParamValueDelegate(IntPtr track, int fx, int param, IntPtr buf, int buf_sz);

    public delegate IntPtr TrackFX_GetFXGUIDDelegate(IntPtr track, int fx);

    public delegate bool TrackFX_GetFXNameDelegate(IntPtr track, int fx, IntPtr buf, int buf_sz);

    public delegate int TrackFX_GetInstrumentDelegate(IntPtr track);

    public delegate int TrackFX_GetNumParamsDelegate(IntPtr track, int fx);

    public delegate bool TrackFX_GetOpenDelegate(IntPtr track, int fx);

    public delegate double TrackFX_GetParamDelegate(IntPtr track, int fx, int param, ref double minvalOut, ref double maxvalOut);

    public delegate bool TrackFX_GetParameterStepSizesDelegate(IntPtr track, int fx, int param, ref double stepOut, ref double smallstepOut, ref double largestepOut, ref bool istoggleOut);

    public delegate double TrackFX_GetParamExDelegate(IntPtr track, int fx, int param, ref double minvalOut, ref double maxvalOut, ref double midvalOut);

    public delegate bool TrackFX_GetParamNameDelegate(IntPtr track, int fx, int param, IntPtr buf, int buf_sz);

    public delegate double TrackFX_GetParamNormalizedDelegate(IntPtr track, int fx, int param);

    public delegate bool TrackFX_GetPresetDelegate(IntPtr track, int fx, IntPtr presetname, int presetname_sz);

    public delegate int TrackFX_GetPresetIndexDelegate(IntPtr track, int fx, ref int numberOfPresetsOut);

    public delegate bool TrackFX_NavigatePresetsDelegate(IntPtr track, int fx, int presetmove);

    public delegate void TrackFX_SetEnabledDelegate(IntPtr track, int fx, bool enabled);

    public delegate bool TrackFX_SetEQBandEnabledDelegate(IntPtr track, int fxidx, int bandtype, int bandidx, bool enable);

    public delegate bool TrackFX_SetEQParamDelegate(IntPtr track, int fxidx, int bandtype, int bandidx, int paramtype, double val, bool isnorm);

    public delegate void TrackFX_SetOpenDelegate(IntPtr track, int fx, bool open);

    public delegate bool TrackFX_SetParamDelegate(IntPtr track, int fx, int param, double val);

    public delegate bool TrackFX_SetParamNormalizedDelegate(IntPtr track, int fx, int param, double value);

    public delegate bool TrackFX_SetPresetDelegate(IntPtr track, int fx, string presetname);

    public delegate bool TrackFX_SetPresetByIndexDelegate(IntPtr track, int fx, int idx);

    public delegate void TrackFX_ShowDelegate(IntPtr track, int index, int showFlag);

    public delegate void TrackList_AdjustWindowsDelegate(bool isMajor);

    public delegate void TrackList_UpdateAllExternalSurfacesDelegate();

    public delegate void Undo_BeginBlockDelegate();

    public delegate void Undo_BeginBlock2Delegate(IntPtr proj);

    public delegate IntPtr Undo_CanRedo2Delegate(IntPtr proj);

    public delegate IntPtr Undo_CanUndo2Delegate(IntPtr proj);

    public delegate int Undo_DoRedo2Delegate(IntPtr proj);

    public delegate int Undo_DoUndo2Delegate(IntPtr proj);

    public delegate void Undo_EndBlockDelegate(string descchange, int extraflags);

    public delegate void Undo_EndBlock2Delegate(IntPtr proj, string descchange, int extraflags);

    public delegate void Undo_OnStateChangeDelegate(string descchange);

    public delegate void Undo_OnStateChange2Delegate(IntPtr proj, string descchange);

    public delegate void Undo_OnStateChange_ItemDelegate(IntPtr proj, string name, IntPtr item);

    public delegate void Undo_OnStateChangeExDelegate(string descchange, int whichStates, int trackparm);

    public delegate void Undo_OnStateChangeEx2Delegate(IntPtr proj, string descchange, int whichStates, int trackparm);

    public delegate void UpdateArrangeDelegate();

    public delegate void UpdateItemInProjectDelegate(IntPtr item);

    public delegate void UpdateTimelineDelegate();

    public delegate bool ValidatePtrDelegate(IntPtr pointer, string ctypename);

    public delegate void ViewPrefsDelegate(int page, string pageByName);

    public delegate bool WDL_VirtualWnd_ScaledBlitBGDelegate(IntPtr dest, IntPtr src, int destx, int desty, int destw, int desth, int clipx, int clipy, int clipw, int cliph, float alpha, int mode);


    public static class ReaperAPI
    {

    public static T GetDelegateForFunctionPointer<T>(IntPtr ptr, CallingConvention conv)
        where T : class
    {
        // see http://www.codeproject.com/Tips/441743/A-look-at-marshalling-delegates-in-NET
        var delegateType = typeof(T);
        var method = delegateType.GetMethod("Invoke");
        var returnType = method.ReturnType;
        var paramTypes = method.GetParameters().Select((x) => x.ParameterType).ToArray();
        var invoke = new DynamicMethod("Invoke", returnType, paramTypes, typeof(Delegate));
        var il = invoke.GetILGenerator();
        for (int i = 0; i < paramTypes.Length; i++)
        {
            il.Emit(OpCodes.Ldarg, i);
        }
        if (IntPtr.Size == sizeof(int))
        {
            il.Emit(OpCodes.Ldc_I4, ptr.ToInt32());
        }
        else
        {
            il.Emit(OpCodes.Ldc_I8, ptr.ToInt64());
        }
        
        il.EmitCalli(OpCodes.Calli, conv, returnType, paramTypes);
        il.Emit(OpCodes.Ret);
        return invoke.CreateDelegate(delegateType) as T;
    }   
    
    
    
    ///<summary>
    ///<para>menuidstr is some unique identifying string</para>
    ///<para>menuname is for main menus only (displayed in a menu bar somewhere), NULL otherwise</para>
    ///<para>kbdsecname is the name of the KbdSectionInfo registered by this plugin, or NULL for the main actions section</para>
    ///</summary>
    public static AddCustomizableMenuDelegate AddCustomizableMenu;
    
    
    ///<summary>
    ///<para>Add an Extensions main menu, which the extension can populate/modify with plugin_register(&#34;hookcustommenu&#34;)</para>
    ///</summary>
    public static AddExtensionsMainMenuDelegate AddExtensionsMainMenu;
    
    
    ///<summary>
    ///<para>creates a new media item.</para>
    ///</summary>
    public static AddMediaItemToTrackDelegate AddMediaItemToTrack;
    
    
    ///<summary>
    ///<para>Returns the index of the created marker/region, or -1 on failure. Supply wantidx&gt;=0 if you want a particular index number, but you&#39;ll get a different index number a region and wantidx is already in use.</para>
    ///</summary>
    public static AddProjectMarkerDelegate AddProjectMarker;
    
    
    ///<summary>
    ///<para>Returns the index of the created marker/region, or -1 on failure. Supply wantidx&gt;=0 if you want a particular index number, but you&#39;ll get a different index number a region and wantidx is already in use. color should be 0 or RGB(x,y,z)|0x1000000</para>
    ///</summary>
    public static AddProjectMarker2Delegate AddProjectMarker2;
    
    
    ///<summary>
    ///<para>creates a new take in an item</para>
    ///</summary>
    public static AddTakeToMediaItemDelegate AddTakeToMediaItem;
    
    
    ///<summary>
    ///<para>Deprecated. Use SetTempoTimeSigMarker with ptidx=-1.</para>
    ///</summary>
    public static AddTempoTimeSigMarkerDelegate AddTempoTimeSigMarker;
    
    
    ///<summary>
    ///<para>forceset=0,doupd=true,centermode=-1 for default</para>
    ///</summary>
    public static adjustZoomDelegate adjustZoom;
    
    
    ///<summary>
    ///</summary>
    public static AnyTrackSoloDelegate AnyTrackSolo;
    
    
    ///<summary>
    ///</summary>
    public static APITestDelegate APITest;
    
    
    ///<summary>
    ///<para>nudgeflag: &amp;1=set to value (otherwise nudge by value), &amp;2=snap</para>
    ///<para>nudgewhat: 0=position, 1=left trim, 2=left edge, 3=right edge, 4=contents, 5=duplicate, 6=edit cursor</para>
    ///<para>nudgeunit: 0=ms, 1=seconds, 2=grid, 3=256th notes, ..., 15=whole notes, 16=measures.beats (1.15 = 1 measure + 1.5 beats), 17=samples, 18=frames, 19=pixels, 20=item lengths, 21=item selections</para>
    ///<para>value: amount to nudge by, or value to set to</para>
    ///<para>reverse: in nudge mode, nudges left (otherwise ignored)</para>
    ///<para>copies: in nudge duplicate mode, number of copies (otherwise ignored)</para>
    ///</summary>
    public static ApplyNudgeDelegate ApplyNudge;
    
    
    ///<summary>
    ///<para>is in pre-buffer? threadsafe</para>
    ///</summary>
    public static Audio_IsPreBufferDelegate Audio_IsPreBuffer;
    
    
    ///<summary>
    ///<para>is audio running at all? threadsafe</para>
    ///</summary>
    public static Audio_IsRunningDelegate Audio_IsRunning;
    
    
    ///<summary>
    ///<para>return &gt;0 on success</para>
    ///</summary>
    public static Audio_RegHardwareHookDelegate Audio_RegHardwareHook;
    
    
    ///<summary>
    ///<para>Validates the current state of the audio accessor -- must ONLY call this from the main thread. Returns true if the state changed.</para>
    ///</summary>
    public static AudioAccessorValidateStateDelegate AudioAccessorValidateState;
    
    
    ///<summary>
    ///<para>-1 = bypass all if not all bypassed,otherwise unbypass all</para>
    ///</summary>
    public static BypassFxAllTracksDelegate BypassFxAllTracks;
    
    
    ///<summary>
    ///</summary>
    public static CalculatePeaksDelegate CalculatePeaks;
    
    
    ///<summary>
    ///<para>NOTE: source samples field is a pointer to floats instead</para>
    ///</summary>
    public static CalculatePeaksFloatSrcPtrDelegate CalculatePeaksFloatSrcPtr;
    
    
    ///<summary>
    ///</summary>
    public static ClearAllRecArmedDelegate ClearAllRecArmed;
    
    
    ///<summary>
    ///<para>resets the global peak caches</para>
    ///</summary>
    public static ClearPeakCacheDelegate ClearPeakCache;
    
    
    ///<summary>
    ///<para>Returns the number of shortcuts that exist for the given command ID.</para>
    ///<para>see GetActionShortcutDesc, DeleteActionShortcut, DoActionShortcutDialog.</para>
    ///</summary>
    public static CountActionShortcutsDelegate CountActionShortcuts;
    
    
    ///<summary>
    ///<para>count the number of items in the project (proj=0 for active project)</para>
    ///</summary>
    public static CountMediaItemsDelegate CountMediaItems;
    
    
    ///<summary>
    ///<para>num_markersOut and num_regionsOut may be NULL.</para>
    ///</summary>
    public static CountProjectMarkersDelegate CountProjectMarkers;
    
    
    ///<summary>
    ///<para>count the number of selected items in the project (proj=0 for active project)</para>
    ///</summary>
    public static CountSelectedMediaItemsDelegate CountSelectedMediaItems;
    
    
    ///<summary>
    ///<para>count the number of selected tracks in the project (proj=0 for active project)</para>
    ///</summary>
    public static CountSelectedTracksDelegate CountSelectedTracks;
    
    
    ///<summary>
    ///<para>count the number of takes in the item</para>
    ///</summary>
    public static CountTakesDelegate CountTakes;
    
    
    ///<summary>
    ///<para>Count the number of FX parameter knobs displayed on the track control panel.</para>
    ///</summary>
    public static CountTCPFXParmsDelegate CountTCPFXParms;
    
    
    ///<summary>
    ///<para>Count the number of tempo/time signature markers in the project. See GetTempoTimeSigMarker, SetTempoTimeSigMarker, AddTempoTimeSigMarker.</para>
    ///</summary>
    public static CountTempoTimeSigMarkersDelegate CountTempoTimeSigMarkers;
    
    
    ///<summary>
    ///<para>see GetTrackEnvelope</para>
    ///</summary>
    public static CountTrackEnvelopesDelegate CountTrackEnvelopes;
    
    
    ///<summary>
    ///<para>count the number of items in the track</para>
    ///</summary>
    public static CountTrackMediaItemsDelegate CountTrackMediaItems;
    
    
    ///<summary>
    ///<para>count the number of tracks in the project (proj=0 for active project)</para>
    ///</summary>
    public static CountTracksDelegate CountTracks;
    
    
    ///<summary>
    ///<para>callback is a function pointer: void (*callback)(void* obj, const char* msg, int msglen), which handles OSC messages sent from REAPER. The function return is a local osc handler. See SendLocalOscMessage, DestroyOscHandler.</para>
    ///</summary>
    public static CreateLocalOscHandlerDelegate CreateLocalOscHandler;
    
    
    ///<summary>
    ///</summary>
    public static CreateMIDIInputDelegate CreateMIDIInput;
    
    
    ///<summary>
    ///</summary>
    public static CreateMIDIOutputDelegate CreateMIDIOutput;
    
    
    ///<summary>
    ///<para>Create a new MIDI media item, containing no MIDI events.</para>
    ///</summary>
    public static CreateNewMIDIItemInProjDelegate CreateNewMIDIItemInProj;
    
    
    ///<summary>
    ///<para>Create an audio accessor object for this take. Must only call from the main thread. See CreateTrackAudioAccessor, DestroyAudioAccessor, GetAudioAccessorHash, GetAudioAccessorStartTime, GetAudioAccessorEndTime, GetAudioAccessorSamples.</para>
    ///</summary>
    public static CreateTakeAudioAccessorDelegate CreateTakeAudioAccessor;
    
    
    ///<summary>
    ///<para>Create an audio accessor object for this track. Must only call from the main thread. See CreateTakeAudioAccessor, DestroyAudioAccessor, GetAudioAccessorHash, GetAudioAccessorStartTime, GetAudioAccessorEndTime, GetAudioAccessorSamples.</para>
    ///</summary>
    public static CreateTrackAudioAccessorDelegate CreateTrackAudioAccessor;
    
    
    ///<summary>
    ///<para>call this to force flushing of the undo states after using CSurf_On*Change()</para>
    ///</summary>
    public static CSurf_FlushUndoDelegate CSurf_FlushUndo;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_GetTouchStateDelegate CSurf_GetTouchState;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_GoEndDelegate CSurf_GoEnd;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_GoStartDelegate CSurf_GoStart;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_NumTracksDelegate CSurf_NumTracks;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnArrowDelegate CSurf_OnArrow;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnFwdDelegate CSurf_OnFwd;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnFXChangeDelegate CSurf_OnFXChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnInputMonitorChangeDelegate CSurf_OnInputMonitorChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnInputMonitorChangeExDelegate CSurf_OnInputMonitorChangeEx;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnMuteChangeDelegate CSurf_OnMuteChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnMuteChangeExDelegate CSurf_OnMuteChangeEx;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnOscControlMessageDelegate CSurf_OnOscControlMessage;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnPanChangeDelegate CSurf_OnPanChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnPanChangeExDelegate CSurf_OnPanChangeEx;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnPauseDelegate CSurf_OnPause;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnPlayDelegate CSurf_OnPlay;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnPlayRateChangeDelegate CSurf_OnPlayRateChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnRecArmChangeDelegate CSurf_OnRecArmChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnRecArmChangeExDelegate CSurf_OnRecArmChangeEx;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnRecordDelegate CSurf_OnRecord;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnRecvPanChangeDelegate CSurf_OnRecvPanChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnRecvVolumeChangeDelegate CSurf_OnRecvVolumeChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnRewDelegate CSurf_OnRew;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnRewFwdDelegate CSurf_OnRewFwd;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnScrollDelegate CSurf_OnScroll;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnSelectedChangeDelegate CSurf_OnSelectedChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnSendPanChangeDelegate CSurf_OnSendPanChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnSendVolumeChangeDelegate CSurf_OnSendVolumeChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnSoloChangeDelegate CSurf_OnSoloChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnSoloChangeExDelegate CSurf_OnSoloChangeEx;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnStopDelegate CSurf_OnStop;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnTempoChangeDelegate CSurf_OnTempoChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnTrackSelectionDelegate CSurf_OnTrackSelection;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnVolumeChangeDelegate CSurf_OnVolumeChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnVolumeChangeExDelegate CSurf_OnVolumeChangeEx;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnWidthChangeDelegate CSurf_OnWidthChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnWidthChangeExDelegate CSurf_OnWidthChangeEx;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_OnZoomDelegate CSurf_OnZoom;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_ResetAllCachedVolPanStatesDelegate CSurf_ResetAllCachedVolPanStates;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_ScrubAmtDelegate CSurf_ScrubAmt;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetAutoModeDelegate CSurf_SetAutoMode;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetPlayStateDelegate CSurf_SetPlayState;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetRepeatStateDelegate CSurf_SetRepeatState;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetSurfaceMuteDelegate CSurf_SetSurfaceMute;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetSurfacePanDelegate CSurf_SetSurfacePan;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetSurfaceRecArmDelegate CSurf_SetSurfaceRecArm;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetSurfaceSelectedDelegate CSurf_SetSurfaceSelected;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetSurfaceSoloDelegate CSurf_SetSurfaceSolo;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetSurfaceVolumeDelegate CSurf_SetSurfaceVolume;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_SetTrackListChangeDelegate CSurf_SetTrackListChange;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_TrackFromIDDelegate CSurf_TrackFromID;
    
    
    ///<summary>
    ///</summary>
    public static CSurf_TrackToIDDelegate CSurf_TrackToID;
    
    
    ///<summary>
    ///</summary>
    public static DB2SLIDERDelegate DB2SLIDER;
    
    
    ///<summary>
    ///<para>Delete the specific shortcut for the given command ID.</para>
    ///<para>See CountActionShortcuts, GetActionShortcutDesc, DoActionShortcutDialog.</para>
    ///</summary>
    public static DeleteActionShortcutDelegate DeleteActionShortcut;
    
    
    ///<summary>
    ///<para>Delete the extended state value for a specific section and key. persist=true means the value should remain deleted the next time REAPER is opened. See SetExtState, GetExtState, HasExtState.</para>
    ///</summary>
    public static DeleteExtStateDelegate DeleteExtState;
    
    
    ///<summary>
    ///<para>Delete a marker.  proj==NULL for the active project.</para>
    ///</summary>
    public static DeleteProjectMarkerDelegate DeleteProjectMarker;
    
    
    ///<summary>
    ///<para>Differs from DeleteProjectMarker only in that markrgnidx is 0 for the first marker/region, 1 for the next, etc (see EnumProjectMarkers3), rather than representing the displayed marker/region ID number (see SetProjectMarker3).</para>
    ///</summary>
    public static DeleteProjectMarkerByIndexDelegate DeleteProjectMarkerByIndex;
    
    
    ///<summary>
    ///<para>Deletes one or more stretch markers. Returns number of stretch markers deleted.</para>
    ///</summary>
    public static DeleteTakeStretchMarkersDelegate DeleteTakeStretchMarkers;
    
    
    ///<summary>
    ///<para>deletes a track</para>
    ///</summary>
    public static DeleteTrackDelegate DeleteTrack;
    
    
    ///<summary>
    ///</summary>
    public static DeleteTrackMediaItemDelegate DeleteTrackMediaItem;
    
    
    ///<summary>
    ///<para>Destroy an audio accessor. Must only call from the main thread. See CreateTakeAudioAccessor, CreateTrackAudioAccessor, GetAudioAccessorHash, GetAudioAccessorStartTime, GetAudioAccessorEndTime, GetAudioAccessorSamples.</para>
    ///</summary>
    public static DestroyAudioAccessorDelegate DestroyAudioAccessor;
    
    
    ///<summary>
    ///<para>See CreateLocalOscHandler, SendLocalOscMessage.</para>
    ///</summary>
    public static DestroyLocalOscHandlerDelegate DestroyLocalOscHandler;
    
    
    ///<summary>
    ///<para>Open the action shortcut dialog to edit or add a shortcut for the given command ID. If (shortcutidx &gt;= 0 &amp;&amp; shortcutidx &lt; CountActionShortcuts()), that specific shortcut will be replaced, otherwise a new shortcut will be added.</para>
    ///<para>See CountActionShortcuts, GetActionShortcutDesc, DeleteActionShortcut.</para>
    ///</summary>
    public static DoActionShortcutDialogDelegate DoActionShortcutDialog;
    
    
    ///<summary>
    ///<para>updates preference for docker window ident_str to be in dock whichDock on next open</para>
    ///</summary>
    public static Dock_UpdateDockIDDelegate Dock_UpdateDockID;
    
    
    ///<summary>
    ///<para>returns dock index that contains hwnd, or -1</para>
    ///</summary>
    public static DockIsChildOfDockDelegate DockIsChildOfDock;
    
    
    ///<summary>
    ///</summary>
    public static DockWindowActivateDelegate DockWindowActivate;
    
    
    ///<summary>
    ///</summary>
    public static DockWindowAddDelegate DockWindowAdd;
    
    
    ///<summary>
    ///</summary>
    public static DockWindowAddExDelegate DockWindowAddEx;
    
    
    ///<summary>
    ///</summary>
    public static DockWindowRefreshDelegate DockWindowRefresh;
    
    
    ///<summary>
    ///</summary>
    public static DockWindowRefreshForHWNDDelegate DockWindowRefreshForHWND;
    
    
    ///<summary>
    ///</summary>
    public static DockWindowRemoveDelegate DockWindowRemove;
    
    
    ///<summary>
    ///<para>Populate destmenu with all the entries and submenus found in srcmenu</para>
    ///</summary>
    public static DuplicateCustomizableMenuDelegate DuplicateCustomizableMenu;
    
    
    ///<summary>
    ///<para>call with a saved window rect for your window and it&#39;ll correct any positioning info.</para>
    ///</summary>
    public static EnsureNotCompletelyOffscreenDelegate EnsureNotCompletelyOffscreen;
    
    
    ///<summary>
    ///<para>Start querying modes at 0, returns FALSE when no more modes possible, sets strOut to NULL if a mode is currently unsupported</para>
    ///</summary>
    public static EnumPitchShiftModesDelegate EnumPitchShiftModes;
    
    
    ///<summary>
    ///<para>Returns submode name, or NULL</para>
    ///</summary>
    public static EnumPitchShiftSubModesDelegate EnumPitchShiftSubModes;
    
    
    ///<summary>
    ///</summary>
    public static EnumProjectMarkersDelegate EnumProjectMarkers;
    
    
    ///<summary>
    ///</summary>
    public static EnumProjectMarkers2Delegate EnumProjectMarkers2;
    
    
    ///<summary>
    ///</summary>
    public static EnumProjectMarkers3Delegate EnumProjectMarkers3;
    
    
    ///<summary>
    ///<para>idx=-1 for current project,projfn can be NULL if not interested in filename</para>
    ///</summary>
    public static EnumProjectsDelegate EnumProjects;
    
    
    ///<summary>
    ///<para>Enumerate which tracks will be rendered within this region when using the region render matrix. When called with rendertrack==0, the function returns the first track that will be rendered (which may be the master track); rendertrack==1 will return the next track rendered, and so on. The function returns NULL when there are no more tracks that will be rendered within this region.</para>
    ///</summary>
    public static EnumRegionRenderMatrixDelegate EnumRegionRenderMatrix;
    
    
    ///<summary>
    ///<para>returns false if there are no plugins on the track that support MIDI programs,or if all programs have been enumerated</para>
    ///</summary>
    public static EnumTrackMIDIProgramNamesDelegate EnumTrackMIDIProgramNames;
    
    
    ///<summary>
    ///<para>returns false if there are no plugins on the track that support MIDI programs,or if all programs have been enumerated</para>
    ///</summary>
    public static EnumTrackMIDIProgramNamesExDelegate EnumTrackMIDIProgramNamesEx;
    
    
    ///<summary>
    ///<para>returns true if path points to a valid, readable file</para>
    ///</summary>
    public static file_existsDelegate file_exists;
    
    
    ///<summary>
    ///<para>time formatting mode overrides: -1=proj default.</para>
    ///<para>0=time</para>
    ///<para>1=measures.beats + time</para>
    ///<para>2=measures.beats</para>
    ///<para>3=seconds</para>
    ///<para>4=samples</para>
    ///<para>5=h:m:s:f</para>
    ///</summary>
    public static format_timestrDelegate format_timestr;
    
    
    ///<summary>
    ///<para>time formatting mode overrides: -1=proj default.</para>
    ///<para>0=time</para>
    ///<para>1=measures.beats + time</para>
    ///<para>2=measures.beats</para>
    ///<para>3=seconds</para>
    ///<para>4=samples</para>
    ///<para>5=h:m:s:f</para>
    ///<para>offset is start of where the length will be calculated from</para>
    ///</summary>
    public static format_timestr_lenDelegate format_timestr_len;
    
    
    ///<summary>
    ///<para>time formatting mode overrides: -1=proj default.</para>
    ///<para>0=time</para>
    ///<para>1=measures.beats + time</para>
    ///<para>2=measures.beats</para>
    ///<para>3=seconds</para>
    ///<para>4=samples</para>
    ///<para>5=h:m:s:f</para>
    ///</summary>
    public static format_timestr_posDelegate format_timestr_pos;
    
    
    ///<summary>
    ///<para>free heap memory returned from a Reaper API function</para>
    ///</summary>
    public static FreeHeapPtrDelegate FreeHeapPtr;
    
    
    ///<summary>
    ///</summary>
    public static genGuidDelegate genGuid;
    
    
    ///<summary>
    ///</summary>
    public static get_config_varDelegate get_config_var;
    
    
    ///<summary>
    ///</summary>
    public static get_ini_fileDelegate get_ini_file;
    
    
    ///<summary>
    ///</summary>
    public static get_midi_config_varDelegate get_midi_config_var;
    
    
    ///<summary>
    ///<para>Get the text description of a specific shortcut for the given command ID.</para>
    ///<para>See CountActionShortcuts,DeleteActionShortcut,DoActionShortcutDialog.</para>
    ///</summary>
    public static GetActionShortcutDescDelegate GetActionShortcutDesc;
    
    
    ///<summary>
    ///<para>get the active take in this item</para>
    ///</summary>
    public static GetActiveTakeDelegate GetActiveTake;
    
    
    ///<summary>
    ///</summary>
    public static GetAppVersionDelegate GetAppVersion;
    
    
    ///<summary>
    ///<para>Get the end time of the audio that can be returned from this accessor. See CreateTakeAudioAccessor, CreateTrackAudioAccessor, DestroyAudioAccessor, GetAudioAccessorHash, GetAudioAccessorStartTime, GetAudioAccessorSamples.</para>
    ///</summary>
    public static GetAudioAccessorEndTimeDelegate GetAudioAccessorEndTime;
    
    
    ///<summary>
    ///<para>Get a short hash string (128 chars or less) that will change only if the underlying samples change.  See CreateTakeAudioAccessor, CreateTrackAudioAccessor, DestroyAudioAccessor, GetAudioAccessorStartTime, GetAudioAccessorEndTime, GetAudioAccessorSamples.</para>
    ///</summary>
    public static GetAudioAccessorHashDelegate GetAudioAccessorHash;
    
    
    ///<summary>
    ///<para>Get a block of samples from the audio accessor. Samples are extracted immediately pre-FX, and returned interleaved (first sample of first channel, first sample of second channel...). Returns 0 if no audio, 1 if audio, -1 on error. See CreateTakeAudioAccessor, CreateTrackAudioAccessor, DestroyAudioAccessor, GetAudioAccessorHash, GetAudioAccessorStartTime, GetAudioAccessorEndTime.</para>
    ///</summary>
    public static GetAudioAccessorSamplesDelegate GetAudioAccessorSamples;
    
    
    ///<summary>
    ///<para>Get the start time of the audio that can be returned from this accessor. See CreateTakeAudioAccessor, CreateTrackAudioAccessor, DestroyAudioAccessor, GetAudioAccessorHash, GetAudioAccessorEndTime, GetAudioAccessorSamples.</para>
    ///</summary>
    public static GetAudioAccessorStartTimeDelegate GetAudioAccessorStartTime;
    
    
    ///<summary>
    ///<para>deprecated?</para>
    ///</summary>
    public static GetColorThemeDelegate GetColorTheme;
    
    
    ///<summary>
    ///<para>returns the whole color theme (icontheme.h) and the size</para>
    ///</summary>
    public static GetColorThemeStructDelegate GetColorThemeStruct;
    
    
    ///<summary>
    ///<para>gets the dock ID desired by ident_str, if any</para>
    ///</summary>
    public static GetConfigWantsDockDelegate GetConfigWantsDock;
    
    
    ///<summary>
    ///<para>gets context menus. submenu 0:trackctl, 1:mediaitems, 2:ruler, 3:empty track area</para>
    ///</summary>
    public static GetContextMenuDelegate GetContextMenu;
    
    
    ///<summary>
    ///<para>returns current project if in load/save (usually only used from project_config_extension_t)</para>
    ///</summary>
    public static GetCurrentProjectInLoadSaveDelegate GetCurrentProjectInLoadSave;
    
    
    ///<summary>
    ///<para>0 if track panels, 1 if items, 2 if envelopes, otherwise unknown</para>
    ///</summary>
    public static GetCursorContextDelegate GetCursorContext;
    
    
    ///<summary>
    ///<para>edit cursor position</para>
    ///</summary>
    public static GetCursorPositionDelegate GetCursorPosition;
    
    
    ///<summary>
    ///<para>edit cursor position</para>
    ///</summary>
    public static GetCursorPositionExDelegate GetCursorPositionEx;
    
    
    ///<summary>
    ///<para>returns the custom take, item, or track color that is used (according to the user preference) to color the media item.  The color is returned as 0x01RRGGBB, so a return of zero means &#34;no color&#34;, not black.</para>
    ///</summary>
    public static GetDisplayedMediaItemColorDelegate GetDisplayedMediaItemColor;
    
    
    ///<summary>
    ///<para>returns the custom take, item, or track color that is used (according to the user preference) to color the media item.  The color is returned as 0x01RRGGBB, so a return of zero means &#34;no color&#34;, not black.</para>
    ///</summary>
    public static GetDisplayedMediaItemColor2Delegate GetDisplayedMediaItemColor2;
    
    
    ///<summary>
    ///</summary>
    public static GetEnvelopeNameDelegate GetEnvelopeName;
    
    
    ///<summary>
    ///<para>returns path of REAPER.exe (not including EXE), i.e. C:\Program Files\REAPER</para>
    ///</summary>
    public static GetExePathDelegate GetExePath;
    
    
    ///<summary>
    ///<para>Get the extended state value for a specific section and key. See SetExtState, DeleteExtState, HasExtState.</para>
    ///</summary>
    public static GetExtStateDelegate GetExtState;
    
    
    ///<summary>
    ///<para>Returns 1 if a track FX window has focus, 2 if an item FX window has focus, 0 if no FX window has focus. tracknumber==0 means the master track, 1 means track 1, etc. itemnumber and fxnumber are zero-based. See GetLastTouchedFX.</para>
    ///</summary>
    public static GetFocusedFXDelegate GetFocusedFX;
    
    
    ///<summary>
    ///<para>returns free disk space in megabytes, pathIdx 0 for normal, 1 for alternate.</para>
    ///</summary>
    public static GetFreeDiskSpaceForRecordPathDelegate GetFreeDiskSpaceForRecordPath;
    
    
    ///<summary>
    ///<para>returns pixels/second</para>
    ///</summary>
    public static GetHZoomLevelDelegate GetHZoomLevel;
    
    
    ///<summary>
    ///<para>returns a named icontheme entry</para>
    ///</summary>
    public static GetIconThemePointerDelegate GetIconThemePointer;
    
    
    ///<summary>
    ///<para>returns a pointer to the icon theme (icontheme.h) and the size of that struct</para>
    ///</summary>
    public static GetIconThemeStructDelegate GetIconThemeStruct;
    
    
    ///<summary>
    ///</summary>
    public static GetInputChannelNameDelegate GetInputChannelName;
    
    
    ///<summary>
    ///<para>Gets the audio device input/output latency in samples</para>
    ///</summary>
    public static GetInputOutputLatencyDelegate GetInputOutputLatency;
    
    
    ///<summary>
    ///<para>returns time of relevant edit, set which_item to the pcm_source (if applicable), flags (if specified) will be set to 1 for edge resizing, 2 for fade change, 4 for item move</para>
    ///</summary>
    public static GetItemEditingTime2Delegate GetItemEditingTime2;
    
    
    ///<summary>
    ///</summary>
    public static GetItemProjectContextDelegate GetItemProjectContext;
    
    
    ///<summary>
    ///<para>Get the last project marker before time, and/or the project region that includes time. markeridx and regionidx are returned not necessarily as the displayed marker/region index, but as the index that can be passed to EnumProjectMarkers. Either or both of markeridx and regionidx may be NULL. See EnumProjectMarkers.</para>
    ///</summary>
    public static GetLastMarkerAndCurRegionDelegate GetLastMarkerAndCurRegion;
    
    
    ///<summary>
    ///<para>Returns true if the last touched FX parameter is valid, false otherwise. tracknumber==0 means the master track, 1 means track 1, etc. fxnumber and paramnumber are zero-based. See GetFocusedFX.</para>
    ///</summary>
    public static GetLastTouchedFXDelegate GetLastTouchedFX;
    
    
    ///<summary>
    ///</summary>
    public static GetLastTouchedTrackDelegate GetLastTouchedTrack;
    
    
    ///<summary>
    ///</summary>
    public static GetMainHwndDelegate GetMainHwnd;
    
    
    ///<summary>
    ///<para>&amp;1=master mute,&amp;2=master solo. This is deprecated as you can just query the master track as well.</para>
    ///</summary>
    public static GetMasterMuteSoloFlagsDelegate GetMasterMuteSoloFlags;
    
    
    ///<summary>
    ///</summary>
    public static GetMasterTrackDelegate GetMasterTrack;
    
    
    ///<summary>
    ///<para>returns &amp;1 if the master track is visible in the TCP, &amp;2 if visible in the mixer. See SetMasterTrackVisibility.</para>
    ///</summary>
    public static GetMasterTrackVisibilityDelegate GetMasterTrackVisibility;
    
    
    ///<summary>
    ///<para>returns max dev for midi inputs/outputs</para>
    ///</summary>
    public static GetMaxMidiInputsDelegate GetMaxMidiInputs;
    
    
    ///<summary>
    ///</summary>
    public static GetMaxMidiOutputsDelegate GetMaxMidiOutputs;
    
    
    ///<summary>
    ///<para>get an item from a project by item count (zero-based) (proj=0 for active project)</para>
    ///</summary>
    public static GetMediaItemDelegate GetMediaItem;
    
    
    ///<summary>
    ///<para>Get parent track of media item</para>
    ///</summary>
    public static GetMediaItem_TrackDelegate GetMediaItem_Track;
    
    
    ///<summary>
    ///<para>Get media item numerical-value attributes.</para>
    ///<para>B_MUTE : bool * to muted state</para>
    ///<para>B_LOOPSRC : bool * to loop source</para>
    ///<para>B_ALLTAKESPLAY : bool * to all takes play</para>
    ///<para>B_UISEL : bool * to ui selected</para>
    ///<para>C_BEATATTACHMODE : char * to one char of beat attached mode, -1=def, 0=time, 1=allbeats, 2=beatsosonly</para>
    ///<para>C_LOCK : char * to one char of lock flags (&amp;1 is locked, currently)</para>
    ///<para>D_VOL : double * of item volume (volume bar)</para>
    ///<para>D_POSITION : double * of item position (seconds)</para>
    ///<para>D_LENGTH : double * of item length (seconds)</para>
    ///<para>D_SNAPOFFSET : double * of item snap offset (seconds)</para>
    ///<para>D_FADEINLEN : double * of item fade in length (manual, seconds)</para>
    ///<para>D_FADEOUTLEN : double * of item fade out length (manual, seconds)</para>
    ///<para>D_FADEINLEN_AUTO : double * of item autofade in length (seconds, -1 for no autofade set)</para>
    ///<para>D_FADEOUTLEN_AUTO : double * of item autofade out length (seconds, -1 for no autofade set)</para>
    ///<para>C_FADEINSHAPE : int * to fadein shape, 0=linear, ...</para>
    ///<para>C_FADEOUTSHAPE : int * to fadeout shape</para>
    ///<para>I_GROUPID : int * to group ID (0 = no group)</para>
    ///<para>I_LASTY : int * to last y position in track (readonly)</para>
    ///<para>I_LASTH : int * to last height in track (readonly)</para>
    ///<para>I_CUSTOMCOLOR : int * : custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>I_CURTAKE : int * to active take</para>
    ///<para>IP_ITEMNUMBER : int, item number within the track (read-only, returns the item number directly)</para>
    ///<para>F_FREEMODE_Y : float * to free mode y position (0..1)</para>
    ///<para>F_FREEMODE_H : float * to free mode height (0..1)</para>
    ///</summary>
    public static GetMediaItemInfo_ValueDelegate GetMediaItemInfo_Value;
    
    
    ///<summary>
    ///</summary>
    public static GetMediaItemNumTakesDelegate GetMediaItemNumTakes;
    
    
    ///<summary>
    ///</summary>
    public static GetMediaItemTakeDelegate GetMediaItemTake;
    
    
    ///<summary>
    ///<para>Get parent item of media item take</para>
    ///</summary>
    public static GetMediaItemTake_ItemDelegate GetMediaItemTake_Item;
    
    
    ///<summary>
    ///<para>Get media source of media item take</para>
    ///</summary>
    public static GetMediaItemTake_SourceDelegate GetMediaItemTake_Source;
    
    
    ///<summary>
    ///<para>Get parent track of media item take</para>
    ///</summary>
    public static GetMediaItemTake_TrackDelegate GetMediaItemTake_Track;
    
    
    ///<summary>
    ///</summary>
    public static GetMediaItemTakeByGUIDDelegate GetMediaItemTakeByGUID;
    
    
    ///<summary>
    ///<para>Get media item take numerical-value attributes.</para>
    ///<para>D_STARTOFFS : double *, start offset in take of item</para>
    ///<para>D_VOL : double *, take volume</para>
    ///<para>D_PAN : double *, take pan</para>
    ///<para>D_PANLAW : double *, take pan law (-1.0=default, 0.5=-6dB, 1.0=+0dB, etc)</para>
    ///<para>D_PLAYRATE : double *, take playrate (1.0=normal, 2.0=doublespeed, etc)</para>
    ///<para>D_PITCH : double *, take pitch adjust (in semitones, 0.0=normal, +12 = one octave up, etc)</para>
    ///<para>B_PPITCH, bool *, preserve pitch when changing rate</para>
    ///<para>I_CHANMODE, int *, channel mode (0=normal, 1=revstereo, 2=downmix, 3=l, 4=r)</para>
    ///<para>I_PITCHMODE, int *, pitch shifter mode, -1=proj default, otherwise high word=shifter low word = parameter</para>
    ///<para>I_CUSTOMCOLOR : int *, custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>IP_TAKENUMBER : int, take number within the item (read-only, returns the take number directly)</para>
    ///</summary>
    public static GetMediaItemTakeInfo_ValueDelegate GetMediaItemTakeInfo_Value;
    
    
    ///<summary>
    ///</summary>
    public static GetMediaItemTrackDelegate GetMediaItemTrack;
    
    
    ///<summary>
    ///<para>Copies the media source filename to typebuf. Note that in-project MIDI media sources have no associated filename.</para>
    ///</summary>
    public static GetMediaSourceFileNameDelegate GetMediaSourceFileName;
    
    
    ///<summary>
    ///<para>Returns the number of channels in the source media.</para>
    ///</summary>
    public static GetMediaSourceNumChannelsDelegate GetMediaSourceNumChannels;
    
    
    ///<summary>
    ///<para>Returns the sample rate. MIDI source media will return zero.</para>
    ///</summary>
    public static GetMediaSourceSampleRateDelegate GetMediaSourceSampleRate;
    
    
    ///<summary>
    ///<para>copies the media source type (&#34;WAV&#34;, &#34;MIDI&#34;, etc) to typebuf</para>
    ///</summary>
    public static GetMediaSourceTypeDelegate GetMediaSourceType;
    
    
    ///<summary>
    ///<para>Get track numerical-value attributes.</para>
    ///<para>B_MUTE : bool * : mute flag</para>
    ///<para>B_PHASE : bool * : invert track phase</para>
    ///<para>IP_TRACKNUMBER : int : track number (returns zero if not found, -1 for master track) (read-only, returns the int directly)</para>
    ///<para>I_SOLO : int * : 0=not soloed, 1=solo, 2=soloed in place</para>
    ///<para>I_FXEN : int * : 0=fx bypassed, nonzero = fx active</para>
    ///<para>I_RECARM : int * : 0=not record armed, 1=record armed</para>
    ///<para>I_RECINPUT : int * : record input. &lt;0 = no input, 0..n = mono hardware input, 512+n = rearoute input, 1024 set for stereo input pair. 4096 set for MIDI input, if set, then low 5 bits represent channel (0=all, 1-16=only chan), then next 5 bits represent physical input (31=all, 30=VKB)</para>
    ///<para>I_RECMODE : int * : record mode (0=input, 1=stereo out, 2=none, 3=stereo out w/latcomp, 4=midi output, 5=mono out, 6=mono out w/ lat comp, 7=midi overdub, 8=midi replace</para>
    ///<para>I_RECMON : int * : record monitor (0=off, 1=normal, 2=not when playing (tapestyle))</para>
    ///<para>I_RECMONITEMS : int * : monitor items while recording (0=off, 1=on)</para>
    ///<para>I_AUTOMODE : int * : track automation mode (0=trim/off, 1=read, 2=touch, 3=write, 4=latch</para>
    ///<para>I_NCHAN : int * : number of track channels, must be 2-64, even</para>
    ///<para>I_SELECTED : int * : track selected? 0 or 1</para>
    ///<para>I_WNDH : int * : current TCP window height (Read-only)</para>
    ///<para>I_FOLDERDEPTH : int * : folder depth change (0=normal, 1=track is a folder parent, -1=track is the last in the innermost folder, -2=track is the last in the innermost and next-innermost folders, etc</para>
    ///<para>I_FOLDERCOMPACT : int * : folder compacting (only valid on folders), 0=normal, 1=small, 2=tiny children</para>
    ///<para>I_MIDIHWOUT : int * : track midi hardware output index (&lt;0 for disabled, low 5 bits are which channels (0=all, 1-16), next 5 bits are output device index (0-31))</para>
    ///<para>I_PERFFLAGS : int * : track perf flags (&amp;1=no media buffering, &amp;2=no anticipative FX)</para>
    ///<para>I_CUSTOMCOLOR : int * : custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>I_HEIGHTOVERRIDE : int * : custom height override for TCP window. 0 for none, otherwise size in pixels</para>
    ///<para>D_VOL : double * : trim volume of track (0 (-inf)..1 (+0dB) .. 2 (+6dB) etc ..)</para>
    ///<para>D_PAN : double * : trim pan of track (-1..1)</para>
    ///<para>D_WIDTH : double * : width of track (-1..1)</para>
    ///<para>D_DUALPANL : double * : dualpan position 1 (-1..1), only if I_PANMODE==6</para>
    ///<para>D_DUALPANR : double * : dualpan position 2 (-1..1), only if I_PANMODE==6</para>
    ///<para>I_PANMODE : int * : pan mode (0 = classic 3.x, 3=new balance, 5=stereo pan, 6 = dual pan)</para>
    ///<para>D_PANLAW : double * : pan law of track. &lt;0 for project default, 1.0 for +0dB, etc</para>
    ///<para>P_ENV : read only, returns TrackEnvelope *, setNewValue=&lt;VOLENV, &lt;PANENV, etc</para>
    ///<para>B_SHOWINMIXER : bool * : show track panel in mixer -- do not use on master</para>
    ///<para>B_SHOWINTCP : bool * : show track panel in tcp -- do not use on master</para>
    ///<para>B_MAINSEND : bool * : track sends audio to parent</para>
    ///<para>B_FREEMODE : bool * : track free-mode enabled (requires UpdateTimeline() after changing etc)</para>
    ///<para>C_BEATATTACHMODE : char * : char * to one char of beat attached mode, -1=def, 0=time, 1=allbeats, 2=beatsposonly</para>
    ///<para>F_MCP_FXSEND_SCALE : float * : scale of fx+send area in MCP (0.0=smallest allowed, 1=max allowed)</para>
    ///<para>F_MCP_SENDRGN_SCALE : float * : scale of send area as proportion of the fx+send total area (0=min allow, 1=max)</para>
    ///</summary>
    public static GetMediaTrackInfo_ValueDelegate GetMediaTrackInfo_Value;
    
    
    ///<summary>
    ///</summary>
    public static GetMidiInputDelegate GetMidiInput;
    
    
    ///<summary>
    ///<para>returns true if device present</para>
    ///</summary>
    public static GetMIDIInputNameDelegate GetMIDIInputName;
    
    
    ///<summary>
    ///</summary>
    public static GetMidiOutputDelegate GetMidiOutput;
    
    
    ///<summary>
    ///<para>returns true if device present</para>
    ///</summary>
    public static GetMIDIOutputNameDelegate GetMIDIOutputName;
    
    
    ///<summary>
    ///<para>Get the leftmost track visible in the mixer</para>
    ///</summary>
    public static GetMixerScrollDelegate GetMixerScroll;
    
    
    ///<summary>
    ///<para>Get the current mouse modifier assignment for a specific modifier key assignment, in a specific context.</para>
    ///<para>action will be filled in with the command ID number for a built-in mouse modifier</para>
    ///<para>or built-in REAPER command ID, or the custom action ID string.</para>
    ///<para>See SetMouseModifier for more information.</para>
    ///</summary>
    public static GetMouseModifierDelegate GetMouseModifier;
    
    
    ///<summary>
    ///<para>Return number of normal audio hardware inputs available</para>
    ///</summary>
    public static GetNumAudioInputsDelegate GetNumAudioInputs;
    
    
    ///<summary>
    ///<para>Return number of normal audio hardware outputs available</para>
    ///</summary>
    public static GetNumAudioOutputsDelegate GetNumAudioOutputs;
    
    
    ///<summary>
    ///<para>returns max number of real midi hardware inputs</para>
    ///</summary>
    public static GetNumMIDIInputsDelegate GetNumMIDIInputs;
    
    
    ///<summary>
    ///<para>returns max number of real midi hardware outputs</para>
    ///</summary>
    public static GetNumMIDIOutputsDelegate GetNumMIDIOutputs;
    
    
    ///<summary>
    ///</summary>
    public static GetNumTracksDelegate GetNumTracks;
    
    
    ///<summary>
    ///</summary>
    public static GetOutputChannelNameDelegate GetOutputChannelName;
    
    
    ///<summary>
    ///<para>returns output latency in seconds</para>
    ///</summary>
    public static GetOutputLatencyDelegate GetOutputLatency;
    
    
    ///<summary>
    ///</summary>
    public static GetParentTrackDelegate GetParentTrack;
    
    
    ///<summary>
    ///<para>get the peak file name for a given file (can be either filename.reapeaks,or a hashed filename in another path)</para>
    ///</summary>
    public static GetPeakFileNameDelegate GetPeakFileName;
    
    
    ///<summary>
    ///<para>get the peak file name for a given file (can be either filename.reapeaks,or a hashed filename in another path)</para>
    ///</summary>
    public static GetPeakFileNameExDelegate GetPeakFileNameEx;
    
    
    ///<summary>
    ///<para>Like GetPeakFileNameEx, but you can specify peaksfileextension such as &#34;.reapeaks&#34;</para>
    ///</summary>
    public static GetPeakFileNameEx2Delegate GetPeakFileNameEx2;
    
    
    ///<summary>
    ///<para>see note in reaper_plugin.h about PCM_source_peaktransfer_t::samplerate</para>
    ///</summary>
    public static GetPeaksBitmapDelegate GetPeaksBitmap;
    
    
    ///<summary>
    ///<para>returns latency-compensated actual-what-you-hear position</para>
    ///</summary>
    public static GetPlayPositionDelegate GetPlayPosition;
    
    
    ///<summary>
    ///<para>returns position of next audio block being processed</para>
    ///</summary>
    public static GetPlayPosition2Delegate GetPlayPosition2;
    
    
    ///<summary>
    ///<para>returns position of next audio block being processed</para>
    ///</summary>
    public static GetPlayPosition2ExDelegate GetPlayPosition2Ex;
    
    
    ///<summary>
    ///<para>returns latency-compensated actual-what-you-hear position</para>
    ///</summary>
    public static GetPlayPositionExDelegate GetPlayPositionEx;
    
    
    ///<summary>
    ///<para>&amp;1=playing,&amp;2=pause,&amp;=4 is recording</para>
    ///</summary>
    public static GetPlayStateDelegate GetPlayState;
    
    
    ///<summary>
    ///<para>&amp;1=playing,&amp;2=pause,&amp;=4 is recording</para>
    ///</summary>
    public static GetPlayStateExDelegate GetPlayStateEx;
    
    
    ///<summary>
    ///</summary>
    public static GetProjectPathDelegate GetProjectPath;
    
    
    ///<summary>
    ///</summary>
    public static GetProjectPathExDelegate GetProjectPathEx;
    
    
    ///<summary>
    ///<para>returns an integer that changes when the project state changes</para>
    ///</summary>
    public static GetProjectStateChangeCountDelegate GetProjectStateChangeCount;
    
    
    ///<summary>
    ///<para>deprecated</para>
    ///</summary>
    public static GetProjectTimeSignatureDelegate GetProjectTimeSignature;
    
    
    ///<summary>
    ///<para>Gets basic time signature (beats per minute, numerator of time signature in bpi)</para>
    ///<para>this does not reflect tempo envelopes but is purely what is set in the project settings.</para>
    ///</summary>
    public static GetProjectTimeSignature2Delegate GetProjectTimeSignature2;
    
    
    ///<summary>
    ///<para>returns path where ini files are stored, other things are in subdirectories.</para>
    ///</summary>
    public static GetResourcePathDelegate GetResourcePath;
    
    
    ///<summary>
    ///<para>get a selected item by selected item count (zero-based) (proj=0 for active project)</para>
    ///</summary>
    public static GetSelectedMediaItemDelegate GetSelectedMediaItem;
    
    
    ///<summary>
    ///<para>get a selected track from a project by selected track count (zero-based) (proj=0 for active project)</para>
    ///</summary>
    public static GetSelectedTrackDelegate GetSelectedTrack;
    
    
    ///<summary>
    ///<para>get the currently selected track envelope, returns 0 if no envelope is selected</para>
    ///</summary>
    public static GetSelectedTrackEnvelopeDelegate GetSelectedTrackEnvelope;
    
    
    ///<summary>
    ///</summary>
    public static GetSet_ArrangeView2Delegate GetSet_ArrangeView2;
    
    
    ///<summary>
    ///</summary>
    public static GetSet_LoopTimeRangeDelegate GetSet_LoopTimeRange;
    
    
    ///<summary>
    ///</summary>
    public static GetSet_LoopTimeRange2Delegate GetSet_LoopTimeRange2;
    
    
    ///<summary>
    ///<para>get or set the state of an envelope as an xml/rpp chunk.</para>
    ///<para>supply str=&#34;&#34; to get the state returned in str.</para>
    ///<para>the returned state string is limited to maxlen characters.</para>
    ///<para>supply str as an xml/rpp string to set the object state.</para>
    ///<para>returns true on success.</para>
    ///</summary>
    public static GetSetEnvelopeStateDelegate GetSetEnvelopeState;
    
    
    ///<summary>
    ///<para>get or set the state of an envelope as an xml/rpp chunk.</para>
    ///<para>set isundo if the state will be used for undo purposes (which may allow REAPER to get the state more efficiently)</para>
    ///<para>supply str=&#34;&#34; to get the state returned in str.</para>
    ///<para>the returned state string is limited to maxlen characters.</para>
    ///<para>supply str as an xml/rpp string to set the object state.</para>
    ///<para>returns true on success.</para>
    ///</summary>
    public static GetSetEnvelopeState2Delegate GetSetEnvelopeState2;
    
    
    ///<summary>
    ///<para>get or set the state of a media item as an xml/rpp chunk.</para>
    ///<para>supply str=&#34;&#34; to get the state returned in str.</para>
    ///<para>the returned state string is limited to maxlen characters.</para>
    ///<para>supply str as an xml/rpp string to set the object state.</para>
    ///<para>returns true on success.</para>
    ///</summary>
    public static GetSetItemStateDelegate GetSetItemState;
    
    
    ///<summary>
    ///<para>get or set the state of a media item as an xml/rpp chunk.</para>
    ///<para>set isundo if the state will be used for undo purposes (which may allow REAPER to get the state more efficiently)</para>
    ///<para>supply str=&#34;&#34; to get the state returned in str.</para>
    ///<para>the returned state string is limited to maxlen characters.</para>
    ///<para>supply str as an xml/rpp string to set the object state.</para>
    ///<para>returns true on success.</para>
    ///</summary>
    public static GetSetItemState2Delegate GetSetItemState2;
    
    
    ///<summary>
    ///<para>P_TRACK : MediaTrack * (read only)</para>
    ///<para>P_NOTES : char * : item note text (do not write to returned pointer, use setNewValue to update)</para>
    ///<para>B_MUTE : bool * to muted state</para>
    ///<para>B_LOOPSRC : bool * to loop source</para>
    ///<para>B_ALLTAKESPLAY : bool * to all takes play</para>
    ///<para>B_UISEL : bool * to ui selected</para>
    ///<para>C_BEATATTACHMODE : char * to one char of beat attached mode, -1=def, 0=time, 1=allbeats, 2=beatsosonly</para>
    ///<para>C_LOCK : char * to one char of lock flags (&amp;1 is locked, currently)</para>
    ///<para>D_VOL : double * of item volume (volume bar)</para>
    ///<para>D_POSITION : double * of item position (seconds)</para>
    ///<para>D_LENGTH : double * of item length (seconds)</para>
    ///<para>D_SNAPOFFSET : double * of item snap offset (seconds)</para>
    ///<para>D_FADEINLEN : double * of item fade in length (manual, seconds)</para>
    ///<para>D_FADEOUTLEN : double * of item fade out length (manual, seconds)</para>
    ///<para>D_FADEINLEN_AUTO : double * of item autofade in length (seconds, -1 for no autofade set)</para>
    ///<para>D_FADEOUTLEN_AUTO : double * of item autofade out length (seconds, -1 for no autofade set)</para>
    ///<para>C_FADEINSHAPE : int * to fadein shape, 0=linear, ...</para>
    ///<para>C_FADEOUTSHAPE : int * to fadeout shape</para>
    ///<para>I_GROUPID : int * to group ID (0 = no group)</para>
    ///<para>I_LASTY : int * to last y position in track (readonly)</para>
    ///<para>I_LASTH : int * to last height in track (readonly)</para>
    ///<para>I_CUSTOMCOLOR : int * : custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>I_CURTAKE : int * to active take</para>
    ///<para>IP_ITEMNUMBER : int, item number within the track (read-only, returns the item number directly)</para>
    ///<para>F_FREEMODE_Y : float * to free mode y position (0..1)</para>
    ///<para>F_FREEMODE_H : float * to free mode height (0..1)</para>
    ///</summary>
    public static GetSetMediaItemInfoDelegate GetSetMediaItemInfo;
    
    
    ///<summary>
    ///<para>P_TRACK : pointer to MediaTrack (read-only)</para>
    ///<para>P_ITEM : pointer to MediaItem (read-only)</para>
    ///<para>P_SOURCE : PCM_source *. Note that if setting this, you should first retrieve the old source, set the new, THEN delete the old.</para>
    ///<para>GUID : GUID * : 16-byte GUID, can query or update</para>
    ///<para>P_NAME : char * to take name</para>
    ///<para>D_STARTOFFS : double *, start offset in take of item</para>
    ///<para>D_VOL : double *, take volume</para>
    ///<para>D_PAN : double *, take pan</para>
    ///<para>D_PANLAW : double *, take pan law (-1.0=default, 0.5=-6dB, 1.0=+0dB, etc)</para>
    ///<para>D_PLAYRATE : double *, take playrate (1.0=normal, 2.0=doublespeed, etc)</para>
    ///<para>D_PITCH : double *, take pitch adjust (in semitones, 0.0=normal, +12 = one octave up, etc)</para>
    ///<para>B_PPITCH, bool *, preserve pitch when changing rate</para>
    ///<para>I_CHANMODE, int *, channel mode (0=normal, 1=revstereo, 2=downmix, 3=l, 4=r)</para>
    ///<para>I_PITCHMODE, int *, pitch shifter mode, -1=proj default, otherwise high word=shifter low word = parameter</para>
    ///<para>I_CUSTOMCOLOR : int *, custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>IP_TAKENUMBER : int, take number within the item (read-only, returns the take number directly)</para>
    ///</summary>
    public static GetSetMediaItemTakeInfoDelegate GetSetMediaItemTakeInfo;
    
    
    ///<summary>
    ///<para>P_NAME : char * to take name</para>
    ///</summary>
    public static GetSetMediaItemTakeInfo_StringDelegate GetSetMediaItemTakeInfo_String;
    
    
    ///<summary>
    ///<para>Get or set track attributes.</para>
    ///<para>P_PARTRACK : MediaTrack * : parent track (read-only)</para>
    ///<para>GUID : GUID * : 16-byte GUID, can query or update (do not use on master though)</para>
    ///<para>P_NAME : char * : track name (on master returns NULL)</para>
    ///<para></para>
    ///<para>B_MUTE : bool * : mute flag</para>
    ///<para>B_PHASE : bool * : invert track phase</para>
    ///<para>IP_TRACKNUMBER : int : track number (returns zero if not found, -1 for master track) (read-only, returns the int directly)</para>
    ///<para>I_SOLO : int * : 0=not soloed, 1=solo, 2=soloed in place</para>
    ///<para>I_FXEN : int * : 0=fx bypassed, nonzero = fx active</para>
    ///<para>I_RECARM : int * : 0=not record armed, 1=record armed</para>
    ///<para>I_RECINPUT : int * : record input. &lt;0 = no input, 0..n = mono hardware input, 512+n = rearoute input, 1024 set for stereo input pair. 4096 set for MIDI input, if set, then low 5 bits represent channel (0=all, 1-16=only chan), then next 5 bits represent physical input (31=all, 30=VKB)</para>
    ///<para>I_RECMODE : int * : record mode (0=input, 1=stereo out, 2=none, 3=stereo out w/latcomp, 4=midi output, 5=mono out, 6=mono out w/ lat comp, 7=midi overdub, 8=midi replace</para>
    ///<para>I_RECMON : int * : record monitor (0=off, 1=normal, 2=not when playing (tapestyle))</para>
    ///<para>I_RECMONITEMS : int * : monitor items while recording (0=off, 1=on)</para>
    ///<para>I_AUTOMODE : int * : track automation mode (0=trim/off, 1=read, 2=touch, 3=write, 4=latch</para>
    ///<para>I_NCHAN : int * : number of track channels, must be 2-64, even</para>
    ///<para>I_SELECTED : int * : track selected? 0 or 1</para>
    ///<para>I_WNDH : int * : current TCP window height (Read-only)</para>
    ///<para>I_FOLDERDEPTH : int * : folder depth change (0=normal, 1=track is a folder parent, -1=track is the last in the innermost folder, -2=track is the last in the innermost and next-innermost folders, etc</para>
    ///<para>I_FOLDERCOMPACT : int * : folder compacting (only valid on folders), 0=normal, 1=small, 2=tiny children</para>
    ///<para>I_MIDIHWOUT : int * : track midi hardware output index (&lt;0 for disabled, low 5 bits are which channels (0=all, 1-16), next 5 bits are output device index (0-31))</para>
    ///<para>I_PERFFLAGS : int * : track perf flags (&amp;1=no media buffering, &amp;2=no anticipative FX)</para>
    ///<para>I_CUSTOMCOLOR : int * : custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>I_HEIGHTOVERRIDE : int * : custom height override for TCP window. 0 for none, otherwise size in pixels</para>
    ///<para>D_VOL : double * : trim volume of track (0 (-inf)..1 (+0dB) .. 2 (+6dB) etc ..)</para>
    ///<para>D_PAN : double * : trim pan of track (-1..1)</para>
    ///<para>D_WIDTH : double * : width of track (-1..1)</para>
    ///<para>D_DUALPANL : double * : dualpan position 1 (-1..1), only if I_PANMODE==6</para>
    ///<para>D_DUALPANR : double * : dualpan position 2 (-1..1), only if I_PANMODE==6</para>
    ///<para>I_PANMODE : int * : pan mode (0 = classic 3.x, 3=new balance, 5=stereo pan, 6 = dual pan)</para>
    ///<para>D_PANLAW : double * : pan law of track. &lt;0 for project default, 1.0 for +0dB, etc</para>
    ///<para>P_ENV : read only, returns TrackEnvelope *, setNewValue=&lt;VOLENV, &lt;PANENV, etc</para>
    ///<para>B_SHOWINMIXER : bool * : show track panel in mixer -- do not use on master</para>
    ///<para>B_SHOWINTCP : bool * : show track panel in tcp -- do not use on master</para>
    ///<para>B_MAINSEND : bool * : track sends audio to parent</para>
    ///<para>B_FREEMODE : bool * : track free-mode enabled (requires UpdateTimeline() after changing etc)</para>
    ///<para>C_BEATATTACHMODE : char * : char * to one char of beat attached mode, -1=def, 0=time, 1=allbeats, 2=beatsposonly</para>
    ///<para>F_MCP_FXSEND_SCALE : float * : scale of fx+send area in MCP (0.0=smallest allowed, 1=max allowed)</para>
    ///<para>F_MCP_SENDRGN_SCALE : float * : scale of send area as proportion of the fx+send total area (0=min allow, 1=max)</para>
    ///</summary>
    public static GetSetMediaTrackInfoDelegate GetSetMediaTrackInfo;
    
    
    ///<summary>
    ///<para>Get or set track string attributes.</para>
    ///<para>P_NAME : char * : track name (on master returns NULL)</para>
    ///<para></para>
    ///</summary>
    public static GetSetMediaTrackInfo_StringDelegate GetSetMediaTrackInfo_String;
    
    
    ///<summary>
    ///<para>get or set the state of a {track,item,envelope} as an xml/rpp chunk</para>
    ///<para>str=&#34;&#34; to get the chunk string returned (must call FreeHeapPtr when done)</para>
    ///<para>supply str to set the state (returns zero)</para>
    ///</summary>
    public static GetSetObjectStateDelegate GetSetObjectState;
    
    
    ///<summary>
    ///<para>get or set the state of a {track,item,envelope} as an xml/rpp chunk</para>
    ///<para>str=&#34;&#34; to get the chunk string returned (must call FreeHeapPtr when done)</para>
    ///<para>supply str to set the state (returns zero)</para>
    ///<para>set isundo if the state will be used for undo purposes (which may allow REAPER to get the state more efficiently</para>
    ///</summary>
    public static GetSetObjectState2Delegate GetSetObjectState2;
    
    
    ///<summary>
    ///<para>-1 == query,0=clear,1=set,&gt;1=toggle . returns new value</para>
    ///</summary>
    public static GetSetRepeatDelegate GetSetRepeat;
    
    
    ///<summary>
    ///<para>-1 == query,0=clear,1=set,&gt;1=toggle . returns new value</para>
    ///</summary>
    public static GetSetRepeatExDelegate GetSetRepeatEx;
    
    
    ///<summary>
    ///<para>Get or set the filename for storage of various track MIDI characteristics. 0=MIDI colormap image file, 1=MIDI bank/program select file, 2=MIDI text string file. If fn != NULL, a new track MIDI storage file will be set; otherwise the existing track MIDI storage file will be returned.</para>
    ///</summary>
    public static GetSetTrackMIDISupportFileDelegate GetSetTrackMIDISupportFile;
    
    
    ///<summary>
    ///<para>category is &lt;0 for receives, 0=sends, &gt;0 for hardware outputs</para>
    ///<para>sendidx is 0..n (NULL on any required parameter to stop)</para>
    ///<para>parameter names:</para>
    ///<para>P_DESTTRACK : read only, returns MediaTrack *, destination track, only applies for sends/recvs</para>
    ///<para>P_SRCTRACK : read only, returns MediaTrack *, source track, only applies for sends/recvs</para>
    ///<para>P_ENV : read only, returns TrackEnvelope *, setNewValue=&lt;VOLENV, &lt;PANENV, etc</para>
    ///<para>B_MUTE : returns bool *, read/write</para>
    ///<para>B_PHASE : returns bool *, read/write, true to flip phase</para>
    ///<para>B_MONO : returns bool *, read/write</para>
    ///<para>D_VOL : returns double *, read/write, 1.0 = +0dB etc</para>
    ///<para>D_PAN : returns double *, read/write, -1..+1</para>
    ///<para>D_PANLAW : returns double *,read/write, 1.0=+0.0db, 0.5=-6dB, -1.0 = projdef etc</para>
    ///<para>I_SENDMODE : returns int *, read/write, 0=post-fader, 1=pre-fx, 2=post-fx(depr), 3=post-fx</para>
    ///<para>I_SRCCHAN : returns int *, read/write, index,&amp;1024=mono, -1 for none</para>
    ///<para>I_DSTCHAN : returns int *, read/write, index, &amp;1024=mono, otherwise stereo pair, hwout:&amp;512=rearoute</para>
    ///<para>I_MIDIFLAGS : returns int *, read/write, low 5 bits=source channel 0=all, 1-16, next 5 bits=dest channel, 0=orig, 1-16=chan</para>
    ///</summary>
    public static GetSetTrackSendInfoDelegate GetSetTrackSendInfo;
    
    
    ///<summary>
    ///<para>get or set the state of a track as an xml/rpp chunk.</para>
    ///<para>supply str=&#34;&#34; to get the state returned in str.</para>
    ///<para>the returned state string is limited to maxlen characters.</para>
    ///<para>supply str as an xml/rpp string to set the object state.</para>
    ///<para>returns true on success.</para>
    ///</summary>
    public static GetSetTrackStateDelegate GetSetTrackState;
    
    
    ///<summary>
    ///<para>get or set the state of a track as an xml/rpp chunk.</para>
    ///<para>set isundo if the state will be used for undo purposes (which may allow REAPER to get the state more efficiently)</para>
    ///<para>supply str=&#34;&#34; to get the state returned in str.</para>
    ///<para>the returned state string is limited to maxlen characters.</para>
    ///<para>supply str as an xml/rpp string to set the object state.</para>
    ///<para>returns true on success.</para>
    ///</summary>
    public static GetSetTrackState2Delegate GetSetTrackState2;
    
    
    ///<summary>
    ///</summary>
    public static GetSubProjectFromSourceDelegate GetSubProjectFromSource;
    
    
    ///<summary>
    ///<para>get a take from an item by take count (zero-based)</para>
    ///</summary>
    public static GetTakeDelegate GetTake;
    
    
    ///<summary>
    ///</summary>
    public static GetTakeEnvelopeByNameDelegate GetTakeEnvelopeByName;
    
    
    ///<summary>
    ///<para>returns NULL if the take is not valid</para>
    ///</summary>
    public static GetTakeNameDelegate GetTakeName;
    
    
    ///<summary>
    ///<para>Returns number of stretch markers in take</para>
    ///</summary>
    public static GetTakeNumStretchMarkersDelegate GetTakeNumStretchMarkers;
    
    
    ///<summary>
    ///<para>Gets information on a stretch marker, idx is 0..n. Returns false if stretch marker not valid. posOut will be set to position in item, srcposOutOptional will be set to source media position. Returns index. if input index is -1, next marker is found using position (or source position if position is -1). If position/source position are used to find marker position, their values are not updated.</para>
    ///</summary>
    public static GetTakeStretchMarkerDelegate GetTakeStretchMarker;
    
    
    ///<summary>
    ///<para>Get information about a specific FX parameter knob (see CountTCPFXParms).</para>
    ///</summary>
    public static GetTCPFXParmDelegate GetTCPFXParm;
    
    
    ///<summary>
    ///<para>finds the playrate and target length to insert this item stretched to a round power-of-2 number of bars, between 1/8 and 256</para>
    ///</summary>
    public static GetTempoMatchPlayRateDelegate GetTempoMatchPlayRate;
    
    
    ///<summary>
    ///<para>Get information about a tempo/time signature marker. See CountTempoTimeSigMarkers, SetTempoTimeSigMarker, AddTempoTimeSigMarker.</para>
    ///</summary>
    public static GetTempoTimeSigMarkerDelegate GetTempoTimeSigMarker;
    
    
    ///<summary>
    ///</summary>
    public static GetToggleCommandStateDelegate GetToggleCommandState;
    
    
    ///<summary>
    ///<para>For the main action context, the MIDI editor, or the media explorer, returns the toggle state of the action. 0=off, 1=on, -1=NA because the action does not have on/off states.  For the MIDI editor, the action state for the most recently focused window will be returned.</para>
    ///</summary>
    public static GetToggleCommandState2Delegate GetToggleCommandState2;
    
    
    ///<summary>
    ///</summary>
    public static GetToggleCommandStateThroughHooksDelegate GetToggleCommandStateThroughHooks;
    
    
    ///<summary>
    ///<para>gets a tooltip window,in case you want to ask it for font information. Can return NULL.</para>
    ///</summary>
    public static GetTooltipWindowDelegate GetTooltipWindow;
    
    
    ///<summary>
    ///<para>get a track from a project by track count (zero-based) (proj=0 for active project)</para>
    ///</summary>
    public static GetTrackDelegate GetTrack;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackAutomationModeDelegate GetTrackAutomationMode;
    
    
    ///<summary>
    ///<para>Returns the track color, as 0x01RRGGBB. Black is returned as 0x01000000, no color setting is returned as 0.</para>
    ///</summary>
    public static GetTrackColorDelegate GetTrackColor;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackEnvelopeDelegate GetTrackEnvelope;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackEnvelopeByNameDelegate GetTrackEnvelopeByName;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackGUIDDelegate GetTrackGUID;
    
    
    ///<summary>
    ///<para>gets track info (returns name).</para>
    ///<para>track index, -1=master, 0..n, or cast a MediaTrack* to int</para>
    ///<para>if flags is non-NULL, will be set to:</para>
    ///<para>&amp;1=folder</para>
    ///<para>&amp;2=selected</para>
    ///<para>&amp;4=has fx enabled</para>
    ///<para>&amp;8=muted</para>
    ///<para>&amp;16=soloed</para>
    ///<para>&amp;32=SIP&#39;d (with &amp;16)</para>
    ///<para>&amp;64=rec armed</para>
    ///</summary>
    public static GetTrackInfoDelegate GetTrackInfo;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackMediaItemDelegate GetTrackMediaItem;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackMIDINoteNameDelegate GetTrackMIDINoteName;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackMIDINoteNameExDelegate GetTrackMIDINoteNameEx;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackMIDINoteRangeDelegate GetTrackMIDINoteRange;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackNumMediaItemsDelegate GetTrackNumMediaItems;
    
    
    ///<summary>
    ///<para>returns number of sends/receives/hardware outputs - category is &lt;0 for receives, 0=sends, &gt;0 for hardware outputs</para>
    ///</summary>
    public static GetTrackNumSendsDelegate GetTrackNumSends;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackReceiveNameDelegate GetTrackReceiveName;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackReceiveUIMuteDelegate GetTrackReceiveUIMute;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackReceiveUIVolPanDelegate GetTrackReceiveUIVolPan;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackSendNameDelegate GetTrackSendName;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackSendUIMuteDelegate GetTrackSendUIMute;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackSendUIVolPanDelegate GetTrackSendUIVolPan;
    
    
    ///<summary>
    ///<para>Gets track state, returns track name.</para>
    ///<para>flags will be set to:</para>
    ///<para>&amp;1=folder</para>
    ///<para>&amp;2=selected</para>
    ///<para>&amp;4=has fx enabled</para>
    ///<para>&amp;8=muted</para>
    ///<para>&amp;16=soloed</para>
    ///<para>&amp;32=SIP&#39;d (with &amp;16)</para>
    ///<para>&amp;64=rec armed</para>
    ///</summary>
    public static GetTrackStateDelegate GetTrackState;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackUIMuteDelegate GetTrackUIMute;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackUIPanDelegate GetTrackUIPan;
    
    
    ///<summary>
    ///</summary>
    public static GetTrackUIVolPanDelegate GetTrackUIVolPan;
    
    
    ///<summary>
    ///<para>returns true if the user selected a valid file, false if the user canceled the dialog</para>
    ///</summary>
    public static GetUserFileNameForReadDelegate GetUserFileNameForRead;
    
    
    ///<summary>
    ///<para>Get values from the user.</para>
    ///<para>If a caption begins with *, for example &#34;*password&#34;, the edit field will not display the input text.</para>
    ///<para>Values are returned as a comma-separated string. Returns false if the user canceled the dialog.</para>
    ///</summary>
    public static GetUserInputsDelegate GetUserInputs;
    
    
    ///<summary>
    ///<para>Go to marker. If use_timeline_order==true, marker_index 1 refers to the first marker on the timeline.  If use_timeline_order==false, marker_index 1 refers to the first marker with the user-editable index of 1.</para>
    ///</summary>
    public static GoToMarkerDelegate GoToMarker;
    
    
    ///<summary>
    ///<para>Seek to region after current region finishes playing (smooth seek). If use_timeline_order==true, region_index 1 refers to the first region on the timeline.  If use_timeline_order==false, region_index 1 refers to the first region with the user-editable index of 1.</para>
    ///</summary>
    public static GoToRegionDelegate GoToRegion;
    
    
    ///<summary>
    ///<para>Runs the system color chooser dialog.  Returns 0 if the user cancels the dialog.</para>
    ///</summary>
    public static GR_SelectColorDelegate GR_SelectColor;
    
    
    ///<summary>
    ///<para>this is just like win32 GetSysColor() but can have overrides.</para>
    ///</summary>
    public static GSC_mainwndDelegate GSC_mainwnd;
    
    
    ///<summary>
    ///<para>dest should be at least 64 chars long to be safe</para>
    ///</summary>
    public static guidToStringDelegate guidToString;
    
    
    ///<summary>
    ///<para>Returns true if there exists an extended state value for a specific section and key. See SetExtState, GetExtState, DeleteExtState.</para>
    ///</summary>
    public static HasExtStateDelegate HasExtState;
    
    
    ///<summary>
    ///<para>returns name of track plugin that is supplying MIDI programs,or NULL if there is none</para>
    ///</summary>
    public static HasTrackMIDIProgramsDelegate HasTrackMIDIPrograms;
    
    
    ///<summary>
    ///<para>returns name of track plugin that is supplying MIDI programs,or NULL if there is none</para>
    ///</summary>
    public static HasTrackMIDIProgramsExDelegate HasTrackMIDIProgramsEx;
    
    
    ///<summary>
    ///</summary>
    public static Help_SetDelegate Help_Set;
    
    
    ///<summary>
    ///</summary>
    public static HiresPeaksFromSourceDelegate HiresPeaksFromSource;
    
    
    ///<summary>
    ///</summary>
    public static image_resolve_fnDelegate image_resolve_fn;
    
    
    ///<summary>
    ///<para>mode: 0=add to current track, 1=add new track, 3=add to selected items as takes, &amp;4=stretch/loop to fit time sel, &amp;8=try to match tempo 1x, &amp;16=try to match tempo 0.5x, &amp;32=try to match tempo 2x</para>
    ///</summary>
    public static InsertMediaDelegate InsertMedia;
    
    
    ///<summary>
    ///</summary>
    public static InsertMediaSectionDelegate InsertMediaSection;
    
    
    ///<summary>
    ///<para>inserts a track at idx,of course this will be clamped to 0..GetNumTracks(). wantDefaults=TRUE for default envelopes/FX,otherwise no enabled fx/env</para>
    ///</summary>
    public static InsertTrackAtIndexDelegate InsertTrackAtIndex;
    
    
    ///<summary>
    ///<para>are we in a realtime audio thread (between OnAudioBuffer calls,not in some worker/anticipative FX thread)? threadsafe</para>
    ///</summary>
    public static IsInRealTimeAudioDelegate IsInRealTimeAudio;
    
    
    ///<summary>
    ///<para>get whether a take will be played (active take, unmuted, etc)</para>
    ///</summary>
    public static IsItemTakeActiveForPlaybackDelegate IsItemTakeActiveForPlayback;
    
    
    ///<summary>
    ///</summary>
    public static IsMediaExtensionDelegate IsMediaExtension;
    
    
    ///<summary>
    ///</summary>
    public static IsMediaItemSelectedDelegate IsMediaItemSelected;
    
    
    ///<summary>
    ///</summary>
    public static IsTrackSelectedDelegate IsTrackSelected;
    
    
    ///<summary>
    ///<para>If mixer==true, returs true if the track is visible in the mixer.  If mixer==false, returns true if the track is visible in the track control panel.</para>
    ///</summary>
    public static IsTrackVisibleDelegate IsTrackVisible;
    
    
    ///<summary>
    ///</summary>
    public static kbd_enumerateActionsDelegate kbd_enumerateActions;
    
    
    ///<summary>
    ///</summary>
    public static kbd_formatKeyNameDelegate kbd_formatKeyName;
    
    
    ///<summary>
    ///</summary>
    public static kbd_getCommandNameDelegate kbd_getCommandName;
    
    
    ///<summary>
    ///</summary>
    public static kbd_getTextFromCmdDelegate kbd_getTextFromCmd;
    
    
    ///<summary>
    ///<para>val/valhw are used for midi stuff.</para>
    ///<para>val=[0..127] and valhw=-1 (midi CC),</para>
    ///<para>valhw &gt;=0 (midi pitch (valhw | val&lt;&lt;7)),</para>
    ///<para>relmode absolute (0) or 1/2/3 for relative adjust modes</para>
    ///</summary>
    public static KBD_OnMainActionExDelegate KBD_OnMainActionEx;
    
    
    ///<summary>
    ///<para>can be called from anywhere (threadsafe)</para>
    ///</summary>
    public static kbd_OnMidiEventDelegate kbd_OnMidiEvent;
    
    
    ///<summary>
    ///<para>can be called from anywhere (threadsafe)</para>
    ///</summary>
    public static kbd_OnMidiListDelegate kbd_OnMidiList;
    
    
    ///<summary>
    ///</summary>
    public static kbd_ProcessActionsMenuDelegate kbd_ProcessActionsMenu;
    
    
    ///<summary>
    ///</summary>
    public static kbd_processMidiEventActionExDelegate kbd_processMidiEventActionEx;
    
    
    ///<summary>
    ///</summary>
    public static kbd_reprocessMenuDelegate kbd_reprocessMenu;
    
    
    ///<summary>
    ///<para>actioncommandID may get modified</para>
    ///</summary>
    public static kbd_RunCommandThroughHooksDelegate kbd_RunCommandThroughHooks;
    
    
    ///<summary>
    ///<para>time formatting mode overrides: -1=proj default.</para>
    ///<para>0=time</para>
    ///<para>1=measures.beats + time</para>
    ///<para>2=measures.beats</para>
    ///<para>3=seconds</para>
    ///<para>4=samples</para>
    ///<para>5=h:m:s:f</para>
    ///</summary>
    public static kbd_translateAcceleratorDelegate kbd_translateAccelerator;
    
    
    ///<summary>
    ///</summary>
    public static kbd_translateMouseDelegate kbd_translateMouse;
    
    
    ///<summary>
    ///<para>Move the loop selection left or right. Returns true if snap is enabled.</para>
    ///</summary>
    public static Loop_OnArrowDelegate Loop_OnArrow;
    
    
    ///<summary>
    ///</summary>
    public static Main_OnCommandDelegate Main_OnCommand;
    
    
    ///<summary>
    ///</summary>
    public static Main_OnCommandExDelegate Main_OnCommandEx;
    
    
    ///<summary>
    ///<para>opens a project. will prompt the user to save, etc.</para>
    ///<para>if you pass a .RTrackTemplate file then it adds that to the project instead.</para>
    ///</summary>
    public static Main_openProjectDelegate Main_openProject;
    
    
    ///<summary>
    ///</summary>
    public static Main_UpdateLoopInfoDelegate Main_UpdateLoopInfo;
    
    
    ///<summary>
    ///<para>marks project as dirty (needing save)</para>
    ///</summary>
    public static MarkProjectDirtyDelegate MarkProjectDirty;
    
    
    ///<summary>
    ///<para>If track is supplied, item is ignored</para>
    ///</summary>
    public static MarkTrackItemsDirtyDelegate MarkTrackItemsDirty;
    
    
    ///<summary>
    ///</summary>
    public static Master_GetPlayRateDelegate Master_GetPlayRate;
    
    
    ///<summary>
    ///</summary>
    public static Master_GetPlayRateAtTimeDelegate Master_GetPlayRateAtTime;
    
    
    ///<summary>
    ///</summary>
    public static Master_GetTempoDelegate Master_GetTempo;
    
    
    ///<summary>
    ///<para>Convert play rate to/from a value between 0 and 1, representing the position on the project playrate slider.</para>
    ///</summary>
    public static Master_NormalizePlayRateDelegate Master_NormalizePlayRate;
    
    
    ///<summary>
    ///<para>Convert the tempo to/from a value between 0 and 1, representing bpm in the range of 40-296 bpm.</para>
    ///</summary>
    public static Master_NormalizeTempoDelegate Master_NormalizeTempo;
    
    
    ///<summary>
    ///<para>type 0=OK,1=OKCANCEL,2=ABORTRETRYIGNORE,3=YESNOCANCEL,4=YESNO,5=RETRYCANCEL : ret 1=OK,2=CANCEL,3=ABORT,4=RETRY,5=IGNORE,6=YES,7=NO</para>
    ///</summary>
    public static MBDelegate MB;
    
    
    ///<summary>
    ///<para>Returns 1 if the track holds the item, 2 if the track is a folder containing the track that holds the item, etc.</para>
    ///</summary>
    public static MediaItemDescendsFromTrackDelegate MediaItemDescendsFromTrack;
    
    
    ///<summary>
    ///<para>Count the number of notes, CC events, and text/sysex events in a given MIDI item.</para>
    ///</summary>
    public static MIDI_CountEvtsDelegate MIDI_CountEvts;
    
    
    ///<summary>
    ///<para>Delete a MIDI CC event.</para>
    ///</summary>
    public static MIDI_DeleteCCDelegate MIDI_DeleteCC;
    
    
    ///<summary>
    ///<para>Delete a MIDI event.</para>
    ///</summary>
    public static MIDI_DeleteEvtDelegate MIDI_DeleteEvt;
    
    
    ///<summary>
    ///<para>Delete a MIDI note.</para>
    ///</summary>
    public static MIDI_DeleteNoteDelegate MIDI_DeleteNote;
    
    
    ///<summary>
    ///<para>Delete a MIDI text or sysex event.</para>
    ///</summary>
    public static MIDI_DeleteTextSysexEvtDelegate MIDI_DeleteTextSysexEvt;
    
    
    ///<summary>
    ///<para>Returns the index of the next selected MIDI CC event after ccidx (-1 if there are no more selected events).</para>
    ///</summary>
    public static MIDI_EnumSelCCDelegate MIDI_EnumSelCC;
    
    
    ///<summary>
    ///<para>Returns the index of the next selected MIDI event after evtidx (-1 if there are no more selected events).</para>
    ///</summary>
    public static MIDI_EnumSelEvtsDelegate MIDI_EnumSelEvts;
    
    
    ///<summary>
    ///<para>Returns the index of the next selected MIDI note after noteidx (-1 if there are no more selected events).</para>
    ///</summary>
    public static MIDI_EnumSelNotesDelegate MIDI_EnumSelNotes;
    
    
    ///<summary>
    ///<para>Returns the index of the next selected MIDI text/sysex event after textsyxidx (-1 if there are no more selected events).</para>
    ///</summary>
    public static MIDI_EnumSelTextSysexEvtsDelegate MIDI_EnumSelTextSysexEvts;
    
    
    ///<summary>
    ///<para>Create a MIDI_eventlist object. The returned object must be deleted with MIDI_eventlist_destroy().</para>
    ///</summary>
    public static MIDI_eventlist_CreateDelegate MIDI_eventlist_Create;
    
    
    ///<summary>
    ///<para>Destroy a MIDI_eventlist object that was created using MIDI_eventlist_Create().</para>
    ///</summary>
    public static MIDI_eventlist_DestroyDelegate MIDI_eventlist_Destroy;
    
    
    ///<summary>
    ///<para>Get MIDI CC event properties.</para>
    ///</summary>
    public static MIDI_GetCCDelegate MIDI_GetCC;
    
    
    ///<summary>
    ///<para>Get MIDI event properties.</para>
    ///</summary>
    public static MIDI_GetEvtDelegate MIDI_GetEvt;
    
    
    ///<summary>
    ///<para>Get MIDI note properties.</para>
    ///</summary>
    public static MIDI_GetNoteDelegate MIDI_GetNote;
    
    
    ///<summary>
    ///<para>Returns the MIDI tick (ppq) position corresponding to the end of the measure.</para>
    ///</summary>
    public static MIDI_GetPPQPos_EndOfMeasureDelegate MIDI_GetPPQPos_EndOfMeasure;
    
    
    ///<summary>
    ///<para>Returns the MIDI tick (ppq) position corresponding to the start of the measure.</para>
    ///</summary>
    public static MIDI_GetPPQPos_StartOfMeasureDelegate MIDI_GetPPQPos_StartOfMeasure;
    
    
    ///<summary>
    ///<para>Returns the MIDI tick (ppq) position corresponding to a specific project time in seconds.</para>
    ///</summary>
    public static MIDI_GetPPQPosFromProjTimeDelegate MIDI_GetPPQPosFromProjTime;
    
    
    ///<summary>
    ///<para>Returns the project time in seconds corresponding to a specific MIDI tick (ppq) position.</para>
    ///</summary>
    public static MIDI_GetProjTimeFromPPQPosDelegate MIDI_GetProjTimeFromPPQPos;
    
    
    ///<summary>
    ///<para>Get MIDI meta-event properties. Allowable types are -1:sysex (msg should not include bounding F0..F7), 1-7:MIDI text event types.</para>
    ///</summary>
    public static MIDI_GetTextSysexEvtDelegate MIDI_GetTextSysexEvt;
    
    
    ///<summary>
    ///<para>Insert a new MIDI CC event.</para>
    ///</summary>
    public static MIDI_InsertCCDelegate MIDI_InsertCC;
    
    
    ///<summary>
    ///<para>Insert a new MIDI event.</para>
    ///</summary>
    public static MIDI_InsertEvtDelegate MIDI_InsertEvt;
    
    
    ///<summary>
    ///<para>Insert a new MIDI note.</para>
    ///</summary>
    public static MIDI_InsertNoteDelegate MIDI_InsertNote;
    
    
    ///<summary>
    ///<para>Insert a new MIDI text or sysex event. Allowable types are -1:sysex (msg should include bounding F0..F7), 1-7:MIDI text event types.</para>
    ///</summary>
    public static MIDI_InsertTextSysexEvtDelegate MIDI_InsertTextSysexEvt;
    
    
    ///<summary>
    ///<para>Reset all MIDI devices</para>
    ///</summary>
    public static midi_reinitDelegate midi_reinit;
    
    
    ///<summary>
    ///<para>Set MIDI CC event properties. Properties passed as NULL will not be set.</para>
    ///</summary>
    public static MIDI_SetCCDelegate MIDI_SetCC;
    
    
    ///<summary>
    ///<para>Set MIDI event properties. Properties passed as NULL will not be set.</para>
    ///</summary>
    public static MIDI_SetEvtDelegate MIDI_SetEvt;
    
    
    ///<summary>
    ///<para>Set MIDI note properties. Properties passed as NULL will not be set.</para>
    ///</summary>
    public static MIDI_SetNoteDelegate MIDI_SetNote;
    
    
    ///<summary>
    ///<para>Set MIDI text or sysex event properties. Properties passed as NULL will not be set. Allowable types are -1:sysex (msg should not include bounding F0..F7), 1-7:MIDI text event types.</para>
    ///</summary>
    public static MIDI_SetTextSysexEvtDelegate MIDI_SetTextSysexEvt;
    
    
    ///<summary>
    ///<para>get a pointer to the focused MIDI editor window</para>
    ///<para>see MIDIEditor_GetMode, MIDIEditor_OnCommand</para>
    ///</summary>
    public static MIDIEditor_GetActiveDelegate MIDIEditor_GetActive;
    
    
    ///<summary>
    ///<para>get the mode of a MIDI editor (0=piano roll, 1=event list, -1=invalid editor)</para>
    ///<para>see MIDIEditor_GetActive, MIDIEditor_OnCommand</para>
    ///</summary>
    public static MIDIEditor_GetModeDelegate MIDIEditor_GetMode;
    
    
    ///<summary>
    ///<para>Get settings from a MIDI editor. setting_desc can be:</para>
    ///<para>snap_enabled: returns 0 or 1</para>
    ///<para>active_note_row: returns 0-127</para>
    ///<para>last_clicked_cc_lane: returns 0-127=CC, 0x100|(0-31)=14-bit CC, 0x200=velocity, 0x201=pitch, 0x202=program, 0x203=channel pressure, 0x204=bank/program select, 0x205=text, 0x206=sysex</para>
    ///<para>default_note_vel: returns 0-127</para>
    ///<para>default_note_chan: returns 0-15</para>
    ///<para>default_note_len: returns default length in MIDI ticks</para>
    ///<para>scale_enabled: returns 0-1</para>
    ///<para>scale_root: returns 0-12 (0=C)</para>
    ///<para>if setting_desc is unsupported, the function returns -1.</para>
    ///<para>See MIDIEditor_GetActive, MIDIEditor_GetSetting_str</para>
    ///</summary>
    public static MIDIEditor_GetSetting_intDelegate MIDIEditor_GetSetting_int;
    
    
    ///<summary>
    ///<para>Get settings from a MIDI editor. setting_desc can be:</para>
    ///<para>last_clicked_cc_lane: returns text description (&#34;velocity&#34;, &#34;pitch&#34;, etc)</para>
    ///<para>scale: returns the scale record, for example &#34;102034050607&#34; for a major scale</para>
    ///<para>if setting_desc is unsupported, the function returns false.</para>
    ///<para>See MIDIEditor_GetActive, MIDIEditor_GetSetting_int</para>
    ///</summary>
    public static MIDIEditor_GetSetting_strDelegate MIDIEditor_GetSetting_str;
    
    
    ///<summary>
    ///<para>get the take that is currently being edited in this MIDI editor</para>
    ///</summary>
    public static MIDIEditor_GetTakeDelegate MIDIEditor_GetTake;
    
    
    ///<summary>
    ///<para>Send an action command to the last focused MIDI editor. Returns false if there is no MIDI editor open, or if the view mode (piano roll or event list) does not match the input.</para>
    ///<para>see MIDIEditor_OnCommand</para>
    ///</summary>
    public static MIDIEditor_LastFocused_OnCommandDelegate MIDIEditor_LastFocused_OnCommand;
    
    
    ///<summary>
    ///<para>Send an action command to a MIDI editor. Returns false if the supplied MIDI editor pointer is not valid (not an open MIDI editor).</para>
    ///<para>see MIDIEditor_GetActive, MIDIEditor_LastFocused_OnCommand</para>
    ///</summary>
    public static MIDIEditor_OnCommandDelegate MIDIEditor_OnCommand;
    
    
    ///<summary>
    ///</summary>
    public static mkpanstrDelegate mkpanstr;
    
    
    ///<summary>
    ///</summary>
    public static mkvolpanstrDelegate mkvolpanstr;
    
    
    ///<summary>
    ///</summary>
    public static mkvolstrDelegate mkvolstr;
    
    
    ///<summary>
    ///</summary>
    public static MoveEditCursorDelegate MoveEditCursor;
    
    
    ///<summary>
    ///<para>returns TRUE if move succeeded</para>
    ///</summary>
    public static MoveMediaItemToTrackDelegate MoveMediaItemToTrack;
    
    
    ///<summary>
    ///</summary>
    public static MuteAllTracksDelegate MuteAllTracks;
    
    
    ///<summary>
    ///</summary>
    public static my_getViewportDelegate my_getViewport;
    
    
    ///<summary>
    ///<para>get the command ID number for named command that was registered by an extension (such as &#34;_SWS_ABOUT&#34;, etc)</para>
    ///</summary>
    public static NamedCommandLookupDelegate NamedCommandLookup;
    
    
    ///<summary>
    ///<para>direct way to simulate pause button hit</para>
    ///</summary>
    public static OnPauseButtonDelegate OnPauseButton;
    
    
    ///<summary>
    ///<para>direct way to simulate pause button hit</para>
    ///</summary>
    public static OnPauseButtonExDelegate OnPauseButtonEx;
    
    
    ///<summary>
    ///<para>direct way to simulate play button hit</para>
    ///</summary>
    public static OnPlayButtonDelegate OnPlayButton;
    
    
    ///<summary>
    ///<para>direct way to simulate play button hit</para>
    ///</summary>
    public static OnPlayButtonExDelegate OnPlayButtonEx;
    
    
    ///<summary>
    ///<para>direct way to simulate stop button hit</para>
    ///</summary>
    public static OnStopButtonDelegate OnStopButton;
    
    
    ///<summary>
    ///<para>direct way to simulate stop button hit</para>
    ///</summary>
    public static OnStopButtonExDelegate OnStopButtonEx;
    
    
    ///<summary>
    ///<para>Send an OSC message directly to REAPER. The value argument may be NULL. The message will be matched against the default OSC patterns. Only supported if control surface support was enabled when installing REAPER.</para>
    ///</summary>
    public static OscLocalMessageToHostDelegate OscLocalMessageToHost;
    
    
    ///<summary>
    ///<para>time formatting mode overrides: -1=proj default.</para>
    ///<para>0=time</para>
    ///<para>1=measures.beats + time</para>
    ///<para>2=measures.beats</para>
    ///<para>3=seconds</para>
    ///<para>4=samples</para>
    ///<para>5=h:m:s:f</para>
    ///</summary>
    public static parse_timestrDelegate parse_timestr;
    
    
    ///<summary>
    ///<para>time formatting mode overrides: -1=proj default.</para>
    ///<para>0=time</para>
    ///<para>1=measures.beats + time</para>
    ///<para>2=measures.beats</para>
    ///<para>3=seconds</para>
    ///<para>4=samples</para>
    ///<para>5=h:m:s:f</para>
    ///</summary>
    public static parse_timestr_lenDelegate parse_timestr_len;
    
    
    ///<summary>
    ///<para>parses time string,modeoverride see above</para>
    ///</summary>
    public static parse_timestr_posDelegate parse_timestr_pos;
    
    
    ///<summary>
    ///</summary>
    public static parsepanstrDelegate parsepanstr;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Sink_CreateDelegate PCM_Sink_Create;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Sink_CreateExDelegate PCM_Sink_CreateEx;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Sink_CreateMIDIFileDelegate PCM_Sink_CreateMIDIFile;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Sink_CreateMIDIFileExDelegate PCM_Sink_CreateMIDIFileEx;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Sink_EnumDelegate PCM_Sink_Enum;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Sink_GetExtensionDelegate PCM_Sink_GetExtension;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Sink_ShowConfigDelegate PCM_Sink_ShowConfig;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Source_CreateFromFileDelegate PCM_Source_CreateFromFile;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Source_CreateFromFileExDelegate PCM_Source_CreateFromFileEx;
    
    
    ///<summary>
    ///<para>Creates a PCM_source from a ISimpleMediaDecoder</para>
    ///<para>(if fn is non-null, it will open the file in dec)</para>
    ///</summary>
    public static PCM_Source_CreateFromSimpleDelegate PCM_Source_CreateFromSimple;
    
    
    ///<summary>
    ///</summary>
    public static PCM_Source_CreateFromTypeDelegate PCM_Source_CreateFromType;
    
    
    ///<summary>
    ///<para>If a section/reverse block, retrieves offset/len/reverse. return true if success</para>
    ///</summary>
    public static PCM_Source_GetSectionInfoDelegate PCM_Source_GetSectionInfo;
    
    
    ///<summary>
    ///</summary>
    public static PeakBuild_CreateDelegate PeakBuild_Create;
    
    
    ///<summary>
    ///</summary>
    public static PeakGet_CreateDelegate PeakGet_Create;
    
    
    ///<summary>
    ///<para>return nonzero on success</para>
    ///</summary>
    public static PlayPreviewDelegate PlayPreview;
    
    
    ///<summary>
    ///<para>return nonzero on success</para>
    ///</summary>
    public static PlayPreviewExDelegate PlayPreviewEx;
    
    
    ///<summary>
    ///<para>return nonzero on success,in these,m_out_chan is a track index (0-n)</para>
    ///</summary>
    public static PlayTrackPreviewDelegate PlayTrackPreview;
    
    
    ///<summary>
    ///<para>return nonzero on success,in these,m_out_chan is a track index (0-n)</para>
    ///</summary>
    public static PlayTrackPreview2Delegate PlayTrackPreview2;
    
    
    ///<summary>
    ///<para>return nonzero on success,in these,m_out_chan is a track index (0-n)</para>
    ///</summary>
    public static PlayTrackPreview2ExDelegate PlayTrackPreview2Ex;
    
    
    ///<summary>
    ///</summary>
    public static plugin_getapiDelegate plugin_getapi;
    
    
    ///<summary>
    ///</summary>
    public static plugin_getFilterListDelegate plugin_getFilterList;
    
    
    ///<summary>
    ///</summary>
    public static plugin_getImportableProjectFilterListDelegate plugin_getImportableProjectFilterList;
    
    
    ///<summary>
    ///<para>like rec-&gt;Register</para>
    ///<para></para>
    ///<para>if you have a function called myfunction(..) that you want to expose to other extensions or plug-ins,</para>
    ///<para>use register(&#34;API_myfunction&#34;,funcaddress), and &#34;-API_myfunction&#34; to remove.</para>
    ///<para>Other extensions then use GetFunc(&#34;myfunction&#34;) to get the function pointer.</para>
    ///<para>REAPER will also export the function address to ReaScript, so your extension could supply</para>
    ///<para>a Python module that provides a wrapper called RPR_myfunction(..).</para>
    ///<para>register(&#34;APIdef_myfunction&#34;,defstring) will include your function declaration and help</para>
    ///<para>in the auto-generated REAPER API header and ReaScript documentation.</para>
    ///<para>defstring is four null-separated fields: return type, argument types, argument names, and help.</para>
    ///<para>Example: double myfunction(char* str, int flag) would have</para>
    ///<para>defstring=&#34;double\0char*,int\0str,flag\0help text for myfunction&#34;</para>
    ///<para></para>
    ///<para>another thing you can register is &#34;hookcommand&#34;, which you pass a callback:</para>
    ///<para>NON_API: bool runCommand(int command, int flag);</para>
    ///<para>register(&#34;hookcommand&#34;,runCommand);</para>
    ///<para>which returns TRUE to eat (process) the command.</para>
    ///<para>flag is usually 0 but can sometimes have useful info depending on the message.</para>
    ///<para>note: it&#39;s OK to call Main_OnCommand() within your runCommand, however you MUST check for recursion if doing so!</para>
    ///<para>in fact, any use of this hook should benefit from a simple reentrancy test...</para>
    ///<para></para>
    ///<para>you can also register &#34;hookcommand2&#34;, which you pass a callback:</para>
    ///<para>NON_API: bool onAction(KbdSectionInfo *sec, int command, int val, int valhw, int relmode, HWND hwnd);</para>
    ///<para>register(&#34;hookcommand2&#34;,onAction);</para>
    ///<para>which returns TRUE to eat (process) the command.</para>
    ///<para>val/valhw are used for actions learned with MIDI/OSC.</para>
    ///<para>val = [0..127] and valhw = -1 for MIDI CC,</para>
    ///<para>valhw &gt;=0 for MIDI pitch or OSC with value = (valhw|val&lt;&lt;7)/16383.0,</para>
    ///<para>relmode absolute(0) or 1/2/3 for relative adjust modes</para>
    ///<para></para>
    ///<para>you can also register command IDs for main window actions,</para>
    ///<para>register with &#34;command_id&#34;, parameter is a unique string with only A-Z, 0-9,</para>
    ///<para>returns command ID for main actions (or 0 if not supported/out of actions)</para>
    ///<para></para>
    ///<para>register(&#34;command_id_lookup&#34;, unique_string) will look up the integer ID of the named action</para>
    ///<para>without registering the string if it doesn&#39;t already exist.</para>
    ///</summary>
    public static plugin_registerDelegate plugin_register;
    
    
    ///<summary>
    ///</summary>
    public static PluginWantsAlwaysRunFxDelegate PluginWantsAlwaysRunFx;
    
    
    ///<summary>
    ///<para>adds prevent_count to the UI refresh prevention state; always add then remove the same amount, or major disfunction will occur</para>
    ///</summary>
    public static PreventUIRefreshDelegate PreventUIRefresh;
    
    
    ///<summary>
    ///</summary>
    public static projectconfig_var_addrDelegate projectconfig_var_addr;
    
    
    ///<summary>
    ///<para>returns offset to pass to projectconfig_var_addr() to get project-config var of name. szout gets size of object.</para>
    ///</summary>
    public static projectconfig_var_getoffsDelegate projectconfig_var_getoffs;
    
    
    ///<summary>
    ///<para>version must be REAPER_PITCHSHIFT_API_VER</para>
    ///</summary>
    public static ReaperGetPitchShiftAPIDelegate ReaperGetPitchShiftAPI;
    
    
    ///<summary>
    ///<para>Causes REAPER to display the error message after the current ReaScript finishes.</para>
    ///</summary>
    public static ReaScriptErrorDelegate ReaScriptError;
    
    
    ///<summary>
    ///</summary>
    public static RecursiveCreateDirectoryDelegate RecursiveCreateDirectory;
    
    
    ///<summary>
    ///<para>refresh the toolbar button state of a toggle action</para>
    ///</summary>
    public static RefreshToolbarDelegate RefreshToolbar;
    
    
    ///<summary>
    ///</summary>
    public static relative_fnDelegate relative_fn;
    
    
    ///<summary>
    ///</summary>
    public static RenderFileSectionDelegate RenderFileSection;
    
    
    ///<summary>
    ///</summary>
    public static Resample_EnumModesDelegate Resample_EnumModes;
    
    
    ///<summary>
    ///</summary>
    public static Resampler_CreateDelegate Resampler_Create;
    
    
    ///<summary>
    ///</summary>
    public static resolve_fnDelegate resolve_fn;
    
    
    ///<summary>
    ///</summary>
    public static resolve_fn2Delegate resolve_fn2;
    
    
    ///<summary>
    ///</summary>
    public static screenset_registerDelegate screenset_register;
    
    
    ///<summary>
    ///</summary>
    public static screenset_registerNewDelegate screenset_registerNew;
    
    
    ///<summary>
    ///</summary>
    public static screenset_unregisterDelegate screenset_unregister;
    
    
    ///<summary>
    ///</summary>
    public static screenset_unregisterByParamDelegate screenset_unregisterByParam;
    
    
    ///<summary>
    ///</summary>
    public static SectionFromUniqueIDDelegate SectionFromUniqueID;
    
    
    ///<summary>
    ///</summary>
    public static SelectAllMediaItemsDelegate SelectAllMediaItems;
    
    
    ///<summary>
    ///</summary>
    public static SelectProjectInstanceDelegate SelectProjectInstance;
    
    
    ///<summary>
    ///<para>Send an OSC message to REAPER. See CreateLocalOscHandler, DestroyLocalOscHandler.</para>
    ///</summary>
    public static SendLocalOscMessageDelegate SendLocalOscMessage;
    
    
    ///<summary>
    ///<para>set this take active in this media item</para>
    ///</summary>
    public static SetActiveTakeDelegate SetActiveTake;
    
    
    ///<summary>
    ///<para>sets all or selected tracks to mode.</para>
    ///</summary>
    public static SetAutomationModeDelegate SetAutomationMode;
    
    
    ///<summary>
    ///<para>set current BPM in project, set wantUndo=true to add undo point</para>
    ///</summary>
    public static SetCurrentBPMDelegate SetCurrentBPM;
    
    
    ///<summary>
    ///</summary>
    public static SetEditCurPosDelegate SetEditCurPos;
    
    
    ///<summary>
    ///</summary>
    public static SetEditCurPos2Delegate SetEditCurPos2;
    
    
    ///<summary>
    ///<para>Set the extended state value for a specific section and key. persist=true means the value should be stored and reloaded the next time REAPER is opened. See GetExtState, DeleteExtState, HasExtState.</para>
    ///</summary>
    public static SetExtStateDelegate SetExtState;
    
    
    ///<summary>
    ///<para>set &amp;1 to show the master track in the TCP, &amp;2 to show in the mixer. Returns the previous visibility state. See GetMasterTrackVisibility.</para>
    ///</summary>
    public static SetMasterTrackVisibilityDelegate SetMasterTrackVisibility;
    
    
    ///<summary>
    ///<para>Set media item numerical-value attributes.</para>
    ///<para>B_MUTE : bool * to muted state</para>
    ///<para>B_LOOPSRC : bool * to loop source</para>
    ///<para>B_ALLTAKESPLAY : bool * to all takes play</para>
    ///<para>B_UISEL : bool * to ui selected</para>
    ///<para>C_BEATATTACHMODE : char * to one char of beat attached mode, -1=def, 0=time, 1=allbeats, 2=beatsosonly</para>
    ///<para>C_LOCK : char * to one char of lock flags (&amp;1 is locked, currently)</para>
    ///<para>D_VOL : double * of item volume (volume bar)</para>
    ///<para>D_POSITION : double * of item position (seconds)</para>
    ///<para>D_LENGTH : double * of item length (seconds)</para>
    ///<para>D_SNAPOFFSET : double * of item snap offset (seconds)</para>
    ///<para>D_FADEINLEN : double * of item fade in length (manual, seconds)</para>
    ///<para>D_FADEOUTLEN : double * of item fade out length (manual, seconds)</para>
    ///<para>D_FADEINLEN_AUTO : double * of item autofade in length (seconds, -1 for no autofade set)</para>
    ///<para>D_FADEOUTLEN_AUTO : double * of item autofade out length (seconds, -1 for no autofade set)</para>
    ///<para>C_FADEINSHAPE : int * to fadein shape, 0=linear, ...</para>
    ///<para>C_FADEOUTSHAPE : int * to fadeout shape</para>
    ///<para>I_GROUPID : int * to group ID (0 = no group)</para>
    ///<para>I_LASTY : int * to last y position in track (readonly)</para>
    ///<para>I_LASTH : int * to last height in track (readonly)</para>
    ///<para>I_CUSTOMCOLOR : int * : custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>I_CURTAKE : int * to active take</para>
    ///<para>IP_ITEMNUMBER : int, item number within the track (read-only, returns the item number directly)</para>
    ///<para>F_FREEMODE_Y : float * to free mode y position (0..1)</para>
    ///<para>F_FREEMODE_H : float * to free mode height (0..1)</para>
    ///</summary>
    public static SetMediaItemInfo_ValueDelegate SetMediaItemInfo_Value;
    
    
    ///<summary>
    ///<para>Redraws the screen only if refreshUI == true.</para>
    ///<para>See UpdateArrange().</para>
    ///</summary>
    public static SetMediaItemLengthDelegate SetMediaItemLength;
    
    
    ///<summary>
    ///<para>Redraws the screen only if refreshUI == true.</para>
    ///<para>See UpdateArrange().</para>
    ///</summary>
    public static SetMediaItemPositionDelegate SetMediaItemPosition;
    
    
    ///<summary>
    ///</summary>
    public static SetMediaItemSelectedDelegate SetMediaItemSelected;
    
    
    ///<summary>
    ///<para>Set media item take numerical-value attributes.</para>
    ///<para>D_STARTOFFS : double *, start offset in take of item</para>
    ///<para>D_VOL : double *, take volume</para>
    ///<para>D_PAN : double *, take pan</para>
    ///<para>D_PANLAW : double *, take pan law (-1.0=default, 0.5=-6dB, 1.0=+0dB, etc)</para>
    ///<para>D_PLAYRATE : double *, take playrate (1.0=normal, 2.0=doublespeed, etc)</para>
    ///<para>D_PITCH : double *, take pitch adjust (in semitones, 0.0=normal, +12 = one octave up, etc)</para>
    ///<para>B_PPITCH, bool *, preserve pitch when changing rate</para>
    ///<para>I_CHANMODE, int *, channel mode (0=normal, 1=revstereo, 2=downmix, 3=l, 4=r)</para>
    ///<para>I_PITCHMODE, int *, pitch shifter mode, -1=proj default, otherwise high word=shifter low word = parameter</para>
    ///<para>I_CUSTOMCOLOR : int *, custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>IP_TAKENUMBER : int, take number within the item (read-only, returns the take number directly)</para>
    ///</summary>
    public static SetMediaItemTakeInfo_ValueDelegate SetMediaItemTakeInfo_Value;
    
    
    ///<summary>
    ///<para>Set track numerical-value attributes.</para>
    ///<para>B_MUTE : bool * : mute flag</para>
    ///<para>B_PHASE : bool * : invert track phase</para>
    ///<para>IP_TRACKNUMBER : int : track number (returns zero if not found, -1 for master track) (read-only, returns the int directly)</para>
    ///<para>I_SOLO : int * : 0=not soloed, 1=solo, 2=soloed in place</para>
    ///<para>I_FXEN : int * : 0=fx bypassed, nonzero = fx active</para>
    ///<para>I_RECARM : int * : 0=not record armed, 1=record armed</para>
    ///<para>I_RECINPUT : int * : record input. &lt;0 = no input, 0..n = mono hardware input, 512+n = rearoute input, 1024 set for stereo input pair. 4096 set for MIDI input, if set, then low 5 bits represent channel (0=all, 1-16=only chan), then next 5 bits represent physical input (31=all, 30=VKB)</para>
    ///<para>I_RECMODE : int * : record mode (0=input, 1=stereo out, 2=none, 3=stereo out w/latcomp, 4=midi output, 5=mono out, 6=mono out w/ lat comp, 7=midi overdub, 8=midi replace</para>
    ///<para>I_RECMON : int * : record monitor (0=off, 1=normal, 2=not when playing (tapestyle))</para>
    ///<para>I_RECMONITEMS : int * : monitor items while recording (0=off, 1=on)</para>
    ///<para>I_AUTOMODE : int * : track automation mode (0=trim/off, 1=read, 2=touch, 3=write, 4=latch</para>
    ///<para>I_NCHAN : int * : number of track channels, must be 2-64, even</para>
    ///<para>I_SELECTED : int * : track selected? 0 or 1</para>
    ///<para>I_WNDH : int * : current TCP window height (Read-only)</para>
    ///<para>I_FOLDERDEPTH : int * : folder depth change (0=normal, 1=track is a folder parent, -1=track is the last in the innermost folder, -2=track is the last in the innermost and next-innermost folders, etc</para>
    ///<para>I_FOLDERCOMPACT : int * : folder compacting (only valid on folders), 0=normal, 1=small, 2=tiny children</para>
    ///<para>I_MIDIHWOUT : int * : track midi hardware output index (&lt;0 for disabled, low 5 bits are which channels (0=all, 1-16), next 5 bits are output device index (0-31))</para>
    ///<para>I_PERFFLAGS : int * : track perf flags (&amp;1=no media buffering, &amp;2=no anticipative FX)</para>
    ///<para>I_CUSTOMCOLOR : int * : custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)</para>
    ///<para>I_HEIGHTOVERRIDE : int * : custom height override for TCP window. 0 for none, otherwise size in pixels</para>
    ///<para>D_VOL : double * : trim volume of track (0 (-inf)..1 (+0dB) .. 2 (+6dB) etc ..)</para>
    ///<para>D_PAN : double * : trim pan of track (-1..1)</para>
    ///<para>D_WIDTH : double * : width of track (-1..1)</para>
    ///<para>D_DUALPANL : double * : dualpan position 1 (-1..1), only if I_PANMODE==6</para>
    ///<para>D_DUALPANR : double * : dualpan position 2 (-1..1), only if I_PANMODE==6</para>
    ///<para>I_PANMODE : int * : pan mode (0 = classic 3.x, 3=new balance, 5=stereo pan, 6 = dual pan)</para>
    ///<para>D_PANLAW : double * : pan law of track. &lt;0 for project default, 1.0 for +0dB, etc</para>
    ///<para>P_ENV : read only, returns TrackEnvelope *, setNewValue=&lt;VOLENV, &lt;PANENV, etc</para>
    ///<para>B_SHOWINMIXER : bool * : show track panel in mixer -- do not use on master</para>
    ///<para>B_SHOWINTCP : bool * : show track panel in tcp -- do not use on master</para>
    ///<para>B_MAINSEND : bool * : track sends audio to parent</para>
    ///<para>B_FREEMODE : bool * : track free-mode enabled (requires UpdateTimeline() after changing etc)</para>
    ///<para>C_BEATATTACHMODE : char * : char * to one char of beat attached mode, -1=def, 0=time, 1=allbeats, 2=beatsposonly</para>
    ///<para>F_MCP_FXSEND_SCALE : float * : scale of fx+send area in MCP (0.0=smallest allowed, 1=max allowed)</para>
    ///<para>F_MCP_SENDRGN_SCALE : float * : scale of send area as proportion of the fx+send total area (0=min allow, 1=max)</para>
    ///</summary>
    public static SetMediaTrackInfo_ValueDelegate SetMediaTrackInfo_Value;
    
    
    ///<summary>
    ///<para>Scroll the mixer so that leftmosttrack is the leftmost visible track. Returns the leftmost track after scrolling, which may be different from the passed-in track if there are not enough tracks to its right.</para>
    ///</summary>
    public static SetMixerScrollDelegate SetMixerScroll;
    
    
    ///<summary>
    ///<para>Set the mouse modifier assignment for a specific modifier key assignment, in a specific context.</para>
    ///<para>Context is a string like &#34;MM_CTX_ITEM&#34;. Find these strings by modifying an assignment in</para>
    ///<para>Preferences/Editing/Mouse Modifiers, then looking in reaper-mouse.ini.</para>
    ///<para>Modifier flag is a number from 0 to 15: add 1 for shift, 2 for control, 4 for alt, 8 for win.</para>
    ///<para>(OSX: add 1 for shift, 2 for command, 4 for opt, 8 for control.)</para>
    ///<para>For left-click and double-click contexts, the action can be any built-in command ID number</para>
    ///<para>or any custom action ID string. Find built-in command IDs in the REAPER actions window</para>
    ///<para>(enable &#34;show action IDs&#34; in the context menu), and find custom action ID strings in reaper-kb.ini.</para>
    ///<para>For built-in mouse modifier behaviors, find action IDs (which will be low numbers)</para>
    ///<para>by modifying an assignment in Preferences/Editing/Mouse Modifiers, then looking in reaper-mouse.ini.</para>
    ///<para>Assigning an action of -1 will reset that mouse modifier behavior to factory default.</para>
    ///<para>See GetMouseModifier.</para>
    ///<para></para>
    ///</summary>
    public static SetMouseModifierDelegate SetMouseModifier;
    
    
    ///<summary>
    ///<para>Set exactly one track selected, deselect all others</para>
    ///</summary>
    public static SetOnlyTrackSelectedDelegate SetOnlyTrackSelected;
    
    
    ///<summary>
    ///</summary>
    public static SetProjectMarkerDelegate SetProjectMarker;
    
    
    ///<summary>
    ///</summary>
    public static SetProjectMarker2Delegate SetProjectMarker2;
    
    
    ///<summary>
    ///<para>color should be 0 to not change, or RGB(x,y,z)|0x1000000 to set custom</para>
    ///</summary>
    public static SetProjectMarker3Delegate SetProjectMarker3;
    
    
    ///<summary>
    ///<para>Differs from SetProjectMarker3 in that markrgnidx is 0 for the first marker/region, 1 for the next, etc (see EnumProjectMarkers3), rather than representing the displayed marker/region ID number (see SetProjectMarker3). Function will fail if attempting to set a duplicate ID number for a region (duplicate ID numbers for markers are OK).</para>
    ///</summary>
    public static SetProjectMarkerByIndexDelegate SetProjectMarkerByIndex;
    
    
    ///<summary>
    ///<para>Add (addorremove &gt; 0) or remove (addorremove &lt; 0) a track from this region when using the region render matrix.</para>
    ///</summary>
    public static SetRegionRenderMatrixDelegate SetRegionRenderMatrix;
    
    
    ///<summary>
    ///<para>Used by pcmsink objects to set an error to display while creating the pcmsink object.</para>
    ///</summary>
    public static SetRenderLastErrorDelegate SetRenderLastError;
    
    
    ///<summary>
    ///<para>Adds or updates a stretch marker. If idx&lt;0, stretch marker will be added. If idx&gt;=0, stretch marker will be updated. When adding, if srcposInOptional is omitted, source position will be auto-calculated. When updating a stretch marker, if srcposInOptional is omitted, srcpos will not be modified. Position/srcposition values will be constrained to nearby stretch markers. Returns index of stretch marker, or -1 if did not insert (or marker already existed at time).</para>
    ///</summary>
    public static SetTakeStretchMarkerDelegate SetTakeStretchMarker;
    
    
    ///<summary>
    ///<para>Set parameters of a tempo/time signature marker. Provide either timepos (with measurepos=-1, beatpos=-1), or measurepos and beatpos (with timepos=-1). If timesig_num and timesig_denom are zero, the previous time signature will be used. ptidx=-1 will insert a new tempo/time signature marker. See CountTempoTimeSigMarkers, GetTempoTimeSigMarker, AddTempoTimeSigMarker.</para>
    ///</summary>
    public static SetTempoTimeSigMarkerDelegate SetTempoTimeSigMarker;
    
    
    ///<summary>
    ///</summary>
    public static SetTrackAutomationModeDelegate SetTrackAutomationMode;
    
    
    ///<summary>
    ///<para>Set the track color, as 0x00RRGGBB.</para>
    ///</summary>
    public static SetTrackColorDelegate SetTrackColor;
    
    
    ///<summary>
    ///</summary>
    public static SetTrackMIDINoteNameDelegate SetTrackMIDINoteName;
    
    
    ///<summary>
    ///</summary>
    public static SetTrackMIDINoteNameExDelegate SetTrackMIDINoteNameEx;
    
    
    ///<summary>
    ///</summary>
    public static SetTrackSelectedDelegate SetTrackSelected;
    
    
    ///<summary>
    ///<para>send_idx&lt;0 for receives, isend=1 for end of edit, -1 for an instant edit (such as reset), 0 for normal tweak.</para>
    ///</summary>
    public static SetTrackSendUIPanDelegate SetTrackSendUIPan;
    
    
    ///<summary>
    ///<para>send_idx&lt;0 for receives, isend=1 for end of edit, -1 for an instant edit (such as reset), 0 for normal tweak.</para>
    ///</summary>
    public static SetTrackSendUIVolDelegate SetTrackSendUIVol;
    
    
    ///<summary>
    ///</summary>
    public static ShowActionListDelegate ShowActionList;
    
    
    ///<summary>
    ///<para>Show a message to the user (also useful for debugging). Send &#34;\n&#34; for newline, &#34;&#34; to clear the console window.</para>
    ///</summary>
    public static ShowConsoleMsgDelegate ShowConsoleMsg;
    
    
    ///<summary>
    ///<para>type 0=OK,1=OKCANCEL,2=ABORTRETRYIGNORE,3=YESNOCANCEL,4=YESNO,5=RETRYCANCEL : ret 1=OK,2=CANCEL,3=ABORT,4=RETRY,5=IGNORE,6=YES,7=NO</para>
    ///</summary>
    public static ShowMessageBoxDelegate ShowMessageBox;
    
    
    ///<summary>
    ///</summary>
    public static SLIDER2DBDelegate SLIDER2DB;
    
    
    ///<summary>
    ///</summary>
    public static SnapToGridDelegate SnapToGrid;
    
    
    ///<summary>
    ///<para>solo=2 for SIP</para>
    ///</summary>
    public static SoloAllTracksDelegate SoloAllTracks;
    
    
    ///<summary>
    ///<para>the original item becomes the left-hand split, the function returns the right-hand split (or NULL if the split failed)</para>
    ///</summary>
    public static SplitMediaItemDelegate SplitMediaItem;
    
    
    ///<summary>
    ///<para>return nonzero on success</para>
    ///</summary>
    public static StopPreviewDelegate StopPreview;
    
    
    ///<summary>
    ///<para>return nonzero on success</para>
    ///</summary>
    public static StopTrackPreviewDelegate StopTrackPreview;
    
    
    ///<summary>
    ///<para>return nonzero on success</para>
    ///</summary>
    public static StopTrackPreview2Delegate StopTrackPreview2;
    
    
    ///<summary>
    ///</summary>
    public static stringToGuidDelegate stringToGuid;
    
    
    ///<summary>
    ///<para>Stuffs a 3 byte MIDI message into either the Virtual MIDI Keyboard queue, or the MIDI-as-control input queue. mode=0 for VKB, 1 for control (actions map etc), 2 for VKB-on-current-channel.</para>
    ///</summary>
    public static StuffMIDIMessageDelegate StuffMIDIMessage;
    
    
    ///<summary>
    ///<para>convert a beat position (or optionally a beats+measures if measures is non-NULL) to time.</para>
    ///</summary>
    public static TimeMap2_beatsToTimeDelegate TimeMap2_beatsToTime;
    
    
    ///<summary>
    ///<para>get the effective BPM at the time (seconds) position (i.e. 2x in /8 signatures)</para>
    ///</summary>
    public static TimeMap2_GetDividedBpmAtTimeDelegate TimeMap2_GetDividedBpmAtTime;
    
    
    ///<summary>
    ///<para>when does the next time map (tempo or time sig) change occur</para>
    ///</summary>
    public static TimeMap2_GetNextChangeTimeDelegate TimeMap2_GetNextChangeTime;
    
    
    ///<summary>
    ///<para>converts project QN position to time.</para>
    ///</summary>
    public static TimeMap2_QNToTimeDelegate TimeMap2_QNToTime;
    
    
    ///<summary>
    ///<para>convert a time into beats.</para>
    ///<para>if measures is non-NULL, measures will be set to the measure count, return value will be beats since measure.</para>
    ///<para>if cml is non-NULL, will be set to current measure length in beats (i.e. time signature numerator)</para>
    ///<para>if fullbeats is non-NULL, and measures is non-NULL, fullbeats will get the full beat count (same value returned if measures is NULL).</para>
    ///<para>if cdenom is non-NULL, will be set to the current time signature denominator.</para>
    ///</summary>
    public static TimeMap2_timeToBeatsDelegate TimeMap2_timeToBeats;
    
    
    ///<summary>
    ///<para>converts project time position to QN position.</para>
    ///</summary>
    public static TimeMap2_timeToQNDelegate TimeMap2_timeToQN;
    
    
    ///<summary>
    ///<para>get the effective BPM at the time (seconds) position (i.e. 2x in /8 signatures)</para>
    ///</summary>
    public static TimeMap_GetDividedBpmAtTimeDelegate TimeMap_GetDividedBpmAtTime;
    
    
    ///<summary>
    ///<para>get the effective time signature and tempo</para>
    ///</summary>
    public static TimeMap_GetTimeSigAtTimeDelegate TimeMap_GetTimeSigAtTime;
    
    
    ///<summary>
    ///<para>converts project QN position to time.</para>
    ///</summary>
    public static TimeMap_QNToTimeDelegate TimeMap_QNToTime;
    
    
    ///<summary>
    ///<para>Converts project quarter note count (QN) to time. QN is counted from the start of the project, regardless of any partial measures. See TimeMap2_QNToTime</para>
    ///</summary>
    public static TimeMap_QNToTime_absDelegate TimeMap_QNToTime_abs;
    
    
    ///<summary>
    ///<para>converts project QN position to time.</para>
    ///</summary>
    public static TimeMap_timeToQNDelegate TimeMap_timeToQN;
    
    
    ///<summary>
    ///<para>Converts project time position to quarter note count (QN). QN is counted from the start of the project, regardless of any partial measures. See TimeMap2_timeToQN</para>
    ///</summary>
    public static TimeMap_timeToQN_absDelegate TimeMap_timeToQN_abs;
    
    
    ///<summary>
    ///<para>send_idx&lt;0 for receives</para>
    ///</summary>
    public static ToggleTrackSendUIMuteDelegate ToggleTrackSendUIMute;
    
    
    ///<summary>
    ///</summary>
    public static Track_GetPeakHoldDBDelegate Track_GetPeakHoldDB;
    
    
    ///<summary>
    ///</summary>
    public static Track_GetPeakInfoDelegate Track_GetPeakInfo;
    
    
    ///<summary>
    ///<para>displays tooltip at location, or removes if empty string</para>
    ///</summary>
    public static TrackCtl_SetToolTipDelegate TrackCtl_SetToolTip;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_EndParamEditDelegate TrackFX_EndParamEdit;
    
    
    ///<summary>
    ///<para>Note: only works with FX that support Cockos VST extensions.</para>
    ///</summary>
    public static TrackFX_FormatParamValueDelegate TrackFX_FormatParamValue;
    
    
    ///<summary>
    ///<para>Note: only works with FX that support Cockos VST extensions.</para>
    ///</summary>
    public static TrackFX_FormatParamValueNormalizedDelegate TrackFX_FormatParamValueNormalized;
    
    
    ///<summary>
    ///<para>Get the index of the first track FX insert that matches fxname. If the FX is not in the chain and instantiate is true, it will be inserted. See TrackFX_GetInstrument, TrackFX_GetEQ.</para>
    ///</summary>
    public static TrackFX_GetByNameDelegate TrackFX_GetByName;
    
    
    ///<summary>
    ///<para>returns index of effect visible in chain, or -1 for chain hidden, or -2 for chain visible but no effect selected</para>
    ///</summary>
    public static TrackFX_GetChainVisibleDelegate TrackFX_GetChainVisible;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetCountDelegate TrackFX_GetCount;
    
    
    ///<summary>
    ///<para>See TrackFX_SetEnabled</para>
    ///</summary>
    public static TrackFX_GetEnabledDelegate TrackFX_GetEnabled;
    
    
    ///<summary>
    ///<para>Get the index of ReaEQ in the track FX chain. If ReaEQ is not in the chain and instantiate is true, it will be inserted. See TrackFX_GetInstrument, TrackFX_GetByName.</para>
    ///</summary>
    public static TrackFX_GetEQDelegate TrackFX_GetEQ;
    
    
    ///<summary>
    ///<para>Returns true if the EQ band is enabled.</para>
    ///<para>Returns false if the band is disabled, or if track/fxidx is not ReaEQ.</para>
    ///<para>Bandtype: 0=lhipass, 1=loshelf, 2=band, 3=notch, 4=hishelf, 5=lopass.</para>
    ///<para>Bandidx: 0=first band matching bandtype, 1=2nd band matching bandtype, etc.</para>
    ///<para>See TrackFX_GetEQ, TrackFX_GetEQParam, TrackFX_SetEQParam, TrackFX_SetEQBandEnabled.</para>
    ///</summary>
    public static TrackFX_GetEQBandEnabledDelegate TrackFX_GetEQBandEnabled;
    
    
    ///<summary>
    ///<para>Returns false if track/fxidx is not ReaEQ.</para>
    ///<para>Bandtype: -1=master gain, 0=lhipass, 1=loshelf, 2=band, 3=notch, 4=hishelf, 5=lopass.</para>
    ///<para>Bandidx (ignored for master gain): 0=first band matching bandtype, 1=2nd band matching bandtype, etc.</para>
    ///<para>Paramtype (ignored for master gain): 0=freq, 1=gain, 2=Q.</para>
    ///<para>See TrackFX_GetEQ, TrackFX_SetEQParam, TrackFX_GetEQBandEnabled, TrackFX_SetEQBandEnabled.</para>
    ///</summary>
    public static TrackFX_GetEQParamDelegate TrackFX_GetEQParam;
    
    
    ///<summary>
    ///<para>returns HWND of floating window for effect index, if any</para>
    ///</summary>
    public static TrackFX_GetFloatingWindowDelegate TrackFX_GetFloatingWindow;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetFormattedParamValueDelegate TrackFX_GetFormattedParamValue;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetFXGUIDDelegate TrackFX_GetFXGUID;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetFXNameDelegate TrackFX_GetFXName;
    
    
    ///<summary>
    ///<para>Get the index of the first track FX insert that is a virtual instrument, or -1 if none. See TrackFX_GetEQ, TrackFX_GetByName.</para>
    ///</summary>
    public static TrackFX_GetInstrumentDelegate TrackFX_GetInstrument;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetNumParamsDelegate TrackFX_GetNumParams;
    
    
    ///<summary>
    ///<para>Returns true if this FX UI is open in the FX chain window or a floating window. See TrackFX_SetOpen</para>
    ///</summary>
    public static TrackFX_GetOpenDelegate TrackFX_GetOpen;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetParamDelegate TrackFX_GetParam;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetParameterStepSizesDelegate TrackFX_GetParameterStepSizes;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetParamExDelegate TrackFX_GetParamEx;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetParamNameDelegate TrackFX_GetParamName;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_GetParamNormalizedDelegate TrackFX_GetParamNormalized;
    
    
    ///<summary>
    ///<para>Get the name of the preset currently showing in the REAPER dropdown. Returns false if the current FX parameters do not exactly match the factory preset (in other words, if the user loaded the preset but moved the knobs afterward. See TrackFX_SetPreset</para>
    ///</summary>
    public static TrackFX_GetPresetDelegate TrackFX_GetPreset;
    
    
    ///<summary>
    ///<para>Returns current preset index, or -1 if error. numberOfPresetsOut will be set to total number of presets available. See TrackFX_SetPresetByIndex</para>
    ///</summary>
    public static TrackFX_GetPresetIndexDelegate TrackFX_GetPresetIndex;
    
    
    ///<summary>
    ///<para>presetmove==1 activates the next preset, presetmove==-1 activates the previous preset, etc.</para>
    ///</summary>
    public static TrackFX_NavigatePresetsDelegate TrackFX_NavigatePresets;
    
    
    ///<summary>
    ///<para>See TrackFX_GetEnabled</para>
    ///</summary>
    public static TrackFX_SetEnabledDelegate TrackFX_SetEnabled;
    
    
    ///<summary>
    ///<para>Enable or disable a ReaEQ band.</para>
    ///<para>Returns false if track/fxidx is not ReaEQ.</para>
    ///<para>Bandtype: 0=lhipass, 1=loshelf, 2=band, 3=notch, 4=hishelf, 5=lopass.</para>
    ///<para>Bandidx: 0=first band matching bandtype, 1=2nd band matching bandtype, etc.</para>
    ///<para>See TrackFX_GetEQ, TrackFX_GetEQParam, TrackFX_SetEQParam, TrackFX_GetEQBandEnabled.</para>
    ///</summary>
    public static TrackFX_SetEQBandEnabledDelegate TrackFX_SetEQBandEnabled;
    
    
    ///<summary>
    ///<para>Returns false if track/fxidx is not ReaEQ. Targets a band matching bandtype.</para>
    ///<para>Bandtype: -1=master gain, 0=lhipass, 1=loshelf, 2=band, 3=notch, 4=hishelf, 5=lopass.</para>
    ///<para>Bandidx (ignored for master gain): 0=target first band matching bandtype, 1=target 2nd band matching bandtype, etc.</para>
    ///<para>Paramtype (ignored for master gain): 0=freq, 1=gain, 2=Q.</para>
    ///<para>See TrackFX_GetEQ, TrackFX_GetEQParam, TrackFX_GetEQBandEnabled, TrackFX_SetEQBandEnabled.</para>
    ///</summary>
    public static TrackFX_SetEQParamDelegate TrackFX_SetEQParam;
    
    
    ///<summary>
    ///<para>Open this FX UI. See TrackFX_GetOpen</para>
    ///</summary>
    public static TrackFX_SetOpenDelegate TrackFX_SetOpen;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_SetParamDelegate TrackFX_SetParam;
    
    
    ///<summary>
    ///</summary>
    public static TrackFX_SetParamNormalizedDelegate TrackFX_SetParamNormalized;
    
    
    ///<summary>
    ///<para>See TrackFX_GetPreset</para>
    ///</summary>
    public static TrackFX_SetPresetDelegate TrackFX_SetPreset;
    
    
    ///<summary>
    ///<para>Sets preset idx for fx fx. Returns true on success.See TrackFX_GetPresetIndex</para>
    ///</summary>
    public static TrackFX_SetPresetByIndexDelegate TrackFX_SetPresetByIndex;
    
    
    ///<summary>
    ///<para>showflag=0 for hidechain, =1 for show chain(index valid), =2 for hide floating window(index valid), =3 for show floating window (index valid)</para>
    ///</summary>
    public static TrackFX_ShowDelegate TrackFX_Show;
    
    
    ///<summary>
    ///</summary>
    public static TrackList_AdjustWindowsDelegate TrackList_AdjustWindows;
    
    
    ///<summary>
    ///</summary>
    public static TrackList_UpdateAllExternalSurfacesDelegate TrackList_UpdateAllExternalSurfaces;
    
    
    ///<summary>
    ///<para>call to start a new block</para>
    ///</summary>
    public static Undo_BeginBlockDelegate Undo_BeginBlock;
    
    
    ///<summary>
    ///<para>call to start a new block</para>
    ///</summary>
    public static Undo_BeginBlock2Delegate Undo_BeginBlock2;
    
    
    ///<summary>
    ///<para>returns string of next action,if able,NULL if not</para>
    ///</summary>
    public static Undo_CanRedo2Delegate Undo_CanRedo2;
    
    
    ///<summary>
    ///<para>returns string of last action,if able,NULL if not</para>
    ///</summary>
    public static Undo_CanUndo2Delegate Undo_CanUndo2;
    
    
    ///<summary>
    ///<para>nonzero if success</para>
    ///</summary>
    public static Undo_DoRedo2Delegate Undo_DoRedo2;
    
    
    ///<summary>
    ///<para>nonzero if success</para>
    ///</summary>
    public static Undo_DoUndo2Delegate Undo_DoUndo2;
    
    
    ///<summary>
    ///<para>call to end the block,with extra flags if any,and a description</para>
    ///</summary>
    public static Undo_EndBlockDelegate Undo_EndBlock;
    
    
    ///<summary>
    ///<para>call to end the block,with extra flags if any,and a description</para>
    ///</summary>
    public static Undo_EndBlock2Delegate Undo_EndBlock2;
    
    
    ///<summary>
    ///<para>limited state change to items</para>
    ///</summary>
    public static Undo_OnStateChangeDelegate Undo_OnStateChange;
    
    
    ///<summary>
    ///<para>limited state change to items</para>
    ///</summary>
    public static Undo_OnStateChange2Delegate Undo_OnStateChange2;
    
    
    ///<summary>
    ///</summary>
    public static Undo_OnStateChange_ItemDelegate Undo_OnStateChange_Item;
    
    
    ///<summary>
    ///<para>trackparm=-1 by default,or if updating one fx chain,you can specify track index</para>
    ///</summary>
    public static Undo_OnStateChangeExDelegate Undo_OnStateChangeEx;
    
    
    ///<summary>
    ///<para>trackparm=-1 by default,or if updating one fx chain,you can specify track index</para>
    ///</summary>
    public static Undo_OnStateChangeEx2Delegate Undo_OnStateChangeEx2;
    
    
    ///<summary>
    ///<para>Redraw the arrange view</para>
    ///</summary>
    public static UpdateArrangeDelegate UpdateArrange;
    
    
    ///<summary>
    ///</summary>
    public static UpdateItemInProjectDelegate UpdateItemInProject;
    
    
    ///<summary>
    ///<para>Redraw the arrange view and ruler</para>
    ///</summary>
    public static UpdateTimelineDelegate UpdateTimeline;
    
    
    ///<summary>
    ///<para>returns true if the pointer is a valid object of the right type</para>
    ///</summary>
    public static ValidatePtrDelegate ValidatePtr;
    
    
    ///<summary>
    ///<para>Opens the prefs to a page, use pageByName if page is 0.</para>
    ///</summary>
    public static ViewPrefsDelegate ViewPrefs;
    
    
    ///<summary>
    ///</summary>
    public static WDL_VirtualWnd_ScaledBlitBGDelegate WDL_VirtualWnd_ScaledBlitBG;
    
    public static void Init(Dictionary<string, IntPtr> reaperFunctions)
    {      
        
        AddCustomizableMenu = GetDelegateForFunctionPointer<AddCustomizableMenuDelegate>(
            reaperFunctions["AddCustomizableMenu"], CallingConvention.Cdecl);
        
        AddExtensionsMainMenu = GetDelegateForFunctionPointer<AddExtensionsMainMenuDelegate>(
            reaperFunctions["AddExtensionsMainMenu"], CallingConvention.Cdecl);
        
        AddMediaItemToTrack = GetDelegateForFunctionPointer<AddMediaItemToTrackDelegate>(
            reaperFunctions["AddMediaItemToTrack"], CallingConvention.Cdecl);
        
        AddProjectMarker = GetDelegateForFunctionPointer<AddProjectMarkerDelegate>(
            reaperFunctions["AddProjectMarker"], CallingConvention.Cdecl);
        
        AddProjectMarker2 = GetDelegateForFunctionPointer<AddProjectMarker2Delegate>(
            reaperFunctions["AddProjectMarker2"], CallingConvention.Cdecl);
        
        AddTakeToMediaItem = GetDelegateForFunctionPointer<AddTakeToMediaItemDelegate>(
            reaperFunctions["AddTakeToMediaItem"], CallingConvention.Cdecl);
        
        AddTempoTimeSigMarker = GetDelegateForFunctionPointer<AddTempoTimeSigMarkerDelegate>(
            reaperFunctions["AddTempoTimeSigMarker"], CallingConvention.Cdecl);
        
        adjustZoom = GetDelegateForFunctionPointer<adjustZoomDelegate>(
            reaperFunctions["adjustZoom"], CallingConvention.Cdecl);
        
        AnyTrackSolo = GetDelegateForFunctionPointer<AnyTrackSoloDelegate>(
            reaperFunctions["AnyTrackSolo"], CallingConvention.Cdecl);
        
        APITest = GetDelegateForFunctionPointer<APITestDelegate>(
            reaperFunctions["APITest"], CallingConvention.Cdecl);
        
        ApplyNudge = GetDelegateForFunctionPointer<ApplyNudgeDelegate>(
            reaperFunctions["ApplyNudge"], CallingConvention.Cdecl);
        
        Audio_IsPreBuffer = GetDelegateForFunctionPointer<Audio_IsPreBufferDelegate>(
            reaperFunctions["Audio_IsPreBuffer"], CallingConvention.Cdecl);
        
        Audio_IsRunning = GetDelegateForFunctionPointer<Audio_IsRunningDelegate>(
            reaperFunctions["Audio_IsRunning"], CallingConvention.Cdecl);
        
        Audio_RegHardwareHook = GetDelegateForFunctionPointer<Audio_RegHardwareHookDelegate>(
            reaperFunctions["Audio_RegHardwareHook"], CallingConvention.Cdecl);
        
        AudioAccessorValidateState = GetDelegateForFunctionPointer<AudioAccessorValidateStateDelegate>(
            reaperFunctions["AudioAccessorValidateState"], CallingConvention.Cdecl);
        
        BypassFxAllTracks = GetDelegateForFunctionPointer<BypassFxAllTracksDelegate>(
            reaperFunctions["BypassFxAllTracks"], CallingConvention.Cdecl);
        
        CalculatePeaks = GetDelegateForFunctionPointer<CalculatePeaksDelegate>(
            reaperFunctions["CalculatePeaks"], CallingConvention.Cdecl);
        
        CalculatePeaksFloatSrcPtr = GetDelegateForFunctionPointer<CalculatePeaksFloatSrcPtrDelegate>(
            reaperFunctions["CalculatePeaksFloatSrcPtr"], CallingConvention.Cdecl);
        
        ClearAllRecArmed = GetDelegateForFunctionPointer<ClearAllRecArmedDelegate>(
            reaperFunctions["ClearAllRecArmed"], CallingConvention.Cdecl);
        
        ClearPeakCache = GetDelegateForFunctionPointer<ClearPeakCacheDelegate>(
            reaperFunctions["ClearPeakCache"], CallingConvention.Cdecl);
        
        CountActionShortcuts = GetDelegateForFunctionPointer<CountActionShortcutsDelegate>(
            reaperFunctions["CountActionShortcuts"], CallingConvention.Cdecl);
        
        CountMediaItems = GetDelegateForFunctionPointer<CountMediaItemsDelegate>(
            reaperFunctions["CountMediaItems"], CallingConvention.Cdecl);
        
        CountProjectMarkers = GetDelegateForFunctionPointer<CountProjectMarkersDelegate>(
            reaperFunctions["CountProjectMarkers"], CallingConvention.Cdecl);
        
        CountSelectedMediaItems = GetDelegateForFunctionPointer<CountSelectedMediaItemsDelegate>(
            reaperFunctions["CountSelectedMediaItems"], CallingConvention.Cdecl);
        
        CountSelectedTracks = GetDelegateForFunctionPointer<CountSelectedTracksDelegate>(
            reaperFunctions["CountSelectedTracks"], CallingConvention.Cdecl);
        
        CountTakes = GetDelegateForFunctionPointer<CountTakesDelegate>(
            reaperFunctions["CountTakes"], CallingConvention.Cdecl);
        
        CountTCPFXParms = GetDelegateForFunctionPointer<CountTCPFXParmsDelegate>(
            reaperFunctions["CountTCPFXParms"], CallingConvention.Cdecl);
        
        CountTempoTimeSigMarkers = GetDelegateForFunctionPointer<CountTempoTimeSigMarkersDelegate>(
            reaperFunctions["CountTempoTimeSigMarkers"], CallingConvention.Cdecl);
        
        CountTrackEnvelopes = GetDelegateForFunctionPointer<CountTrackEnvelopesDelegate>(
            reaperFunctions["CountTrackEnvelopes"], CallingConvention.Cdecl);
        
        CountTrackMediaItems = GetDelegateForFunctionPointer<CountTrackMediaItemsDelegate>(
            reaperFunctions["CountTrackMediaItems"], CallingConvention.Cdecl);
        
        CountTracks = GetDelegateForFunctionPointer<CountTracksDelegate>(
            reaperFunctions["CountTracks"], CallingConvention.Cdecl);
        
        CreateLocalOscHandler = GetDelegateForFunctionPointer<CreateLocalOscHandlerDelegate>(
            reaperFunctions["CreateLocalOscHandler"], CallingConvention.Cdecl);
        
        CreateMIDIInput = GetDelegateForFunctionPointer<CreateMIDIInputDelegate>(
            reaperFunctions["CreateMIDIInput"], CallingConvention.Cdecl);
        
        CreateMIDIOutput = GetDelegateForFunctionPointer<CreateMIDIOutputDelegate>(
            reaperFunctions["CreateMIDIOutput"], CallingConvention.Cdecl);
        
        CreateNewMIDIItemInProj = GetDelegateForFunctionPointer<CreateNewMIDIItemInProjDelegate>(
            reaperFunctions["CreateNewMIDIItemInProj"], CallingConvention.Cdecl);
        
        CreateTakeAudioAccessor = GetDelegateForFunctionPointer<CreateTakeAudioAccessorDelegate>(
            reaperFunctions["CreateTakeAudioAccessor"], CallingConvention.Cdecl);
        
        CreateTrackAudioAccessor = GetDelegateForFunctionPointer<CreateTrackAudioAccessorDelegate>(
            reaperFunctions["CreateTrackAudioAccessor"], CallingConvention.Cdecl);
        
        CSurf_FlushUndo = GetDelegateForFunctionPointer<CSurf_FlushUndoDelegate>(
            reaperFunctions["CSurf_FlushUndo"], CallingConvention.Cdecl);
        
        CSurf_GetTouchState = GetDelegateForFunctionPointer<CSurf_GetTouchStateDelegate>(
            reaperFunctions["CSurf_GetTouchState"], CallingConvention.Cdecl);
        
        CSurf_GoEnd = GetDelegateForFunctionPointer<CSurf_GoEndDelegate>(
            reaperFunctions["CSurf_GoEnd"], CallingConvention.Cdecl);
        
        CSurf_GoStart = GetDelegateForFunctionPointer<CSurf_GoStartDelegate>(
            reaperFunctions["CSurf_GoStart"], CallingConvention.Cdecl);
        
        CSurf_NumTracks = GetDelegateForFunctionPointer<CSurf_NumTracksDelegate>(
            reaperFunctions["CSurf_NumTracks"], CallingConvention.Cdecl);
        
        CSurf_OnArrow = GetDelegateForFunctionPointer<CSurf_OnArrowDelegate>(
            reaperFunctions["CSurf_OnArrow"], CallingConvention.Cdecl);
        
        CSurf_OnFwd = GetDelegateForFunctionPointer<CSurf_OnFwdDelegate>(
            reaperFunctions["CSurf_OnFwd"], CallingConvention.Cdecl);
        
        CSurf_OnFXChange = GetDelegateForFunctionPointer<CSurf_OnFXChangeDelegate>(
            reaperFunctions["CSurf_OnFXChange"], CallingConvention.Cdecl);
        
        CSurf_OnInputMonitorChange = GetDelegateForFunctionPointer<CSurf_OnInputMonitorChangeDelegate>(
            reaperFunctions["CSurf_OnInputMonitorChange"], CallingConvention.Cdecl);
        
        CSurf_OnInputMonitorChangeEx = GetDelegateForFunctionPointer<CSurf_OnInputMonitorChangeExDelegate>(
            reaperFunctions["CSurf_OnInputMonitorChangeEx"], CallingConvention.Cdecl);
        
        CSurf_OnMuteChange = GetDelegateForFunctionPointer<CSurf_OnMuteChangeDelegate>(
            reaperFunctions["CSurf_OnMuteChange"], CallingConvention.Cdecl);
        
        CSurf_OnMuteChangeEx = GetDelegateForFunctionPointer<CSurf_OnMuteChangeExDelegate>(
            reaperFunctions["CSurf_OnMuteChangeEx"], CallingConvention.Cdecl);
        
        CSurf_OnOscControlMessage = GetDelegateForFunctionPointer<CSurf_OnOscControlMessageDelegate>(
            reaperFunctions["CSurf_OnOscControlMessage"], CallingConvention.Cdecl);
        
        CSurf_OnPanChange = GetDelegateForFunctionPointer<CSurf_OnPanChangeDelegate>(
            reaperFunctions["CSurf_OnPanChange"], CallingConvention.Cdecl);
        
        CSurf_OnPanChangeEx = GetDelegateForFunctionPointer<CSurf_OnPanChangeExDelegate>(
            reaperFunctions["CSurf_OnPanChangeEx"], CallingConvention.Cdecl);
        
        CSurf_OnPause = GetDelegateForFunctionPointer<CSurf_OnPauseDelegate>(
            reaperFunctions["CSurf_OnPause"], CallingConvention.Cdecl);
        
        CSurf_OnPlay = GetDelegateForFunctionPointer<CSurf_OnPlayDelegate>(
            reaperFunctions["CSurf_OnPlay"], CallingConvention.Cdecl);
        
        CSurf_OnPlayRateChange = GetDelegateForFunctionPointer<CSurf_OnPlayRateChangeDelegate>(
            reaperFunctions["CSurf_OnPlayRateChange"], CallingConvention.Cdecl);
        
        CSurf_OnRecArmChange = GetDelegateForFunctionPointer<CSurf_OnRecArmChangeDelegate>(
            reaperFunctions["CSurf_OnRecArmChange"], CallingConvention.Cdecl);
        
        CSurf_OnRecArmChangeEx = GetDelegateForFunctionPointer<CSurf_OnRecArmChangeExDelegate>(
            reaperFunctions["CSurf_OnRecArmChangeEx"], CallingConvention.Cdecl);
        
        CSurf_OnRecord = GetDelegateForFunctionPointer<CSurf_OnRecordDelegate>(
            reaperFunctions["CSurf_OnRecord"], CallingConvention.Cdecl);
        
        CSurf_OnRecvPanChange = GetDelegateForFunctionPointer<CSurf_OnRecvPanChangeDelegate>(
            reaperFunctions["CSurf_OnRecvPanChange"], CallingConvention.Cdecl);
        
        CSurf_OnRecvVolumeChange = GetDelegateForFunctionPointer<CSurf_OnRecvVolumeChangeDelegate>(
            reaperFunctions["CSurf_OnRecvVolumeChange"], CallingConvention.Cdecl);
        
        CSurf_OnRew = GetDelegateForFunctionPointer<CSurf_OnRewDelegate>(
            reaperFunctions["CSurf_OnRew"], CallingConvention.Cdecl);
        
        CSurf_OnRewFwd = GetDelegateForFunctionPointer<CSurf_OnRewFwdDelegate>(
            reaperFunctions["CSurf_OnRewFwd"], CallingConvention.Cdecl);
        
        CSurf_OnScroll = GetDelegateForFunctionPointer<CSurf_OnScrollDelegate>(
            reaperFunctions["CSurf_OnScroll"], CallingConvention.Cdecl);
        
        CSurf_OnSelectedChange = GetDelegateForFunctionPointer<CSurf_OnSelectedChangeDelegate>(
            reaperFunctions["CSurf_OnSelectedChange"], CallingConvention.Cdecl);
        
        CSurf_OnSendPanChange = GetDelegateForFunctionPointer<CSurf_OnSendPanChangeDelegate>(
            reaperFunctions["CSurf_OnSendPanChange"], CallingConvention.Cdecl);
        
        CSurf_OnSendVolumeChange = GetDelegateForFunctionPointer<CSurf_OnSendVolumeChangeDelegate>(
            reaperFunctions["CSurf_OnSendVolumeChange"], CallingConvention.Cdecl);
        
        CSurf_OnSoloChange = GetDelegateForFunctionPointer<CSurf_OnSoloChangeDelegate>(
            reaperFunctions["CSurf_OnSoloChange"], CallingConvention.Cdecl);
        
        CSurf_OnSoloChangeEx = GetDelegateForFunctionPointer<CSurf_OnSoloChangeExDelegate>(
            reaperFunctions["CSurf_OnSoloChangeEx"], CallingConvention.Cdecl);
        
        CSurf_OnStop = GetDelegateForFunctionPointer<CSurf_OnStopDelegate>(
            reaperFunctions["CSurf_OnStop"], CallingConvention.Cdecl);
        
        CSurf_OnTempoChange = GetDelegateForFunctionPointer<CSurf_OnTempoChangeDelegate>(
            reaperFunctions["CSurf_OnTempoChange"], CallingConvention.Cdecl);
        
        CSurf_OnTrackSelection = GetDelegateForFunctionPointer<CSurf_OnTrackSelectionDelegate>(
            reaperFunctions["CSurf_OnTrackSelection"], CallingConvention.Cdecl);
        
        CSurf_OnVolumeChange = GetDelegateForFunctionPointer<CSurf_OnVolumeChangeDelegate>(
            reaperFunctions["CSurf_OnVolumeChange"], CallingConvention.Cdecl);
        
        CSurf_OnVolumeChangeEx = GetDelegateForFunctionPointer<CSurf_OnVolumeChangeExDelegate>(
            reaperFunctions["CSurf_OnVolumeChangeEx"], CallingConvention.Cdecl);
        
        CSurf_OnWidthChange = GetDelegateForFunctionPointer<CSurf_OnWidthChangeDelegate>(
            reaperFunctions["CSurf_OnWidthChange"], CallingConvention.Cdecl);
        
        CSurf_OnWidthChangeEx = GetDelegateForFunctionPointer<CSurf_OnWidthChangeExDelegate>(
            reaperFunctions["CSurf_OnWidthChangeEx"], CallingConvention.Cdecl);
        
        CSurf_OnZoom = GetDelegateForFunctionPointer<CSurf_OnZoomDelegate>(
            reaperFunctions["CSurf_OnZoom"], CallingConvention.Cdecl);
        
        CSurf_ResetAllCachedVolPanStates = GetDelegateForFunctionPointer<CSurf_ResetAllCachedVolPanStatesDelegate>(
            reaperFunctions["CSurf_ResetAllCachedVolPanStates"], CallingConvention.Cdecl);
        
        CSurf_ScrubAmt = GetDelegateForFunctionPointer<CSurf_ScrubAmtDelegate>(
            reaperFunctions["CSurf_ScrubAmt"], CallingConvention.Cdecl);
        
        CSurf_SetAutoMode = GetDelegateForFunctionPointer<CSurf_SetAutoModeDelegate>(
            reaperFunctions["CSurf_SetAutoMode"], CallingConvention.Cdecl);
        
        CSurf_SetPlayState = GetDelegateForFunctionPointer<CSurf_SetPlayStateDelegate>(
            reaperFunctions["CSurf_SetPlayState"], CallingConvention.Cdecl);
        
        CSurf_SetRepeatState = GetDelegateForFunctionPointer<CSurf_SetRepeatStateDelegate>(
            reaperFunctions["CSurf_SetRepeatState"], CallingConvention.Cdecl);
        
        CSurf_SetSurfaceMute = GetDelegateForFunctionPointer<CSurf_SetSurfaceMuteDelegate>(
            reaperFunctions["CSurf_SetSurfaceMute"], CallingConvention.Cdecl);
        
        CSurf_SetSurfacePan = GetDelegateForFunctionPointer<CSurf_SetSurfacePanDelegate>(
            reaperFunctions["CSurf_SetSurfacePan"], CallingConvention.Cdecl);
        
        CSurf_SetSurfaceRecArm = GetDelegateForFunctionPointer<CSurf_SetSurfaceRecArmDelegate>(
            reaperFunctions["CSurf_SetSurfaceRecArm"], CallingConvention.Cdecl);
        
        CSurf_SetSurfaceSelected = GetDelegateForFunctionPointer<CSurf_SetSurfaceSelectedDelegate>(
            reaperFunctions["CSurf_SetSurfaceSelected"], CallingConvention.Cdecl);
        
        CSurf_SetSurfaceSolo = GetDelegateForFunctionPointer<CSurf_SetSurfaceSoloDelegate>(
            reaperFunctions["CSurf_SetSurfaceSolo"], CallingConvention.Cdecl);
        
        CSurf_SetSurfaceVolume = GetDelegateForFunctionPointer<CSurf_SetSurfaceVolumeDelegate>(
            reaperFunctions["CSurf_SetSurfaceVolume"], CallingConvention.Cdecl);
        
        CSurf_SetTrackListChange = GetDelegateForFunctionPointer<CSurf_SetTrackListChangeDelegate>(
            reaperFunctions["CSurf_SetTrackListChange"], CallingConvention.Cdecl);
        
        CSurf_TrackFromID = GetDelegateForFunctionPointer<CSurf_TrackFromIDDelegate>(
            reaperFunctions["CSurf_TrackFromID"], CallingConvention.Cdecl);
        
        CSurf_TrackToID = GetDelegateForFunctionPointer<CSurf_TrackToIDDelegate>(
            reaperFunctions["CSurf_TrackToID"], CallingConvention.Cdecl);
        
        DB2SLIDER = GetDelegateForFunctionPointer<DB2SLIDERDelegate>(
            reaperFunctions["DB2SLIDER"], CallingConvention.Cdecl);
        
        DeleteActionShortcut = GetDelegateForFunctionPointer<DeleteActionShortcutDelegate>(
            reaperFunctions["DeleteActionShortcut"], CallingConvention.Cdecl);
        
        DeleteExtState = GetDelegateForFunctionPointer<DeleteExtStateDelegate>(
            reaperFunctions["DeleteExtState"], CallingConvention.Cdecl);
        
        DeleteProjectMarker = GetDelegateForFunctionPointer<DeleteProjectMarkerDelegate>(
            reaperFunctions["DeleteProjectMarker"], CallingConvention.Cdecl);
        
        DeleteProjectMarkerByIndex = GetDelegateForFunctionPointer<DeleteProjectMarkerByIndexDelegate>(
            reaperFunctions["DeleteProjectMarkerByIndex"], CallingConvention.Cdecl);
        
        DeleteTakeStretchMarkers = GetDelegateForFunctionPointer<DeleteTakeStretchMarkersDelegate>(
            reaperFunctions["DeleteTakeStretchMarkers"], CallingConvention.Cdecl);
        
        DeleteTrack = GetDelegateForFunctionPointer<DeleteTrackDelegate>(
            reaperFunctions["DeleteTrack"], CallingConvention.Cdecl);
        
        DeleteTrackMediaItem = GetDelegateForFunctionPointer<DeleteTrackMediaItemDelegate>(
            reaperFunctions["DeleteTrackMediaItem"], CallingConvention.Cdecl);
        
        DestroyAudioAccessor = GetDelegateForFunctionPointer<DestroyAudioAccessorDelegate>(
            reaperFunctions["DestroyAudioAccessor"], CallingConvention.Cdecl);
        
        DestroyLocalOscHandler = GetDelegateForFunctionPointer<DestroyLocalOscHandlerDelegate>(
            reaperFunctions["DestroyLocalOscHandler"], CallingConvention.Cdecl);
        
        DoActionShortcutDialog = GetDelegateForFunctionPointer<DoActionShortcutDialogDelegate>(
            reaperFunctions["DoActionShortcutDialog"], CallingConvention.Cdecl);
        
        Dock_UpdateDockID = GetDelegateForFunctionPointer<Dock_UpdateDockIDDelegate>(
            reaperFunctions["Dock_UpdateDockID"], CallingConvention.Cdecl);
        
        DockIsChildOfDock = GetDelegateForFunctionPointer<DockIsChildOfDockDelegate>(
            reaperFunctions["DockIsChildOfDock"], CallingConvention.Cdecl);
        
        DockWindowActivate = GetDelegateForFunctionPointer<DockWindowActivateDelegate>(
            reaperFunctions["DockWindowActivate"], CallingConvention.Cdecl);
        
        DockWindowAdd = GetDelegateForFunctionPointer<DockWindowAddDelegate>(
            reaperFunctions["DockWindowAdd"], CallingConvention.Cdecl);
        
        DockWindowAddEx = GetDelegateForFunctionPointer<DockWindowAddExDelegate>(
            reaperFunctions["DockWindowAddEx"], CallingConvention.Cdecl);
        
        DockWindowRefresh = GetDelegateForFunctionPointer<DockWindowRefreshDelegate>(
            reaperFunctions["DockWindowRefresh"], CallingConvention.Cdecl);
        
        DockWindowRefreshForHWND = GetDelegateForFunctionPointer<DockWindowRefreshForHWNDDelegate>(
            reaperFunctions["DockWindowRefreshForHWND"], CallingConvention.Cdecl);
        
        DockWindowRemove = GetDelegateForFunctionPointer<DockWindowRemoveDelegate>(
            reaperFunctions["DockWindowRemove"], CallingConvention.Cdecl);
        
        DuplicateCustomizableMenu = GetDelegateForFunctionPointer<DuplicateCustomizableMenuDelegate>(
            reaperFunctions["DuplicateCustomizableMenu"], CallingConvention.Cdecl);
        
        EnsureNotCompletelyOffscreen = GetDelegateForFunctionPointer<EnsureNotCompletelyOffscreenDelegate>(
            reaperFunctions["EnsureNotCompletelyOffscreen"], CallingConvention.Cdecl);
        
        EnumPitchShiftModes = GetDelegateForFunctionPointer<EnumPitchShiftModesDelegate>(
            reaperFunctions["EnumPitchShiftModes"], CallingConvention.Cdecl);
        
        EnumPitchShiftSubModes = GetDelegateForFunctionPointer<EnumPitchShiftSubModesDelegate>(
            reaperFunctions["EnumPitchShiftSubModes"], CallingConvention.Cdecl);
        
        EnumProjectMarkers = GetDelegateForFunctionPointer<EnumProjectMarkersDelegate>(
            reaperFunctions["EnumProjectMarkers"], CallingConvention.Cdecl);
        
        EnumProjectMarkers2 = GetDelegateForFunctionPointer<EnumProjectMarkers2Delegate>(
            reaperFunctions["EnumProjectMarkers2"], CallingConvention.Cdecl);
        
        EnumProjectMarkers3 = GetDelegateForFunctionPointer<EnumProjectMarkers3Delegate>(
            reaperFunctions["EnumProjectMarkers3"], CallingConvention.Cdecl);
        
        EnumProjects = GetDelegateForFunctionPointer<EnumProjectsDelegate>(
            reaperFunctions["EnumProjects"], CallingConvention.Cdecl);
        
        EnumRegionRenderMatrix = GetDelegateForFunctionPointer<EnumRegionRenderMatrixDelegate>(
            reaperFunctions["EnumRegionRenderMatrix"], CallingConvention.Cdecl);
        
        EnumTrackMIDIProgramNames = GetDelegateForFunctionPointer<EnumTrackMIDIProgramNamesDelegate>(
            reaperFunctions["EnumTrackMIDIProgramNames"], CallingConvention.Cdecl);
        
        EnumTrackMIDIProgramNamesEx = GetDelegateForFunctionPointer<EnumTrackMIDIProgramNamesExDelegate>(
            reaperFunctions["EnumTrackMIDIProgramNamesEx"], CallingConvention.Cdecl);
        
        file_exists = GetDelegateForFunctionPointer<file_existsDelegate>(
            reaperFunctions["file_exists"], CallingConvention.Cdecl);
        
        format_timestr = GetDelegateForFunctionPointer<format_timestrDelegate>(
            reaperFunctions["format_timestr"], CallingConvention.Cdecl);
        
        format_timestr_len = GetDelegateForFunctionPointer<format_timestr_lenDelegate>(
            reaperFunctions["format_timestr_len"], CallingConvention.Cdecl);
        
        format_timestr_pos = GetDelegateForFunctionPointer<format_timestr_posDelegate>(
            reaperFunctions["format_timestr_pos"], CallingConvention.Cdecl);
        
        FreeHeapPtr = GetDelegateForFunctionPointer<FreeHeapPtrDelegate>(
            reaperFunctions["FreeHeapPtr"], CallingConvention.Cdecl);
        
        genGuid = GetDelegateForFunctionPointer<genGuidDelegate>(
            reaperFunctions["genGuid"], CallingConvention.Cdecl);
        
        get_config_var = GetDelegateForFunctionPointer<get_config_varDelegate>(
            reaperFunctions["get_config_var"], CallingConvention.Cdecl);
        
        get_ini_file = GetDelegateForFunctionPointer<get_ini_fileDelegate>(
            reaperFunctions["get_ini_file"], CallingConvention.Cdecl);
        
        get_midi_config_var = GetDelegateForFunctionPointer<get_midi_config_varDelegate>(
            reaperFunctions["get_midi_config_var"], CallingConvention.Cdecl);
        
        GetActionShortcutDesc = GetDelegateForFunctionPointer<GetActionShortcutDescDelegate>(
            reaperFunctions["GetActionShortcutDesc"], CallingConvention.Cdecl);
        
        GetActiveTake = GetDelegateForFunctionPointer<GetActiveTakeDelegate>(
            reaperFunctions["GetActiveTake"], CallingConvention.Cdecl);
        
        GetAppVersion = GetDelegateForFunctionPointer<GetAppVersionDelegate>(
            reaperFunctions["GetAppVersion"], CallingConvention.Cdecl);
        
        GetAudioAccessorEndTime = GetDelegateForFunctionPointer<GetAudioAccessorEndTimeDelegate>(
            reaperFunctions["GetAudioAccessorEndTime"], CallingConvention.Cdecl);
        
        GetAudioAccessorHash = GetDelegateForFunctionPointer<GetAudioAccessorHashDelegate>(
            reaperFunctions["GetAudioAccessorHash"], CallingConvention.Cdecl);
        
        GetAudioAccessorSamples = GetDelegateForFunctionPointer<GetAudioAccessorSamplesDelegate>(
            reaperFunctions["GetAudioAccessorSamples"], CallingConvention.Cdecl);
        
        GetAudioAccessorStartTime = GetDelegateForFunctionPointer<GetAudioAccessorStartTimeDelegate>(
            reaperFunctions["GetAudioAccessorStartTime"], CallingConvention.Cdecl);
        
        GetColorTheme = GetDelegateForFunctionPointer<GetColorThemeDelegate>(
            reaperFunctions["GetColorTheme"], CallingConvention.Cdecl);
        
        GetColorThemeStruct = GetDelegateForFunctionPointer<GetColorThemeStructDelegate>(
            reaperFunctions["GetColorThemeStruct"], CallingConvention.Cdecl);
        
        GetConfigWantsDock = GetDelegateForFunctionPointer<GetConfigWantsDockDelegate>(
            reaperFunctions["GetConfigWantsDock"], CallingConvention.Cdecl);
        
        GetContextMenu = GetDelegateForFunctionPointer<GetContextMenuDelegate>(
            reaperFunctions["GetContextMenu"], CallingConvention.Cdecl);
        
        GetCurrentProjectInLoadSave = GetDelegateForFunctionPointer<GetCurrentProjectInLoadSaveDelegate>(
            reaperFunctions["GetCurrentProjectInLoadSave"], CallingConvention.Cdecl);
        
        GetCursorContext = GetDelegateForFunctionPointer<GetCursorContextDelegate>(
            reaperFunctions["GetCursorContext"], CallingConvention.Cdecl);
        
        GetCursorPosition = GetDelegateForFunctionPointer<GetCursorPositionDelegate>(
            reaperFunctions["GetCursorPosition"], CallingConvention.Cdecl);
        
        GetCursorPositionEx = GetDelegateForFunctionPointer<GetCursorPositionExDelegate>(
            reaperFunctions["GetCursorPositionEx"], CallingConvention.Cdecl);
        
        GetDisplayedMediaItemColor = GetDelegateForFunctionPointer<GetDisplayedMediaItemColorDelegate>(
            reaperFunctions["GetDisplayedMediaItemColor"], CallingConvention.Cdecl);
        
        GetDisplayedMediaItemColor2 = GetDelegateForFunctionPointer<GetDisplayedMediaItemColor2Delegate>(
            reaperFunctions["GetDisplayedMediaItemColor2"], CallingConvention.Cdecl);
        
        GetEnvelopeName = GetDelegateForFunctionPointer<GetEnvelopeNameDelegate>(
            reaperFunctions["GetEnvelopeName"], CallingConvention.Cdecl);
        
        GetExePath = GetDelegateForFunctionPointer<GetExePathDelegate>(
            reaperFunctions["GetExePath"], CallingConvention.Cdecl);
        
        GetExtState = GetDelegateForFunctionPointer<GetExtStateDelegate>(
            reaperFunctions["GetExtState"], CallingConvention.Cdecl);
        
        GetFocusedFX = GetDelegateForFunctionPointer<GetFocusedFXDelegate>(
            reaperFunctions["GetFocusedFX"], CallingConvention.Cdecl);
        
        GetFreeDiskSpaceForRecordPath = GetDelegateForFunctionPointer<GetFreeDiskSpaceForRecordPathDelegate>(
            reaperFunctions["GetFreeDiskSpaceForRecordPath"], CallingConvention.Cdecl);
        
        GetHZoomLevel = GetDelegateForFunctionPointer<GetHZoomLevelDelegate>(
            reaperFunctions["GetHZoomLevel"], CallingConvention.Cdecl);
        
        GetIconThemePointer = GetDelegateForFunctionPointer<GetIconThemePointerDelegate>(
            reaperFunctions["GetIconThemePointer"], CallingConvention.Cdecl);
        
        GetIconThemeStruct = GetDelegateForFunctionPointer<GetIconThemeStructDelegate>(
            reaperFunctions["GetIconThemeStruct"], CallingConvention.Cdecl);
        
        GetInputChannelName = GetDelegateForFunctionPointer<GetInputChannelNameDelegate>(
            reaperFunctions["GetInputChannelName"], CallingConvention.Cdecl);
        
        GetInputOutputLatency = GetDelegateForFunctionPointer<GetInputOutputLatencyDelegate>(
            reaperFunctions["GetInputOutputLatency"], CallingConvention.Cdecl);
        
        GetItemEditingTime2 = GetDelegateForFunctionPointer<GetItemEditingTime2Delegate>(
            reaperFunctions["GetItemEditingTime2"], CallingConvention.Cdecl);
        
        GetItemProjectContext = GetDelegateForFunctionPointer<GetItemProjectContextDelegate>(
            reaperFunctions["GetItemProjectContext"], CallingConvention.Cdecl);
        
        GetLastMarkerAndCurRegion = GetDelegateForFunctionPointer<GetLastMarkerAndCurRegionDelegate>(
            reaperFunctions["GetLastMarkerAndCurRegion"], CallingConvention.Cdecl);
        
        GetLastTouchedFX = GetDelegateForFunctionPointer<GetLastTouchedFXDelegate>(
            reaperFunctions["GetLastTouchedFX"], CallingConvention.Cdecl);
        
        GetLastTouchedTrack = GetDelegateForFunctionPointer<GetLastTouchedTrackDelegate>(
            reaperFunctions["GetLastTouchedTrack"], CallingConvention.Cdecl);
        
        GetMainHwnd = GetDelegateForFunctionPointer<GetMainHwndDelegate>(
            reaperFunctions["GetMainHwnd"], CallingConvention.Cdecl);
        
        GetMasterMuteSoloFlags = GetDelegateForFunctionPointer<GetMasterMuteSoloFlagsDelegate>(
            reaperFunctions["GetMasterMuteSoloFlags"], CallingConvention.Cdecl);
        
        GetMasterTrack = GetDelegateForFunctionPointer<GetMasterTrackDelegate>(
            reaperFunctions["GetMasterTrack"], CallingConvention.Cdecl);
        
        GetMasterTrackVisibility = GetDelegateForFunctionPointer<GetMasterTrackVisibilityDelegate>(
            reaperFunctions["GetMasterTrackVisibility"], CallingConvention.Cdecl);
        
        GetMaxMidiInputs = GetDelegateForFunctionPointer<GetMaxMidiInputsDelegate>(
            reaperFunctions["GetMaxMidiInputs"], CallingConvention.Cdecl);
        
        GetMaxMidiOutputs = GetDelegateForFunctionPointer<GetMaxMidiOutputsDelegate>(
            reaperFunctions["GetMaxMidiOutputs"], CallingConvention.Cdecl);
        
        GetMediaItem = GetDelegateForFunctionPointer<GetMediaItemDelegate>(
            reaperFunctions["GetMediaItem"], CallingConvention.Cdecl);
        
        GetMediaItem_Track = GetDelegateForFunctionPointer<GetMediaItem_TrackDelegate>(
            reaperFunctions["GetMediaItem_Track"], CallingConvention.Cdecl);
        
        GetMediaItemInfo_Value = GetDelegateForFunctionPointer<GetMediaItemInfo_ValueDelegate>(
            reaperFunctions["GetMediaItemInfo_Value"], CallingConvention.Cdecl);
        
        GetMediaItemNumTakes = GetDelegateForFunctionPointer<GetMediaItemNumTakesDelegate>(
            reaperFunctions["GetMediaItemNumTakes"], CallingConvention.Cdecl);
        
        GetMediaItemTake = GetDelegateForFunctionPointer<GetMediaItemTakeDelegate>(
            reaperFunctions["GetMediaItemTake"], CallingConvention.Cdecl);
        
        GetMediaItemTake_Item = GetDelegateForFunctionPointer<GetMediaItemTake_ItemDelegate>(
            reaperFunctions["GetMediaItemTake_Item"], CallingConvention.Cdecl);
        
        GetMediaItemTake_Source = GetDelegateForFunctionPointer<GetMediaItemTake_SourceDelegate>(
            reaperFunctions["GetMediaItemTake_Source"], CallingConvention.Cdecl);
        
        GetMediaItemTake_Track = GetDelegateForFunctionPointer<GetMediaItemTake_TrackDelegate>(
            reaperFunctions["GetMediaItemTake_Track"], CallingConvention.Cdecl);
        
        GetMediaItemTakeByGUID = GetDelegateForFunctionPointer<GetMediaItemTakeByGUIDDelegate>(
            reaperFunctions["GetMediaItemTakeByGUID"], CallingConvention.Cdecl);
        
        GetMediaItemTakeInfo_Value = GetDelegateForFunctionPointer<GetMediaItemTakeInfo_ValueDelegate>(
            reaperFunctions["GetMediaItemTakeInfo_Value"], CallingConvention.Cdecl);
        
        GetMediaItemTrack = GetDelegateForFunctionPointer<GetMediaItemTrackDelegate>(
            reaperFunctions["GetMediaItemTrack"], CallingConvention.Cdecl);
        
        GetMediaSourceFileName = GetDelegateForFunctionPointer<GetMediaSourceFileNameDelegate>(
            reaperFunctions["GetMediaSourceFileName"], CallingConvention.Cdecl);
        
        GetMediaSourceNumChannels = GetDelegateForFunctionPointer<GetMediaSourceNumChannelsDelegate>(
            reaperFunctions["GetMediaSourceNumChannels"], CallingConvention.Cdecl);
        
        GetMediaSourceSampleRate = GetDelegateForFunctionPointer<GetMediaSourceSampleRateDelegate>(
            reaperFunctions["GetMediaSourceSampleRate"], CallingConvention.Cdecl);
        
        GetMediaSourceType = GetDelegateForFunctionPointer<GetMediaSourceTypeDelegate>(
            reaperFunctions["GetMediaSourceType"], CallingConvention.Cdecl);
        
        GetMediaTrackInfo_Value = GetDelegateForFunctionPointer<GetMediaTrackInfo_ValueDelegate>(
            reaperFunctions["GetMediaTrackInfo_Value"], CallingConvention.Cdecl);
        
        GetMidiInput = GetDelegateForFunctionPointer<GetMidiInputDelegate>(
            reaperFunctions["GetMidiInput"], CallingConvention.Cdecl);
        
        GetMIDIInputName = GetDelegateForFunctionPointer<GetMIDIInputNameDelegate>(
            reaperFunctions["GetMIDIInputName"], CallingConvention.Cdecl);
        
        GetMidiOutput = GetDelegateForFunctionPointer<GetMidiOutputDelegate>(
            reaperFunctions["GetMidiOutput"], CallingConvention.Cdecl);
        
        GetMIDIOutputName = GetDelegateForFunctionPointer<GetMIDIOutputNameDelegate>(
            reaperFunctions["GetMIDIOutputName"], CallingConvention.Cdecl);
        
        GetMixerScroll = GetDelegateForFunctionPointer<GetMixerScrollDelegate>(
            reaperFunctions["GetMixerScroll"], CallingConvention.Cdecl);
        
        GetMouseModifier = GetDelegateForFunctionPointer<GetMouseModifierDelegate>(
            reaperFunctions["GetMouseModifier"], CallingConvention.Cdecl);
        
        GetNumAudioInputs = GetDelegateForFunctionPointer<GetNumAudioInputsDelegate>(
            reaperFunctions["GetNumAudioInputs"], CallingConvention.Cdecl);
        
        GetNumAudioOutputs = GetDelegateForFunctionPointer<GetNumAudioOutputsDelegate>(
            reaperFunctions["GetNumAudioOutputs"], CallingConvention.Cdecl);
        
        GetNumMIDIInputs = GetDelegateForFunctionPointer<GetNumMIDIInputsDelegate>(
            reaperFunctions["GetNumMIDIInputs"], CallingConvention.Cdecl);
        
        GetNumMIDIOutputs = GetDelegateForFunctionPointer<GetNumMIDIOutputsDelegate>(
            reaperFunctions["GetNumMIDIOutputs"], CallingConvention.Cdecl);
        
        GetNumTracks = GetDelegateForFunctionPointer<GetNumTracksDelegate>(
            reaperFunctions["GetNumTracks"], CallingConvention.Cdecl);
        
        GetOutputChannelName = GetDelegateForFunctionPointer<GetOutputChannelNameDelegate>(
            reaperFunctions["GetOutputChannelName"], CallingConvention.Cdecl);
        
        GetOutputLatency = GetDelegateForFunctionPointer<GetOutputLatencyDelegate>(
            reaperFunctions["GetOutputLatency"], CallingConvention.Cdecl);
        
        GetParentTrack = GetDelegateForFunctionPointer<GetParentTrackDelegate>(
            reaperFunctions["GetParentTrack"], CallingConvention.Cdecl);
        
        GetPeakFileName = GetDelegateForFunctionPointer<GetPeakFileNameDelegate>(
            reaperFunctions["GetPeakFileName"], CallingConvention.Cdecl);
        
        GetPeakFileNameEx = GetDelegateForFunctionPointer<GetPeakFileNameExDelegate>(
            reaperFunctions["GetPeakFileNameEx"], CallingConvention.Cdecl);
        
        GetPeakFileNameEx2 = GetDelegateForFunctionPointer<GetPeakFileNameEx2Delegate>(
            reaperFunctions["GetPeakFileNameEx2"], CallingConvention.Cdecl);
        
        GetPeaksBitmap = GetDelegateForFunctionPointer<GetPeaksBitmapDelegate>(
            reaperFunctions["GetPeaksBitmap"], CallingConvention.Cdecl);
        
        GetPlayPosition = GetDelegateForFunctionPointer<GetPlayPositionDelegate>(
            reaperFunctions["GetPlayPosition"], CallingConvention.Cdecl);
        
        GetPlayPosition2 = GetDelegateForFunctionPointer<GetPlayPosition2Delegate>(
            reaperFunctions["GetPlayPosition2"], CallingConvention.Cdecl);
        
        GetPlayPosition2Ex = GetDelegateForFunctionPointer<GetPlayPosition2ExDelegate>(
            reaperFunctions["GetPlayPosition2Ex"], CallingConvention.Cdecl);
        
        GetPlayPositionEx = GetDelegateForFunctionPointer<GetPlayPositionExDelegate>(
            reaperFunctions["GetPlayPositionEx"], CallingConvention.Cdecl);
        
        GetPlayState = GetDelegateForFunctionPointer<GetPlayStateDelegate>(
            reaperFunctions["GetPlayState"], CallingConvention.Cdecl);
        
        GetPlayStateEx = GetDelegateForFunctionPointer<GetPlayStateExDelegate>(
            reaperFunctions["GetPlayStateEx"], CallingConvention.Cdecl);
        
        GetProjectPath = GetDelegateForFunctionPointer<GetProjectPathDelegate>(
            reaperFunctions["GetProjectPath"], CallingConvention.Cdecl);
        
        GetProjectPathEx = GetDelegateForFunctionPointer<GetProjectPathExDelegate>(
            reaperFunctions["GetProjectPathEx"], CallingConvention.Cdecl);
        
        GetProjectStateChangeCount = GetDelegateForFunctionPointer<GetProjectStateChangeCountDelegate>(
            reaperFunctions["GetProjectStateChangeCount"], CallingConvention.Cdecl);
        
        GetProjectTimeSignature = GetDelegateForFunctionPointer<GetProjectTimeSignatureDelegate>(
            reaperFunctions["GetProjectTimeSignature"], CallingConvention.Cdecl);
        
        GetProjectTimeSignature2 = GetDelegateForFunctionPointer<GetProjectTimeSignature2Delegate>(
            reaperFunctions["GetProjectTimeSignature2"], CallingConvention.Cdecl);
        
        GetResourcePath = GetDelegateForFunctionPointer<GetResourcePathDelegate>(
            reaperFunctions["GetResourcePath"], CallingConvention.Cdecl);
        
        GetSelectedMediaItem = GetDelegateForFunctionPointer<GetSelectedMediaItemDelegate>(
            reaperFunctions["GetSelectedMediaItem"], CallingConvention.Cdecl);
        
        GetSelectedTrack = GetDelegateForFunctionPointer<GetSelectedTrackDelegate>(
            reaperFunctions["GetSelectedTrack"], CallingConvention.Cdecl);
        
        GetSelectedTrackEnvelope = GetDelegateForFunctionPointer<GetSelectedTrackEnvelopeDelegate>(
            reaperFunctions["GetSelectedTrackEnvelope"], CallingConvention.Cdecl);
        
        GetSet_ArrangeView2 = GetDelegateForFunctionPointer<GetSet_ArrangeView2Delegate>(
            reaperFunctions["GetSet_ArrangeView2"], CallingConvention.Cdecl);
        
        GetSet_LoopTimeRange = GetDelegateForFunctionPointer<GetSet_LoopTimeRangeDelegate>(
            reaperFunctions["GetSet_LoopTimeRange"], CallingConvention.Cdecl);
        
        GetSet_LoopTimeRange2 = GetDelegateForFunctionPointer<GetSet_LoopTimeRange2Delegate>(
            reaperFunctions["GetSet_LoopTimeRange2"], CallingConvention.Cdecl);
        
        GetSetEnvelopeState = GetDelegateForFunctionPointer<GetSetEnvelopeStateDelegate>(
            reaperFunctions["GetSetEnvelopeState"], CallingConvention.Cdecl);
        
        GetSetEnvelopeState2 = GetDelegateForFunctionPointer<GetSetEnvelopeState2Delegate>(
            reaperFunctions["GetSetEnvelopeState2"], CallingConvention.Cdecl);
        
        GetSetItemState = GetDelegateForFunctionPointer<GetSetItemStateDelegate>(
            reaperFunctions["GetSetItemState"], CallingConvention.Cdecl);
        
        GetSetItemState2 = GetDelegateForFunctionPointer<GetSetItemState2Delegate>(
            reaperFunctions["GetSetItemState2"], CallingConvention.Cdecl);
        
        GetSetMediaItemInfo = GetDelegateForFunctionPointer<GetSetMediaItemInfoDelegate>(
            reaperFunctions["GetSetMediaItemInfo"], CallingConvention.Cdecl);
        
        GetSetMediaItemTakeInfo = GetDelegateForFunctionPointer<GetSetMediaItemTakeInfoDelegate>(
            reaperFunctions["GetSetMediaItemTakeInfo"], CallingConvention.Cdecl);
        
        GetSetMediaItemTakeInfo_String = GetDelegateForFunctionPointer<GetSetMediaItemTakeInfo_StringDelegate>(
            reaperFunctions["GetSetMediaItemTakeInfo_String"], CallingConvention.Cdecl);
        
        GetSetMediaTrackInfo = GetDelegateForFunctionPointer<GetSetMediaTrackInfoDelegate>(
            reaperFunctions["GetSetMediaTrackInfo"], CallingConvention.Cdecl);
        
        GetSetMediaTrackInfo_String = GetDelegateForFunctionPointer<GetSetMediaTrackInfo_StringDelegate>(
            reaperFunctions["GetSetMediaTrackInfo_String"], CallingConvention.Cdecl);
        
        GetSetObjectState = GetDelegateForFunctionPointer<GetSetObjectStateDelegate>(
            reaperFunctions["GetSetObjectState"], CallingConvention.Cdecl);
        
        GetSetObjectState2 = GetDelegateForFunctionPointer<GetSetObjectState2Delegate>(
            reaperFunctions["GetSetObjectState2"], CallingConvention.Cdecl);
        
        GetSetRepeat = GetDelegateForFunctionPointer<GetSetRepeatDelegate>(
            reaperFunctions["GetSetRepeat"], CallingConvention.Cdecl);
        
        GetSetRepeatEx = GetDelegateForFunctionPointer<GetSetRepeatExDelegate>(
            reaperFunctions["GetSetRepeatEx"], CallingConvention.Cdecl);
        
        GetSetTrackMIDISupportFile = GetDelegateForFunctionPointer<GetSetTrackMIDISupportFileDelegate>(
            reaperFunctions["GetSetTrackMIDISupportFile"], CallingConvention.Cdecl);
        
        GetSetTrackSendInfo = GetDelegateForFunctionPointer<GetSetTrackSendInfoDelegate>(
            reaperFunctions["GetSetTrackSendInfo"], CallingConvention.Cdecl);
        
        GetSetTrackState = GetDelegateForFunctionPointer<GetSetTrackStateDelegate>(
            reaperFunctions["GetSetTrackState"], CallingConvention.Cdecl);
        
        GetSetTrackState2 = GetDelegateForFunctionPointer<GetSetTrackState2Delegate>(
            reaperFunctions["GetSetTrackState2"], CallingConvention.Cdecl);
        
        GetSubProjectFromSource = GetDelegateForFunctionPointer<GetSubProjectFromSourceDelegate>(
            reaperFunctions["GetSubProjectFromSource"], CallingConvention.Cdecl);
        
        GetTake = GetDelegateForFunctionPointer<GetTakeDelegate>(
            reaperFunctions["GetTake"], CallingConvention.Cdecl);
        
        GetTakeEnvelopeByName = GetDelegateForFunctionPointer<GetTakeEnvelopeByNameDelegate>(
            reaperFunctions["GetTakeEnvelopeByName"], CallingConvention.Cdecl);
        
        GetTakeName = GetDelegateForFunctionPointer<GetTakeNameDelegate>(
            reaperFunctions["GetTakeName"], CallingConvention.Cdecl);
        
        GetTakeNumStretchMarkers = GetDelegateForFunctionPointer<GetTakeNumStretchMarkersDelegate>(
            reaperFunctions["GetTakeNumStretchMarkers"], CallingConvention.Cdecl);
        
        GetTakeStretchMarker = GetDelegateForFunctionPointer<GetTakeStretchMarkerDelegate>(
            reaperFunctions["GetTakeStretchMarker"], CallingConvention.Cdecl);
        
        GetTCPFXParm = GetDelegateForFunctionPointer<GetTCPFXParmDelegate>(
            reaperFunctions["GetTCPFXParm"], CallingConvention.Cdecl);
        
        GetTempoMatchPlayRate = GetDelegateForFunctionPointer<GetTempoMatchPlayRateDelegate>(
            reaperFunctions["GetTempoMatchPlayRate"], CallingConvention.Cdecl);
        
        GetTempoTimeSigMarker = GetDelegateForFunctionPointer<GetTempoTimeSigMarkerDelegate>(
            reaperFunctions["GetTempoTimeSigMarker"], CallingConvention.Cdecl);
        
        GetToggleCommandState = GetDelegateForFunctionPointer<GetToggleCommandStateDelegate>(
            reaperFunctions["GetToggleCommandState"], CallingConvention.Cdecl);
        
        GetToggleCommandState2 = GetDelegateForFunctionPointer<GetToggleCommandState2Delegate>(
            reaperFunctions["GetToggleCommandState2"], CallingConvention.Cdecl);
        
        GetToggleCommandStateThroughHooks = GetDelegateForFunctionPointer<GetToggleCommandStateThroughHooksDelegate>(
            reaperFunctions["GetToggleCommandStateThroughHooks"], CallingConvention.Cdecl);
        
        GetTooltipWindow = GetDelegateForFunctionPointer<GetTooltipWindowDelegate>(
            reaperFunctions["GetTooltipWindow"], CallingConvention.Cdecl);
        
        GetTrack = GetDelegateForFunctionPointer<GetTrackDelegate>(
            reaperFunctions["GetTrack"], CallingConvention.Cdecl);
        
        GetTrackAutomationMode = GetDelegateForFunctionPointer<GetTrackAutomationModeDelegate>(
            reaperFunctions["GetTrackAutomationMode"], CallingConvention.Cdecl);
        
        GetTrackColor = GetDelegateForFunctionPointer<GetTrackColorDelegate>(
            reaperFunctions["GetTrackColor"], CallingConvention.Cdecl);
        
        GetTrackEnvelope = GetDelegateForFunctionPointer<GetTrackEnvelopeDelegate>(
            reaperFunctions["GetTrackEnvelope"], CallingConvention.Cdecl);
        
        GetTrackEnvelopeByName = GetDelegateForFunctionPointer<GetTrackEnvelopeByNameDelegate>(
            reaperFunctions["GetTrackEnvelopeByName"], CallingConvention.Cdecl);
        
        GetTrackGUID = GetDelegateForFunctionPointer<GetTrackGUIDDelegate>(
            reaperFunctions["GetTrackGUID"], CallingConvention.Cdecl);
        
        GetTrackInfo = GetDelegateForFunctionPointer<GetTrackInfoDelegate>(
            reaperFunctions["GetTrackInfo"], CallingConvention.Cdecl);
        
        GetTrackMediaItem = GetDelegateForFunctionPointer<GetTrackMediaItemDelegate>(
            reaperFunctions["GetTrackMediaItem"], CallingConvention.Cdecl);
        
        GetTrackMIDINoteName = GetDelegateForFunctionPointer<GetTrackMIDINoteNameDelegate>(
            reaperFunctions["GetTrackMIDINoteName"], CallingConvention.Cdecl);
        
        GetTrackMIDINoteNameEx = GetDelegateForFunctionPointer<GetTrackMIDINoteNameExDelegate>(
            reaperFunctions["GetTrackMIDINoteNameEx"], CallingConvention.Cdecl);
        
        GetTrackMIDINoteRange = GetDelegateForFunctionPointer<GetTrackMIDINoteRangeDelegate>(
            reaperFunctions["GetTrackMIDINoteRange"], CallingConvention.Cdecl);
        
        GetTrackNumMediaItems = GetDelegateForFunctionPointer<GetTrackNumMediaItemsDelegate>(
            reaperFunctions["GetTrackNumMediaItems"], CallingConvention.Cdecl);
        
        GetTrackNumSends = GetDelegateForFunctionPointer<GetTrackNumSendsDelegate>(
            reaperFunctions["GetTrackNumSends"], CallingConvention.Cdecl);
        
        GetTrackReceiveName = GetDelegateForFunctionPointer<GetTrackReceiveNameDelegate>(
            reaperFunctions["GetTrackReceiveName"], CallingConvention.Cdecl);
        
        GetTrackReceiveUIMute = GetDelegateForFunctionPointer<GetTrackReceiveUIMuteDelegate>(
            reaperFunctions["GetTrackReceiveUIMute"], CallingConvention.Cdecl);
        
        GetTrackReceiveUIVolPan = GetDelegateForFunctionPointer<GetTrackReceiveUIVolPanDelegate>(
            reaperFunctions["GetTrackReceiveUIVolPan"], CallingConvention.Cdecl);
        
        GetTrackSendName = GetDelegateForFunctionPointer<GetTrackSendNameDelegate>(
            reaperFunctions["GetTrackSendName"], CallingConvention.Cdecl);
        
        GetTrackSendUIMute = GetDelegateForFunctionPointer<GetTrackSendUIMuteDelegate>(
            reaperFunctions["GetTrackSendUIMute"], CallingConvention.Cdecl);
        
        GetTrackSendUIVolPan = GetDelegateForFunctionPointer<GetTrackSendUIVolPanDelegate>(
            reaperFunctions["GetTrackSendUIVolPan"], CallingConvention.Cdecl);
        
        GetTrackState = GetDelegateForFunctionPointer<GetTrackStateDelegate>(
            reaperFunctions["GetTrackState"], CallingConvention.Cdecl);
        
        GetTrackUIMute = GetDelegateForFunctionPointer<GetTrackUIMuteDelegate>(
            reaperFunctions["GetTrackUIMute"], CallingConvention.Cdecl);
        
        GetTrackUIPan = GetDelegateForFunctionPointer<GetTrackUIPanDelegate>(
            reaperFunctions["GetTrackUIPan"], CallingConvention.Cdecl);
        
        GetTrackUIVolPan = GetDelegateForFunctionPointer<GetTrackUIVolPanDelegate>(
            reaperFunctions["GetTrackUIVolPan"], CallingConvention.Cdecl);
        
        GetUserFileNameForRead = GetDelegateForFunctionPointer<GetUserFileNameForReadDelegate>(
            reaperFunctions["GetUserFileNameForRead"], CallingConvention.Cdecl);
        
        GetUserInputs = GetDelegateForFunctionPointer<GetUserInputsDelegate>(
            reaperFunctions["GetUserInputs"], CallingConvention.Cdecl);
        
        GoToMarker = GetDelegateForFunctionPointer<GoToMarkerDelegate>(
            reaperFunctions["GoToMarker"], CallingConvention.Cdecl);
        
        GoToRegion = GetDelegateForFunctionPointer<GoToRegionDelegate>(
            reaperFunctions["GoToRegion"], CallingConvention.Cdecl);
        
        GR_SelectColor = GetDelegateForFunctionPointer<GR_SelectColorDelegate>(
            reaperFunctions["GR_SelectColor"], CallingConvention.Cdecl);
        
        GSC_mainwnd = GetDelegateForFunctionPointer<GSC_mainwndDelegate>(
            reaperFunctions["GSC_mainwnd"], CallingConvention.Cdecl);
        
        guidToString = GetDelegateForFunctionPointer<guidToStringDelegate>(
            reaperFunctions["guidToString"], CallingConvention.Cdecl);
        
        HasExtState = GetDelegateForFunctionPointer<HasExtStateDelegate>(
            reaperFunctions["HasExtState"], CallingConvention.Cdecl);
        
        HasTrackMIDIPrograms = GetDelegateForFunctionPointer<HasTrackMIDIProgramsDelegate>(
            reaperFunctions["HasTrackMIDIPrograms"], CallingConvention.Cdecl);
        
        HasTrackMIDIProgramsEx = GetDelegateForFunctionPointer<HasTrackMIDIProgramsExDelegate>(
            reaperFunctions["HasTrackMIDIProgramsEx"], CallingConvention.Cdecl);
        
        Help_Set = GetDelegateForFunctionPointer<Help_SetDelegate>(
            reaperFunctions["Help_Set"], CallingConvention.Cdecl);
        
        HiresPeaksFromSource = GetDelegateForFunctionPointer<HiresPeaksFromSourceDelegate>(
            reaperFunctions["HiresPeaksFromSource"], CallingConvention.Cdecl);
        
        image_resolve_fn = GetDelegateForFunctionPointer<image_resolve_fnDelegate>(
            reaperFunctions["image_resolve_fn"], CallingConvention.Cdecl);
        
        InsertMedia = GetDelegateForFunctionPointer<InsertMediaDelegate>(
            reaperFunctions["InsertMedia"], CallingConvention.Cdecl);
        
        InsertMediaSection = GetDelegateForFunctionPointer<InsertMediaSectionDelegate>(
            reaperFunctions["InsertMediaSection"], CallingConvention.Cdecl);
        
        InsertTrackAtIndex = GetDelegateForFunctionPointer<InsertTrackAtIndexDelegate>(
            reaperFunctions["InsertTrackAtIndex"], CallingConvention.Cdecl);
        
        IsInRealTimeAudio = GetDelegateForFunctionPointer<IsInRealTimeAudioDelegate>(
            reaperFunctions["IsInRealTimeAudio"], CallingConvention.Cdecl);
        
        IsItemTakeActiveForPlayback = GetDelegateForFunctionPointer<IsItemTakeActiveForPlaybackDelegate>(
            reaperFunctions["IsItemTakeActiveForPlayback"], CallingConvention.Cdecl);
        
        IsMediaExtension = GetDelegateForFunctionPointer<IsMediaExtensionDelegate>(
            reaperFunctions["IsMediaExtension"], CallingConvention.Cdecl);
        
        IsMediaItemSelected = GetDelegateForFunctionPointer<IsMediaItemSelectedDelegate>(
            reaperFunctions["IsMediaItemSelected"], CallingConvention.Cdecl);
        
        IsTrackSelected = GetDelegateForFunctionPointer<IsTrackSelectedDelegate>(
            reaperFunctions["IsTrackSelected"], CallingConvention.Cdecl);
        
        IsTrackVisible = GetDelegateForFunctionPointer<IsTrackVisibleDelegate>(
            reaperFunctions["IsTrackVisible"], CallingConvention.Cdecl);
        
        kbd_enumerateActions = GetDelegateForFunctionPointer<kbd_enumerateActionsDelegate>(
            reaperFunctions["kbd_enumerateActions"], CallingConvention.Cdecl);
        
        kbd_formatKeyName = GetDelegateForFunctionPointer<kbd_formatKeyNameDelegate>(
            reaperFunctions["kbd_formatKeyName"], CallingConvention.Cdecl);
        
        kbd_getCommandName = GetDelegateForFunctionPointer<kbd_getCommandNameDelegate>(
            reaperFunctions["kbd_getCommandName"], CallingConvention.Cdecl);
        
        kbd_getTextFromCmd = GetDelegateForFunctionPointer<kbd_getTextFromCmdDelegate>(
            reaperFunctions["kbd_getTextFromCmd"], CallingConvention.Cdecl);
        
        KBD_OnMainActionEx = GetDelegateForFunctionPointer<KBD_OnMainActionExDelegate>(
            reaperFunctions["KBD_OnMainActionEx"], CallingConvention.Cdecl);
        
        kbd_OnMidiEvent = GetDelegateForFunctionPointer<kbd_OnMidiEventDelegate>(
            reaperFunctions["kbd_OnMidiEvent"], CallingConvention.Cdecl);
        
        kbd_OnMidiList = GetDelegateForFunctionPointer<kbd_OnMidiListDelegate>(
            reaperFunctions["kbd_OnMidiList"], CallingConvention.Cdecl);
        
        kbd_ProcessActionsMenu = GetDelegateForFunctionPointer<kbd_ProcessActionsMenuDelegate>(
            reaperFunctions["kbd_ProcessActionsMenu"], CallingConvention.Cdecl);
        
        kbd_processMidiEventActionEx = GetDelegateForFunctionPointer<kbd_processMidiEventActionExDelegate>(
            reaperFunctions["kbd_processMidiEventActionEx"], CallingConvention.Cdecl);
        
        kbd_reprocessMenu = GetDelegateForFunctionPointer<kbd_reprocessMenuDelegate>(
            reaperFunctions["kbd_reprocessMenu"], CallingConvention.Cdecl);
        
        kbd_RunCommandThroughHooks = GetDelegateForFunctionPointer<kbd_RunCommandThroughHooksDelegate>(
            reaperFunctions["kbd_RunCommandThroughHooks"], CallingConvention.Cdecl);
        
        kbd_translateAccelerator = GetDelegateForFunctionPointer<kbd_translateAcceleratorDelegate>(
            reaperFunctions["kbd_translateAccelerator"], CallingConvention.Cdecl);
        
        kbd_translateMouse = GetDelegateForFunctionPointer<kbd_translateMouseDelegate>(
            reaperFunctions["kbd_translateMouse"], CallingConvention.Cdecl);
        
        Loop_OnArrow = GetDelegateForFunctionPointer<Loop_OnArrowDelegate>(
            reaperFunctions["Loop_OnArrow"], CallingConvention.Cdecl);
        
        Main_OnCommand = GetDelegateForFunctionPointer<Main_OnCommandDelegate>(
            reaperFunctions["Main_OnCommand"], CallingConvention.Cdecl);
        
        Main_OnCommandEx = GetDelegateForFunctionPointer<Main_OnCommandExDelegate>(
            reaperFunctions["Main_OnCommandEx"], CallingConvention.Cdecl);
        
        Main_openProject = GetDelegateForFunctionPointer<Main_openProjectDelegate>(
            reaperFunctions["Main_openProject"], CallingConvention.Cdecl);
        
        Main_UpdateLoopInfo = GetDelegateForFunctionPointer<Main_UpdateLoopInfoDelegate>(
            reaperFunctions["Main_UpdateLoopInfo"], CallingConvention.Cdecl);
        
        MarkProjectDirty = GetDelegateForFunctionPointer<MarkProjectDirtyDelegate>(
            reaperFunctions["MarkProjectDirty"], CallingConvention.Cdecl);
        
        MarkTrackItemsDirty = GetDelegateForFunctionPointer<MarkTrackItemsDirtyDelegate>(
            reaperFunctions["MarkTrackItemsDirty"], CallingConvention.Cdecl);
        
        Master_GetPlayRate = GetDelegateForFunctionPointer<Master_GetPlayRateDelegate>(
            reaperFunctions["Master_GetPlayRate"], CallingConvention.Cdecl);
        
        Master_GetPlayRateAtTime = GetDelegateForFunctionPointer<Master_GetPlayRateAtTimeDelegate>(
            reaperFunctions["Master_GetPlayRateAtTime"], CallingConvention.Cdecl);
        
        Master_GetTempo = GetDelegateForFunctionPointer<Master_GetTempoDelegate>(
            reaperFunctions["Master_GetTempo"], CallingConvention.Cdecl);
        
        Master_NormalizePlayRate = GetDelegateForFunctionPointer<Master_NormalizePlayRateDelegate>(
            reaperFunctions["Master_NormalizePlayRate"], CallingConvention.Cdecl);
        
        Master_NormalizeTempo = GetDelegateForFunctionPointer<Master_NormalizeTempoDelegate>(
            reaperFunctions["Master_NormalizeTempo"], CallingConvention.Cdecl);
        
        MB = GetDelegateForFunctionPointer<MBDelegate>(
            reaperFunctions["MB"], CallingConvention.Cdecl);
        
        MediaItemDescendsFromTrack = GetDelegateForFunctionPointer<MediaItemDescendsFromTrackDelegate>(
            reaperFunctions["MediaItemDescendsFromTrack"], CallingConvention.Cdecl);
        
        MIDI_CountEvts = GetDelegateForFunctionPointer<MIDI_CountEvtsDelegate>(
            reaperFunctions["MIDI_CountEvts"], CallingConvention.Cdecl);
        
        MIDI_DeleteCC = GetDelegateForFunctionPointer<MIDI_DeleteCCDelegate>(
            reaperFunctions["MIDI_DeleteCC"], CallingConvention.Cdecl);
        
        MIDI_DeleteEvt = GetDelegateForFunctionPointer<MIDI_DeleteEvtDelegate>(
            reaperFunctions["MIDI_DeleteEvt"], CallingConvention.Cdecl);
        
        MIDI_DeleteNote = GetDelegateForFunctionPointer<MIDI_DeleteNoteDelegate>(
            reaperFunctions["MIDI_DeleteNote"], CallingConvention.Cdecl);
        
        MIDI_DeleteTextSysexEvt = GetDelegateForFunctionPointer<MIDI_DeleteTextSysexEvtDelegate>(
            reaperFunctions["MIDI_DeleteTextSysexEvt"], CallingConvention.Cdecl);
        
        MIDI_EnumSelCC = GetDelegateForFunctionPointer<MIDI_EnumSelCCDelegate>(
            reaperFunctions["MIDI_EnumSelCC"], CallingConvention.Cdecl);
        
        MIDI_EnumSelEvts = GetDelegateForFunctionPointer<MIDI_EnumSelEvtsDelegate>(
            reaperFunctions["MIDI_EnumSelEvts"], CallingConvention.Cdecl);
        
        MIDI_EnumSelNotes = GetDelegateForFunctionPointer<MIDI_EnumSelNotesDelegate>(
            reaperFunctions["MIDI_EnumSelNotes"], CallingConvention.Cdecl);
        
        MIDI_EnumSelTextSysexEvts = GetDelegateForFunctionPointer<MIDI_EnumSelTextSysexEvtsDelegate>(
            reaperFunctions["MIDI_EnumSelTextSysexEvts"], CallingConvention.Cdecl);
        
        MIDI_eventlist_Create = GetDelegateForFunctionPointer<MIDI_eventlist_CreateDelegate>(
            reaperFunctions["MIDI_eventlist_Create"], CallingConvention.Cdecl);
        
        MIDI_eventlist_Destroy = GetDelegateForFunctionPointer<MIDI_eventlist_DestroyDelegate>(
            reaperFunctions["MIDI_eventlist_Destroy"], CallingConvention.Cdecl);
        
        MIDI_GetCC = GetDelegateForFunctionPointer<MIDI_GetCCDelegate>(
            reaperFunctions["MIDI_GetCC"], CallingConvention.Cdecl);
        
        MIDI_GetEvt = GetDelegateForFunctionPointer<MIDI_GetEvtDelegate>(
            reaperFunctions["MIDI_GetEvt"], CallingConvention.Cdecl);
        
        MIDI_GetNote = GetDelegateForFunctionPointer<MIDI_GetNoteDelegate>(
            reaperFunctions["MIDI_GetNote"], CallingConvention.Cdecl);
        
        MIDI_GetPPQPos_EndOfMeasure = GetDelegateForFunctionPointer<MIDI_GetPPQPos_EndOfMeasureDelegate>(
            reaperFunctions["MIDI_GetPPQPos_EndOfMeasure"], CallingConvention.Cdecl);
        
        MIDI_GetPPQPos_StartOfMeasure = GetDelegateForFunctionPointer<MIDI_GetPPQPos_StartOfMeasureDelegate>(
            reaperFunctions["MIDI_GetPPQPos_StartOfMeasure"], CallingConvention.Cdecl);
        
        MIDI_GetPPQPosFromProjTime = GetDelegateForFunctionPointer<MIDI_GetPPQPosFromProjTimeDelegate>(
            reaperFunctions["MIDI_GetPPQPosFromProjTime"], CallingConvention.Cdecl);
        
        MIDI_GetProjTimeFromPPQPos = GetDelegateForFunctionPointer<MIDI_GetProjTimeFromPPQPosDelegate>(
            reaperFunctions["MIDI_GetProjTimeFromPPQPos"], CallingConvention.Cdecl);
        
        MIDI_GetTextSysexEvt = GetDelegateForFunctionPointer<MIDI_GetTextSysexEvtDelegate>(
            reaperFunctions["MIDI_GetTextSysexEvt"], CallingConvention.Cdecl);
        
        MIDI_InsertCC = GetDelegateForFunctionPointer<MIDI_InsertCCDelegate>(
            reaperFunctions["MIDI_InsertCC"], CallingConvention.Cdecl);
        
        MIDI_InsertEvt = GetDelegateForFunctionPointer<MIDI_InsertEvtDelegate>(
            reaperFunctions["MIDI_InsertEvt"], CallingConvention.Cdecl);
        
        MIDI_InsertNote = GetDelegateForFunctionPointer<MIDI_InsertNoteDelegate>(
            reaperFunctions["MIDI_InsertNote"], CallingConvention.Cdecl);
        
        MIDI_InsertTextSysexEvt = GetDelegateForFunctionPointer<MIDI_InsertTextSysexEvtDelegate>(
            reaperFunctions["MIDI_InsertTextSysexEvt"], CallingConvention.Cdecl);
        
        midi_reinit = GetDelegateForFunctionPointer<midi_reinitDelegate>(
            reaperFunctions["midi_reinit"], CallingConvention.Cdecl);
        
        MIDI_SetCC = GetDelegateForFunctionPointer<MIDI_SetCCDelegate>(
            reaperFunctions["MIDI_SetCC"], CallingConvention.Cdecl);
        
        MIDI_SetEvt = GetDelegateForFunctionPointer<MIDI_SetEvtDelegate>(
            reaperFunctions["MIDI_SetEvt"], CallingConvention.Cdecl);
        
        MIDI_SetNote = GetDelegateForFunctionPointer<MIDI_SetNoteDelegate>(
            reaperFunctions["MIDI_SetNote"], CallingConvention.Cdecl);
        
        MIDI_SetTextSysexEvt = GetDelegateForFunctionPointer<MIDI_SetTextSysexEvtDelegate>(
            reaperFunctions["MIDI_SetTextSysexEvt"], CallingConvention.Cdecl);
        
        MIDIEditor_GetActive = GetDelegateForFunctionPointer<MIDIEditor_GetActiveDelegate>(
            reaperFunctions["MIDIEditor_GetActive"], CallingConvention.Cdecl);
        
        MIDIEditor_GetMode = GetDelegateForFunctionPointer<MIDIEditor_GetModeDelegate>(
            reaperFunctions["MIDIEditor_GetMode"], CallingConvention.Cdecl);
        
        MIDIEditor_GetSetting_int = GetDelegateForFunctionPointer<MIDIEditor_GetSetting_intDelegate>(
            reaperFunctions["MIDIEditor_GetSetting_int"], CallingConvention.Cdecl);
        
        MIDIEditor_GetSetting_str = GetDelegateForFunctionPointer<MIDIEditor_GetSetting_strDelegate>(
            reaperFunctions["MIDIEditor_GetSetting_str"], CallingConvention.Cdecl);
        
        MIDIEditor_GetTake = GetDelegateForFunctionPointer<MIDIEditor_GetTakeDelegate>(
            reaperFunctions["MIDIEditor_GetTake"], CallingConvention.Cdecl);
        
        MIDIEditor_LastFocused_OnCommand = GetDelegateForFunctionPointer<MIDIEditor_LastFocused_OnCommandDelegate>(
            reaperFunctions["MIDIEditor_LastFocused_OnCommand"], CallingConvention.Cdecl);
        
        MIDIEditor_OnCommand = GetDelegateForFunctionPointer<MIDIEditor_OnCommandDelegate>(
            reaperFunctions["MIDIEditor_OnCommand"], CallingConvention.Cdecl);
        
        mkpanstr = GetDelegateForFunctionPointer<mkpanstrDelegate>(
            reaperFunctions["mkpanstr"], CallingConvention.Cdecl);
        
        mkvolpanstr = GetDelegateForFunctionPointer<mkvolpanstrDelegate>(
            reaperFunctions["mkvolpanstr"], CallingConvention.Cdecl);
        
        mkvolstr = GetDelegateForFunctionPointer<mkvolstrDelegate>(
            reaperFunctions["mkvolstr"], CallingConvention.Cdecl);
        
        MoveEditCursor = GetDelegateForFunctionPointer<MoveEditCursorDelegate>(
            reaperFunctions["MoveEditCursor"], CallingConvention.Cdecl);
        
        MoveMediaItemToTrack = GetDelegateForFunctionPointer<MoveMediaItemToTrackDelegate>(
            reaperFunctions["MoveMediaItemToTrack"], CallingConvention.Cdecl);
        
        MuteAllTracks = GetDelegateForFunctionPointer<MuteAllTracksDelegate>(
            reaperFunctions["MuteAllTracks"], CallingConvention.Cdecl);
        
        my_getViewport = GetDelegateForFunctionPointer<my_getViewportDelegate>(
            reaperFunctions["my_getViewport"], CallingConvention.Cdecl);
        
        NamedCommandLookup = GetDelegateForFunctionPointer<NamedCommandLookupDelegate>(
            reaperFunctions["NamedCommandLookup"], CallingConvention.Cdecl);
        
        OnPauseButton = GetDelegateForFunctionPointer<OnPauseButtonDelegate>(
            reaperFunctions["OnPauseButton"], CallingConvention.Cdecl);
        
        OnPauseButtonEx = GetDelegateForFunctionPointer<OnPauseButtonExDelegate>(
            reaperFunctions["OnPauseButtonEx"], CallingConvention.Cdecl);
        
        OnPlayButton = GetDelegateForFunctionPointer<OnPlayButtonDelegate>(
            reaperFunctions["OnPlayButton"], CallingConvention.Cdecl);
        
        OnPlayButtonEx = GetDelegateForFunctionPointer<OnPlayButtonExDelegate>(
            reaperFunctions["OnPlayButtonEx"], CallingConvention.Cdecl);
        
        OnStopButton = GetDelegateForFunctionPointer<OnStopButtonDelegate>(
            reaperFunctions["OnStopButton"], CallingConvention.Cdecl);
        
        OnStopButtonEx = GetDelegateForFunctionPointer<OnStopButtonExDelegate>(
            reaperFunctions["OnStopButtonEx"], CallingConvention.Cdecl);
        
        OscLocalMessageToHost = GetDelegateForFunctionPointer<OscLocalMessageToHostDelegate>(
            reaperFunctions["OscLocalMessageToHost"], CallingConvention.Cdecl);
        
        parse_timestr = GetDelegateForFunctionPointer<parse_timestrDelegate>(
            reaperFunctions["parse_timestr"], CallingConvention.Cdecl);
        
        parse_timestr_len = GetDelegateForFunctionPointer<parse_timestr_lenDelegate>(
            reaperFunctions["parse_timestr_len"], CallingConvention.Cdecl);
        
        parse_timestr_pos = GetDelegateForFunctionPointer<parse_timestr_posDelegate>(
            reaperFunctions["parse_timestr_pos"], CallingConvention.Cdecl);
        
        parsepanstr = GetDelegateForFunctionPointer<parsepanstrDelegate>(
            reaperFunctions["parsepanstr"], CallingConvention.Cdecl);
        
        PCM_Sink_Create = GetDelegateForFunctionPointer<PCM_Sink_CreateDelegate>(
            reaperFunctions["PCM_Sink_Create"], CallingConvention.Cdecl);
        
        PCM_Sink_CreateEx = GetDelegateForFunctionPointer<PCM_Sink_CreateExDelegate>(
            reaperFunctions["PCM_Sink_CreateEx"], CallingConvention.Cdecl);
        
        PCM_Sink_CreateMIDIFile = GetDelegateForFunctionPointer<PCM_Sink_CreateMIDIFileDelegate>(
            reaperFunctions["PCM_Sink_CreateMIDIFile"], CallingConvention.Cdecl);
        
        PCM_Sink_CreateMIDIFileEx = GetDelegateForFunctionPointer<PCM_Sink_CreateMIDIFileExDelegate>(
            reaperFunctions["PCM_Sink_CreateMIDIFileEx"], CallingConvention.Cdecl);
        
        PCM_Sink_Enum = GetDelegateForFunctionPointer<PCM_Sink_EnumDelegate>(
            reaperFunctions["PCM_Sink_Enum"], CallingConvention.Cdecl);
        
        PCM_Sink_GetExtension = GetDelegateForFunctionPointer<PCM_Sink_GetExtensionDelegate>(
            reaperFunctions["PCM_Sink_GetExtension"], CallingConvention.Cdecl);
        
        PCM_Sink_ShowConfig = GetDelegateForFunctionPointer<PCM_Sink_ShowConfigDelegate>(
            reaperFunctions["PCM_Sink_ShowConfig"], CallingConvention.Cdecl);
        
        PCM_Source_CreateFromFile = GetDelegateForFunctionPointer<PCM_Source_CreateFromFileDelegate>(
            reaperFunctions["PCM_Source_CreateFromFile"], CallingConvention.Cdecl);
        
        PCM_Source_CreateFromFileEx = GetDelegateForFunctionPointer<PCM_Source_CreateFromFileExDelegate>(
            reaperFunctions["PCM_Source_CreateFromFileEx"], CallingConvention.Cdecl);
        
        PCM_Source_CreateFromSimple = GetDelegateForFunctionPointer<PCM_Source_CreateFromSimpleDelegate>(
            reaperFunctions["PCM_Source_CreateFromSimple"], CallingConvention.Cdecl);
        
        PCM_Source_CreateFromType = GetDelegateForFunctionPointer<PCM_Source_CreateFromTypeDelegate>(
            reaperFunctions["PCM_Source_CreateFromType"], CallingConvention.Cdecl);
        
        PCM_Source_GetSectionInfo = GetDelegateForFunctionPointer<PCM_Source_GetSectionInfoDelegate>(
            reaperFunctions["PCM_Source_GetSectionInfo"], CallingConvention.Cdecl);
        
        PeakBuild_Create = GetDelegateForFunctionPointer<PeakBuild_CreateDelegate>(
            reaperFunctions["PeakBuild_Create"], CallingConvention.Cdecl);
        
        PeakGet_Create = GetDelegateForFunctionPointer<PeakGet_CreateDelegate>(
            reaperFunctions["PeakGet_Create"], CallingConvention.Cdecl);
        
        PlayPreview = GetDelegateForFunctionPointer<PlayPreviewDelegate>(
            reaperFunctions["PlayPreview"], CallingConvention.Cdecl);
        
        PlayPreviewEx = GetDelegateForFunctionPointer<PlayPreviewExDelegate>(
            reaperFunctions["PlayPreviewEx"], CallingConvention.Cdecl);
        
        PlayTrackPreview = GetDelegateForFunctionPointer<PlayTrackPreviewDelegate>(
            reaperFunctions["PlayTrackPreview"], CallingConvention.Cdecl);
        
        PlayTrackPreview2 = GetDelegateForFunctionPointer<PlayTrackPreview2Delegate>(
            reaperFunctions["PlayTrackPreview2"], CallingConvention.Cdecl);
        
        PlayTrackPreview2Ex = GetDelegateForFunctionPointer<PlayTrackPreview2ExDelegate>(
            reaperFunctions["PlayTrackPreview2Ex"], CallingConvention.Cdecl);
        
        plugin_getapi = GetDelegateForFunctionPointer<plugin_getapiDelegate>(
            reaperFunctions["plugin_getapi"], CallingConvention.Cdecl);
        
        plugin_getFilterList = GetDelegateForFunctionPointer<plugin_getFilterListDelegate>(
            reaperFunctions["plugin_getFilterList"], CallingConvention.Cdecl);
        
        plugin_getImportableProjectFilterList = GetDelegateForFunctionPointer<plugin_getImportableProjectFilterListDelegate>(
            reaperFunctions["plugin_getImportableProjectFilterList"], CallingConvention.Cdecl);
        
        plugin_register = GetDelegateForFunctionPointer<plugin_registerDelegate>(
            reaperFunctions["plugin_register"], CallingConvention.Cdecl);
        
        PluginWantsAlwaysRunFx = GetDelegateForFunctionPointer<PluginWantsAlwaysRunFxDelegate>(
            reaperFunctions["PluginWantsAlwaysRunFx"], CallingConvention.Cdecl);
        
        PreventUIRefresh = GetDelegateForFunctionPointer<PreventUIRefreshDelegate>(
            reaperFunctions["PreventUIRefresh"], CallingConvention.Cdecl);
        
        projectconfig_var_addr = GetDelegateForFunctionPointer<projectconfig_var_addrDelegate>(
            reaperFunctions["projectconfig_var_addr"], CallingConvention.Cdecl);
        
        projectconfig_var_getoffs = GetDelegateForFunctionPointer<projectconfig_var_getoffsDelegate>(
            reaperFunctions["projectconfig_var_getoffs"], CallingConvention.Cdecl);
        
        ReaperGetPitchShiftAPI = GetDelegateForFunctionPointer<ReaperGetPitchShiftAPIDelegate>(
            reaperFunctions["ReaperGetPitchShiftAPI"], CallingConvention.Cdecl);
        
        ReaScriptError = GetDelegateForFunctionPointer<ReaScriptErrorDelegate>(
            reaperFunctions["ReaScriptError"], CallingConvention.Cdecl);
        
        RecursiveCreateDirectory = GetDelegateForFunctionPointer<RecursiveCreateDirectoryDelegate>(
            reaperFunctions["RecursiveCreateDirectory"], CallingConvention.Cdecl);
        
        RefreshToolbar = GetDelegateForFunctionPointer<RefreshToolbarDelegate>(
            reaperFunctions["RefreshToolbar"], CallingConvention.Cdecl);
        
        relative_fn = GetDelegateForFunctionPointer<relative_fnDelegate>(
            reaperFunctions["relative_fn"], CallingConvention.Cdecl);
        
        RenderFileSection = GetDelegateForFunctionPointer<RenderFileSectionDelegate>(
            reaperFunctions["RenderFileSection"], CallingConvention.Cdecl);
        
        Resample_EnumModes = GetDelegateForFunctionPointer<Resample_EnumModesDelegate>(
            reaperFunctions["Resample_EnumModes"], CallingConvention.Cdecl);
        
        Resampler_Create = GetDelegateForFunctionPointer<Resampler_CreateDelegate>(
            reaperFunctions["Resampler_Create"], CallingConvention.Cdecl);
        
        resolve_fn = GetDelegateForFunctionPointer<resolve_fnDelegate>(
            reaperFunctions["resolve_fn"], CallingConvention.Cdecl);
        
        resolve_fn2 = GetDelegateForFunctionPointer<resolve_fn2Delegate>(
            reaperFunctions["resolve_fn2"], CallingConvention.Cdecl);
        
        screenset_register = GetDelegateForFunctionPointer<screenset_registerDelegate>(
            reaperFunctions["screenset_register"], CallingConvention.Cdecl);
        
        screenset_registerNew = GetDelegateForFunctionPointer<screenset_registerNewDelegate>(
            reaperFunctions["screenset_registerNew"], CallingConvention.Cdecl);
        
        screenset_unregister = GetDelegateForFunctionPointer<screenset_unregisterDelegate>(
            reaperFunctions["screenset_unregister"], CallingConvention.Cdecl);
        
        screenset_unregisterByParam = GetDelegateForFunctionPointer<screenset_unregisterByParamDelegate>(
            reaperFunctions["screenset_unregisterByParam"], CallingConvention.Cdecl);
        
        SectionFromUniqueID = GetDelegateForFunctionPointer<SectionFromUniqueIDDelegate>(
            reaperFunctions["SectionFromUniqueID"], CallingConvention.Cdecl);
        
        SelectAllMediaItems = GetDelegateForFunctionPointer<SelectAllMediaItemsDelegate>(
            reaperFunctions["SelectAllMediaItems"], CallingConvention.Cdecl);
        
        SelectProjectInstance = GetDelegateForFunctionPointer<SelectProjectInstanceDelegate>(
            reaperFunctions["SelectProjectInstance"], CallingConvention.Cdecl);
        
        SendLocalOscMessage = GetDelegateForFunctionPointer<SendLocalOscMessageDelegate>(
            reaperFunctions["SendLocalOscMessage"], CallingConvention.Cdecl);
        
        SetActiveTake = GetDelegateForFunctionPointer<SetActiveTakeDelegate>(
            reaperFunctions["SetActiveTake"], CallingConvention.Cdecl);
        
        SetAutomationMode = GetDelegateForFunctionPointer<SetAutomationModeDelegate>(
            reaperFunctions["SetAutomationMode"], CallingConvention.Cdecl);
        
        SetCurrentBPM = GetDelegateForFunctionPointer<SetCurrentBPMDelegate>(
            reaperFunctions["SetCurrentBPM"], CallingConvention.Cdecl);
        
        SetEditCurPos = GetDelegateForFunctionPointer<SetEditCurPosDelegate>(
            reaperFunctions["SetEditCurPos"], CallingConvention.Cdecl);
        
        SetEditCurPos2 = GetDelegateForFunctionPointer<SetEditCurPos2Delegate>(
            reaperFunctions["SetEditCurPos2"], CallingConvention.Cdecl);
        
        SetExtState = GetDelegateForFunctionPointer<SetExtStateDelegate>(
            reaperFunctions["SetExtState"], CallingConvention.Cdecl);
        
        SetMasterTrackVisibility = GetDelegateForFunctionPointer<SetMasterTrackVisibilityDelegate>(
            reaperFunctions["SetMasterTrackVisibility"], CallingConvention.Cdecl);
        
        SetMediaItemInfo_Value = GetDelegateForFunctionPointer<SetMediaItemInfo_ValueDelegate>(
            reaperFunctions["SetMediaItemInfo_Value"], CallingConvention.Cdecl);
        
        SetMediaItemLength = GetDelegateForFunctionPointer<SetMediaItemLengthDelegate>(
            reaperFunctions["SetMediaItemLength"], CallingConvention.Cdecl);
        
        SetMediaItemPosition = GetDelegateForFunctionPointer<SetMediaItemPositionDelegate>(
            reaperFunctions["SetMediaItemPosition"], CallingConvention.Cdecl);
        
        SetMediaItemSelected = GetDelegateForFunctionPointer<SetMediaItemSelectedDelegate>(
            reaperFunctions["SetMediaItemSelected"], CallingConvention.Cdecl);
        
        SetMediaItemTakeInfo_Value = GetDelegateForFunctionPointer<SetMediaItemTakeInfo_ValueDelegate>(
            reaperFunctions["SetMediaItemTakeInfo_Value"], CallingConvention.Cdecl);
        
        SetMediaTrackInfo_Value = GetDelegateForFunctionPointer<SetMediaTrackInfo_ValueDelegate>(
            reaperFunctions["SetMediaTrackInfo_Value"], CallingConvention.Cdecl);
        
        SetMixerScroll = GetDelegateForFunctionPointer<SetMixerScrollDelegate>(
            reaperFunctions["SetMixerScroll"], CallingConvention.Cdecl);
        
        SetMouseModifier = GetDelegateForFunctionPointer<SetMouseModifierDelegate>(
            reaperFunctions["SetMouseModifier"], CallingConvention.Cdecl);
        
        SetOnlyTrackSelected = GetDelegateForFunctionPointer<SetOnlyTrackSelectedDelegate>(
            reaperFunctions["SetOnlyTrackSelected"], CallingConvention.Cdecl);
        
        SetProjectMarker = GetDelegateForFunctionPointer<SetProjectMarkerDelegate>(
            reaperFunctions["SetProjectMarker"], CallingConvention.Cdecl);
        
        SetProjectMarker2 = GetDelegateForFunctionPointer<SetProjectMarker2Delegate>(
            reaperFunctions["SetProjectMarker2"], CallingConvention.Cdecl);
        
        SetProjectMarker3 = GetDelegateForFunctionPointer<SetProjectMarker3Delegate>(
            reaperFunctions["SetProjectMarker3"], CallingConvention.Cdecl);
        
        SetProjectMarkerByIndex = GetDelegateForFunctionPointer<SetProjectMarkerByIndexDelegate>(
            reaperFunctions["SetProjectMarkerByIndex"], CallingConvention.Cdecl);
        
        SetRegionRenderMatrix = GetDelegateForFunctionPointer<SetRegionRenderMatrixDelegate>(
            reaperFunctions["SetRegionRenderMatrix"], CallingConvention.Cdecl);
        
        SetRenderLastError = GetDelegateForFunctionPointer<SetRenderLastErrorDelegate>(
            reaperFunctions["SetRenderLastError"], CallingConvention.Cdecl);
        
        SetTakeStretchMarker = GetDelegateForFunctionPointer<SetTakeStretchMarkerDelegate>(
            reaperFunctions["SetTakeStretchMarker"], CallingConvention.Cdecl);
        
        SetTempoTimeSigMarker = GetDelegateForFunctionPointer<SetTempoTimeSigMarkerDelegate>(
            reaperFunctions["SetTempoTimeSigMarker"], CallingConvention.Cdecl);
        
        SetTrackAutomationMode = GetDelegateForFunctionPointer<SetTrackAutomationModeDelegate>(
            reaperFunctions["SetTrackAutomationMode"], CallingConvention.Cdecl);
        
        SetTrackColor = GetDelegateForFunctionPointer<SetTrackColorDelegate>(
            reaperFunctions["SetTrackColor"], CallingConvention.Cdecl);
        
        SetTrackMIDINoteName = GetDelegateForFunctionPointer<SetTrackMIDINoteNameDelegate>(
            reaperFunctions["SetTrackMIDINoteName"], CallingConvention.Cdecl);
        
        SetTrackMIDINoteNameEx = GetDelegateForFunctionPointer<SetTrackMIDINoteNameExDelegate>(
            reaperFunctions["SetTrackMIDINoteNameEx"], CallingConvention.Cdecl);
        
        SetTrackSelected = GetDelegateForFunctionPointer<SetTrackSelectedDelegate>(
            reaperFunctions["SetTrackSelected"], CallingConvention.Cdecl);
        
        SetTrackSendUIPan = GetDelegateForFunctionPointer<SetTrackSendUIPanDelegate>(
            reaperFunctions["SetTrackSendUIPan"], CallingConvention.Cdecl);
        
        SetTrackSendUIVol = GetDelegateForFunctionPointer<SetTrackSendUIVolDelegate>(
            reaperFunctions["SetTrackSendUIVol"], CallingConvention.Cdecl);
        
        ShowActionList = GetDelegateForFunctionPointer<ShowActionListDelegate>(
            reaperFunctions["ShowActionList"], CallingConvention.Cdecl);
        
        ShowConsoleMsg = GetDelegateForFunctionPointer<ShowConsoleMsgDelegate>(
            reaperFunctions["ShowConsoleMsg"], CallingConvention.Cdecl);
        
        ShowMessageBox = GetDelegateForFunctionPointer<ShowMessageBoxDelegate>(
            reaperFunctions["ShowMessageBox"], CallingConvention.Cdecl);
        
        SLIDER2DB = GetDelegateForFunctionPointer<SLIDER2DBDelegate>(
            reaperFunctions["SLIDER2DB"], CallingConvention.Cdecl);
        
        SnapToGrid = GetDelegateForFunctionPointer<SnapToGridDelegate>(
            reaperFunctions["SnapToGrid"], CallingConvention.Cdecl);
        
        SoloAllTracks = GetDelegateForFunctionPointer<SoloAllTracksDelegate>(
            reaperFunctions["SoloAllTracks"], CallingConvention.Cdecl);
        
        SplitMediaItem = GetDelegateForFunctionPointer<SplitMediaItemDelegate>(
            reaperFunctions["SplitMediaItem"], CallingConvention.Cdecl);
        
        StopPreview = GetDelegateForFunctionPointer<StopPreviewDelegate>(
            reaperFunctions["StopPreview"], CallingConvention.Cdecl);
        
        StopTrackPreview = GetDelegateForFunctionPointer<StopTrackPreviewDelegate>(
            reaperFunctions["StopTrackPreview"], CallingConvention.Cdecl);
        
        StopTrackPreview2 = GetDelegateForFunctionPointer<StopTrackPreview2Delegate>(
            reaperFunctions["StopTrackPreview2"], CallingConvention.Cdecl);
        
        stringToGuid = GetDelegateForFunctionPointer<stringToGuidDelegate>(
            reaperFunctions["stringToGuid"], CallingConvention.Cdecl);
        
        StuffMIDIMessage = GetDelegateForFunctionPointer<StuffMIDIMessageDelegate>(
            reaperFunctions["StuffMIDIMessage"], CallingConvention.Cdecl);
        
        TimeMap2_beatsToTime = GetDelegateForFunctionPointer<TimeMap2_beatsToTimeDelegate>(
            reaperFunctions["TimeMap2_beatsToTime"], CallingConvention.Cdecl);
        
        TimeMap2_GetDividedBpmAtTime = GetDelegateForFunctionPointer<TimeMap2_GetDividedBpmAtTimeDelegate>(
            reaperFunctions["TimeMap2_GetDividedBpmAtTime"], CallingConvention.Cdecl);
        
        TimeMap2_GetNextChangeTime = GetDelegateForFunctionPointer<TimeMap2_GetNextChangeTimeDelegate>(
            reaperFunctions["TimeMap2_GetNextChangeTime"], CallingConvention.Cdecl);
        
        TimeMap2_QNToTime = GetDelegateForFunctionPointer<TimeMap2_QNToTimeDelegate>(
            reaperFunctions["TimeMap2_QNToTime"], CallingConvention.Cdecl);
        
        TimeMap2_timeToBeats = GetDelegateForFunctionPointer<TimeMap2_timeToBeatsDelegate>(
            reaperFunctions["TimeMap2_timeToBeats"], CallingConvention.Cdecl);
        
        TimeMap2_timeToQN = GetDelegateForFunctionPointer<TimeMap2_timeToQNDelegate>(
            reaperFunctions["TimeMap2_timeToQN"], CallingConvention.Cdecl);
        
        TimeMap_GetDividedBpmAtTime = GetDelegateForFunctionPointer<TimeMap_GetDividedBpmAtTimeDelegate>(
            reaperFunctions["TimeMap_GetDividedBpmAtTime"], CallingConvention.Cdecl);
        
        TimeMap_GetTimeSigAtTime = GetDelegateForFunctionPointer<TimeMap_GetTimeSigAtTimeDelegate>(
            reaperFunctions["TimeMap_GetTimeSigAtTime"], CallingConvention.Cdecl);
        
        TimeMap_QNToTime = GetDelegateForFunctionPointer<TimeMap_QNToTimeDelegate>(
            reaperFunctions["TimeMap_QNToTime"], CallingConvention.Cdecl);
        
        TimeMap_QNToTime_abs = GetDelegateForFunctionPointer<TimeMap_QNToTime_absDelegate>(
            reaperFunctions["TimeMap_QNToTime_abs"], CallingConvention.Cdecl);
        
        TimeMap_timeToQN = GetDelegateForFunctionPointer<TimeMap_timeToQNDelegate>(
            reaperFunctions["TimeMap_timeToQN"], CallingConvention.Cdecl);
        
        TimeMap_timeToQN_abs = GetDelegateForFunctionPointer<TimeMap_timeToQN_absDelegate>(
            reaperFunctions["TimeMap_timeToQN_abs"], CallingConvention.Cdecl);
        
        ToggleTrackSendUIMute = GetDelegateForFunctionPointer<ToggleTrackSendUIMuteDelegate>(
            reaperFunctions["ToggleTrackSendUIMute"], CallingConvention.Cdecl);
        
        Track_GetPeakHoldDB = GetDelegateForFunctionPointer<Track_GetPeakHoldDBDelegate>(
            reaperFunctions["Track_GetPeakHoldDB"], CallingConvention.Cdecl);
        
        Track_GetPeakInfo = GetDelegateForFunctionPointer<Track_GetPeakInfoDelegate>(
            reaperFunctions["Track_GetPeakInfo"], CallingConvention.Cdecl);
        
        TrackCtl_SetToolTip = GetDelegateForFunctionPointer<TrackCtl_SetToolTipDelegate>(
            reaperFunctions["TrackCtl_SetToolTip"], CallingConvention.Cdecl);
        
        TrackFX_EndParamEdit = GetDelegateForFunctionPointer<TrackFX_EndParamEditDelegate>(
            reaperFunctions["TrackFX_EndParamEdit"], CallingConvention.Cdecl);
        
        TrackFX_FormatParamValue = GetDelegateForFunctionPointer<TrackFX_FormatParamValueDelegate>(
            reaperFunctions["TrackFX_FormatParamValue"], CallingConvention.Cdecl);
        
        TrackFX_FormatParamValueNormalized = GetDelegateForFunctionPointer<TrackFX_FormatParamValueNormalizedDelegate>(
            reaperFunctions["TrackFX_FormatParamValueNormalized"], CallingConvention.Cdecl);
        
        TrackFX_GetByName = GetDelegateForFunctionPointer<TrackFX_GetByNameDelegate>(
            reaperFunctions["TrackFX_GetByName"], CallingConvention.Cdecl);
        
        TrackFX_GetChainVisible = GetDelegateForFunctionPointer<TrackFX_GetChainVisibleDelegate>(
            reaperFunctions["TrackFX_GetChainVisible"], CallingConvention.Cdecl);
        
        TrackFX_GetCount = GetDelegateForFunctionPointer<TrackFX_GetCountDelegate>(
            reaperFunctions["TrackFX_GetCount"], CallingConvention.Cdecl);
        
        TrackFX_GetEnabled = GetDelegateForFunctionPointer<TrackFX_GetEnabledDelegate>(
            reaperFunctions["TrackFX_GetEnabled"], CallingConvention.Cdecl);
        
        TrackFX_GetEQ = GetDelegateForFunctionPointer<TrackFX_GetEQDelegate>(
            reaperFunctions["TrackFX_GetEQ"], CallingConvention.Cdecl);
        
        TrackFX_GetEQBandEnabled = GetDelegateForFunctionPointer<TrackFX_GetEQBandEnabledDelegate>(
            reaperFunctions["TrackFX_GetEQBandEnabled"], CallingConvention.Cdecl);
        
        TrackFX_GetEQParam = GetDelegateForFunctionPointer<TrackFX_GetEQParamDelegate>(
            reaperFunctions["TrackFX_GetEQParam"], CallingConvention.Cdecl);
        
        TrackFX_GetFloatingWindow = GetDelegateForFunctionPointer<TrackFX_GetFloatingWindowDelegate>(
            reaperFunctions["TrackFX_GetFloatingWindow"], CallingConvention.Cdecl);
        
        TrackFX_GetFormattedParamValue = GetDelegateForFunctionPointer<TrackFX_GetFormattedParamValueDelegate>(
            reaperFunctions["TrackFX_GetFormattedParamValue"], CallingConvention.Cdecl);
        
        TrackFX_GetFXGUID = GetDelegateForFunctionPointer<TrackFX_GetFXGUIDDelegate>(
            reaperFunctions["TrackFX_GetFXGUID"], CallingConvention.Cdecl);
        
        TrackFX_GetFXName = GetDelegateForFunctionPointer<TrackFX_GetFXNameDelegate>(
            reaperFunctions["TrackFX_GetFXName"], CallingConvention.Cdecl);
        
        TrackFX_GetInstrument = GetDelegateForFunctionPointer<TrackFX_GetInstrumentDelegate>(
            reaperFunctions["TrackFX_GetInstrument"], CallingConvention.Cdecl);
        
        TrackFX_GetNumParams = GetDelegateForFunctionPointer<TrackFX_GetNumParamsDelegate>(
            reaperFunctions["TrackFX_GetNumParams"], CallingConvention.Cdecl);
        
        TrackFX_GetOpen = GetDelegateForFunctionPointer<TrackFX_GetOpenDelegate>(
            reaperFunctions["TrackFX_GetOpen"], CallingConvention.Cdecl);
        
        TrackFX_GetParam = GetDelegateForFunctionPointer<TrackFX_GetParamDelegate>(
            reaperFunctions["TrackFX_GetParam"], CallingConvention.Cdecl);
        
        TrackFX_GetParameterStepSizes = GetDelegateForFunctionPointer<TrackFX_GetParameterStepSizesDelegate>(
            reaperFunctions["TrackFX_GetParameterStepSizes"], CallingConvention.Cdecl);
        
        TrackFX_GetParamEx = GetDelegateForFunctionPointer<TrackFX_GetParamExDelegate>(
            reaperFunctions["TrackFX_GetParamEx"], CallingConvention.Cdecl);
        
        TrackFX_GetParamName = GetDelegateForFunctionPointer<TrackFX_GetParamNameDelegate>(
            reaperFunctions["TrackFX_GetParamName"], CallingConvention.Cdecl);
        
        TrackFX_GetParamNormalized = GetDelegateForFunctionPointer<TrackFX_GetParamNormalizedDelegate>(
            reaperFunctions["TrackFX_GetParamNormalized"], CallingConvention.Cdecl);
        
        TrackFX_GetPreset = GetDelegateForFunctionPointer<TrackFX_GetPresetDelegate>(
            reaperFunctions["TrackFX_GetPreset"], CallingConvention.Cdecl);
        
        TrackFX_GetPresetIndex = GetDelegateForFunctionPointer<TrackFX_GetPresetIndexDelegate>(
            reaperFunctions["TrackFX_GetPresetIndex"], CallingConvention.Cdecl);
        
        TrackFX_NavigatePresets = GetDelegateForFunctionPointer<TrackFX_NavigatePresetsDelegate>(
            reaperFunctions["TrackFX_NavigatePresets"], CallingConvention.Cdecl);
        
        TrackFX_SetEnabled = GetDelegateForFunctionPointer<TrackFX_SetEnabledDelegate>(
            reaperFunctions["TrackFX_SetEnabled"], CallingConvention.Cdecl);
        
        TrackFX_SetEQBandEnabled = GetDelegateForFunctionPointer<TrackFX_SetEQBandEnabledDelegate>(
            reaperFunctions["TrackFX_SetEQBandEnabled"], CallingConvention.Cdecl);
        
        TrackFX_SetEQParam = GetDelegateForFunctionPointer<TrackFX_SetEQParamDelegate>(
            reaperFunctions["TrackFX_SetEQParam"], CallingConvention.Cdecl);
        
        TrackFX_SetOpen = GetDelegateForFunctionPointer<TrackFX_SetOpenDelegate>(
            reaperFunctions["TrackFX_SetOpen"], CallingConvention.Cdecl);
        
        TrackFX_SetParam = GetDelegateForFunctionPointer<TrackFX_SetParamDelegate>(
            reaperFunctions["TrackFX_SetParam"], CallingConvention.Cdecl);
        
        TrackFX_SetParamNormalized = GetDelegateForFunctionPointer<TrackFX_SetParamNormalizedDelegate>(
            reaperFunctions["TrackFX_SetParamNormalized"], CallingConvention.Cdecl);
        
        TrackFX_SetPreset = GetDelegateForFunctionPointer<TrackFX_SetPresetDelegate>(
            reaperFunctions["TrackFX_SetPreset"], CallingConvention.Cdecl);
        
        TrackFX_SetPresetByIndex = GetDelegateForFunctionPointer<TrackFX_SetPresetByIndexDelegate>(
            reaperFunctions["TrackFX_SetPresetByIndex"], CallingConvention.Cdecl);
        
        TrackFX_Show = GetDelegateForFunctionPointer<TrackFX_ShowDelegate>(
            reaperFunctions["TrackFX_Show"], CallingConvention.Cdecl);
        
        TrackList_AdjustWindows = GetDelegateForFunctionPointer<TrackList_AdjustWindowsDelegate>(
            reaperFunctions["TrackList_AdjustWindows"], CallingConvention.Cdecl);
        
        TrackList_UpdateAllExternalSurfaces = GetDelegateForFunctionPointer<TrackList_UpdateAllExternalSurfacesDelegate>(
            reaperFunctions["TrackList_UpdateAllExternalSurfaces"], CallingConvention.Cdecl);
        
        Undo_BeginBlock = GetDelegateForFunctionPointer<Undo_BeginBlockDelegate>(
            reaperFunctions["Undo_BeginBlock"], CallingConvention.Cdecl);
        
        Undo_BeginBlock2 = GetDelegateForFunctionPointer<Undo_BeginBlock2Delegate>(
            reaperFunctions["Undo_BeginBlock2"], CallingConvention.Cdecl);
        
        Undo_CanRedo2 = GetDelegateForFunctionPointer<Undo_CanRedo2Delegate>(
            reaperFunctions["Undo_CanRedo2"], CallingConvention.Cdecl);
        
        Undo_CanUndo2 = GetDelegateForFunctionPointer<Undo_CanUndo2Delegate>(
            reaperFunctions["Undo_CanUndo2"], CallingConvention.Cdecl);
        
        Undo_DoRedo2 = GetDelegateForFunctionPointer<Undo_DoRedo2Delegate>(
            reaperFunctions["Undo_DoRedo2"], CallingConvention.Cdecl);
        
        Undo_DoUndo2 = GetDelegateForFunctionPointer<Undo_DoUndo2Delegate>(
            reaperFunctions["Undo_DoUndo2"], CallingConvention.Cdecl);
        
        Undo_EndBlock = GetDelegateForFunctionPointer<Undo_EndBlockDelegate>(
            reaperFunctions["Undo_EndBlock"], CallingConvention.Cdecl);
        
        Undo_EndBlock2 = GetDelegateForFunctionPointer<Undo_EndBlock2Delegate>(
            reaperFunctions["Undo_EndBlock2"], CallingConvention.Cdecl);
        
        Undo_OnStateChange = GetDelegateForFunctionPointer<Undo_OnStateChangeDelegate>(
            reaperFunctions["Undo_OnStateChange"], CallingConvention.Cdecl);
        
        Undo_OnStateChange2 = GetDelegateForFunctionPointer<Undo_OnStateChange2Delegate>(
            reaperFunctions["Undo_OnStateChange2"], CallingConvention.Cdecl);
        
        Undo_OnStateChange_Item = GetDelegateForFunctionPointer<Undo_OnStateChange_ItemDelegate>(
            reaperFunctions["Undo_OnStateChange_Item"], CallingConvention.Cdecl);
        
        Undo_OnStateChangeEx = GetDelegateForFunctionPointer<Undo_OnStateChangeExDelegate>(
            reaperFunctions["Undo_OnStateChangeEx"], CallingConvention.Cdecl);
        
        Undo_OnStateChangeEx2 = GetDelegateForFunctionPointer<Undo_OnStateChangeEx2Delegate>(
            reaperFunctions["Undo_OnStateChangeEx2"], CallingConvention.Cdecl);
        
        UpdateArrange = GetDelegateForFunctionPointer<UpdateArrangeDelegate>(
            reaperFunctions["UpdateArrange"], CallingConvention.Cdecl);
        
        UpdateItemInProject = GetDelegateForFunctionPointer<UpdateItemInProjectDelegate>(
            reaperFunctions["UpdateItemInProject"], CallingConvention.Cdecl);
        
        UpdateTimeline = GetDelegateForFunctionPointer<UpdateTimelineDelegate>(
            reaperFunctions["UpdateTimeline"], CallingConvention.Cdecl);
        
        ValidatePtr = GetDelegateForFunctionPointer<ValidatePtrDelegate>(
            reaperFunctions["ValidatePtr"], CallingConvention.Cdecl);
        
        ViewPrefs = GetDelegateForFunctionPointer<ViewPrefsDelegate>(
            reaperFunctions["ViewPrefs"], CallingConvention.Cdecl);
        
        WDL_VirtualWnd_ScaledBlitBG = GetDelegateForFunctionPointer<WDL_VirtualWnd_ScaledBlitBGDelegate>(
            reaperFunctions["WDL_VirtualWnd_ScaledBlitBG"], CallingConvention.Cdecl);
        
        
        ReaperMain.InitFromConfig();
    }   
    
    }
}