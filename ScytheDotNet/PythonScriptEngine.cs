﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IronPython.Hosting;
using Microsoft.Scripting.Hosting;
using Microsoft.Scripting;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace Scythe
{
    public class PythonScriptEngine
    {
        private ScriptEngine engine;

        public PythonScriptEngine()
        {
            engine = Python.CreateEngine();
            engine.Runtime.LoadAssembly(Assembly.GetExecutingAssembly());
        }

        private Func<CommandHandler.CommandDelegate, Action>
            CommandDecorator(string key, string description)
        {
            Func<CommandHandler.CommandDelegate, Action> decorator = command =>
            {
                CommandHandler.RegisterMainCommand(key, description, command);
                Action wrapper = () =>
                {
                    try
                    {
                        command();
                    }
                    catch (Exception ex)
                    {
                        ExceptionOperations eo;
                        eo = engine.GetService<ExceptionOperations>();
                        string error = eo.FormatException(ex);
                        string msg = "Syntax error";
                        ReaperAPI.MB(error, msg, 0);
                    }
                };
                return wrapper;
            };
           
            return decorator;
        }

        public void Add(string path)
        {
            try
            {
                ScriptSource script;
                script = engine.CreateScriptSourceFromFile(path);
                CompiledCode code = script.Compile();
                ScriptScope scope = engine.CreateScope();
                scope.SetVariable("command",
                    new Func<string, string, Func<CommandHandler.CommandDelegate, Action>>(CommandDecorator));
                code.Execute(scope);
            }
            catch (Exception e)
            {
                ExceptionOperations eo;
                eo = engine.GetService<ExceptionOperations>();
                string error = eo.FormatException(e);
                string caption;
                string msg = "Syntax error in \"{0}\"";
                caption = String.Format(msg, Path.GetFileName(path));
                ReaperAPI.MB(error, caption, 0);
            }
        }
    }
}
