﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class TakeList : IEnumerable<Take>, IReaperEntityList<Take>
    {
        private Item item;

        internal TakeList(Item item)
        {
            this.item = item;
        }

        public int Count
        {
            get { return ReaperAPI.CountTakes(item.Ptr); }
        }

        public Take At(int index)
        {
            return this[index];
        }

        public Take this[int index]
        {
            get
            {
                return new Take(ReaperAPI.GetTake(item.Ptr, index));
            }
        }

        public Take Add()
        {
            return new Take(ReaperAPI.AddTakeToMediaItem(item.Ptr));
        }

        public IEnumerator<Take> GetEnumerator()
        {
            return new ReaperEntityEnumerator<Take>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ReaperEntityEnumerator<Take>(this);
        }
    }
}
