﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class Take : ReaperBaseEntity
    {

        public Take(IntPtr take) : base(take)
        {
        }

        public MidiSource MidiSource
        {
            get
            {
                if (IsMidi)
                {
                    return new MidiSource(Ptr);
                }
                return null;
            }
        }

        public PCMSource PCMSource
        {
            get
            {
                if (!IsMidi)
                {
                    return new PCMSource(Ptr);
                }
                return null;
            }
        }

        public bool IsMidi
        {
            get
            {
                using (MarshalString typeBuffer = new MarshalString(128))
                {
                    IntPtr source = ReaperAPI.GetMediaItemTake_Source(Ptr);
                    ReaperAPI.GetMediaSourceType(source, typeBuffer.Ptr, 128);
                    return typeBuffer.Value.Contains("MIDI");
                }
            }
        }

        public double Volume
        {
            get
            {
                return GetValueDouble("D_VOL");
            }
            set
            {
                SetValue("D_VOL", value);
            }
        }

        public double StartOffset
        {
            get
            {
                return GetValueDouble("D_STARTOFFS");
            }
            set
            {
                SetValue("D_STARTOFFS", value);
            }
        }

        public double Pan
        {
            get
            {
                return GetValueDouble("D_PAN");
            }
            set
            {
                SetValue("D_PAN", value);
            }
        }

        protected override double GetValueDouble(string key)
        {
            return ReaperAPI.GetMediaItemTakeInfo_Value(Ptr, key);
        }

        protected override void SetValue(string key, double value)
        {
            ReaperAPI.SetMediaItemTakeInfo_Value(Ptr, key, value);
        }
    }
}
