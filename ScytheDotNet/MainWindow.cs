﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class MainWindow : System.Windows.Forms.IWin32Window
    {
        public MainWindow()
        {
            mHWND = ReaperAPI.GetMainHwnd();
        }

        public IntPtr Handle
        {
            get { return mHWND; }
        }

        private IntPtr mHWND;
    }
}
