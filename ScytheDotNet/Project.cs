﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class Project
    {
        private IntPtr project;
        private TrackList trackList;
        private ProjectItemList itemList;

        internal Project(IntPtr project)
        {
            this.project = project;
            trackList = new TrackList(this);
            itemList = new ProjectItemList(this);
        }

        internal IntPtr Ptr
        {
            get
            {
                return project;
            }
        }

        public double CursorPosition
        {
            get
            {
                return ReaperAPI.GetCursorPositionEx(project);
            }

            set
            {
                ReaperAPI.SetEditCurPos2(project, value, true, true); 
            }
        }

        public TrackList Tracks
        {
            get { return trackList; }
        }

        public ProjectItemList Items
        {
            get { return itemList; }
        }

        public static Project Current
        {
            get
            {
                return new Project(IntPtr.Zero);
            }
        }
    }
}
