﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{   
    public abstract class ReaperBaseEntity
    {
        protected IntPtr entity;

        public ReaperBaseEntity(IntPtr entity)
        {
            this.entity = entity;
        }

        internal IntPtr Ptr
        {
            get { return entity; }
        }

        protected abstract double GetValueDouble(string key);

        protected abstract void SetValue(string key, double value);

        protected int GetValueInteger(string key)
        {
            return (int)GetValueDouble(key);
        }

        protected bool GetValueBoolean(string key)
        {
            return GetValueDouble(key) > 0.0;
        }

        protected void SetValue(string key, bool value)
        {
            SetValue(key, value ? 1.0 : 0.0);
        }

        protected void SetValue(string key, int value)
        {
            SetValue(key, (double)value);
        }
    }
}
