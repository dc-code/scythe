﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Scythe
{
    public class Plugins : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true, IsRequired = false, IsKey = false)]
        public PluginElementCollection Assemblies
        {
            get { return (PluginElementCollection)base[""]; }
        }
    }


    [ConfigurationCollection(typeof(PluginElement), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
    public class PluginElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new PluginElement();
        }

        protected override ConfigurationElement CreateNewElement(string elementName)
        {
            var element = new PluginElement();
            if (elementName == "script")
            {
                element.Type = PluginType.Script;
            } else 
            {
                element.Type = PluginType.Assembly;
            }

            return element;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMapAlternate; }
        }  

        protected override bool IsElementName(string elementName)
        {
            return elementName == "assembly" || elementName == "script";
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return element;
        }
    }

    public enum PluginType
    {
        Script, 
        Assembly
    }

    public class PluginElement : ConfigurationElement
    {
        public PluginType Type { get; set; }
        [ConfigurationProperty("path", IsRequired = true)]
        public string Path
        {
            get
            {
                return (string)base["path"];
            }
        }

        [ConfigurationProperty("entrypoint", IsRequired = false)]
        public string EntryPoint
        {
            get
            {
                return (string)base["entrypoint"];
            }
        }
    }
}
