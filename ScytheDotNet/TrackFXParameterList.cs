﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class TrackFxParameterList : IEnumerable<TrackFXParameter>, IReaperEntityList<TrackFXParameter>
    {
        private TrackFx trackFx;

        internal TrackFxParameterList(TrackFx trackFx)
        {
            this.trackFx = trackFx;
        }

        public int Count
        {
            get
            {
                return ReaperAPI.TrackFX_GetNumParams(trackFx.Track.Ptr, trackFx.Index);
            }
        }

        public TrackFXParameter At(int index)
        {
            return this[index];
        }

        public TrackFXParameter this[int index]
        {
            get
            {
                return new TrackFXParameter(trackFx, index);
            }
        }

        public IEnumerator<TrackFXParameter> GetEnumerator()
        {
            return new ReaperEntityEnumerator<TrackFXParameter>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ReaperEntityEnumerator<TrackFXParameter>(this);
        }
    }
}
