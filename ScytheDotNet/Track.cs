﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Scythe
{
    public class Track : ReaperBaseEntity
    {
        private TrackItemList itemList;
        private TrackFxList fxList;

        internal Track(IntPtr track) : base(track)
        {
            itemList = new TrackItemList(this);
            fxList = new TrackFxList(this);
        }

        protected override void SetValue(string key, double value)
        {
            ReaperAPI.SetMediaTrackInfo_Value(Ptr, key, value);
        }

        protected override double GetValueDouble(string key)
        {
            return ReaperAPI.GetMediaTrackInfo_Value(Ptr, key);
        }

        public TrackItemList Items
        {
            get
            {
                return itemList;
            }
        }

        public TrackFxList Fx
        {
            get
            {
                return fxList;
            }
        }

        public bool Selected
        {
            get
            {
                return GetValueBoolean("I_SELECTED");
            }
            set
            {
                SetValue("I_SELECTED", value);
            }
        }

        public string Name
        {
            get
            {
                using (MarshalString nameBuffer = new MarshalString(512))
                {
                    ReaperAPI.GetSetMediaTrackInfo_String(entity, "P_NAME", nameBuffer.Ptr, false);
                    return nameBuffer.Value;
                }
            }
            set
            {
                using (MarshalString nameBuffer = new MarshalString(value))
                {
                    ReaperAPI.GetSetMediaTrackInfo_String(entity, "P_NAME", nameBuffer.Ptr, true);
                }
            }
        }

        public double Volume
        {
            get 
            {
                return GetValueDouble("D_VOL");
            }
            set
            {
                SetValue("D_VOL", value); 
            }
        }

        public bool Muted
        {
            get
            {
                return GetValueBoolean("B_MUTE");
            }
            set
            {
                SetValue("B_MUTE", value);
            }
        }

        public bool InvertedPhase
        {
            get
            {
                return GetValueBoolean("B_PHASE");
            }
            set
            {
                SetValue("B_PHASE", value);
            }
        }
    }
}
