﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public abstract class ItemList: IEnumerable<Item>, IReaperEntityList<Item>
    {
        public abstract int Count { get; }
        
        public abstract Item this[int index] { get; }

        public Item At(int index)
        {
            return this[index];
        }

        public IEnumerator<Item> GetEnumerator()
        {
            return new ReaperEntityEnumerator<Item>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ReaperEntityEnumerator<Item>(this);
        }
    }

    public class TrackItemList : ItemList
    {
        private Track track;

        internal TrackItemList(Track track)
        {
            this.track = track;
        }

        public override int Count
        {
            get { return ReaperAPI.CountTrackMediaItems(track.Ptr); }
        }

        public override Item this[int index]
        {
            get
            {
                return new Item(ReaperAPI.GetTrackMediaItem(track.Ptr, index));
            }
        }

        public Item Add()
        {
            return new Item(ReaperAPI.AddMediaItemToTrack(track.Ptr));
        }

        public void Remove(Item item)
        {
            ReaperAPI.DeleteTrackMediaItem(track.Ptr, item.Ptr);
        }
    }

    public class ProjectItemList : ItemList
    {
        private Project project;

        internal ProjectItemList(Project project)
        {
            this.project = project;
        }

        public override int Count
        {
            get { return ReaperAPI.CountMediaItems(project.Ptr); }
        }

        public override Item this[int index]
        {
            get
            {
                return new Item(ReaperAPI.GetMediaItem(project.Ptr, index));
            }
        }
    }
}
