﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class TrackFxList : IEnumerable<TrackFx>, IReaperEntityList<TrackFx>
    {
        private Track track;

        internal TrackFxList(Track track)
        {
            this.track = track;
        }

        public int Count
        {
            get
            {
                return ReaperAPI.TrackFX_GetCount(track.Ptr);
            }
        }

        public TrackFx At(int index)
        {
            return this[index];
        }

        public TrackFx this[int index]
        {
            get
            {
                return new TrackFx(track, index);
            }
        }

        public IEnumerator<TrackFx> GetEnumerator()
        {
            return new ReaperEntityEnumerator<TrackFx>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ReaperEntityEnumerator<TrackFx>(this);
        }
    }
}
