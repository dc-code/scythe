﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class TrackFXParameter
    {
        private TrackFx trackFx;
        private int index;

        internal TrackFXParameter(TrackFx trackFx, int index)
        {
            this.index = index;
            this.trackFx = trackFx;
        }

        public string Name
        {
            get
            {
                using (MarshalString nameBuffer = new MarshalString(512))
                {
                    ReaperAPI.TrackFX_GetParamName(trackFx.Track.Ptr, trackFx.Index,
                        index, nameBuffer.Ptr, 512);
                    return nameBuffer.Value;
                }
            }
        }

        public double MinValue
        {
            get
            {
                double minOut = 0.0;
                double maxOut = 0.0;
                ReaperAPI.TrackFX_GetParam(trackFx.Track.Ptr, trackFx.Index, index, ref minOut, ref maxOut);
                return minOut;
            }
        }

        public double MaxValue
        {
            get
            {
                double minOut = 0.0;
                double maxOut = 0.0;
                ReaperAPI.TrackFX_GetParam(trackFx.Track.Ptr, trackFx.Index, index, ref minOut, ref maxOut);
                return maxOut;
            }
        }

        public double Value
        {
            get
            {
                double minOut = 0.0;
                double maxOut = 0.0;
                return ReaperAPI.TrackFX_GetParam(trackFx.Track.Ptr, trackFx.Index, index, ref minOut, ref maxOut);
            }
            set
            {
                ReaperAPI.TrackFX_SetParam(trackFx.Track.Ptr, trackFx.Index, index, value);
            }
        }

        public string FormattedValue
        {
            get
            {
                using (MarshalString nameBuffer = new MarshalString(512))
                {
                    ReaperAPI.TrackFX_GetFormattedParamValue(trackFx.Track.Ptr,
                        trackFx.Index, index, nameBuffer.Ptr, 512);
                    return nameBuffer.Value;
                }
            }
        }
    }
}
