﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Scythe
{
    public class TrackFx
    {
        private Track track;
        private TrackFxParameterList parameters;
        private int index;
        
        internal TrackFx(Track track, int index)
        {
            this.track = track;
            this.index = index;
            parameters = new TrackFxParameterList(this);
        }

        public TrackFxParameterList Parameters
        {
            get
            {
                return parameters;
            }
        }

        public Track Track
        {
            get
            {
                return track;
            }
        }

        public int Index
        {
            get
            {
                return index;
            }
        }

        public string Name
        {
            get
            {
                using (MarshalString nameBuffer = new MarshalString(512))
                {
                    ReaperAPI.TrackFX_GetFXName(track.Ptr, index, nameBuffer.Ptr, 512);
                    return nameBuffer.Value;
                }
            }
        }

        public bool DialogIsOpen
        {
            get
            {
                return ReaperAPI.TrackFX_GetOpen(track.Ptr, index);
            }
        }

        public void ShowDialog(bool show = true)
        {
            ReaperAPI.TrackFX_SetOpen(track.Ptr, index, show); 
        }

        public bool Enabled
        {
            get
            {
                return ReaperAPI.TrackFX_GetEnabled(track.Ptr, index);
            }
            set
            {
                ReaperAPI.TrackFX_SetEnabled(track.Ptr, index, value);
            }
        }
    }
}
