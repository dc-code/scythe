﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Scythe
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Accel
    {
        public byte fVirt;
        public ushort key;
        public ushort cmd;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct GAccelRegister
    {
        public Accel accel;

        [MarshalAs(UnmanagedType.LPStr)]
        public string desc;
    }

    public static class CommandHandler
    {
        public delegate void CommandDelegate();
        private delegate int HookMainCommandDelegate(int command, int flag);

        private static Dictionary<int, CommandDelegate> commands = new Dictionary<int, CommandDelegate>();
        private static Dictionary<string, int> commandKeys = new Dictionary<string, int>();
        private static bool inHookCommand = false;
        private static HookMainCommandDelegate hookCommand;
        private static GCHandle pinnedHookCommand;
        public static void RegisterMainCommand(string key, string description, CommandDelegate command)
        {
            // update command delegate if key already exists
            if (commandKeys.ContainsKey(key))
            {
                commands[commandKeys[key]] = command;
                return;
            }

            if (commands.Count == 0)
            {
                hookCommand = HookCommand;
                // Pin the delegate so it doesn't get moved around.
                pinnedHookCommand = GCHandle.Alloc(hookCommand);
                ReaperAPI.plugin_register("hookcommand", Marshal.GetFunctionPointerForDelegate(hookCommand));
            }
             
            IntPtr cmdKey = Marshal.StringToHGlobalAnsi(key);
            ushort commandId = (ushort)ReaperAPI.plugin_register("command_id", cmdKey);
            Marshal.FreeHGlobal(cmdKey);
            commandKeys[key] = commandId;

            commands[commandId] = command;
            
            GAccelRegister register;
            register.accel.cmd = commandId; 
            register.accel.fVirt = 0;
            register.accel.key = 0;
            register.desc = description;
            IntPtr structure = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(GAccelRegister)));
            Marshal.StructureToPtr(register, structure, false);
            ReaperAPI.plugin_register("gaccel", structure);
            Marshal.FreeHGlobal(structure);
        }

        private static int HookCommand(int commandId, int flag)
        {
            if (inHookCommand)
            {
                inHookCommand = false;
                return 0;
            }

            if (commands.ContainsKey(commandId))
            {
                commands[commandId]();
                inHookCommand = false;
                return 1;
            }

            inHookCommand = false;
            return 0;
        }

    }
}
