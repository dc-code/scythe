﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class TrackList : IEnumerable<Track>, IReaperEntityList<Track>
    {
        private Project project;

        internal TrackList(Project project)
        {
            this.project = project;
        }

        public int Count
        {
            get { return ReaperAPI.CountTracks(project.Ptr); }
        }

        public Track At(int index)
        {
            return this[index];
        }

        public Track this[int index]
        {
            get
            {
                return new Track(ReaperAPI.GetTrack(project.Ptr, index));
            }
        }

        public void InsertAt(int index, bool wantDefaults = true)
        {
            ReaperAPI.InsertTrackAtIndex(index, wantDefaults);
        }

        public void Remove(Track track)
        {
            ReaperAPI.DeleteTrack(track.Ptr);
        }

        public IEnumerator<Track> GetEnumerator()
        {
            return new ReaperEntityEnumerator<Track>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ReaperEntityEnumerator<Track>(this);
        }
    }
}
