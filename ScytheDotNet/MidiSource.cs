﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class MidiSource
    {
        private IntPtr take;
        private SortedSet<MidiNote> midiNotes;

        public MidiSource(IntPtr take)
        {
            this.take = take;
        }

        public ICollection<MidiNote> MidiNotes
        {
            get
            {
                if (midiNotes == null)
                {
                    midiNotes = new SortedSet<MidiNote>();
                    int noteCount = NoteCount();
                    for (int i = 0; i < noteCount; ++i)
                    {
                        midiNotes.Add(new MidiNote(this, i));
                    }
                }
                return midiNotes;
            }
        }

        private int NoteCount()
        {
            int noteCount = 0;
            int rubbish = 0;
            int rubbish2 = 0;
            ReaperAPI.MIDI_CountEvts(take, ref noteCount, ref rubbish, ref rubbish2);
            return noteCount;
        }

        public void Save()
        {
            SaveMidiNotes();
        }

        public void SaveMidiNotes()
        {

        }

        public IntPtr Ptr
        {
            get
            {
                return take;
            }
        }
    }
}
