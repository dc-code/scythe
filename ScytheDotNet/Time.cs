﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{

    public class Beats
    {
        private double beats;
        private int measure;

        private int measureNumerator;
        private int measureDenominator;
        
        private Project project;

        public Beats(Project project = null) : this(new Time(0.0), project)
        {}

        public Beats(Time time, Project project = null)
        {
            if (project == null)
            {
                this.project = Project.Current;
            }
            else
            {
                this.project = project;
            }

            SetFromTime(time.Value);
        }

        public Beats(double beats, Project project = null)
        {
            if (project == null)
            {
                this.project = Project.Current;
            }
            else
            {
                this.project = project;
            }

            int measures = 0;
            double time = ReaperAPI.TimeMap2_beatsToTime(project.Ptr, beats, ref measures);

            SetFromTime(time);
        }

        private void SetFromTime(double time)
        {
            ReaperAPI.TimeMap2_timeToBeats(project.Ptr, time,
                ref measure, ref measureNumerator,
                ref beats, ref measureDenominator);

        }

        public Time toTime()
        {
            int measures = 0;
            return new Time(ReaperAPI.TimeMap2_beatsToTime(project.Ptr, beats, ref measures), project);
        }

        public int Measure
        {
            get
            {
                return measure;
            }
        }

        public int TimeSignatureNumerator
        {
            get
            {
                return measureNumerator;
            }
        }

        public int TimeSignatureDenominator
        {
            get
            {
                return measureDenominator;
            }
        }

        public double Value
        {
            get
            {
                return beats;
            }
            set
            {
                beats = value;
                SetFromTime(toTime());
            }
        }

        public Project Project
        {
            get
            {
                return project;
            }
        }

        public static Beats operator +(Beats lhs, double rhs)
        {
            return new Beats(lhs.Value + rhs, lhs.Project);
        }

        public static Beats operator -(Beats lhs, double rhs)
        {
            return new Beats(lhs.Value - rhs, lhs.Project);
        }
    }

    public class Time
    {
        private double time;
        private Project project;

        public Time(Project project = null) : this(0.0, project)
        {}

        public Time(double t, Project project = null)
        {
            if (project == null)
            {
                this.project = Project.Current;
            }
            else
            {
                this.project = project;
            }

            time = t;
        }

        public static implicit operator double(Time t)
        {
            return t.Value;
        }

        public double Value
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
            }
        }

        public Beats toBeats()
        {
            return new Beats(this, project);
        }
    }
}
