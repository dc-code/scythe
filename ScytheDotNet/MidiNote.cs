﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class MidiNote : IComparable<MidiNote>
    {
        private int sourceIndex = -1;
        private bool selected;
        private bool muted;
        private double startPpq;
        private double endPpq;
        private int channel;
        private int pitch;
        private int velocity;
        
        public MidiNote(MidiSource source, int index)
        {
            sourceIndex = index;
            ReaperAPI.MIDI_GetNote(source.Ptr, index, ref selected,
                ref muted, ref startPpq, ref endPpq, ref channel, ref pitch,
                ref velocity);
        }

        public MidiNote(bool selected, bool muted, double startPpq, double endPpq,
            int channel, int pitch, int velocity)
        {
            sourceIndex = -1;
            this.selected = selected;
            this.muted = muted;
            this.startPpq = startPpq;
            this.endPpq = endPpq;
            this.channel = channel;
            this.pitch = pitch;
            this.velocity = velocity;
        }

        public MidiNote(MidiNote note)
            : this(note.selected, note.muted, note.startPpq, note.endPpq,
            note.channel, note.pitch, note.velocity)
        {
        }

        public int CompareTo(MidiNote other)
        {
            if (StartPPQ < other.StartPPQ)
            {
                return -1;
            }

            if (StartPPQ == other.StartPPQ)
            {
                if (Channel < other.Channel)
                {
                    return -1;
                }

                if (Channel == other.Channel)
                {
                    if (Pitch < other.Pitch)
                    {
                        return -1;
                    }
                    return 1;
                }
                return 1;
            }
            return 1;
        }

        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
            }
        }

        public bool Muted
        {
            get
            {
                return muted;
            }
            set
            {
                muted = value;
            }
        }

        public double StartPPQ
        {
            get
            {
                return startPpq;
            }
            set
            {
                startPpq = value;
            }
        }

        public double EndPPQ
        {
            get
            {
                return endPpq;
            }
            set
            {
                endPpq = value;
            }
        }

        public int Channel
        {
            get
            {
                return channel;
            }
            set
            {
                channel = value;
            }
        }

        public int Pitch
        {
            get
            {
                return pitch;
            }
            set
            {
                pitch = value;
            }
        }

        public int Velocity
        {
            get
            {
                return velocity;
            }
            set
            {
                velocity = value;
            }
        }
    }
}
