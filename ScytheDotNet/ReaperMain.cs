﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Configuration;
using System.Reflection;

namespace Scythe
{
    public static class ReaperMain
    {
        private static PythonScriptEngine python = new PythonScriptEngine();
        private static List<FileSystemWatcher> scriptChangeWatchers = new List<FileSystemWatcher>();

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            FileInfo fInfo = new FileInfo(e.FullPath);
            while (IsFileLocked(fInfo))
            {
                Thread.Sleep(100);
            }
            python.Add(e.FullPath);
        }

        static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }


        public static void InitFromConfig()
        { 
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler((object sender, ResolveEventArgs args) =>
            {
                var scytheDotNet = Assembly.GetExecutingAssembly();
                if (args.Name.StartsWith(scytheDotNet.GetName().Name))
                {
                    return scytheDotNet;
                }
                return null;
            });

            var configuration = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

            Plugins plugins = (Plugins)configuration.GetSection("plugins");
            foreach (PluginElement plugin in plugins.Assemblies)
            {
                var path = plugin.Path;
                var entryPoint = plugin.EntryPoint;
                if (plugin.Type == PluginType.Assembly)
                {
                    Assembly assembly = null;
                    try
                    {
                        assembly = Assembly.LoadFile(path);
                    }
                    catch (Exception)
                    {
                        ReaperAPI.ShowMessageBox(String.Format("Unable to load: {0}", path), "Scythe Error", 0x30);
                        continue;
                    }

                    var fullTypeName = entryPoint.Substring(0, entryPoint.LastIndexOf('.'));
                    var methodName = entryPoint.Substring(entryPoint.LastIndexOf('.') + 1);
                    var entryPointType = assembly.GetType(fullTypeName);
                    if (entryPointType == null)
                    {
                        ReaperAPI.ShowMessageBox(String.Format(
                            "Unable to find type '{0}' in library: {1}", fullTypeName, path), 
                            "Scythe Error", 0x30);
                        continue;
                    }
                    var method = entryPointType.GetMethod(methodName);
                    if (method == null)
                    {
                        ReaperAPI.ShowMessageBox(String.Format(
                            "Unable to find method '{0}' in library: {1}", methodName, path),
                            "Scythe Error", 0x30);
                        continue;
                    }
                    method.Invoke(null, new object[] { });
                }

                if (plugin.Type == PluginType.Script)
                {
                    var fileName = Path.GetFileName(plugin.Path);
                    var dirName = Path.GetDirectoryName(plugin.Path);

                    var scriptFiles = Directory.GetFiles(dirName, fileName);
                    foreach (var script in scriptFiles)
                    {
                        python.Add(script);
                    }

                    var watcher = new FileSystemWatcher(dirName, fileName);
                    watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastWrite;
                    watcher.Changed += new FileSystemEventHandler(OnChanged);
                    watcher.EnableRaisingEvents = true;
                    scriptChangeWatchers.Add(watcher);
                }
            }
        }
    }
}
