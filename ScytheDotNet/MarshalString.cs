﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Scythe
{
    public class MarshalString : IDisposable
    {
        private IntPtr ptr = IntPtr.Zero;
        private bool reaperAllocated = false;
        private bool disposed = false;

        public MarshalString(int size)
        {
            ptr = Marshal.AllocHGlobal(size);
        }

        public MarshalString(string s)
        {
            ptr = Marshal.StringToHGlobalAnsi(s);
        }

        public MarshalString(IntPtr reaperPtr)
        {
            ptr = reaperPtr;
            reaperAllocated = true;
        }

        public string Value
        {
            get
            {
                return Marshal.PtrToStringAnsi(ptr);
            }
        }

        public IntPtr Ptr
        {
            get
            {
                return ptr;
            }
        }

        public void Dispose()
        {
            if (disposed)
            {
                return;
            }

            if (reaperAllocated)
            {
                ReaperAPI.FreeHeapPtr(ptr);
            }
            else
            {
                Marshal.FreeHGlobal(ptr);
            }
            GC.SuppressFinalize(this);
            disposed = true;
        }
    }
}
