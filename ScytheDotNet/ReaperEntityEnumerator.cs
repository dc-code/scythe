﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public interface IReaperEntityList<T>
    {
        int Count {get;}

        T At(int index);
    }

    public class ReaperEntityEnumerator<T> : IEnumerator<T> where T: class
    {
        private int index;
        private IReaperEntityList<T> entityList;
        public ReaperEntityEnumerator(IReaperEntityList<T> entityList)
        {
            this.entityList = entityList;
            Reset();
        }

        public T Current
        {
            get
            {
                if (index < 0 || index >= entityList.Count)
                {
                    return null;
                }
                return entityList.At(index);
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return ((IEnumerator<T>)this).Current;
            }
        }

        public bool MoveNext()
        {
            index += 1;
            return index < entityList.Count;
        }

        public void Reset()
        {
            index = -1;
        }

        public void Dispose()
        {
        }
    }
}
