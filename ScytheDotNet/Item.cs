﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Scythe
{
    public class Item : ReaperBaseEntity
    {
        private TakeList takeList;

        public Item(IntPtr item) : base(item)
        {
            this.takeList = new TakeList(this);
        }

        public TakeList Takes
        {
            get
            {
                return takeList;
            }
        }

        public Take ActiveTake
        {
            get { return new Take(ReaperAPI.GetActiveTake(Ptr)); }
            set { ReaperAPI.SetActiveTake(value.Ptr); }
        }

        public Track Track
        {
            get { return new Track(ReaperAPI.GetMediaItem_Track(Ptr)); }
        }

        /// B_LOOPSRC : bool * to loop source
        public bool LoopSource
        {
            get { return GetValueBoolean("B_LOOPSRC"); }
            set { SetValue("B_LOOPSRC", value); }
        }

        /// B_ALLTAKESPLAY : bool * to all takes play
        public bool AllTakesPlay
        {
            get { return GetValueBoolean("B_ALLTAKESPLAY"); }
            set { SetValue("B_ALLTAKESPLAY", value); }
        }

        /// C_BEATATTACHMODE : char * to one char of beat attached mode, -1=def, 0=time, 1=allbeats, 2=beatsosonly
        /// C_LOCK : char * to one char of lock flags (&amp;1 is locked, currently)
        public bool Locked
        {
            get { return GetValueBoolean("C_LOCK"); }
            set { SetValue("C_LOCK", value); }
        }
        /// D_VOL : double * of item volume (volume bar)
        public double Volume
        {
            get { return GetValueDouble("D_VOL"); }
            set { SetValue("D_VOL", value); }
        }
        /// D_SNAPOFFSET : double * of item snap offset (seconds)
        public double SnapOffset
        {
            get { return GetValueDouble("D_SNAPOFFSET"); }
            set { SetValue("D_SNAPOFFSET", value); }
        }
        /// D_FADEINLEN : double * of item fade in length (manual, seconds)
        public double FadeInLength
        {
            get { return GetValueDouble("D_FADEINLEN"); }
            set { SetValue("D_FADEINLEN", value); }
        }

        /// D_FADEOUTLEN : double * of item fade out length (manual, seconds)
        public double FadeOutLength
        {
            get { return GetValueDouble("D_FADEOUTLEN"); }
            set { SetValue("D_FADEOUTLEN", value); }
        }

        /// D_FADEINLEN_AUTO : double * of item autofade in length (seconds, -1 for no autofade set)
        /// D_FADEOUTLEN_AUTO : double * of item autofade out length (seconds, -1 for no autofade set)
        /// C_FADEINSHAPE : int * to fadein shape, 0=linear, ...
        /// C_FADEOUTSHAPE : int * to fadeout shape
        
        /// I_GROUPID : int * to group ID (0 = no group)
        public int GroupId
        {
            get { return GetValueInteger("I_GROUPID"); }
            set { SetValue("I_GROUPID", value); }
        }

        /// I_LASTY : int * to last y position in track (readonly)
        public int LastYPosition
        {
            get { return GetValueInteger("I_LASTY"); }
            set { SetValue("I_GROUPID", value); }
        }

        /// I_LASTH : int * to last height in track (readonly)
        public int LastHeight
        {
            get { return GetValueInteger("I_LASTH"); }
        }
                
        /// I_CUSTOMCOLOR : int * : custom color, windows standard color order (i.e. RGB(r,g,b)|0x100000). if you do not |0x100000, then it will not be used (though will store the color anyway)
        /// I_CURTAKE : int * to active take
        /// IP_ITEMNUMBER : int, item number within the track (read-only, returns the item number directly)
        /// F_FREEMODE_Y : float * to free mode y position (0..1)
        /// F_FREEMODE_H : float * to free mode height (0..1)

        public bool Muted
        {
            get { return GetValueBoolean("B_MUTE"); }
            set { SetValue("B_MUTE", value); }
        }

        public bool Selected
        {
            get { return GetValueBoolean("B_UISEL"); }
            set { SetValue("B_UISEL", value); }
        }

        public double Position
        {
            get { return GetValueDouble("D_POSITION"); }
            set { SetValue("D_POSITION", value); }
        }

        public double Length
        {
            get { return GetValueDouble("D_LENGTH"); }
            set { SetValue("D_LENGTH", value); }
        }

        protected override void SetValue(string key, double value)
        {
            ReaperAPI.SetMediaItemInfo_Value(entity, key, value);
        }

        protected override double GetValueDouble(string key)
        {
            return ReaperAPI.GetMediaItemInfo_Value(entity, key);
        }
    }
}
