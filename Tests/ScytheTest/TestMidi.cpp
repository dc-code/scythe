#define BOOST_TEST_MODULE MidiTest
#include <boost/test/unit_test.hpp>
#include "Midi.h"
#include <list>

struct NotesFixture {
    NotesFixture() : notes( NULL ) 
    {
        noteStore.reserve(128);
    }
    ~NotesFixture() {}

    MidiNote* addNote(double startPPQ = 0.0, int channel = 0, int pitch = 0)
    {
        MidiNote note;
        ::memset(&note, 0, sizeof(note));
        note.startPPQOriginal = startPPQ;
        note.channelOriginal = channel;
        note.pitchOriginal = pitch;
        noteStore.push_back(note);
        notes.add(&noteStore.back());
        return &noteStore.back();
    }

    MidiNote* at(int index)
    {
        std::list<MidiNote*>::const_iterator iter = notes.notes().begin();
        int i = 0;
        while (i++ < index)
        {
            ++iter;
        }
        return *iter;
    }

    ReaperMidiNotes notes;
    std::vector<MidiNote> noteStore;
};

BOOST_FIXTURE_TEST_SUITE(TestNotes, NotesFixture )

BOOST_AUTO_TEST_CASE(add_single_note)
{
    addNote(0.0);
    BOOST_CHECK_EQUAL(1, notes.notes().size());
    BOOST_CHECK_EQUAL(0.0, at(0)->startPPQOriginal);
}

BOOST_AUTO_TEST_CASE(add_two_notes)
{
    addNote(0.0);
    addNote(1.0);
    BOOST_CHECK_EQUAL(2, notes.notes().size());
    BOOST_CHECK_EQUAL(0.0, at(0)->startPPQOriginal);
    BOOST_CHECK_EQUAL(1.0, at(1)->startPPQOriginal);
}

BOOST_AUTO_TEST_CASE(add_two_notes_same_ppq_different_channel)
{
    addNote(0.0, 10);
    addNote(0.0, 4);
    BOOST_CHECK_EQUAL(2, notes.notes().size());
    BOOST_CHECK_EQUAL(4, at(0)->channelOriginal);
    BOOST_CHECK_EQUAL(10, at(1)->channelOriginal);
}

BOOST_AUTO_TEST_CASE( add_three_notes )
{
    addNote(0.0);
    addNote(1.0);
    addNote(0.5);
    BOOST_CHECK_EQUAL(3, notes.notes().size());
    BOOST_CHECK_EQUAL(0.0, at(0)->startPPQOriginal);
    BOOST_CHECK_EQUAL(0.5, at(1)->startPPQOriginal);
    BOOST_CHECK_EQUAL(1.0, at(2)->startPPQOriginal);
}

BOOST_AUTO_TEST_CASE( note_updated_on_save )
{
    MidiNote *note = addNote(45.0);
    note->startPPQ = 32.0;
    note->channel = 4;
    note->channelOriginal = 5;
    notes.save();
    BOOST_CHECK_EQUAL(32.0, at(0)->startPPQ);
    BOOST_CHECK_EQUAL(4, at(0)->channel);
}

BOOST_AUTO_TEST_CASE( save_2_notes )
{
    MidiNote *note = addNote(1.0);
    note->startPPQ = 1.5;
    MidiNote *note2 = addNote(2.0);
    note2->startPPQ = 0.5;
    notes.save();
    BOOST_CHECK_EQUAL(0.5, at(0)->startPPQ);
    BOOST_CHECK_EQUAL(1.5, at(1)->startPPQ);
}

BOOST_AUTO_TEST_CASE( save_2_notes_a )
{
    MidiNote *note = addNote(1.0);
    note->startPPQ = 2.5;
    MidiNote *note2 = addNote(2.0);
    note2->startPPQ = 3.5;
    notes.save();
    BOOST_CHECK_EQUAL(2.5, at(0)->startPPQ);
    BOOST_CHECK_EQUAL(3.5, at(1)->startPPQ);
}

BOOST_AUTO_TEST_CASE( save_2_notes_b )
{
    MidiNote *note = addNote(1.0);
    note->startPPQ = 2.5;
    MidiNote *note2 = addNote(2.0);
    note2->startPPQ = 1.5;
    notes.save();
    BOOST_CHECK_EQUAL(1.5, notes.notes().front()->startPPQ);
    BOOST_CHECK_EQUAL(2.5, notes.notes().back()->startPPQ);
}

BOOST_AUTO_TEST_CASE( save_3_notes_a )
{
    MidiNote *note = addNote(1.0);
    note->startPPQ = 2.5;
    MidiNote *note2 = addNote(2.0);
    note2->startPPQ = 1.5;
    MidiNote *note3 = addNote(0.0);
    note3->startPPQ = 1.8;
    notes.save();
    BOOST_CHECK_EQUAL(1.5, at(0)->startPPQ);
    BOOST_CHECK_EQUAL(1.8, at(1)->startPPQ);
    BOOST_CHECK_EQUAL(2.5, at(2)->startPPQ);
}

BOOST_AUTO_TEST_SUITE_END()
