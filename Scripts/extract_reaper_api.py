#!/usr/bin/env python

import argparse
import os
import re
import sys
import collections
import shutil
from jinja2 import Template

FUNCTION_REGISTER_FILENAME = "reaper_function_register.inc"
FUNCTIONS_FILENAME = "reaper_functions.inc"


def get_function_name_match(line):
    pattern = "\(\*([_a-zA-Z0-9]+)\)\("
    return re.search(pattern, line)


def get_function_parameters(line):
    pattern = "\(([_a-zA-Z].+)\)"
    parameters = re.search(pattern, line)
    if not parameters:
        return []
    parameters = parameters.group(1)
    parameters = parameters.split(',')
    parameters = [p.strip() for p in parameters]
    return parameters


def get_function_return_type(line):
    return re.match('(.+) \(', line).group(1)

FunctionParameter = collections.namedtuple(
    "FunctionParameter", ("name", "base_type", "pointer_count", "is_const"))

parameter_map = {
    ('size_t', 0, False): 'uint',
    ('char', 1, True): 'string',
    ('bool', 0, False): 'bool',
    ('bool', 1, False): 'ref bool',
    ('bool', 1, True): 'ref bool',
    ('unsigned', 0, False): 'uint',
    ('void', 0, False): 'void',
    ('double', 0, False): 'double',
    ('double', 1, False): 'ref double',
    ('double', 1, True): 'ref double',
    ('float', 1, False): 'ref float',
    ('int', 1, False): 'ref int',
    ('int', 1, True): 'ref int',
    ('char', 2, False): 'ref IntPtr',
    ('float', 0, False): 'float',
    ('int', 0, False): 'int',
    ('HWND', 0, False): 'IntPtr',
    ('INT_PTR', 0, False): 'IntPtr',
    ('DWORD', 0, False): 'int',
    ('HMENU', 0, False): 'IntPtr',
    ('UINT', 0, False): 'uint',
    ('LICE_pixel', 0, False): 'uint',
    ('screensetNewCallbackFunc', 0, False): 'IntPtr'
    }


def get_parameter_metadata(parameter):
    pointer_count = parameter.count('*')
    parameter = parameter.replace('*', ' ').strip()
    parameter_components = [p.strip() for p in parameter.split(' ')]
    name = parameter_components[-1]
    const = 'const' in parameter_components
    if const:
        base_type = parameter_components[1]
    else:
        base_type = parameter_components[0]
    function_parameter = FunctionParameter(name, base_type,
                                           pointer_count, const)
    return function_parameter


def write_csharp_file(functions, project_dir):
    csharp_signatures = [create_delegate_signature(f) for f in functions]
    csharp_function_names = [
        ("%sDelegate" % f.name, f.name, f.comments) for f in functions]
    tmpl = Template(open('reaper.cs.tmpl', 'r').read())
    rendered_tmpl = tmpl.render(delegate_definitions=csharp_signatures,
                                function_names=csharp_function_names)
    output_file = open(os.path.join(project_dir, 'reaper.cs'), 'w')
    output_file.write(rendered_tmpl)


def get_csharp_type(parameter, is_return_type=False):
    p = get_parameter_metadata(parameter)
    key = (p.base_type, p.pointer_count, p.is_const)
    if p.name.lower() == 'stringneedbig':
        return 'IntPtr'
    if is_return_type and p.pointer_count > 0:
        return 'IntPtr'
    if key in parameter_map:
        return parameter_map[key]
    elif p.pointer_count > 0:
        return 'IntPtr'
    else:
        assert 0


def fix_name(name):
    if name == 'in':
        return 'in_'
    if name == 'out':
        return 'out_'
    return name


def create_delegate_signature(function):
    return_type = get_csharp_type(function.return_type, True)
    parameters = ", ".join(["%s %s" % (get_csharp_type(p),
                                       fix_name(get_parameter_metadata(p).name)) for p in function.parameters])
    return 'public delegate %s %sDelegate(%s);' % (return_type, function.name,
                                           parameters)


def find_function_header():
    reaper_dirs = []
    if "REAPER_DIR" in os.environ:
        reaper_dirs.append(os.environ["REAPER_DIR"])
    # check normal places
    reaper_dirs.extend([r"c:\Program Files (x86)\REAPER",
                        r"c:\Program Files\REAPER"])
    # start looking
    print "Looking for reaper_plugin_functions.h"
    for reaper_dir in reaper_dirs:
        print "...looking in '%s'..." % reaper_dir
        header = os.path.join(reaper_dir, "reaper_plugin_functions.h")
        if os.path.exists(header):
            print "...found one in '%s'..." % reaper_dir
            return header
    return None


def output_lines(path, lines):
    try:
        os.makedirs(os.path.dirname(path))
    except:
        pass
    f = open(path, "wb")
    for line in lines:
        f.write("%s\n" % line)
    f.close()


def parse_function_header(path):
    f = open(path, "rb")
    functions = []
    ReaperFunction = collections.namedtuple(
        "ReaperFunction", ("name", "definition", "parameters",
                           "return_type", "comments"))
    comments = []
    for line in f.readlines():
        is_function_declaration = "REAPERAPI_DECL" in line and ";" in line
        is_comment_line = line.startswith('//')
        if not is_comment_line and not is_function_declaration:
            comments = []
        elif is_comment_line:
            comments.append(line[2:].strip())
        elif is_function_declaration:
            match = get_function_name_match(line)
            if match is None:
                comments = []
                continue
            function_name = match.group(1)
            function = line.replace("REAPERAPI_DECL", "")
            function = function.strip()
            function_parameters = get_function_parameters(function)
            function_return_type = get_function_return_type(function)
            functions.append(ReaperFunction(name=function_name,
                                            definition=function,
                                            parameters=function_parameters,
                                            return_type=function_return_type,
                                            comments=comments))
            comments = []
    functions.sort(key=lambda x: x.name.lower())
    return functions


def main():
    parser = argparse.ArgumentParser(
        description="Extract REAPER API for ReaperLib")
    parser.add_argument("cpp_project_dir")
    parser.add_argument("csharp_project_dir")
    args = parser.parse_args()

    plugin_functions_header = find_function_header()
    if plugin_functions_header is None:
        print "Can not find reaper_plugin_functions.h"
        sys.exit(1)

    functions = parse_function_header(plugin_functions_header)
    functions = [f for f in functions if not f.name.startswith('__')]
    functions = [f for f in functions if not f.name.startswith('BR_')]
    functions = [f for f in functions if not f.name.startswith('FNG_')]
    functions = [f for f in functions if not f.name.startswith('LICE_')]
    functions = [f for f in functions if not f.name.startswith('SNM_')]
    functions = [f for f in functions if not f.name.startswith('ULT_')]
    write_csharp_file(functions, args.csharp_project_dir)

    print "Writing function definitions to %s" % FUNCTIONS_FILENAME
    function_definitions = ["REAPERAPI_DECL %s" % function.definition for function in functions]
    output_lines(os.path.join(args.cpp_project_dir, FUNCTIONS_FILENAME),
                 function_definitions)
    print "Writing function registering code to %s" % (
        FUNCTION_REGISTER_FILENAME)
    function_register_lines = [
        "REGISTER_FUNCTION(%s);" % function.name for function in functions]
    output_lines(os.path.join(args.cpp_project_dir,
                              FUNCTION_REGISTER_FILENAME),
                 function_register_lines)
    print "Copying function header to %s" % (args.cpp_project_dir)
    shutil.copy(plugin_functions_header, args.cpp_project_dir)

if __name__ == "__main__":
    main()
