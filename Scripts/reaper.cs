using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Reaper
{

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool AddCustomizableMenuDelegate([MarshalAs(UnmanagedType.LPStr)] String menuidstr, [MarshalAs(UnmanagedType.LPStr)] String menuname, [MarshalAs(UnmanagedType.LPStr)] String kbdsecname, bool addtomainmenu);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool AddExtensionsMainMenuDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr AddMediaItemToTrackDelegate(IntPtr tr);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int AddProjectMarkerDelegate(IntPtr proj, bool isrgn, double pos, double rgnend, [MarshalAs(UnmanagedType.LPStr)] String name, int wantidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int AddProjectMarker2Delegate(IntPtr proj, bool isrgn, double pos, double rgnend, [MarshalAs(UnmanagedType.LPStr)] String name, int wantidx, int color);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr AddTakeToMediaItemDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool AddTempoTimeSigMarkerDelegate(IntPtr proj, double timepos, double bpm, int timesig_num, int timesig_denom, bool lineartempochange);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void adjustZoomDelegate(double amt, int forceset, bool doupd, int centermode);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool AnyTrackSoloDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void APITestDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool ApplyNudgeDelegate(IntPtr project, int nudgeflag, int nudgewhat, int nudgeunits, double value, bool reverse, int copies);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int Audio_IsPreBufferDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int Audio_IsRunningDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int Audio_RegHardwareHookDelegate(bool isAdd, IntPtr reg);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool AudioAccessorValidateStateDelegate(IntPtr accessor);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void BypassFxAllTracksDelegate(int bypass);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CalculatePeaksDelegate(IntPtr srcBlock, IntPtr pksBlock);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CalculatePeaksFloatSrcPtrDelegate(IntPtr srcBlock, IntPtr pksBlock);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void ClearAllRecArmedDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void ClearPeakCacheDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountActionShortcutsDelegate(IntPtr section, int cmdID);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountMediaItemsDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountProjectMarkersDelegate(IntPtr proj, IntPtr num_markersOut, IntPtr num_regionsOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountSelectedMediaItemsDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountSelectedTracksDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountTakesDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountTCPFXParmsDelegate(IntPtr project, IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountTempoTimeSigMarkersDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountTrackEnvelopesDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountTrackMediaItemsDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CountTracksDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr CreateLocalOscHandlerDelegate(IntPtr obj, IntPtr callback);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr CreateMIDIInputDelegate(int dev);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr CreateMIDIOutputDelegate(int dev, bool streamMode, IntPtr msoffset100);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr CreateNewMIDIItemInProjDelegate(IntPtr track, double starttime, double endtime);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr CreateTakeAudioAccessorDelegate(IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr CreateTrackAudioAccessorDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_FlushUndoDelegate(bool force);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_GetTouchStateDelegate(IntPtr trackid, int isPan);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_GoEndDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_GoStartDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CSurf_NumTracksDelegate(bool mcpView);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnArrowDelegate(int whichdir, bool wantzoom);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnFwdDelegate(int seekplay);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_OnFXChangeDelegate(IntPtr trackid, int en);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CSurf_OnInputMonitorChangeDelegate(IntPtr trackid, int monitor);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CSurf_OnInputMonitorChangeExDelegate(IntPtr trackid, int monitor, bool allowgang);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_OnMuteChangeDelegate(IntPtr trackid, int mute);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_OnMuteChangeExDelegate(IntPtr trackid, int mute, bool allowgang);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnOscControlMessageDelegate([MarshalAs(UnmanagedType.LPStr)] String msg, IntPtr arg);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnPanChangeDelegate(IntPtr trackid, double pan, bool relative);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnPanChangeExDelegate(IntPtr trackid, double pan, bool relative, bool allowGang);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnPauseDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnPlayDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnPlayRateChangeDelegate(double playrate);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_OnRecArmChangeDelegate(IntPtr trackid, int recarm);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_OnRecArmChangeExDelegate(IntPtr trackid, int recarm, bool allowgang);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnRecordDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnRecvPanChangeDelegate(IntPtr trackid, int recv_index, double pan, bool relative);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnRecvVolumeChangeDelegate(IntPtr trackid, int recv_index, double volume, bool relative);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnRewDelegate(int seekplay);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnRewFwdDelegate(int seekplay, int dir);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnScrollDelegate(int xdir, int ydir);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_OnSelectedChangeDelegate(IntPtr trackid, int selected);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnSendPanChangeDelegate(IntPtr trackid, int send_index, double pan, bool relative);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnSendVolumeChangeDelegate(IntPtr trackid, int send_index, double volume, bool relative);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_OnSoloChangeDelegate(IntPtr trackid, int solo);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool CSurf_OnSoloChangeExDelegate(IntPtr trackid, int solo, bool allowgang);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnStopDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnTempoChangeDelegate(double bpm);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnTrackSelectionDelegate(IntPtr trackid);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnVolumeChangeDelegate(IntPtr trackid, double volume, bool relative);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnVolumeChangeExDelegate(IntPtr trackid, double volume, bool relative, bool allowGang);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnWidthChangeDelegate(IntPtr trackid, double width, bool relative);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double CSurf_OnWidthChangeExDelegate(IntPtr trackid, double width, bool relative, bool allowGang);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_OnZoomDelegate(int xdir, int ydir);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_ResetAllCachedVolPanStatesDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_ScrubAmtDelegate(double amt);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetAutoModeDelegate(int mode, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetPlayStateDelegate(bool play, bool pause, bool rec, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetRepeatStateDelegate(bool rep, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetSurfaceMuteDelegate(IntPtr trackid, bool mute, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetSurfacePanDelegate(IntPtr trackid, double pan, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetSurfaceRecArmDelegate(IntPtr trackid, bool recarm, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetSurfaceSelectedDelegate(IntPtr trackid, bool selected, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetSurfaceSoloDelegate(IntPtr trackid, bool solo, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetSurfaceVolumeDelegate(IntPtr trackid, double volume, IntPtr ignoresurf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void CSurf_SetTrackListChangeDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr CSurf_TrackFromIDDelegate(int idx, bool mcpView);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int CSurf_TrackToIDDelegate(IntPtr track, bool mcpView);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double DB2SLIDERDelegate(double x);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool DeleteActionShortcutDelegate(IntPtr section, int cmdID, int shortcutidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DeleteExtStateDelegate([MarshalAs(UnmanagedType.LPStr)] String section, [MarshalAs(UnmanagedType.LPStr)] String key, bool persist);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool DeleteProjectMarkerDelegate(IntPtr proj, int markrgnindexnumber, bool isrgn);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool DeleteProjectMarkerByIndexDelegate(IntPtr proj, int markrgnidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int DeleteTakeStretchMarkersDelegate(IntPtr take, int idx, IntPtr countInOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DeleteTrackDelegate(IntPtr tr);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool DeleteTrackMediaItemDelegate(IntPtr tr, IntPtr it);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DestroyAudioAccessorDelegate(IntPtr accessor);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DestroyLocalOscHandlerDelegate(IntPtr local_osc_handler);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool DoActionShortcutDialogDelegate(IntPtr hwnd, IntPtr section, int cmdID, int shortcutidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Dock_UpdateDockIDDelegate([MarshalAs(UnmanagedType.LPStr)] String ident_str, int whichDock);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int DockIsChildOfDockDelegate(IntPtr hwnd, IntPtr isFloatingDockerOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DockWindowActivateDelegate(IntPtr hwnd);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DockWindowAddDelegate(IntPtr hwnd, [MarshalAs(UnmanagedType.LPStr)] String name, int pos, bool allowShow);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DockWindowAddExDelegate(IntPtr hwnd, [MarshalAs(UnmanagedType.LPStr)] String name, [MarshalAs(UnmanagedType.LPStr)] String identstr, bool allowShow);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DockWindowRefreshDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DockWindowRefreshForHWNDDelegate(IntPtr hwnd);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void DockWindowRemoveDelegate(IntPtr hwnd);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool DuplicateCustomizableMenuDelegate(IntPtr srcmenu, IntPtr destmenu);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void EnsureNotCompletelyOffscreenDelegate(IntPtr rOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool EnumPitchShiftModesDelegate(int mode, IntPtr strOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr EnumPitchShiftSubModesDelegate(int mode, int submode);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int EnumProjectMarkersDelegate(int idx, IntPtr isrgnOut, IntPtr posOut, IntPtr rgnendOut, IntPtr nameOut, IntPtr markrgnindexnumberOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int EnumProjectMarkers2Delegate(IntPtr proj, int idx, IntPtr isrgnOut, IntPtr posOut, IntPtr rgnendOut, IntPtr nameOut, IntPtr markrgnindexnumberOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int EnumProjectMarkers3Delegate(IntPtr proj, int idx, IntPtr isrgnOut, IntPtr posOut, IntPtr rgnendOut, IntPtr nameOut, IntPtr markrgnindexnumberOut, IntPtr colorOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr EnumProjectsDelegate(int idx, [MarshalAs(UnmanagedType.LPStr)] String projfn, int projfn_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr EnumRegionRenderMatrixDelegate(IntPtr proj, int regionindex, int rendertrack);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool EnumTrackMIDIProgramNamesDelegate(int track, int programNumber, [MarshalAs(UnmanagedType.LPStr)] String programName, int programName_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool EnumTrackMIDIProgramNamesExDelegate(IntPtr proj, IntPtr track, int programNumber, [MarshalAs(UnmanagedType.LPStr)] String programName, int programName_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool file_existsDelegate([MarshalAs(UnmanagedType.LPStr)] String path);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void format_timestrDelegate(double tpos, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void format_timestr_lenDelegate(double tpos, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz, double offset, int modeoverride);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void format_timestr_posDelegate(double tpos, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz, int modeoverride);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void FreeHeapPtrDelegate(IntPtr ptr);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void genGuidDelegate(IntPtr g);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr get_config_varDelegate([MarshalAs(UnmanagedType.LPStr)] String name, IntPtr szOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr get_ini_fileDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr get_midi_config_varDelegate([MarshalAs(UnmanagedType.LPStr)] String name, IntPtr szOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetActionShortcutDescDelegate(IntPtr section, int cmdID, int shortcutidx, [MarshalAs(UnmanagedType.LPStr)] String desc, int desclen);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetActiveTakeDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetAppVersionDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetAudioAccessorEndTimeDelegate(IntPtr accessor);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetAudioAccessorHashDelegate(IntPtr accessor, [MarshalAs(UnmanagedType.LPStr)] String hashNeed128);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetAudioAccessorSamplesDelegate(IntPtr accessor, int samplerate, int numchannels, double starttime_sec, int numsamplesperchannel, IntPtr samplebuffer);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetAudioAccessorStartTimeDelegate(IntPtr accessor);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetColorThemeDelegate(int idx, int defval);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetColorThemeStructDelegate(IntPtr szOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetConfigWantsDockDelegate([MarshalAs(UnmanagedType.LPStr)] String ident_str);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetContextMenuDelegate(int idx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetCurrentProjectInLoadSaveDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetCursorContextDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetCursorPositionDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetCursorPositionExDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetDisplayedMediaItemColorDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetDisplayedMediaItemColor2Delegate(IntPtr item, IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetEnvelopeNameDelegate(IntPtr env, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetExePathDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetExtStateDelegate([MarshalAs(UnmanagedType.LPStr)] String section, [MarshalAs(UnmanagedType.LPStr)] String key);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetFocusedFXDelegate(IntPtr tracknumberOut, IntPtr itemnumberOut, IntPtr fxnumberOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetFreeDiskSpaceForRecordPathDelegate(IntPtr proj, int pathidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetHZoomLevelDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetIconThemePointerDelegate([MarshalAs(UnmanagedType.LPStr)] String name);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetIconThemeStructDelegate(IntPtr szOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetInputChannelNameDelegate(int channelIndex);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetInputOutputLatencyDelegate(IntPtr inputlatencyOut, IntPtr outputLatencyOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetItemEditingTime2Delegate(IntPtr which_itemOut, IntPtr flagsOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetItemProjectContextDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetLastMarkerAndCurRegionDelegate(IntPtr proj, double time, IntPtr markeridxOut, IntPtr regionidxOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetLastTouchedFXDelegate(IntPtr tracknumberOut, IntPtr fxnumberOut, IntPtr paramnumberOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetLastTouchedTrackDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMainHwndDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetMasterMuteSoloFlagsDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMasterTrackDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetMasterTrackVisibilityDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetMaxMidiInputsDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetMaxMidiOutputsDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMediaItemDelegate(IntPtr proj, int itemidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMediaItem_TrackDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetMediaItemInfo_ValueDelegate(IntPtr item, [MarshalAs(UnmanagedType.LPStr)] String parmname);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetMediaItemNumTakesDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMediaItemTakeDelegate(IntPtr item, int tk);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMediaItemTake_ItemDelegate(IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMediaItemTake_SourceDelegate(IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMediaItemTake_TrackDelegate(IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMediaItemTakeByGUIDDelegate(IntPtr project, IntPtr guid);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetMediaItemTakeInfo_ValueDelegate(IntPtr take, [MarshalAs(UnmanagedType.LPStr)] String parmname);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMediaItemTrackDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetMediaSourceFileNameDelegate(IntPtr source, [MarshalAs(UnmanagedType.LPStr)] String filenamebuf, int filenamebuf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetMediaSourceNumChannelsDelegate(IntPtr source);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetMediaSourceSampleRateDelegate(IntPtr source);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetMediaSourceTypeDelegate(IntPtr source, [MarshalAs(UnmanagedType.LPStr)] String typebuf, int typebuf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetMediaTrackInfo_ValueDelegate(IntPtr tr, [MarshalAs(UnmanagedType.LPStr)] String parmname);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMidiInputDelegate(int idx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetMIDIInputNameDelegate(int dev, [MarshalAs(UnmanagedType.LPStr)] String nameout, int nameout_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMidiOutputDelegate(int idx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetMIDIOutputNameDelegate(int dev, [MarshalAs(UnmanagedType.LPStr)] String nameout, int nameout_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetMixerScrollDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetMouseModifierDelegate([MarshalAs(UnmanagedType.LPStr)] String context, int modifier_flag, [MarshalAs(UnmanagedType.LPStr)] String action, int action_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetNumAudioInputsDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetNumAudioOutputsDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetNumMIDIInputsDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetNumMIDIOutputsDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetNumTracksDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetOutputChannelNameDelegate(int channelIndex);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetOutputLatencyDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetParentTrackDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetPeakFileNameDelegate([MarshalAs(UnmanagedType.LPStr)] String fn, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetPeakFileNameExDelegate([MarshalAs(UnmanagedType.LPStr)] String fn, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz, bool forWrite);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetPeakFileNameEx2Delegate([MarshalAs(UnmanagedType.LPStr)] String fn, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz, bool forWrite, [MarshalAs(UnmanagedType.LPStr)] String peaksfileextension);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetPeaksBitmapDelegate(IntPtr pks, double maxamp, int w, int h, IntPtr bmp);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetPlayPositionDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetPlayPosition2Delegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetPlayPosition2ExDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double GetPlayPositionExDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetPlayStateDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetPlayStateExDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetProjectPathDelegate([MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetProjectPathExDelegate(IntPtr proj, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetProjectStateChangeCountDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetProjectTimeSignatureDelegate(IntPtr bpmOut, IntPtr bpiOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetProjectTimeSignature2Delegate(IntPtr proj, IntPtr bpmOut, IntPtr bpiOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetResourcePathDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSelectedMediaItemDelegate(IntPtr proj, int selitem);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSelectedTrackDelegate(IntPtr proj, int seltrackidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSelectedTrackEnvelopeDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetSet_ArrangeView2Delegate(IntPtr proj, bool isSet, int screen_x_start, int screen_x_end, IntPtr start_timeOut, IntPtr end_timeOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetSet_LoopTimeRangeDelegate(bool isSet, bool isLoop, IntPtr startOut, IntPtr endOut, bool allowautoseek);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetSet_LoopTimeRange2Delegate(IntPtr proj, bool isSet, bool isLoop, IntPtr startOut, IntPtr endOut, bool allowautoseek);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetSetEnvelopeStateDelegate(IntPtr env, [MarshalAs(UnmanagedType.LPStr)] String str, int str_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetSetEnvelopeState2Delegate(IntPtr env, [MarshalAs(UnmanagedType.LPStr)] String str, int str_sz, bool isundo);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetSetItemStateDelegate(IntPtr item, [MarshalAs(UnmanagedType.LPStr)] String str, int str_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetSetItemState2Delegate(IntPtr item, [MarshalAs(UnmanagedType.LPStr)] String str, int str_sz, bool isundo);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSetMediaItemInfoDelegate(IntPtr item, [MarshalAs(UnmanagedType.LPStr)] String parmname, IntPtr setNewValue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSetMediaItemTakeInfoDelegate(IntPtr tk, [MarshalAs(UnmanagedType.LPStr)] String parmname, IntPtr setNewValue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetSetMediaItemTakeInfo_StringDelegate(IntPtr tk, [MarshalAs(UnmanagedType.LPStr)] String parmname, IntPtr stringNeedBig, bool setnewvalue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSetMediaTrackInfoDelegate(IntPtr tr, [MarshalAs(UnmanagedType.LPStr)] String parmname, IntPtr setNewVanue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetSetMediaTrackInfo_StringDelegate(IntPtr tr, [MarshalAs(UnmanagedType.LPStr)] String parmname, IntPtr stringNeedBig, bool setnewvalue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSetObjectStateDelegate(IntPtr obj, [MarshalAs(UnmanagedType.LPStr)] String str);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSetObjectState2Delegate(IntPtr obj, [MarshalAs(UnmanagedType.LPStr)] String str, bool isundo);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetSetRepeatDelegate(int val);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetSetRepeatExDelegate(IntPtr proj, int val);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSetTrackMIDISupportFileDelegate(IntPtr proj, IntPtr track, int which, [MarshalAs(UnmanagedType.LPStr)] String filename);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSetTrackSendInfoDelegate(IntPtr tr, int category, int sendidx, [MarshalAs(UnmanagedType.LPStr)] String parmname, IntPtr setNewValue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetSetTrackStateDelegate(IntPtr track, [MarshalAs(UnmanagedType.LPStr)] String str, int str_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetSetTrackState2Delegate(IntPtr track, [MarshalAs(UnmanagedType.LPStr)] String str, int str_sz, bool isundo);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetSubProjectFromSourceDelegate(IntPtr src);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTakeDelegate(IntPtr item, int takeidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTakeEnvelopeByNameDelegate(IntPtr take, [MarshalAs(UnmanagedType.LPStr)] String envname);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTakeNameDelegate(IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetTakeNumStretchMarkersDelegate(IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetTakeStretchMarkerDelegate(IntPtr take, int idx, IntPtr posOut, IntPtr srcposOutOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTCPFXParmDelegate(IntPtr project, IntPtr track, int index, IntPtr fxindexOut, IntPtr parmidxOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTempoMatchPlayRateDelegate(IntPtr source, double srcscale, double position, double mult, IntPtr rateOut, IntPtr targetlenOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTempoTimeSigMarkerDelegate(IntPtr proj, int ptidx, IntPtr timeposOut, IntPtr measureposOut, IntPtr beatposOut, IntPtr bpmOut, IntPtr timesig_numOut, IntPtr timesig_denomOut, IntPtr lineartempoOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetToggleCommandStateDelegate(int command_id);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetToggleCommandState2Delegate(IntPtr section, int cmdID);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetToggleCommandStateThroughHooksDelegate(IntPtr sec, int command_id);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTooltipWindowDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackDelegate(IntPtr proj, int trackidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetTrackAutomationModeDelegate(IntPtr tr);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetTrackColorDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackEnvelopeDelegate(IntPtr track, int envidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackEnvelopeByNameDelegate(IntPtr track, [MarshalAs(UnmanagedType.LPStr)] String envname);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackGUIDDelegate(IntPtr tr);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackInfoDelegate(IntPtr track, IntPtr flags);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackMediaItemDelegate(IntPtr tr, int itemidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackMIDINoteNameDelegate(int track, int note, int chan);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackMIDINoteNameExDelegate(IntPtr proj, IntPtr track, int note, int chan);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GetTrackMIDINoteRangeDelegate(IntPtr proj, IntPtr track, IntPtr note_loOut, IntPtr note_hiOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetTrackNumMediaItemsDelegate(IntPtr tr);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GetTrackNumSendsDelegate(IntPtr tr, int category);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackReceiveNameDelegate(IntPtr track, int recv_index, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackReceiveUIMuteDelegate(IntPtr track, int recv_index, IntPtr muteOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackReceiveUIVolPanDelegate(IntPtr track, int recv_index, IntPtr volumeOut, IntPtr panOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackSendNameDelegate(IntPtr track, int send_index, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackSendUIMuteDelegate(IntPtr track, int send_index, IntPtr muteOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackSendUIVolPanDelegate(IntPtr track, int send_index, IntPtr volumeOut, IntPtr panOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr GetTrackStateDelegate(IntPtr track, IntPtr flagsOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackUIMuteDelegate(IntPtr track, IntPtr muteOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackUIPanDelegate(IntPtr track, IntPtr pan1Out, IntPtr pan2Out, IntPtr panmodeOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetTrackUIVolPanDelegate(IntPtr track, IntPtr volumeOut, IntPtr panOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetUserFileNameForReadDelegate([MarshalAs(UnmanagedType.LPStr)] String filenameNeed4096, [MarshalAs(UnmanagedType.LPStr)] String title, [MarshalAs(UnmanagedType.LPStr)] String defext);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool GetUserInputsDelegate([MarshalAs(UnmanagedType.LPStr)] String title, int num_inputs, [MarshalAs(UnmanagedType.LPStr)] String captions_csv, [MarshalAs(UnmanagedType.LPStr)] String retvals_csv, int retvals_csv_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GoToMarkerDelegate(IntPtr proj, int marker_index, bool use_timeline_order);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void GoToRegionDelegate(IntPtr proj, int region_index, bool use_timeline_order);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GR_SelectColorDelegate(IntPtr hwnd, IntPtr colorOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int GSC_mainwndDelegate(int t);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void guidToStringDelegate(IntPtr g, [MarshalAs(UnmanagedType.LPStr)] String destNeed64);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool HasExtStateDelegate([MarshalAs(UnmanagedType.LPStr)] String section, [MarshalAs(UnmanagedType.LPStr)] String key);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr HasTrackMIDIProgramsDelegate(int track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr HasTrackMIDIProgramsExDelegate(IntPtr proj, IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Help_SetDelegate([MarshalAs(UnmanagedType.LPStr)] String helpstring, bool is_temporary_help);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void HiresPeaksFromSourceDelegate(IntPtr src, IntPtr block);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void image_resolve_fnDelegate([MarshalAs(UnmanagedType.LPStr)] String in_, [MarshalAs(UnmanagedType.LPStr)] String out_, int out_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int InsertMediaDelegate([MarshalAs(UnmanagedType.LPStr)] String file, int mode);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int InsertMediaSectionDelegate([MarshalAs(UnmanagedType.LPStr)] String file, int mode, double startpct, double endpct, double pitchshift);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void InsertTrackAtIndexDelegate(int idx, bool wantDefaults);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int IsInRealTimeAudioDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool IsItemTakeActiveForPlaybackDelegate(IntPtr item, IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool IsMediaExtensionDelegate([MarshalAs(UnmanagedType.LPStr)] String ext, bool wantOthers);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool IsMediaItemSelectedDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool IsTrackSelectedDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool IsTrackVisibleDelegate(IntPtr track, bool mixer);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int kbd_enumerateActionsDelegate(IntPtr section, int idx, IntPtr nameOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void kbd_formatKeyNameDelegate(IntPtr ac, [MarshalAs(UnmanagedType.LPStr)] String s);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void kbd_getCommandNameDelegate(int cmd, [MarshalAs(UnmanagedType.LPStr)] String s, IntPtr section);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr kbd_getTextFromCmdDelegate(int cmd, IntPtr section);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int KBD_OnMainActionExDelegate(int cmd, int val, int valhw, int relmode, IntPtr hwnd, IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void kbd_OnMidiEventDelegate(IntPtr evt, int dev_index);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void kbd_OnMidiListDelegate(IntPtr list, int dev_index);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void kbd_ProcessActionsMenuDelegate(IntPtr menu, IntPtr section);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool kbd_processMidiEventActionExDelegate(IntPtr evt, IntPtr section, IntPtr hwndCtx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void kbd_reprocessMenuDelegate(IntPtr menu, IntPtr section);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool kbd_RunCommandThroughHooksDelegate(IntPtr section, IntPtr actionCommandID, IntPtr val, IntPtr valhw, IntPtr relmode, IntPtr hwnd);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int kbd_translateAcceleratorDelegate(IntPtr hwnd, IntPtr msg, IntPtr section);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool kbd_translateMouseDelegate(IntPtr winmsg, IntPtr midimsg);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool Loop_OnArrowDelegate(IntPtr project, int direction);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Main_OnCommandDelegate(int command, int flag);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Main_OnCommandExDelegate(int command, int flag, IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Main_openProjectDelegate([MarshalAs(UnmanagedType.LPStr)] String name);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Main_UpdateLoopInfoDelegate(int ignoremask);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void MarkProjectDirtyDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void MarkTrackItemsDirtyDelegate(IntPtr track, IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double Master_GetPlayRateDelegate(IntPtr project);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double Master_GetPlayRateAtTimeDelegate(double time_s, IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double Master_GetTempoDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double Master_NormalizePlayRateDelegate(double playrate, bool isnormalized);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double Master_NormalizeTempoDelegate(double bpm, bool isnormalized);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MBDelegate([MarshalAs(UnmanagedType.LPStr)] String msg, [MarshalAs(UnmanagedType.LPStr)] String title, int type);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MediaItemDescendsFromTrackDelegate(IntPtr item, IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MIDI_CountEvtsDelegate(IntPtr take, IntPtr notecntOut, IntPtr ccevtcntOut, IntPtr textsyxevtcntOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_DeleteCCDelegate(IntPtr take, int ccidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_DeleteEvtDelegate(IntPtr take, int evtidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_DeleteNoteDelegate(IntPtr take, int noteidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_DeleteTextSysexEvtDelegate(IntPtr take, int textsyxevtidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MIDI_EnumSelCCDelegate(IntPtr take, int ccidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MIDI_EnumSelEvtsDelegate(IntPtr take, int evtidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MIDI_EnumSelNotesDelegate(IntPtr take, int noteidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MIDI_EnumSelTextSysexEvtsDelegate(IntPtr take, int textsyxidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr MIDI_eventlist_CreateDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void MIDI_eventlist_DestroyDelegate(IntPtr evtlist);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_GetCCDelegate(IntPtr take, int ccidx, IntPtr selectedOut, IntPtr mutedOut, IntPtr ppqposOut, IntPtr chanmsgOut, IntPtr chanOut, IntPtr msg2Out, IntPtr msg3Out);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_GetEvtDelegate(IntPtr take, int evtidx, IntPtr selectedOut, IntPtr mutedOut, IntPtr ppqposOut, [MarshalAs(UnmanagedType.LPStr)] String msg, IntPtr msg_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_GetNoteDelegate(IntPtr take, int noteidx, IntPtr selectedOut, IntPtr mutedOut, IntPtr startppqposOut, IntPtr endppqposOut, IntPtr chanOut, IntPtr pitchOut, IntPtr velOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double MIDI_GetPPQPos_EndOfMeasureDelegate(IntPtr take, double ppqpos);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double MIDI_GetPPQPos_StartOfMeasureDelegate(IntPtr take, double ppqpos);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double MIDI_GetPPQPosFromProjTimeDelegate(IntPtr take, double projtime);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double MIDI_GetProjTimeFromPPQPosDelegate(IntPtr take, double ppqpos);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_GetTextSysexEvtDelegate(IntPtr take, int textsyxevtidx, IntPtr selectedOutOptional, IntPtr mutedOutOptional, IntPtr ppqposOutOptional, IntPtr typeOutOptional, [MarshalAs(UnmanagedType.LPStr)] String msgOptional, IntPtr msgOptional_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_InsertCCDelegate(IntPtr take, bool selected, bool muted, double ppqpos, int chanmsg, int chan, int msg2, int msg3);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_InsertEvtDelegate(IntPtr take, bool selected, bool muted, double ppqpos, [MarshalAs(UnmanagedType.LPStr)] String msg, int msg_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_InsertNoteDelegate(IntPtr take, bool selected, bool muted, double startppqpos, double endppqpos, int chan, int pitch, int vel);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_InsertTextSysexEvtDelegate(IntPtr take, bool selected, bool muted, double ppqpos, int type, [MarshalAs(UnmanagedType.LPStr)] String msg, int msg_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void midi_reinitDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_SetCCDelegate(IntPtr take, int ccidx, IntPtr selectedInOptional, IntPtr mutedInOptional, IntPtr ppqposInOptional, IntPtr chanmsgInOptional, IntPtr chanInOptional, IntPtr msg2InOptional, IntPtr msg3InOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_SetEvtDelegate(IntPtr take, int evtidx, IntPtr selectedInOptional, IntPtr mutedInOptional, IntPtr ppqposInOptional, [MarshalAs(UnmanagedType.LPStr)] String msgOptional, int msgOptional_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_SetNoteDelegate(IntPtr take, int noteidx, IntPtr selectedInOptional, IntPtr mutedInOptional, IntPtr startppqposInOptional, IntPtr endppqposInOptional, IntPtr chanInOptional, IntPtr pitchInOptional, IntPtr velInOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDI_SetTextSysexEvtDelegate(IntPtr take, int textsyxevtidx, IntPtr selectedInOptional, IntPtr mutedInOptional, IntPtr ppqposInOptional, IntPtr typeInOptional, [MarshalAs(UnmanagedType.LPStr)] String msgOptional, int msgOptional_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr MIDIEditor_GetActiveDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MIDIEditor_GetModeDelegate(IntPtr midieditor);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int MIDIEditor_GetSetting_intDelegate(IntPtr midieditor, [MarshalAs(UnmanagedType.LPStr)] String setting_desc);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDIEditor_GetSetting_strDelegate(IntPtr midieditor, [MarshalAs(UnmanagedType.LPStr)] String setting_desc, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr MIDIEditor_GetTakeDelegate(IntPtr midieditor);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDIEditor_LastFocused_OnCommandDelegate(int command_id, bool islistviewcommand);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MIDIEditor_OnCommandDelegate(IntPtr midieditor, int command_id);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void mkpanstrDelegate([MarshalAs(UnmanagedType.LPStr)] String strNeed64, double pan);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void mkvolpanstrDelegate([MarshalAs(UnmanagedType.LPStr)] String strNeed64, double vol, double pan);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void mkvolstrDelegate([MarshalAs(UnmanagedType.LPStr)] String strNeed64, double vol);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void MoveEditCursorDelegate(double adjamt, bool dosel);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool MoveMediaItemToTrackDelegate(IntPtr item, IntPtr desttr);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void MuteAllTracksDelegate(bool mute);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void my_getViewportDelegate(IntPtr r, IntPtr sr, bool wantWorkArea);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int NamedCommandLookupDelegate([MarshalAs(UnmanagedType.LPStr)] String command_name);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnPauseButtonDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnPauseButtonExDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnPlayButtonDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnPlayButtonExDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnStopButtonDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OnStopButtonExDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void OscLocalMessageToHostDelegate([MarshalAs(UnmanagedType.LPStr)] String message, IntPtr valueInOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double parse_timestrDelegate([MarshalAs(UnmanagedType.LPStr)] String buf);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double parse_timestr_lenDelegate([MarshalAs(UnmanagedType.LPStr)] String buf, double offset, int modeoverride);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double parse_timestr_posDelegate([MarshalAs(UnmanagedType.LPStr)] String buf, int modeoverride);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double parsepanstrDelegate([MarshalAs(UnmanagedType.LPStr)] String str);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Sink_CreateDelegate([MarshalAs(UnmanagedType.LPStr)] String filename, [MarshalAs(UnmanagedType.LPStr)] String cfg, int cfg_sz, int nch, int srate, bool buildpeaks);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Sink_CreateExDelegate(IntPtr proj, [MarshalAs(UnmanagedType.LPStr)] String filename, [MarshalAs(UnmanagedType.LPStr)] String cfg, int cfg_sz, int nch, int srate, bool buildpeaks);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Sink_CreateMIDIFileDelegate([MarshalAs(UnmanagedType.LPStr)] String filename, [MarshalAs(UnmanagedType.LPStr)] String cfg, int cfg_sz, double bpm, int div);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Sink_CreateMIDIFileExDelegate(IntPtr proj, [MarshalAs(UnmanagedType.LPStr)] String filename, [MarshalAs(UnmanagedType.LPStr)] String cfg, int cfg_sz, double bpm, int div);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate uint PCM_Sink_EnumDelegate(int idx, IntPtr descstrOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Sink_GetExtensionDelegate([MarshalAs(UnmanagedType.LPStr)] String data, int data_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Sink_ShowConfigDelegate([MarshalAs(UnmanagedType.LPStr)] String cfg, int cfg_sz, IntPtr hwndParent);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Source_CreateFromFileDelegate([MarshalAs(UnmanagedType.LPStr)] String filename);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Source_CreateFromFileExDelegate([MarshalAs(UnmanagedType.LPStr)] String filename, bool forcenoMidiImp);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Source_CreateFromSimpleDelegate(IntPtr dec, [MarshalAs(UnmanagedType.LPStr)] String fn);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PCM_Source_CreateFromTypeDelegate([MarshalAs(UnmanagedType.LPStr)] String sourcetype);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool PCM_Source_GetSectionInfoDelegate(IntPtr src, IntPtr offsOut, IntPtr lenOut, IntPtr revOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PeakBuild_CreateDelegate(IntPtr src, [MarshalAs(UnmanagedType.LPStr)] String fn, int srate, int nch);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr PeakGet_CreateDelegate([MarshalAs(UnmanagedType.LPStr)] String fn, int srate, int nch);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int PlayPreviewDelegate(IntPtr preview);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int PlayPreviewExDelegate(IntPtr preview, int bufflags, double MSI);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int PlayTrackPreviewDelegate(IntPtr preview);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int PlayTrackPreview2Delegate(IntPtr proj, IntPtr preview);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int PlayTrackPreview2ExDelegate(IntPtr proj, IntPtr preview, int flags, double msi);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr plugin_getapiDelegate([MarshalAs(UnmanagedType.LPStr)] String name);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr plugin_getFilterListDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr plugin_getImportableProjectFilterListDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int plugin_registerDelegate([MarshalAs(UnmanagedType.LPStr)] String name, IntPtr infostruct);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void PluginWantsAlwaysRunFxDelegate(int amt);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void PreventUIRefreshDelegate(int prevent_count);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr projectconfig_var_addrDelegate(IntPtr proj, int idx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int projectconfig_var_getoffsDelegate([MarshalAs(UnmanagedType.LPStr)] String name, IntPtr szOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr ReaperGetPitchShiftAPIDelegate(int version);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void ReaScriptErrorDelegate([MarshalAs(UnmanagedType.LPStr)] String errmsg);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int RecursiveCreateDirectoryDelegate([MarshalAs(UnmanagedType.LPStr)] String path, uint ignored);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void RefreshToolbarDelegate(int command_id);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void relative_fnDelegate([MarshalAs(UnmanagedType.LPStr)] String in_, [MarshalAs(UnmanagedType.LPStr)] String out_, int out_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool RenderFileSectionDelegate([MarshalAs(UnmanagedType.LPStr)] String source_filename, [MarshalAs(UnmanagedType.LPStr)] String target_filename, double start_percent, double end_percent, double playrate);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr Resample_EnumModesDelegate(int mode);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr Resampler_CreateDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void resolve_fnDelegate([MarshalAs(UnmanagedType.LPStr)] String in_, [MarshalAs(UnmanagedType.LPStr)] String out_, int out_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void resolve_fn2Delegate([MarshalAs(UnmanagedType.LPStr)] String in_, [MarshalAs(UnmanagedType.LPStr)] String out_, int out_sz, [MarshalAs(UnmanagedType.LPStr)] String checkSubDirOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void screenset_registerDelegate([MarshalAs(UnmanagedType.LPStr)] String id, IntPtr callbackFunc, IntPtr param);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void screenset_registerNewDelegate([MarshalAs(UnmanagedType.LPStr)] String id, IntPtr callbackFunc, IntPtr param);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void screenset_unregisterDelegate([MarshalAs(UnmanagedType.LPStr)] String id);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void screenset_unregisterByParamDelegate(IntPtr param);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr SectionFromUniqueIDDelegate(int uniqueID);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SelectAllMediaItemsDelegate(IntPtr proj, bool selected);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SelectProjectInstanceDelegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SendLocalOscMessageDelegate(IntPtr local_osc_handler, [MarshalAs(UnmanagedType.LPStr)] String msg, int msglen);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetActiveTakeDelegate(IntPtr take);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetAutomationModeDelegate(int mode, bool onlySel);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetCurrentBPMDelegate(IntPtr __proj, double bpm, bool wantUndo);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetEditCurPosDelegate(double time, bool moveview, bool seekplay);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetEditCurPos2Delegate(IntPtr proj, double time, bool moveview, bool seekplay);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetExtStateDelegate([MarshalAs(UnmanagedType.LPStr)] String section, [MarshalAs(UnmanagedType.LPStr)] String key, [MarshalAs(UnmanagedType.LPStr)] String value, bool persist);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int SetMasterTrackVisibilityDelegate(int flag);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetMediaItemInfo_ValueDelegate(IntPtr item, [MarshalAs(UnmanagedType.LPStr)] String parmname, double newvalue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetMediaItemLengthDelegate(IntPtr item, double length, bool refreshUI);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetMediaItemPositionDelegate(IntPtr item, double position, bool refreshUI);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetMediaItemSelectedDelegate(IntPtr item, bool selected);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetMediaItemTakeInfo_ValueDelegate(IntPtr take, [MarshalAs(UnmanagedType.LPStr)] String parmname, double newvalue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetMediaTrackInfo_ValueDelegate(IntPtr tr, [MarshalAs(UnmanagedType.LPStr)] String parmname, double newvalue);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr SetMixerScrollDelegate(IntPtr leftmosttrack);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetMouseModifierDelegate([MarshalAs(UnmanagedType.LPStr)] String context, int modifier_flag, [MarshalAs(UnmanagedType.LPStr)] String action);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetOnlyTrackSelectedDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetProjectMarkerDelegate(int markrgnindexnumber, bool isrgn, double pos, double rgnend, [MarshalAs(UnmanagedType.LPStr)] String name);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetProjectMarker2Delegate(IntPtr proj, int markrgnindexnumber, bool isrgn, double pos, double rgnend, [MarshalAs(UnmanagedType.LPStr)] String name);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetProjectMarker3Delegate(IntPtr proj, int markrgnindexnumber, bool isrgn, double pos, double rgnend, [MarshalAs(UnmanagedType.LPStr)] String name, int color);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetProjectMarkerByIndexDelegate(IntPtr proj, int markrgnidx, bool isrgn, double pos, double rgnend, int IDnumber, [MarshalAs(UnmanagedType.LPStr)] String name, int color);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetRegionRenderMatrixDelegate(IntPtr proj, int regionindex, IntPtr track, int addorremove);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetRenderLastErrorDelegate([MarshalAs(UnmanagedType.LPStr)] String errorstr);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int SetTakeStretchMarkerDelegate(IntPtr take, int idx, double pos, IntPtr srcposInOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetTempoTimeSigMarkerDelegate(IntPtr proj, int ptidx, double timepos, int measurepos, double beatpos, double bpm, int timesig_num, int timesig_denom, bool lineartempo);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetTrackAutomationModeDelegate(IntPtr tr, int mode);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetTrackColorDelegate(IntPtr track, int color);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetTrackMIDINoteNameDelegate(int track, int note, int chan, [MarshalAs(UnmanagedType.LPStr)] String name);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetTrackMIDINoteNameExDelegate(IntPtr proj, IntPtr track, int note, int chan, [MarshalAs(UnmanagedType.LPStr)] String name);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SetTrackSelectedDelegate(IntPtr track, bool selected);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetTrackSendUIPanDelegate(IntPtr track, int send_idx, double pan, int isend);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool SetTrackSendUIVolDelegate(IntPtr track, int send_idx, double vol, int isend);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void ShowActionListDelegate(IntPtr caller, IntPtr callerWnd);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void ShowConsoleMsgDelegate([MarshalAs(UnmanagedType.LPStr)] String msg);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int ShowMessageBoxDelegate([MarshalAs(UnmanagedType.LPStr)] String msg, [MarshalAs(UnmanagedType.LPStr)] String title, int type);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double SLIDER2DBDelegate(double y);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double SnapToGridDelegate(IntPtr project, double time_pos);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void SoloAllTracksDelegate(int solo);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr SplitMediaItemDelegate(IntPtr item, double position);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int StopPreviewDelegate(IntPtr preview);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int StopTrackPreviewDelegate(IntPtr preview);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int StopTrackPreview2Delegate(IntPtr proj, IntPtr preview);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void stringToGuidDelegate([MarshalAs(UnmanagedType.LPStr)] String str, IntPtr g);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void StuffMIDIMessageDelegate(int mode, int msg1, int msg2, int msg3);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap2_beatsToTimeDelegate(IntPtr proj, double tpos, IntPtr measuresOutOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap2_GetDividedBpmAtTimeDelegate(IntPtr proj, double time);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap2_GetNextChangeTimeDelegate(IntPtr proj, double time);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap2_QNToTimeDelegate(IntPtr proj, double qn);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap2_timeToBeatsDelegate(IntPtr proj, double tpos, IntPtr measuresOutOptional, IntPtr cmlOutOptional, IntPtr fullbeatsOutOptional, IntPtr cdenomOutOptional);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap2_timeToQNDelegate(IntPtr proj, double tpos);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap_GetDividedBpmAtTimeDelegate(double time);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void TimeMap_GetTimeSigAtTimeDelegate(IntPtr proj, double time, IntPtr timesig_numOut, IntPtr timesig_denomOut, IntPtr tempoOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap_QNToTimeDelegate(double qn);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap_QNToTime_absDelegate(IntPtr proj, double qn);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap_timeToQNDelegate(double qn);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TimeMap_timeToQN_absDelegate(IntPtr proj, double tpos);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool ToggleTrackSendUIMuteDelegate(IntPtr track, int send_idx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double Track_GetPeakHoldDBDelegate(IntPtr track, int channel, bool clear);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double Track_GetPeakInfoDelegate(IntPtr track, int channel);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void TrackCtl_SetToolTipDelegate([MarshalAs(UnmanagedType.LPStr)] String fmt, int xpos, int ypos, bool topmost);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_EndParamEditDelegate(IntPtr track, int fx, int param);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_FormatParamValueDelegate(IntPtr track, int fx, int param, double val, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_FormatParamValueNormalizedDelegate(IntPtr track, int fx, int param, double value, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int TrackFX_GetByNameDelegate(IntPtr track, [MarshalAs(UnmanagedType.LPStr)] String fxname, bool instantiate);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int TrackFX_GetChainVisibleDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int TrackFX_GetCountDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetEnabledDelegate(IntPtr track, int fx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int TrackFX_GetEQDelegate(IntPtr track, bool instantiate);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetEQBandEnabledDelegate(IntPtr track, int fxidx, int bandtype, int bandidx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetEQParamDelegate(IntPtr track, int fxidx, int paramidx, IntPtr bandtypeOut, IntPtr bandidxOut, IntPtr paramtypeOut, IntPtr normvalOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr TrackFX_GetFloatingWindowDelegate(IntPtr track, int index);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetFormattedParamValueDelegate(IntPtr track, int fx, int param, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr TrackFX_GetFXGUIDDelegate(IntPtr track, int fx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetFXNameDelegate(IntPtr track, int fx, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int TrackFX_GetInstrumentDelegate(IntPtr track);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int TrackFX_GetNumParamsDelegate(IntPtr track, int fx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetOpenDelegate(IntPtr track, int fx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TrackFX_GetParamDelegate(IntPtr track, int fx, int param, IntPtr minvalOut, IntPtr maxvalOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetParameterStepSizesDelegate(IntPtr track, int fx, int param, IntPtr stepOut, IntPtr smallstepOut, IntPtr largestepOut, IntPtr istoggleOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TrackFX_GetParamExDelegate(IntPtr track, int fx, int param, IntPtr minvalOut, IntPtr maxvalOut, IntPtr midvalOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetParamNameDelegate(IntPtr track, int fx, int param, [MarshalAs(UnmanagedType.LPStr)] String buf, int buf_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate double TrackFX_GetParamNormalizedDelegate(IntPtr track, int fx, int param);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_GetPresetDelegate(IntPtr track, int fx, [MarshalAs(UnmanagedType.LPStr)] String presetname, int presetname_sz);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int TrackFX_GetPresetIndexDelegate(IntPtr track, int fx, IntPtr numberOfPresetsOut);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_NavigatePresetsDelegate(IntPtr track, int fx, int presetmove);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void TrackFX_SetEnabledDelegate(IntPtr track, int fx, bool enabled);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_SetEQBandEnabledDelegate(IntPtr track, int fxidx, int bandtype, int bandidx, bool enable);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_SetEQParamDelegate(IntPtr track, int fxidx, int bandtype, int bandidx, int paramtype, double val, bool isnorm);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void TrackFX_SetOpenDelegate(IntPtr track, int fx, bool open);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_SetParamDelegate(IntPtr track, int fx, int param, double val);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_SetParamNormalizedDelegate(IntPtr track, int fx, int param, double value);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_SetPresetDelegate(IntPtr track, int fx, [MarshalAs(UnmanagedType.LPStr)] String presetname);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool TrackFX_SetPresetByIndexDelegate(IntPtr track, int fx, int idx);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void TrackFX_ShowDelegate(IntPtr track, int index, int showFlag);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void TrackList_AdjustWindowsDelegate(bool isMajor);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void TrackList_UpdateAllExternalSurfacesDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_BeginBlockDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_BeginBlock2Delegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr Undo_CanRedo2Delegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate IntPtr Undo_CanUndo2Delegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int Undo_DoRedo2Delegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int Undo_DoUndo2Delegate(IntPtr proj);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_EndBlockDelegate([MarshalAs(UnmanagedType.LPStr)] String descchange, int extraflags);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_EndBlock2Delegate(IntPtr proj, [MarshalAs(UnmanagedType.LPStr)] String descchange, int extraflags);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_OnStateChangeDelegate([MarshalAs(UnmanagedType.LPStr)] String descchange);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_OnStateChange2Delegate(IntPtr proj, [MarshalAs(UnmanagedType.LPStr)] String descchange);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_OnStateChange_ItemDelegate(IntPtr proj, [MarshalAs(UnmanagedType.LPStr)] String name, IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_OnStateChangeExDelegate([MarshalAs(UnmanagedType.LPStr)] String descchange, int whichStates, int trackparm);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void Undo_OnStateChangeEx2Delegate(IntPtr proj, [MarshalAs(UnmanagedType.LPStr)] String descchange, int whichStates, int trackparm);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void UpdateArrangeDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void UpdateItemInProjectDelegate(IntPtr item);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void UpdateTimelineDelegate();        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool ValidatePtrDelegate(IntPtr pointer, [MarshalAs(UnmanagedType.LPStr)] String ctypename);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void ViewPrefsDelegate(int page, [MarshalAs(UnmanagedType.LPStr)] String pageByName);        

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate bool WDL_VirtualWnd_ScaledBlitBGDelegate(IntPtr dest, IntPtr src, int destx, int desty, int destw, int desth, int clipx, int clipy, int clipw, int cliph, float alpha, int mode);        


    public static class ReaperAPI
    {   
    
    
    public static AddCustomizableMenuDelegate AddCustomizableMenu;
    
    public static AddExtensionsMainMenuDelegate AddExtensionsMainMenu;
    
    public static AddMediaItemToTrackDelegate AddMediaItemToTrack;
    
    public static AddProjectMarkerDelegate AddProjectMarker;
    
    public static AddProjectMarker2Delegate AddProjectMarker2;
    
    public static AddTakeToMediaItemDelegate AddTakeToMediaItem;
    
    public static AddTempoTimeSigMarkerDelegate AddTempoTimeSigMarker;
    
    public static adjustZoomDelegate adjustZoom;
    
    public static AnyTrackSoloDelegate AnyTrackSolo;
    
    public static APITestDelegate APITest;
    
    public static ApplyNudgeDelegate ApplyNudge;
    
    public static Audio_IsPreBufferDelegate Audio_IsPreBuffer;
    
    public static Audio_IsRunningDelegate Audio_IsRunning;
    
    public static Audio_RegHardwareHookDelegate Audio_RegHardwareHook;
    
    public static AudioAccessorValidateStateDelegate AudioAccessorValidateState;
    
    public static BypassFxAllTracksDelegate BypassFxAllTracks;
    
    public static CalculatePeaksDelegate CalculatePeaks;
    
    public static CalculatePeaksFloatSrcPtrDelegate CalculatePeaksFloatSrcPtr;
    
    public static ClearAllRecArmedDelegate ClearAllRecArmed;
    
    public static ClearPeakCacheDelegate ClearPeakCache;
    
    public static CountActionShortcutsDelegate CountActionShortcuts;
    
    public static CountMediaItemsDelegate CountMediaItems;
    
    public static CountProjectMarkersDelegate CountProjectMarkers;
    
    public static CountSelectedMediaItemsDelegate CountSelectedMediaItems;
    
    public static CountSelectedTracksDelegate CountSelectedTracks;
    
    public static CountTakesDelegate CountTakes;
    
    public static CountTCPFXParmsDelegate CountTCPFXParms;
    
    public static CountTempoTimeSigMarkersDelegate CountTempoTimeSigMarkers;
    
    public static CountTrackEnvelopesDelegate CountTrackEnvelopes;
    
    public static CountTrackMediaItemsDelegate CountTrackMediaItems;
    
    public static CountTracksDelegate CountTracks;
    
    public static CreateLocalOscHandlerDelegate CreateLocalOscHandler;
    
    public static CreateMIDIInputDelegate CreateMIDIInput;
    
    public static CreateMIDIOutputDelegate CreateMIDIOutput;
    
    public static CreateNewMIDIItemInProjDelegate CreateNewMIDIItemInProj;
    
    public static CreateTakeAudioAccessorDelegate CreateTakeAudioAccessor;
    
    public static CreateTrackAudioAccessorDelegate CreateTrackAudioAccessor;
    
    public static CSurf_FlushUndoDelegate CSurf_FlushUndo;
    
    public static CSurf_GetTouchStateDelegate CSurf_GetTouchState;
    
    public static CSurf_GoEndDelegate CSurf_GoEnd;
    
    public static CSurf_GoStartDelegate CSurf_GoStart;
    
    public static CSurf_NumTracksDelegate CSurf_NumTracks;
    
    public static CSurf_OnArrowDelegate CSurf_OnArrow;
    
    public static CSurf_OnFwdDelegate CSurf_OnFwd;
    
    public static CSurf_OnFXChangeDelegate CSurf_OnFXChange;
    
    public static CSurf_OnInputMonitorChangeDelegate CSurf_OnInputMonitorChange;
    
    public static CSurf_OnInputMonitorChangeExDelegate CSurf_OnInputMonitorChangeEx;
    
    public static CSurf_OnMuteChangeDelegate CSurf_OnMuteChange;
    
    public static CSurf_OnMuteChangeExDelegate CSurf_OnMuteChangeEx;
    
    public static CSurf_OnOscControlMessageDelegate CSurf_OnOscControlMessage;
    
    public static CSurf_OnPanChangeDelegate CSurf_OnPanChange;
    
    public static CSurf_OnPanChangeExDelegate CSurf_OnPanChangeEx;
    
    public static CSurf_OnPauseDelegate CSurf_OnPause;
    
    public static CSurf_OnPlayDelegate CSurf_OnPlay;
    
    public static CSurf_OnPlayRateChangeDelegate CSurf_OnPlayRateChange;
    
    public static CSurf_OnRecArmChangeDelegate CSurf_OnRecArmChange;
    
    public static CSurf_OnRecArmChangeExDelegate CSurf_OnRecArmChangeEx;
    
    public static CSurf_OnRecordDelegate CSurf_OnRecord;
    
    public static CSurf_OnRecvPanChangeDelegate CSurf_OnRecvPanChange;
    
    public static CSurf_OnRecvVolumeChangeDelegate CSurf_OnRecvVolumeChange;
    
    public static CSurf_OnRewDelegate CSurf_OnRew;
    
    public static CSurf_OnRewFwdDelegate CSurf_OnRewFwd;
    
    public static CSurf_OnScrollDelegate CSurf_OnScroll;
    
    public static CSurf_OnSelectedChangeDelegate CSurf_OnSelectedChange;
    
    public static CSurf_OnSendPanChangeDelegate CSurf_OnSendPanChange;
    
    public static CSurf_OnSendVolumeChangeDelegate CSurf_OnSendVolumeChange;
    
    public static CSurf_OnSoloChangeDelegate CSurf_OnSoloChange;
    
    public static CSurf_OnSoloChangeExDelegate CSurf_OnSoloChangeEx;
    
    public static CSurf_OnStopDelegate CSurf_OnStop;
    
    public static CSurf_OnTempoChangeDelegate CSurf_OnTempoChange;
    
    public static CSurf_OnTrackSelectionDelegate CSurf_OnTrackSelection;
    
    public static CSurf_OnVolumeChangeDelegate CSurf_OnVolumeChange;
    
    public static CSurf_OnVolumeChangeExDelegate CSurf_OnVolumeChangeEx;
    
    public static CSurf_OnWidthChangeDelegate CSurf_OnWidthChange;
    
    public static CSurf_OnWidthChangeExDelegate CSurf_OnWidthChangeEx;
    
    public static CSurf_OnZoomDelegate CSurf_OnZoom;
    
    public static CSurf_ResetAllCachedVolPanStatesDelegate CSurf_ResetAllCachedVolPanStates;
    
    public static CSurf_ScrubAmtDelegate CSurf_ScrubAmt;
    
    public static CSurf_SetAutoModeDelegate CSurf_SetAutoMode;
    
    public static CSurf_SetPlayStateDelegate CSurf_SetPlayState;
    
    public static CSurf_SetRepeatStateDelegate CSurf_SetRepeatState;
    
    public static CSurf_SetSurfaceMuteDelegate CSurf_SetSurfaceMute;
    
    public static CSurf_SetSurfacePanDelegate CSurf_SetSurfacePan;
    
    public static CSurf_SetSurfaceRecArmDelegate CSurf_SetSurfaceRecArm;
    
    public static CSurf_SetSurfaceSelectedDelegate CSurf_SetSurfaceSelected;
    
    public static CSurf_SetSurfaceSoloDelegate CSurf_SetSurfaceSolo;
    
    public static CSurf_SetSurfaceVolumeDelegate CSurf_SetSurfaceVolume;
    
    public static CSurf_SetTrackListChangeDelegate CSurf_SetTrackListChange;
    
    public static CSurf_TrackFromIDDelegate CSurf_TrackFromID;
    
    public static CSurf_TrackToIDDelegate CSurf_TrackToID;
    
    public static DB2SLIDERDelegate DB2SLIDER;
    
    public static DeleteActionShortcutDelegate DeleteActionShortcut;
    
    public static DeleteExtStateDelegate DeleteExtState;
    
    public static DeleteProjectMarkerDelegate DeleteProjectMarker;
    
    public static DeleteProjectMarkerByIndexDelegate DeleteProjectMarkerByIndex;
    
    public static DeleteTakeStretchMarkersDelegate DeleteTakeStretchMarkers;
    
    public static DeleteTrackDelegate DeleteTrack;
    
    public static DeleteTrackMediaItemDelegate DeleteTrackMediaItem;
    
    public static DestroyAudioAccessorDelegate DestroyAudioAccessor;
    
    public static DestroyLocalOscHandlerDelegate DestroyLocalOscHandler;
    
    public static DoActionShortcutDialogDelegate DoActionShortcutDialog;
    
    public static Dock_UpdateDockIDDelegate Dock_UpdateDockID;
    
    public static DockIsChildOfDockDelegate DockIsChildOfDock;
    
    public static DockWindowActivateDelegate DockWindowActivate;
    
    public static DockWindowAddDelegate DockWindowAdd;
    
    public static DockWindowAddExDelegate DockWindowAddEx;
    
    public static DockWindowRefreshDelegate DockWindowRefresh;
    
    public static DockWindowRefreshForHWNDDelegate DockWindowRefreshForHWND;
    
    public static DockWindowRemoveDelegate DockWindowRemove;
    
    public static DuplicateCustomizableMenuDelegate DuplicateCustomizableMenu;
    
    public static EnsureNotCompletelyOffscreenDelegate EnsureNotCompletelyOffscreen;
    
    public static EnumPitchShiftModesDelegate EnumPitchShiftModes;
    
    public static EnumPitchShiftSubModesDelegate EnumPitchShiftSubModes;
    
    public static EnumProjectMarkersDelegate EnumProjectMarkers;
    
    public static EnumProjectMarkers2Delegate EnumProjectMarkers2;
    
    public static EnumProjectMarkers3Delegate EnumProjectMarkers3;
    
    public static EnumProjectsDelegate EnumProjects;
    
    public static EnumRegionRenderMatrixDelegate EnumRegionRenderMatrix;
    
    public static EnumTrackMIDIProgramNamesDelegate EnumTrackMIDIProgramNames;
    
    public static EnumTrackMIDIProgramNamesExDelegate EnumTrackMIDIProgramNamesEx;
    
    public static file_existsDelegate file_exists;
    
    public static format_timestrDelegate format_timestr;
    
    public static format_timestr_lenDelegate format_timestr_len;
    
    public static format_timestr_posDelegate format_timestr_pos;
    
    public static FreeHeapPtrDelegate FreeHeapPtr;
    
    public static genGuidDelegate genGuid;
    
    public static get_config_varDelegate get_config_var;
    
    public static get_ini_fileDelegate get_ini_file;
    
    public static get_midi_config_varDelegate get_midi_config_var;
    
    public static GetActionShortcutDescDelegate GetActionShortcutDesc;
    
    public static GetActiveTakeDelegate GetActiveTake;
    
    public static GetAppVersionDelegate GetAppVersion;
    
    public static GetAudioAccessorEndTimeDelegate GetAudioAccessorEndTime;
    
    public static GetAudioAccessorHashDelegate GetAudioAccessorHash;
    
    public static GetAudioAccessorSamplesDelegate GetAudioAccessorSamples;
    
    public static GetAudioAccessorStartTimeDelegate GetAudioAccessorStartTime;
    
    public static GetColorThemeDelegate GetColorTheme;
    
    public static GetColorThemeStructDelegate GetColorThemeStruct;
    
    public static GetConfigWantsDockDelegate GetConfigWantsDock;
    
    public static GetContextMenuDelegate GetContextMenu;
    
    public static GetCurrentProjectInLoadSaveDelegate GetCurrentProjectInLoadSave;
    
    public static GetCursorContextDelegate GetCursorContext;
    
    public static GetCursorPositionDelegate GetCursorPosition;
    
    public static GetCursorPositionExDelegate GetCursorPositionEx;
    
    public static GetDisplayedMediaItemColorDelegate GetDisplayedMediaItemColor;
    
    public static GetDisplayedMediaItemColor2Delegate GetDisplayedMediaItemColor2;
    
    public static GetEnvelopeNameDelegate GetEnvelopeName;
    
    public static GetExePathDelegate GetExePath;
    
    public static GetExtStateDelegate GetExtState;
    
    public static GetFocusedFXDelegate GetFocusedFX;
    
    public static GetFreeDiskSpaceForRecordPathDelegate GetFreeDiskSpaceForRecordPath;
    
    public static GetHZoomLevelDelegate GetHZoomLevel;
    
    public static GetIconThemePointerDelegate GetIconThemePointer;
    
    public static GetIconThemeStructDelegate GetIconThemeStruct;
    
    public static GetInputChannelNameDelegate GetInputChannelName;
    
    public static GetInputOutputLatencyDelegate GetInputOutputLatency;
    
    public static GetItemEditingTime2Delegate GetItemEditingTime2;
    
    public static GetItemProjectContextDelegate GetItemProjectContext;
    
    public static GetLastMarkerAndCurRegionDelegate GetLastMarkerAndCurRegion;
    
    public static GetLastTouchedFXDelegate GetLastTouchedFX;
    
    public static GetLastTouchedTrackDelegate GetLastTouchedTrack;
    
    public static GetMainHwndDelegate GetMainHwnd;
    
    public static GetMasterMuteSoloFlagsDelegate GetMasterMuteSoloFlags;
    
    public static GetMasterTrackDelegate GetMasterTrack;
    
    public static GetMasterTrackVisibilityDelegate GetMasterTrackVisibility;
    
    public static GetMaxMidiInputsDelegate GetMaxMidiInputs;
    
    public static GetMaxMidiOutputsDelegate GetMaxMidiOutputs;
    
    public static GetMediaItemDelegate GetMediaItem;
    
    public static GetMediaItem_TrackDelegate GetMediaItem_Track;
    
    public static GetMediaItemInfo_ValueDelegate GetMediaItemInfo_Value;
    
    public static GetMediaItemNumTakesDelegate GetMediaItemNumTakes;
    
    public static GetMediaItemTakeDelegate GetMediaItemTake;
    
    public static GetMediaItemTake_ItemDelegate GetMediaItemTake_Item;
    
    public static GetMediaItemTake_SourceDelegate GetMediaItemTake_Source;
    
    public static GetMediaItemTake_TrackDelegate GetMediaItemTake_Track;
    
    public static GetMediaItemTakeByGUIDDelegate GetMediaItemTakeByGUID;
    
    public static GetMediaItemTakeInfo_ValueDelegate GetMediaItemTakeInfo_Value;
    
    public static GetMediaItemTrackDelegate GetMediaItemTrack;
    
    public static GetMediaSourceFileNameDelegate GetMediaSourceFileName;
    
    public static GetMediaSourceNumChannelsDelegate GetMediaSourceNumChannels;
    
    public static GetMediaSourceSampleRateDelegate GetMediaSourceSampleRate;
    
    public static GetMediaSourceTypeDelegate GetMediaSourceType;
    
    public static GetMediaTrackInfo_ValueDelegate GetMediaTrackInfo_Value;
    
    public static GetMidiInputDelegate GetMidiInput;
    
    public static GetMIDIInputNameDelegate GetMIDIInputName;
    
    public static GetMidiOutputDelegate GetMidiOutput;
    
    public static GetMIDIOutputNameDelegate GetMIDIOutputName;
    
    public static GetMixerScrollDelegate GetMixerScroll;
    
    public static GetMouseModifierDelegate GetMouseModifier;
    
    public static GetNumAudioInputsDelegate GetNumAudioInputs;
    
    public static GetNumAudioOutputsDelegate GetNumAudioOutputs;
    
    public static GetNumMIDIInputsDelegate GetNumMIDIInputs;
    
    public static GetNumMIDIOutputsDelegate GetNumMIDIOutputs;
    
    public static GetNumTracksDelegate GetNumTracks;
    
    public static GetOutputChannelNameDelegate GetOutputChannelName;
    
    public static GetOutputLatencyDelegate GetOutputLatency;
    
    public static GetParentTrackDelegate GetParentTrack;
    
    public static GetPeakFileNameDelegate GetPeakFileName;
    
    public static GetPeakFileNameExDelegate GetPeakFileNameEx;
    
    public static GetPeakFileNameEx2Delegate GetPeakFileNameEx2;
    
    public static GetPeaksBitmapDelegate GetPeaksBitmap;
    
    public static GetPlayPositionDelegate GetPlayPosition;
    
    public static GetPlayPosition2Delegate GetPlayPosition2;
    
    public static GetPlayPosition2ExDelegate GetPlayPosition2Ex;
    
    public static GetPlayPositionExDelegate GetPlayPositionEx;
    
    public static GetPlayStateDelegate GetPlayState;
    
    public static GetPlayStateExDelegate GetPlayStateEx;
    
    public static GetProjectPathDelegate GetProjectPath;
    
    public static GetProjectPathExDelegate GetProjectPathEx;
    
    public static GetProjectStateChangeCountDelegate GetProjectStateChangeCount;
    
    public static GetProjectTimeSignatureDelegate GetProjectTimeSignature;
    
    public static GetProjectTimeSignature2Delegate GetProjectTimeSignature2;
    
    public static GetResourcePathDelegate GetResourcePath;
    
    public static GetSelectedMediaItemDelegate GetSelectedMediaItem;
    
    public static GetSelectedTrackDelegate GetSelectedTrack;
    
    public static GetSelectedTrackEnvelopeDelegate GetSelectedTrackEnvelope;
    
    public static GetSet_ArrangeView2Delegate GetSet_ArrangeView2;
    
    public static GetSet_LoopTimeRangeDelegate GetSet_LoopTimeRange;
    
    public static GetSet_LoopTimeRange2Delegate GetSet_LoopTimeRange2;
    
    public static GetSetEnvelopeStateDelegate GetSetEnvelopeState;
    
    public static GetSetEnvelopeState2Delegate GetSetEnvelopeState2;
    
    public static GetSetItemStateDelegate GetSetItemState;
    
    public static GetSetItemState2Delegate GetSetItemState2;
    
    public static GetSetMediaItemInfoDelegate GetSetMediaItemInfo;
    
    public static GetSetMediaItemTakeInfoDelegate GetSetMediaItemTakeInfo;
    
    public static GetSetMediaItemTakeInfo_StringDelegate GetSetMediaItemTakeInfo_String;
    
    public static GetSetMediaTrackInfoDelegate GetSetMediaTrackInfo;
    
    public static GetSetMediaTrackInfo_StringDelegate GetSetMediaTrackInfo_String;
    
    public static GetSetObjectStateDelegate GetSetObjectState;
    
    public static GetSetObjectState2Delegate GetSetObjectState2;
    
    public static GetSetRepeatDelegate GetSetRepeat;
    
    public static GetSetRepeatExDelegate GetSetRepeatEx;
    
    public static GetSetTrackMIDISupportFileDelegate GetSetTrackMIDISupportFile;
    
    public static GetSetTrackSendInfoDelegate GetSetTrackSendInfo;
    
    public static GetSetTrackStateDelegate GetSetTrackState;
    
    public static GetSetTrackState2Delegate GetSetTrackState2;
    
    public static GetSubProjectFromSourceDelegate GetSubProjectFromSource;
    
    public static GetTakeDelegate GetTake;
    
    public static GetTakeEnvelopeByNameDelegate GetTakeEnvelopeByName;
    
    public static GetTakeNameDelegate GetTakeName;
    
    public static GetTakeNumStretchMarkersDelegate GetTakeNumStretchMarkers;
    
    public static GetTakeStretchMarkerDelegate GetTakeStretchMarker;
    
    public static GetTCPFXParmDelegate GetTCPFXParm;
    
    public static GetTempoMatchPlayRateDelegate GetTempoMatchPlayRate;
    
    public static GetTempoTimeSigMarkerDelegate GetTempoTimeSigMarker;
    
    public static GetToggleCommandStateDelegate GetToggleCommandState;
    
    public static GetToggleCommandState2Delegate GetToggleCommandState2;
    
    public static GetToggleCommandStateThroughHooksDelegate GetToggleCommandStateThroughHooks;
    
    public static GetTooltipWindowDelegate GetTooltipWindow;
    
    public static GetTrackDelegate GetTrack;
    
    public static GetTrackAutomationModeDelegate GetTrackAutomationMode;
    
    public static GetTrackColorDelegate GetTrackColor;
    
    public static GetTrackEnvelopeDelegate GetTrackEnvelope;
    
    public static GetTrackEnvelopeByNameDelegate GetTrackEnvelopeByName;
    
    public static GetTrackGUIDDelegate GetTrackGUID;
    
    public static GetTrackInfoDelegate GetTrackInfo;
    
    public static GetTrackMediaItemDelegate GetTrackMediaItem;
    
    public static GetTrackMIDINoteNameDelegate GetTrackMIDINoteName;
    
    public static GetTrackMIDINoteNameExDelegate GetTrackMIDINoteNameEx;
    
    public static GetTrackMIDINoteRangeDelegate GetTrackMIDINoteRange;
    
    public static GetTrackNumMediaItemsDelegate GetTrackNumMediaItems;
    
    public static GetTrackNumSendsDelegate GetTrackNumSends;
    
    public static GetTrackReceiveNameDelegate GetTrackReceiveName;
    
    public static GetTrackReceiveUIMuteDelegate GetTrackReceiveUIMute;
    
    public static GetTrackReceiveUIVolPanDelegate GetTrackReceiveUIVolPan;
    
    public static GetTrackSendNameDelegate GetTrackSendName;
    
    public static GetTrackSendUIMuteDelegate GetTrackSendUIMute;
    
    public static GetTrackSendUIVolPanDelegate GetTrackSendUIVolPan;
    
    public static GetTrackStateDelegate GetTrackState;
    
    public static GetTrackUIMuteDelegate GetTrackUIMute;
    
    public static GetTrackUIPanDelegate GetTrackUIPan;
    
    public static GetTrackUIVolPanDelegate GetTrackUIVolPan;
    
    public static GetUserFileNameForReadDelegate GetUserFileNameForRead;
    
    public static GetUserInputsDelegate GetUserInputs;
    
    public static GoToMarkerDelegate GoToMarker;
    
    public static GoToRegionDelegate GoToRegion;
    
    public static GR_SelectColorDelegate GR_SelectColor;
    
    public static GSC_mainwndDelegate GSC_mainwnd;
    
    public static guidToStringDelegate guidToString;
    
    public static HasExtStateDelegate HasExtState;
    
    public static HasTrackMIDIProgramsDelegate HasTrackMIDIPrograms;
    
    public static HasTrackMIDIProgramsExDelegate HasTrackMIDIProgramsEx;
    
    public static Help_SetDelegate Help_Set;
    
    public static HiresPeaksFromSourceDelegate HiresPeaksFromSource;
    
    public static image_resolve_fnDelegate image_resolve_fn;
    
    public static InsertMediaDelegate InsertMedia;
    
    public static InsertMediaSectionDelegate InsertMediaSection;
    
    public static InsertTrackAtIndexDelegate InsertTrackAtIndex;
    
    public static IsInRealTimeAudioDelegate IsInRealTimeAudio;
    
    public static IsItemTakeActiveForPlaybackDelegate IsItemTakeActiveForPlayback;
    
    public static IsMediaExtensionDelegate IsMediaExtension;
    
    public static IsMediaItemSelectedDelegate IsMediaItemSelected;
    
    public static IsTrackSelectedDelegate IsTrackSelected;
    
    public static IsTrackVisibleDelegate IsTrackVisible;
    
    public static kbd_enumerateActionsDelegate kbd_enumerateActions;
    
    public static kbd_formatKeyNameDelegate kbd_formatKeyName;
    
    public static kbd_getCommandNameDelegate kbd_getCommandName;
    
    public static kbd_getTextFromCmdDelegate kbd_getTextFromCmd;
    
    public static KBD_OnMainActionExDelegate KBD_OnMainActionEx;
    
    public static kbd_OnMidiEventDelegate kbd_OnMidiEvent;
    
    public static kbd_OnMidiListDelegate kbd_OnMidiList;
    
    public static kbd_ProcessActionsMenuDelegate kbd_ProcessActionsMenu;
    
    public static kbd_processMidiEventActionExDelegate kbd_processMidiEventActionEx;
    
    public static kbd_reprocessMenuDelegate kbd_reprocessMenu;
    
    public static kbd_RunCommandThroughHooksDelegate kbd_RunCommandThroughHooks;
    
    public static kbd_translateAcceleratorDelegate kbd_translateAccelerator;
    
    public static kbd_translateMouseDelegate kbd_translateMouse;
    
    public static Loop_OnArrowDelegate Loop_OnArrow;
    
    public static Main_OnCommandDelegate Main_OnCommand;
    
    public static Main_OnCommandExDelegate Main_OnCommandEx;
    
    public static Main_openProjectDelegate Main_openProject;
    
    public static Main_UpdateLoopInfoDelegate Main_UpdateLoopInfo;
    
    public static MarkProjectDirtyDelegate MarkProjectDirty;
    
    public static MarkTrackItemsDirtyDelegate MarkTrackItemsDirty;
    
    public static Master_GetPlayRateDelegate Master_GetPlayRate;
    
    public static Master_GetPlayRateAtTimeDelegate Master_GetPlayRateAtTime;
    
    public static Master_GetTempoDelegate Master_GetTempo;
    
    public static Master_NormalizePlayRateDelegate Master_NormalizePlayRate;
    
    public static Master_NormalizeTempoDelegate Master_NormalizeTempo;
    
    public static MBDelegate MB;
    
    public static MediaItemDescendsFromTrackDelegate MediaItemDescendsFromTrack;
    
    public static MIDI_CountEvtsDelegate MIDI_CountEvts;
    
    public static MIDI_DeleteCCDelegate MIDI_DeleteCC;
    
    public static MIDI_DeleteEvtDelegate MIDI_DeleteEvt;
    
    public static MIDI_DeleteNoteDelegate MIDI_DeleteNote;
    
    public static MIDI_DeleteTextSysexEvtDelegate MIDI_DeleteTextSysexEvt;
    
    public static MIDI_EnumSelCCDelegate MIDI_EnumSelCC;
    
    public static MIDI_EnumSelEvtsDelegate MIDI_EnumSelEvts;
    
    public static MIDI_EnumSelNotesDelegate MIDI_EnumSelNotes;
    
    public static MIDI_EnumSelTextSysexEvtsDelegate MIDI_EnumSelTextSysexEvts;
    
    public static MIDI_eventlist_CreateDelegate MIDI_eventlist_Create;
    
    public static MIDI_eventlist_DestroyDelegate MIDI_eventlist_Destroy;
    
    public static MIDI_GetCCDelegate MIDI_GetCC;
    
    public static MIDI_GetEvtDelegate MIDI_GetEvt;
    
    public static MIDI_GetNoteDelegate MIDI_GetNote;
    
    public static MIDI_GetPPQPos_EndOfMeasureDelegate MIDI_GetPPQPos_EndOfMeasure;
    
    public static MIDI_GetPPQPos_StartOfMeasureDelegate MIDI_GetPPQPos_StartOfMeasure;
    
    public static MIDI_GetPPQPosFromProjTimeDelegate MIDI_GetPPQPosFromProjTime;
    
    public static MIDI_GetProjTimeFromPPQPosDelegate MIDI_GetProjTimeFromPPQPos;
    
    public static MIDI_GetTextSysexEvtDelegate MIDI_GetTextSysexEvt;
    
    public static MIDI_InsertCCDelegate MIDI_InsertCC;
    
    public static MIDI_InsertEvtDelegate MIDI_InsertEvt;
    
    public static MIDI_InsertNoteDelegate MIDI_InsertNote;
    
    public static MIDI_InsertTextSysexEvtDelegate MIDI_InsertTextSysexEvt;
    
    public static midi_reinitDelegate midi_reinit;
    
    public static MIDI_SetCCDelegate MIDI_SetCC;
    
    public static MIDI_SetEvtDelegate MIDI_SetEvt;
    
    public static MIDI_SetNoteDelegate MIDI_SetNote;
    
    public static MIDI_SetTextSysexEvtDelegate MIDI_SetTextSysexEvt;
    
    public static MIDIEditor_GetActiveDelegate MIDIEditor_GetActive;
    
    public static MIDIEditor_GetModeDelegate MIDIEditor_GetMode;
    
    public static MIDIEditor_GetSetting_intDelegate MIDIEditor_GetSetting_int;
    
    public static MIDIEditor_GetSetting_strDelegate MIDIEditor_GetSetting_str;
    
    public static MIDIEditor_GetTakeDelegate MIDIEditor_GetTake;
    
    public static MIDIEditor_LastFocused_OnCommandDelegate MIDIEditor_LastFocused_OnCommand;
    
    public static MIDIEditor_OnCommandDelegate MIDIEditor_OnCommand;
    
    public static mkpanstrDelegate mkpanstr;
    
    public static mkvolpanstrDelegate mkvolpanstr;
    
    public static mkvolstrDelegate mkvolstr;
    
    public static MoveEditCursorDelegate MoveEditCursor;
    
    public static MoveMediaItemToTrackDelegate MoveMediaItemToTrack;
    
    public static MuteAllTracksDelegate MuteAllTracks;
    
    public static my_getViewportDelegate my_getViewport;
    
    public static NamedCommandLookupDelegate NamedCommandLookup;
    
    public static OnPauseButtonDelegate OnPauseButton;
    
    public static OnPauseButtonExDelegate OnPauseButtonEx;
    
    public static OnPlayButtonDelegate OnPlayButton;
    
    public static OnPlayButtonExDelegate OnPlayButtonEx;
    
    public static OnStopButtonDelegate OnStopButton;
    
    public static OnStopButtonExDelegate OnStopButtonEx;
    
    public static OscLocalMessageToHostDelegate OscLocalMessageToHost;
    
    public static parse_timestrDelegate parse_timestr;
    
    public static parse_timestr_lenDelegate parse_timestr_len;
    
    public static parse_timestr_posDelegate parse_timestr_pos;
    
    public static parsepanstrDelegate parsepanstr;
    
    public static PCM_Sink_CreateDelegate PCM_Sink_Create;
    
    public static PCM_Sink_CreateExDelegate PCM_Sink_CreateEx;
    
    public static PCM_Sink_CreateMIDIFileDelegate PCM_Sink_CreateMIDIFile;
    
    public static PCM_Sink_CreateMIDIFileExDelegate PCM_Sink_CreateMIDIFileEx;
    
    public static PCM_Sink_EnumDelegate PCM_Sink_Enum;
    
    public static PCM_Sink_GetExtensionDelegate PCM_Sink_GetExtension;
    
    public static PCM_Sink_ShowConfigDelegate PCM_Sink_ShowConfig;
    
    public static PCM_Source_CreateFromFileDelegate PCM_Source_CreateFromFile;
    
    public static PCM_Source_CreateFromFileExDelegate PCM_Source_CreateFromFileEx;
    
    public static PCM_Source_CreateFromSimpleDelegate PCM_Source_CreateFromSimple;
    
    public static PCM_Source_CreateFromTypeDelegate PCM_Source_CreateFromType;
    
    public static PCM_Source_GetSectionInfoDelegate PCM_Source_GetSectionInfo;
    
    public static PeakBuild_CreateDelegate PeakBuild_Create;
    
    public static PeakGet_CreateDelegate PeakGet_Create;
    
    public static PlayPreviewDelegate PlayPreview;
    
    public static PlayPreviewExDelegate PlayPreviewEx;
    
    public static PlayTrackPreviewDelegate PlayTrackPreview;
    
    public static PlayTrackPreview2Delegate PlayTrackPreview2;
    
    public static PlayTrackPreview2ExDelegate PlayTrackPreview2Ex;
    
    public static plugin_getapiDelegate plugin_getapi;
    
    public static plugin_getFilterListDelegate plugin_getFilterList;
    
    public static plugin_getImportableProjectFilterListDelegate plugin_getImportableProjectFilterList;
    
    public static plugin_registerDelegate plugin_register;
    
    public static PluginWantsAlwaysRunFxDelegate PluginWantsAlwaysRunFx;
    
    public static PreventUIRefreshDelegate PreventUIRefresh;
    
    public static projectconfig_var_addrDelegate projectconfig_var_addr;
    
    public static projectconfig_var_getoffsDelegate projectconfig_var_getoffs;
    
    public static ReaperGetPitchShiftAPIDelegate ReaperGetPitchShiftAPI;
    
    public static ReaScriptErrorDelegate ReaScriptError;
    
    public static RecursiveCreateDirectoryDelegate RecursiveCreateDirectory;
    
    public static RefreshToolbarDelegate RefreshToolbar;
    
    public static relative_fnDelegate relative_fn;
    
    public static RenderFileSectionDelegate RenderFileSection;
    
    public static Resample_EnumModesDelegate Resample_EnumModes;
    
    public static Resampler_CreateDelegate Resampler_Create;
    
    public static resolve_fnDelegate resolve_fn;
    
    public static resolve_fn2Delegate resolve_fn2;
    
    public static screenset_registerDelegate screenset_register;
    
    public static screenset_registerNewDelegate screenset_registerNew;
    
    public static screenset_unregisterDelegate screenset_unregister;
    
    public static screenset_unregisterByParamDelegate screenset_unregisterByParam;
    
    public static SectionFromUniqueIDDelegate SectionFromUniqueID;
    
    public static SelectAllMediaItemsDelegate SelectAllMediaItems;
    
    public static SelectProjectInstanceDelegate SelectProjectInstance;
    
    public static SendLocalOscMessageDelegate SendLocalOscMessage;
    
    public static SetActiveTakeDelegate SetActiveTake;
    
    public static SetAutomationModeDelegate SetAutomationMode;
    
    public static SetCurrentBPMDelegate SetCurrentBPM;
    
    public static SetEditCurPosDelegate SetEditCurPos;
    
    public static SetEditCurPos2Delegate SetEditCurPos2;
    
    public static SetExtStateDelegate SetExtState;
    
    public static SetMasterTrackVisibilityDelegate SetMasterTrackVisibility;
    
    public static SetMediaItemInfo_ValueDelegate SetMediaItemInfo_Value;
    
    public static SetMediaItemLengthDelegate SetMediaItemLength;
    
    public static SetMediaItemPositionDelegate SetMediaItemPosition;
    
    public static SetMediaItemSelectedDelegate SetMediaItemSelected;
    
    public static SetMediaItemTakeInfo_ValueDelegate SetMediaItemTakeInfo_Value;
    
    public static SetMediaTrackInfo_ValueDelegate SetMediaTrackInfo_Value;
    
    public static SetMixerScrollDelegate SetMixerScroll;
    
    public static SetMouseModifierDelegate SetMouseModifier;
    
    public static SetOnlyTrackSelectedDelegate SetOnlyTrackSelected;
    
    public static SetProjectMarkerDelegate SetProjectMarker;
    
    public static SetProjectMarker2Delegate SetProjectMarker2;
    
    public static SetProjectMarker3Delegate SetProjectMarker3;
    
    public static SetProjectMarkerByIndexDelegate SetProjectMarkerByIndex;
    
    public static SetRegionRenderMatrixDelegate SetRegionRenderMatrix;
    
    public static SetRenderLastErrorDelegate SetRenderLastError;
    
    public static SetTakeStretchMarkerDelegate SetTakeStretchMarker;
    
    public static SetTempoTimeSigMarkerDelegate SetTempoTimeSigMarker;
    
    public static SetTrackAutomationModeDelegate SetTrackAutomationMode;
    
    public static SetTrackColorDelegate SetTrackColor;
    
    public static SetTrackMIDINoteNameDelegate SetTrackMIDINoteName;
    
    public static SetTrackMIDINoteNameExDelegate SetTrackMIDINoteNameEx;
    
    public static SetTrackSelectedDelegate SetTrackSelected;
    
    public static SetTrackSendUIPanDelegate SetTrackSendUIPan;
    
    public static SetTrackSendUIVolDelegate SetTrackSendUIVol;
    
    public static ShowActionListDelegate ShowActionList;
    
    public static ShowConsoleMsgDelegate ShowConsoleMsg;
    
    public static ShowMessageBoxDelegate ShowMessageBox;
    
    public static SLIDER2DBDelegate SLIDER2DB;
    
    public static SnapToGridDelegate SnapToGrid;
    
    public static SoloAllTracksDelegate SoloAllTracks;
    
    public static SplitMediaItemDelegate SplitMediaItem;
    
    public static StopPreviewDelegate StopPreview;
    
    public static StopTrackPreviewDelegate StopTrackPreview;
    
    public static StopTrackPreview2Delegate StopTrackPreview2;
    
    public static stringToGuidDelegate stringToGuid;
    
    public static StuffMIDIMessageDelegate StuffMIDIMessage;
    
    public static TimeMap2_beatsToTimeDelegate TimeMap2_beatsToTime;
    
    public static TimeMap2_GetDividedBpmAtTimeDelegate TimeMap2_GetDividedBpmAtTime;
    
    public static TimeMap2_GetNextChangeTimeDelegate TimeMap2_GetNextChangeTime;
    
    public static TimeMap2_QNToTimeDelegate TimeMap2_QNToTime;
    
    public static TimeMap2_timeToBeatsDelegate TimeMap2_timeToBeats;
    
    public static TimeMap2_timeToQNDelegate TimeMap2_timeToQN;
    
    public static TimeMap_GetDividedBpmAtTimeDelegate TimeMap_GetDividedBpmAtTime;
    
    public static TimeMap_GetTimeSigAtTimeDelegate TimeMap_GetTimeSigAtTime;
    
    public static TimeMap_QNToTimeDelegate TimeMap_QNToTime;
    
    public static TimeMap_QNToTime_absDelegate TimeMap_QNToTime_abs;
    
    public static TimeMap_timeToQNDelegate TimeMap_timeToQN;
    
    public static TimeMap_timeToQN_absDelegate TimeMap_timeToQN_abs;
    
    public static ToggleTrackSendUIMuteDelegate ToggleTrackSendUIMute;
    
    public static Track_GetPeakHoldDBDelegate Track_GetPeakHoldDB;
    
    public static Track_GetPeakInfoDelegate Track_GetPeakInfo;
    
    public static TrackCtl_SetToolTipDelegate TrackCtl_SetToolTip;
    
    public static TrackFX_EndParamEditDelegate TrackFX_EndParamEdit;
    
    public static TrackFX_FormatParamValueDelegate TrackFX_FormatParamValue;
    
    public static TrackFX_FormatParamValueNormalizedDelegate TrackFX_FormatParamValueNormalized;
    
    public static TrackFX_GetByNameDelegate TrackFX_GetByName;
    
    public static TrackFX_GetChainVisibleDelegate TrackFX_GetChainVisible;
    
    public static TrackFX_GetCountDelegate TrackFX_GetCount;
    
    public static TrackFX_GetEnabledDelegate TrackFX_GetEnabled;
    
    public static TrackFX_GetEQDelegate TrackFX_GetEQ;
    
    public static TrackFX_GetEQBandEnabledDelegate TrackFX_GetEQBandEnabled;
    
    public static TrackFX_GetEQParamDelegate TrackFX_GetEQParam;
    
    public static TrackFX_GetFloatingWindowDelegate TrackFX_GetFloatingWindow;
    
    public static TrackFX_GetFormattedParamValueDelegate TrackFX_GetFormattedParamValue;
    
    public static TrackFX_GetFXGUIDDelegate TrackFX_GetFXGUID;
    
    public static TrackFX_GetFXNameDelegate TrackFX_GetFXName;
    
    public static TrackFX_GetInstrumentDelegate TrackFX_GetInstrument;
    
    public static TrackFX_GetNumParamsDelegate TrackFX_GetNumParams;
    
    public static TrackFX_GetOpenDelegate TrackFX_GetOpen;
    
    public static TrackFX_GetParamDelegate TrackFX_GetParam;
    
    public static TrackFX_GetParameterStepSizesDelegate TrackFX_GetParameterStepSizes;
    
    public static TrackFX_GetParamExDelegate TrackFX_GetParamEx;
    
    public static TrackFX_GetParamNameDelegate TrackFX_GetParamName;
    
    public static TrackFX_GetParamNormalizedDelegate TrackFX_GetParamNormalized;
    
    public static TrackFX_GetPresetDelegate TrackFX_GetPreset;
    
    public static TrackFX_GetPresetIndexDelegate TrackFX_GetPresetIndex;
    
    public static TrackFX_NavigatePresetsDelegate TrackFX_NavigatePresets;
    
    public static TrackFX_SetEnabledDelegate TrackFX_SetEnabled;
    
    public static TrackFX_SetEQBandEnabledDelegate TrackFX_SetEQBandEnabled;
    
    public static TrackFX_SetEQParamDelegate TrackFX_SetEQParam;
    
    public static TrackFX_SetOpenDelegate TrackFX_SetOpen;
    
    public static TrackFX_SetParamDelegate TrackFX_SetParam;
    
    public static TrackFX_SetParamNormalizedDelegate TrackFX_SetParamNormalized;
    
    public static TrackFX_SetPresetDelegate TrackFX_SetPreset;
    
    public static TrackFX_SetPresetByIndexDelegate TrackFX_SetPresetByIndex;
    
    public static TrackFX_ShowDelegate TrackFX_Show;
    
    public static TrackList_AdjustWindowsDelegate TrackList_AdjustWindows;
    
    public static TrackList_UpdateAllExternalSurfacesDelegate TrackList_UpdateAllExternalSurfaces;
    
    public static Undo_BeginBlockDelegate Undo_BeginBlock;
    
    public static Undo_BeginBlock2Delegate Undo_BeginBlock2;
    
    public static Undo_CanRedo2Delegate Undo_CanRedo2;
    
    public static Undo_CanUndo2Delegate Undo_CanUndo2;
    
    public static Undo_DoRedo2Delegate Undo_DoRedo2;
    
    public static Undo_DoUndo2Delegate Undo_DoUndo2;
    
    public static Undo_EndBlockDelegate Undo_EndBlock;
    
    public static Undo_EndBlock2Delegate Undo_EndBlock2;
    
    public static Undo_OnStateChangeDelegate Undo_OnStateChange;
    
    public static Undo_OnStateChange2Delegate Undo_OnStateChange2;
    
    public static Undo_OnStateChange_ItemDelegate Undo_OnStateChange_Item;
    
    public static Undo_OnStateChangeExDelegate Undo_OnStateChangeEx;
    
    public static Undo_OnStateChangeEx2Delegate Undo_OnStateChangeEx2;
    
    public static UpdateArrangeDelegate UpdateArrange;
    
    public static UpdateItemInProjectDelegate UpdateItemInProject;
    
    public static UpdateTimelineDelegate UpdateTimeline;
    
    public static ValidatePtrDelegate ValidatePtr;
    
    public static ViewPrefsDelegate ViewPrefs;
    
    public static WDL_VirtualWnd_ScaledBlitBGDelegate WDL_VirtualWnd_ScaledBlitBG;
    
    public static void Init(Dictionary<string, IntPtr> reaperFunctions)
    {      
        
        AddCustomizableMenu = (AddCustomizableMenuDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AddCustomizableMenu"], typeof(AddCustomizableMenuDelegate));
        
        AddExtensionsMainMenu = (AddExtensionsMainMenuDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AddExtensionsMainMenu"], typeof(AddExtensionsMainMenuDelegate));
        
        AddMediaItemToTrack = (AddMediaItemToTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AddMediaItemToTrack"], typeof(AddMediaItemToTrackDelegate));
        
        AddProjectMarker = (AddProjectMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AddProjectMarker"], typeof(AddProjectMarkerDelegate));
        
        AddProjectMarker2 = (AddProjectMarker2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AddProjectMarker2"], typeof(AddProjectMarker2Delegate));
        
        AddTakeToMediaItem = (AddTakeToMediaItemDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AddTakeToMediaItem"], typeof(AddTakeToMediaItemDelegate));
        
        AddTempoTimeSigMarker = (AddTempoTimeSigMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AddTempoTimeSigMarker"], typeof(AddTempoTimeSigMarkerDelegate));
        
        adjustZoom = (adjustZoomDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["adjustZoom"], typeof(adjustZoomDelegate));
        
        AnyTrackSolo = (AnyTrackSoloDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AnyTrackSolo"], typeof(AnyTrackSoloDelegate));
        
        APITest = (APITestDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["APITest"], typeof(APITestDelegate));
        
        ApplyNudge = (ApplyNudgeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ApplyNudge"], typeof(ApplyNudgeDelegate));
        
        Audio_IsPreBuffer = (Audio_IsPreBufferDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Audio_IsPreBuffer"], typeof(Audio_IsPreBufferDelegate));
        
        Audio_IsRunning = (Audio_IsRunningDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Audio_IsRunning"], typeof(Audio_IsRunningDelegate));
        
        Audio_RegHardwareHook = (Audio_RegHardwareHookDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Audio_RegHardwareHook"], typeof(Audio_RegHardwareHookDelegate));
        
        AudioAccessorValidateState = (AudioAccessorValidateStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["AudioAccessorValidateState"], typeof(AudioAccessorValidateStateDelegate));
        
        BypassFxAllTracks = (BypassFxAllTracksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["BypassFxAllTracks"], typeof(BypassFxAllTracksDelegate));
        
        CalculatePeaks = (CalculatePeaksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CalculatePeaks"], typeof(CalculatePeaksDelegate));
        
        CalculatePeaksFloatSrcPtr = (CalculatePeaksFloatSrcPtrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CalculatePeaksFloatSrcPtr"], typeof(CalculatePeaksFloatSrcPtrDelegate));
        
        ClearAllRecArmed = (ClearAllRecArmedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ClearAllRecArmed"], typeof(ClearAllRecArmedDelegate));
        
        ClearPeakCache = (ClearPeakCacheDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ClearPeakCache"], typeof(ClearPeakCacheDelegate));
        
        CountActionShortcuts = (CountActionShortcutsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountActionShortcuts"], typeof(CountActionShortcutsDelegate));
        
        CountMediaItems = (CountMediaItemsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountMediaItems"], typeof(CountMediaItemsDelegate));
        
        CountProjectMarkers = (CountProjectMarkersDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountProjectMarkers"], typeof(CountProjectMarkersDelegate));
        
        CountSelectedMediaItems = (CountSelectedMediaItemsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountSelectedMediaItems"], typeof(CountSelectedMediaItemsDelegate));
        
        CountSelectedTracks = (CountSelectedTracksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountSelectedTracks"], typeof(CountSelectedTracksDelegate));
        
        CountTakes = (CountTakesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountTakes"], typeof(CountTakesDelegate));
        
        CountTCPFXParms = (CountTCPFXParmsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountTCPFXParms"], typeof(CountTCPFXParmsDelegate));
        
        CountTempoTimeSigMarkers = (CountTempoTimeSigMarkersDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountTempoTimeSigMarkers"], typeof(CountTempoTimeSigMarkersDelegate));
        
        CountTrackEnvelopes = (CountTrackEnvelopesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountTrackEnvelopes"], typeof(CountTrackEnvelopesDelegate));
        
        CountTrackMediaItems = (CountTrackMediaItemsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountTrackMediaItems"], typeof(CountTrackMediaItemsDelegate));
        
        CountTracks = (CountTracksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CountTracks"], typeof(CountTracksDelegate));
        
        CreateLocalOscHandler = (CreateLocalOscHandlerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CreateLocalOscHandler"], typeof(CreateLocalOscHandlerDelegate));
        
        CreateMIDIInput = (CreateMIDIInputDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CreateMIDIInput"], typeof(CreateMIDIInputDelegate));
        
        CreateMIDIOutput = (CreateMIDIOutputDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CreateMIDIOutput"], typeof(CreateMIDIOutputDelegate));
        
        CreateNewMIDIItemInProj = (CreateNewMIDIItemInProjDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CreateNewMIDIItemInProj"], typeof(CreateNewMIDIItemInProjDelegate));
        
        CreateTakeAudioAccessor = (CreateTakeAudioAccessorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CreateTakeAudioAccessor"], typeof(CreateTakeAudioAccessorDelegate));
        
        CreateTrackAudioAccessor = (CreateTrackAudioAccessorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CreateTrackAudioAccessor"], typeof(CreateTrackAudioAccessorDelegate));
        
        CSurf_FlushUndo = (CSurf_FlushUndoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_FlushUndo"], typeof(CSurf_FlushUndoDelegate));
        
        CSurf_GetTouchState = (CSurf_GetTouchStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_GetTouchState"], typeof(CSurf_GetTouchStateDelegate));
        
        CSurf_GoEnd = (CSurf_GoEndDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_GoEnd"], typeof(CSurf_GoEndDelegate));
        
        CSurf_GoStart = (CSurf_GoStartDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_GoStart"], typeof(CSurf_GoStartDelegate));
        
        CSurf_NumTracks = (CSurf_NumTracksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_NumTracks"], typeof(CSurf_NumTracksDelegate));
        
        CSurf_OnArrow = (CSurf_OnArrowDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnArrow"], typeof(CSurf_OnArrowDelegate));
        
        CSurf_OnFwd = (CSurf_OnFwdDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnFwd"], typeof(CSurf_OnFwdDelegate));
        
        CSurf_OnFXChange = (CSurf_OnFXChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnFXChange"], typeof(CSurf_OnFXChangeDelegate));
        
        CSurf_OnInputMonitorChange = (CSurf_OnInputMonitorChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnInputMonitorChange"], typeof(CSurf_OnInputMonitorChangeDelegate));
        
        CSurf_OnInputMonitorChangeEx = (CSurf_OnInputMonitorChangeExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnInputMonitorChangeEx"], typeof(CSurf_OnInputMonitorChangeExDelegate));
        
        CSurf_OnMuteChange = (CSurf_OnMuteChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnMuteChange"], typeof(CSurf_OnMuteChangeDelegate));
        
        CSurf_OnMuteChangeEx = (CSurf_OnMuteChangeExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnMuteChangeEx"], typeof(CSurf_OnMuteChangeExDelegate));
        
        CSurf_OnOscControlMessage = (CSurf_OnOscControlMessageDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnOscControlMessage"], typeof(CSurf_OnOscControlMessageDelegate));
        
        CSurf_OnPanChange = (CSurf_OnPanChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnPanChange"], typeof(CSurf_OnPanChangeDelegate));
        
        CSurf_OnPanChangeEx = (CSurf_OnPanChangeExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnPanChangeEx"], typeof(CSurf_OnPanChangeExDelegate));
        
        CSurf_OnPause = (CSurf_OnPauseDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnPause"], typeof(CSurf_OnPauseDelegate));
        
        CSurf_OnPlay = (CSurf_OnPlayDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnPlay"], typeof(CSurf_OnPlayDelegate));
        
        CSurf_OnPlayRateChange = (CSurf_OnPlayRateChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnPlayRateChange"], typeof(CSurf_OnPlayRateChangeDelegate));
        
        CSurf_OnRecArmChange = (CSurf_OnRecArmChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnRecArmChange"], typeof(CSurf_OnRecArmChangeDelegate));
        
        CSurf_OnRecArmChangeEx = (CSurf_OnRecArmChangeExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnRecArmChangeEx"], typeof(CSurf_OnRecArmChangeExDelegate));
        
        CSurf_OnRecord = (CSurf_OnRecordDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnRecord"], typeof(CSurf_OnRecordDelegate));
        
        CSurf_OnRecvPanChange = (CSurf_OnRecvPanChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnRecvPanChange"], typeof(CSurf_OnRecvPanChangeDelegate));
        
        CSurf_OnRecvVolumeChange = (CSurf_OnRecvVolumeChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnRecvVolumeChange"], typeof(CSurf_OnRecvVolumeChangeDelegate));
        
        CSurf_OnRew = (CSurf_OnRewDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnRew"], typeof(CSurf_OnRewDelegate));
        
        CSurf_OnRewFwd = (CSurf_OnRewFwdDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnRewFwd"], typeof(CSurf_OnRewFwdDelegate));
        
        CSurf_OnScroll = (CSurf_OnScrollDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnScroll"], typeof(CSurf_OnScrollDelegate));
        
        CSurf_OnSelectedChange = (CSurf_OnSelectedChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnSelectedChange"], typeof(CSurf_OnSelectedChangeDelegate));
        
        CSurf_OnSendPanChange = (CSurf_OnSendPanChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnSendPanChange"], typeof(CSurf_OnSendPanChangeDelegate));
        
        CSurf_OnSendVolumeChange = (CSurf_OnSendVolumeChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnSendVolumeChange"], typeof(CSurf_OnSendVolumeChangeDelegate));
        
        CSurf_OnSoloChange = (CSurf_OnSoloChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnSoloChange"], typeof(CSurf_OnSoloChangeDelegate));
        
        CSurf_OnSoloChangeEx = (CSurf_OnSoloChangeExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnSoloChangeEx"], typeof(CSurf_OnSoloChangeExDelegate));
        
        CSurf_OnStop = (CSurf_OnStopDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnStop"], typeof(CSurf_OnStopDelegate));
        
        CSurf_OnTempoChange = (CSurf_OnTempoChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnTempoChange"], typeof(CSurf_OnTempoChangeDelegate));
        
        CSurf_OnTrackSelection = (CSurf_OnTrackSelectionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnTrackSelection"], typeof(CSurf_OnTrackSelectionDelegate));
        
        CSurf_OnVolumeChange = (CSurf_OnVolumeChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnVolumeChange"], typeof(CSurf_OnVolumeChangeDelegate));
        
        CSurf_OnVolumeChangeEx = (CSurf_OnVolumeChangeExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnVolumeChangeEx"], typeof(CSurf_OnVolumeChangeExDelegate));
        
        CSurf_OnWidthChange = (CSurf_OnWidthChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnWidthChange"], typeof(CSurf_OnWidthChangeDelegate));
        
        CSurf_OnWidthChangeEx = (CSurf_OnWidthChangeExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnWidthChangeEx"], typeof(CSurf_OnWidthChangeExDelegate));
        
        CSurf_OnZoom = (CSurf_OnZoomDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_OnZoom"], typeof(CSurf_OnZoomDelegate));
        
        CSurf_ResetAllCachedVolPanStates = (CSurf_ResetAllCachedVolPanStatesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_ResetAllCachedVolPanStates"], typeof(CSurf_ResetAllCachedVolPanStatesDelegate));
        
        CSurf_ScrubAmt = (CSurf_ScrubAmtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_ScrubAmt"], typeof(CSurf_ScrubAmtDelegate));
        
        CSurf_SetAutoMode = (CSurf_SetAutoModeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetAutoMode"], typeof(CSurf_SetAutoModeDelegate));
        
        CSurf_SetPlayState = (CSurf_SetPlayStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetPlayState"], typeof(CSurf_SetPlayStateDelegate));
        
        CSurf_SetRepeatState = (CSurf_SetRepeatStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetRepeatState"], typeof(CSurf_SetRepeatStateDelegate));
        
        CSurf_SetSurfaceMute = (CSurf_SetSurfaceMuteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetSurfaceMute"], typeof(CSurf_SetSurfaceMuteDelegate));
        
        CSurf_SetSurfacePan = (CSurf_SetSurfacePanDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetSurfacePan"], typeof(CSurf_SetSurfacePanDelegate));
        
        CSurf_SetSurfaceRecArm = (CSurf_SetSurfaceRecArmDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetSurfaceRecArm"], typeof(CSurf_SetSurfaceRecArmDelegate));
        
        CSurf_SetSurfaceSelected = (CSurf_SetSurfaceSelectedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetSurfaceSelected"], typeof(CSurf_SetSurfaceSelectedDelegate));
        
        CSurf_SetSurfaceSolo = (CSurf_SetSurfaceSoloDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetSurfaceSolo"], typeof(CSurf_SetSurfaceSoloDelegate));
        
        CSurf_SetSurfaceVolume = (CSurf_SetSurfaceVolumeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetSurfaceVolume"], typeof(CSurf_SetSurfaceVolumeDelegate));
        
        CSurf_SetTrackListChange = (CSurf_SetTrackListChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_SetTrackListChange"], typeof(CSurf_SetTrackListChangeDelegate));
        
        CSurf_TrackFromID = (CSurf_TrackFromIDDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_TrackFromID"], typeof(CSurf_TrackFromIDDelegate));
        
        CSurf_TrackToID = (CSurf_TrackToIDDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["CSurf_TrackToID"], typeof(CSurf_TrackToIDDelegate));
        
        DB2SLIDER = (DB2SLIDERDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DB2SLIDER"], typeof(DB2SLIDERDelegate));
        
        DeleteActionShortcut = (DeleteActionShortcutDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DeleteActionShortcut"], typeof(DeleteActionShortcutDelegate));
        
        DeleteExtState = (DeleteExtStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DeleteExtState"], typeof(DeleteExtStateDelegate));
        
        DeleteProjectMarker = (DeleteProjectMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DeleteProjectMarker"], typeof(DeleteProjectMarkerDelegate));
        
        DeleteProjectMarkerByIndex = (DeleteProjectMarkerByIndexDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DeleteProjectMarkerByIndex"], typeof(DeleteProjectMarkerByIndexDelegate));
        
        DeleteTakeStretchMarkers = (DeleteTakeStretchMarkersDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DeleteTakeStretchMarkers"], typeof(DeleteTakeStretchMarkersDelegate));
        
        DeleteTrack = (DeleteTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DeleteTrack"], typeof(DeleteTrackDelegate));
        
        DeleteTrackMediaItem = (DeleteTrackMediaItemDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DeleteTrackMediaItem"], typeof(DeleteTrackMediaItemDelegate));
        
        DestroyAudioAccessor = (DestroyAudioAccessorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DestroyAudioAccessor"], typeof(DestroyAudioAccessorDelegate));
        
        DestroyLocalOscHandler = (DestroyLocalOscHandlerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DestroyLocalOscHandler"], typeof(DestroyLocalOscHandlerDelegate));
        
        DoActionShortcutDialog = (DoActionShortcutDialogDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DoActionShortcutDialog"], typeof(DoActionShortcutDialogDelegate));
        
        Dock_UpdateDockID = (Dock_UpdateDockIDDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Dock_UpdateDockID"], typeof(Dock_UpdateDockIDDelegate));
        
        DockIsChildOfDock = (DockIsChildOfDockDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DockIsChildOfDock"], typeof(DockIsChildOfDockDelegate));
        
        DockWindowActivate = (DockWindowActivateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DockWindowActivate"], typeof(DockWindowActivateDelegate));
        
        DockWindowAdd = (DockWindowAddDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DockWindowAdd"], typeof(DockWindowAddDelegate));
        
        DockWindowAddEx = (DockWindowAddExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DockWindowAddEx"], typeof(DockWindowAddExDelegate));
        
        DockWindowRefresh = (DockWindowRefreshDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DockWindowRefresh"], typeof(DockWindowRefreshDelegate));
        
        DockWindowRefreshForHWND = (DockWindowRefreshForHWNDDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DockWindowRefreshForHWND"], typeof(DockWindowRefreshForHWNDDelegate));
        
        DockWindowRemove = (DockWindowRemoveDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DockWindowRemove"], typeof(DockWindowRemoveDelegate));
        
        DuplicateCustomizableMenu = (DuplicateCustomizableMenuDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["DuplicateCustomizableMenu"], typeof(DuplicateCustomizableMenuDelegate));
        
        EnsureNotCompletelyOffscreen = (EnsureNotCompletelyOffscreenDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnsureNotCompletelyOffscreen"], typeof(EnsureNotCompletelyOffscreenDelegate));
        
        EnumPitchShiftModes = (EnumPitchShiftModesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumPitchShiftModes"], typeof(EnumPitchShiftModesDelegate));
        
        EnumPitchShiftSubModes = (EnumPitchShiftSubModesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumPitchShiftSubModes"], typeof(EnumPitchShiftSubModesDelegate));
        
        EnumProjectMarkers = (EnumProjectMarkersDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumProjectMarkers"], typeof(EnumProjectMarkersDelegate));
        
        EnumProjectMarkers2 = (EnumProjectMarkers2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumProjectMarkers2"], typeof(EnumProjectMarkers2Delegate));
        
        EnumProjectMarkers3 = (EnumProjectMarkers3Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumProjectMarkers3"], typeof(EnumProjectMarkers3Delegate));
        
        EnumProjects = (EnumProjectsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumProjects"], typeof(EnumProjectsDelegate));
        
        EnumRegionRenderMatrix = (EnumRegionRenderMatrixDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumRegionRenderMatrix"], typeof(EnumRegionRenderMatrixDelegate));
        
        EnumTrackMIDIProgramNames = (EnumTrackMIDIProgramNamesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumTrackMIDIProgramNames"], typeof(EnumTrackMIDIProgramNamesDelegate));
        
        EnumTrackMIDIProgramNamesEx = (EnumTrackMIDIProgramNamesExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["EnumTrackMIDIProgramNamesEx"], typeof(EnumTrackMIDIProgramNamesExDelegate));
        
        file_exists = (file_existsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["file_exists"], typeof(file_existsDelegate));
        
        format_timestr = (format_timestrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["format_timestr"], typeof(format_timestrDelegate));
        
        format_timestr_len = (format_timestr_lenDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["format_timestr_len"], typeof(format_timestr_lenDelegate));
        
        format_timestr_pos = (format_timestr_posDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["format_timestr_pos"], typeof(format_timestr_posDelegate));
        
        FreeHeapPtr = (FreeHeapPtrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["FreeHeapPtr"], typeof(FreeHeapPtrDelegate));
        
        genGuid = (genGuidDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["genGuid"], typeof(genGuidDelegate));
        
        get_config_var = (get_config_varDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["get_config_var"], typeof(get_config_varDelegate));
        
        get_ini_file = (get_ini_fileDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["get_ini_file"], typeof(get_ini_fileDelegate));
        
        get_midi_config_var = (get_midi_config_varDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["get_midi_config_var"], typeof(get_midi_config_varDelegate));
        
        GetActionShortcutDesc = (GetActionShortcutDescDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetActionShortcutDesc"], typeof(GetActionShortcutDescDelegate));
        
        GetActiveTake = (GetActiveTakeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetActiveTake"], typeof(GetActiveTakeDelegate));
        
        GetAppVersion = (GetAppVersionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetAppVersion"], typeof(GetAppVersionDelegate));
        
        GetAudioAccessorEndTime = (GetAudioAccessorEndTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetAudioAccessorEndTime"], typeof(GetAudioAccessorEndTimeDelegate));
        
        GetAudioAccessorHash = (GetAudioAccessorHashDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetAudioAccessorHash"], typeof(GetAudioAccessorHashDelegate));
        
        GetAudioAccessorSamples = (GetAudioAccessorSamplesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetAudioAccessorSamples"], typeof(GetAudioAccessorSamplesDelegate));
        
        GetAudioAccessorStartTime = (GetAudioAccessorStartTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetAudioAccessorStartTime"], typeof(GetAudioAccessorStartTimeDelegate));
        
        GetColorTheme = (GetColorThemeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetColorTheme"], typeof(GetColorThemeDelegate));
        
        GetColorThemeStruct = (GetColorThemeStructDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetColorThemeStruct"], typeof(GetColorThemeStructDelegate));
        
        GetConfigWantsDock = (GetConfigWantsDockDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetConfigWantsDock"], typeof(GetConfigWantsDockDelegate));
        
        GetContextMenu = (GetContextMenuDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetContextMenu"], typeof(GetContextMenuDelegate));
        
        GetCurrentProjectInLoadSave = (GetCurrentProjectInLoadSaveDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetCurrentProjectInLoadSave"], typeof(GetCurrentProjectInLoadSaveDelegate));
        
        GetCursorContext = (GetCursorContextDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetCursorContext"], typeof(GetCursorContextDelegate));
        
        GetCursorPosition = (GetCursorPositionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetCursorPosition"], typeof(GetCursorPositionDelegate));
        
        GetCursorPositionEx = (GetCursorPositionExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetCursorPositionEx"], typeof(GetCursorPositionExDelegate));
        
        GetDisplayedMediaItemColor = (GetDisplayedMediaItemColorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetDisplayedMediaItemColor"], typeof(GetDisplayedMediaItemColorDelegate));
        
        GetDisplayedMediaItemColor2 = (GetDisplayedMediaItemColor2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetDisplayedMediaItemColor2"], typeof(GetDisplayedMediaItemColor2Delegate));
        
        GetEnvelopeName = (GetEnvelopeNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetEnvelopeName"], typeof(GetEnvelopeNameDelegate));
        
        GetExePath = (GetExePathDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetExePath"], typeof(GetExePathDelegate));
        
        GetExtState = (GetExtStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetExtState"], typeof(GetExtStateDelegate));
        
        GetFocusedFX = (GetFocusedFXDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetFocusedFX"], typeof(GetFocusedFXDelegate));
        
        GetFreeDiskSpaceForRecordPath = (GetFreeDiskSpaceForRecordPathDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetFreeDiskSpaceForRecordPath"], typeof(GetFreeDiskSpaceForRecordPathDelegate));
        
        GetHZoomLevel = (GetHZoomLevelDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetHZoomLevel"], typeof(GetHZoomLevelDelegate));
        
        GetIconThemePointer = (GetIconThemePointerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetIconThemePointer"], typeof(GetIconThemePointerDelegate));
        
        GetIconThemeStruct = (GetIconThemeStructDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetIconThemeStruct"], typeof(GetIconThemeStructDelegate));
        
        GetInputChannelName = (GetInputChannelNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetInputChannelName"], typeof(GetInputChannelNameDelegate));
        
        GetInputOutputLatency = (GetInputOutputLatencyDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetInputOutputLatency"], typeof(GetInputOutputLatencyDelegate));
        
        GetItemEditingTime2 = (GetItemEditingTime2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetItemEditingTime2"], typeof(GetItemEditingTime2Delegate));
        
        GetItemProjectContext = (GetItemProjectContextDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetItemProjectContext"], typeof(GetItemProjectContextDelegate));
        
        GetLastMarkerAndCurRegion = (GetLastMarkerAndCurRegionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetLastMarkerAndCurRegion"], typeof(GetLastMarkerAndCurRegionDelegate));
        
        GetLastTouchedFX = (GetLastTouchedFXDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetLastTouchedFX"], typeof(GetLastTouchedFXDelegate));
        
        GetLastTouchedTrack = (GetLastTouchedTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetLastTouchedTrack"], typeof(GetLastTouchedTrackDelegate));
        
        GetMainHwnd = (GetMainHwndDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMainHwnd"], typeof(GetMainHwndDelegate));
        
        GetMasterMuteSoloFlags = (GetMasterMuteSoloFlagsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMasterMuteSoloFlags"], typeof(GetMasterMuteSoloFlagsDelegate));
        
        GetMasterTrack = (GetMasterTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMasterTrack"], typeof(GetMasterTrackDelegate));
        
        GetMasterTrackVisibility = (GetMasterTrackVisibilityDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMasterTrackVisibility"], typeof(GetMasterTrackVisibilityDelegate));
        
        GetMaxMidiInputs = (GetMaxMidiInputsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMaxMidiInputs"], typeof(GetMaxMidiInputsDelegate));
        
        GetMaxMidiOutputs = (GetMaxMidiOutputsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMaxMidiOutputs"], typeof(GetMaxMidiOutputsDelegate));
        
        GetMediaItem = (GetMediaItemDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItem"], typeof(GetMediaItemDelegate));
        
        GetMediaItem_Track = (GetMediaItem_TrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItem_Track"], typeof(GetMediaItem_TrackDelegate));
        
        GetMediaItemInfo_Value = (GetMediaItemInfo_ValueDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemInfo_Value"], typeof(GetMediaItemInfo_ValueDelegate));
        
        GetMediaItemNumTakes = (GetMediaItemNumTakesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemNumTakes"], typeof(GetMediaItemNumTakesDelegate));
        
        GetMediaItemTake = (GetMediaItemTakeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemTake"], typeof(GetMediaItemTakeDelegate));
        
        GetMediaItemTake_Item = (GetMediaItemTake_ItemDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemTake_Item"], typeof(GetMediaItemTake_ItemDelegate));
        
        GetMediaItemTake_Source = (GetMediaItemTake_SourceDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemTake_Source"], typeof(GetMediaItemTake_SourceDelegate));
        
        GetMediaItemTake_Track = (GetMediaItemTake_TrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemTake_Track"], typeof(GetMediaItemTake_TrackDelegate));
        
        GetMediaItemTakeByGUID = (GetMediaItemTakeByGUIDDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemTakeByGUID"], typeof(GetMediaItemTakeByGUIDDelegate));
        
        GetMediaItemTakeInfo_Value = (GetMediaItemTakeInfo_ValueDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemTakeInfo_Value"], typeof(GetMediaItemTakeInfo_ValueDelegate));
        
        GetMediaItemTrack = (GetMediaItemTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaItemTrack"], typeof(GetMediaItemTrackDelegate));
        
        GetMediaSourceFileName = (GetMediaSourceFileNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaSourceFileName"], typeof(GetMediaSourceFileNameDelegate));
        
        GetMediaSourceNumChannels = (GetMediaSourceNumChannelsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaSourceNumChannels"], typeof(GetMediaSourceNumChannelsDelegate));
        
        GetMediaSourceSampleRate = (GetMediaSourceSampleRateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaSourceSampleRate"], typeof(GetMediaSourceSampleRateDelegate));
        
        GetMediaSourceType = (GetMediaSourceTypeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaSourceType"], typeof(GetMediaSourceTypeDelegate));
        
        GetMediaTrackInfo_Value = (GetMediaTrackInfo_ValueDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMediaTrackInfo_Value"], typeof(GetMediaTrackInfo_ValueDelegate));
        
        GetMidiInput = (GetMidiInputDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMidiInput"], typeof(GetMidiInputDelegate));
        
        GetMIDIInputName = (GetMIDIInputNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMIDIInputName"], typeof(GetMIDIInputNameDelegate));
        
        GetMidiOutput = (GetMidiOutputDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMidiOutput"], typeof(GetMidiOutputDelegate));
        
        GetMIDIOutputName = (GetMIDIOutputNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMIDIOutputName"], typeof(GetMIDIOutputNameDelegate));
        
        GetMixerScroll = (GetMixerScrollDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMixerScroll"], typeof(GetMixerScrollDelegate));
        
        GetMouseModifier = (GetMouseModifierDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetMouseModifier"], typeof(GetMouseModifierDelegate));
        
        GetNumAudioInputs = (GetNumAudioInputsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetNumAudioInputs"], typeof(GetNumAudioInputsDelegate));
        
        GetNumAudioOutputs = (GetNumAudioOutputsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetNumAudioOutputs"], typeof(GetNumAudioOutputsDelegate));
        
        GetNumMIDIInputs = (GetNumMIDIInputsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetNumMIDIInputs"], typeof(GetNumMIDIInputsDelegate));
        
        GetNumMIDIOutputs = (GetNumMIDIOutputsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetNumMIDIOutputs"], typeof(GetNumMIDIOutputsDelegate));
        
        GetNumTracks = (GetNumTracksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetNumTracks"], typeof(GetNumTracksDelegate));
        
        GetOutputChannelName = (GetOutputChannelNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetOutputChannelName"], typeof(GetOutputChannelNameDelegate));
        
        GetOutputLatency = (GetOutputLatencyDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetOutputLatency"], typeof(GetOutputLatencyDelegate));
        
        GetParentTrack = (GetParentTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetParentTrack"], typeof(GetParentTrackDelegate));
        
        GetPeakFileName = (GetPeakFileNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPeakFileName"], typeof(GetPeakFileNameDelegate));
        
        GetPeakFileNameEx = (GetPeakFileNameExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPeakFileNameEx"], typeof(GetPeakFileNameExDelegate));
        
        GetPeakFileNameEx2 = (GetPeakFileNameEx2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPeakFileNameEx2"], typeof(GetPeakFileNameEx2Delegate));
        
        GetPeaksBitmap = (GetPeaksBitmapDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPeaksBitmap"], typeof(GetPeaksBitmapDelegate));
        
        GetPlayPosition = (GetPlayPositionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPlayPosition"], typeof(GetPlayPositionDelegate));
        
        GetPlayPosition2 = (GetPlayPosition2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPlayPosition2"], typeof(GetPlayPosition2Delegate));
        
        GetPlayPosition2Ex = (GetPlayPosition2ExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPlayPosition2Ex"], typeof(GetPlayPosition2ExDelegate));
        
        GetPlayPositionEx = (GetPlayPositionExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPlayPositionEx"], typeof(GetPlayPositionExDelegate));
        
        GetPlayState = (GetPlayStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPlayState"], typeof(GetPlayStateDelegate));
        
        GetPlayStateEx = (GetPlayStateExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetPlayStateEx"], typeof(GetPlayStateExDelegate));
        
        GetProjectPath = (GetProjectPathDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetProjectPath"], typeof(GetProjectPathDelegate));
        
        GetProjectPathEx = (GetProjectPathExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetProjectPathEx"], typeof(GetProjectPathExDelegate));
        
        GetProjectStateChangeCount = (GetProjectStateChangeCountDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetProjectStateChangeCount"], typeof(GetProjectStateChangeCountDelegate));
        
        GetProjectTimeSignature = (GetProjectTimeSignatureDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetProjectTimeSignature"], typeof(GetProjectTimeSignatureDelegate));
        
        GetProjectTimeSignature2 = (GetProjectTimeSignature2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetProjectTimeSignature2"], typeof(GetProjectTimeSignature2Delegate));
        
        GetResourcePath = (GetResourcePathDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetResourcePath"], typeof(GetResourcePathDelegate));
        
        GetSelectedMediaItem = (GetSelectedMediaItemDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSelectedMediaItem"], typeof(GetSelectedMediaItemDelegate));
        
        GetSelectedTrack = (GetSelectedTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSelectedTrack"], typeof(GetSelectedTrackDelegate));
        
        GetSelectedTrackEnvelope = (GetSelectedTrackEnvelopeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSelectedTrackEnvelope"], typeof(GetSelectedTrackEnvelopeDelegate));
        
        GetSet_ArrangeView2 = (GetSet_ArrangeView2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSet_ArrangeView2"], typeof(GetSet_ArrangeView2Delegate));
        
        GetSet_LoopTimeRange = (GetSet_LoopTimeRangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSet_LoopTimeRange"], typeof(GetSet_LoopTimeRangeDelegate));
        
        GetSet_LoopTimeRange2 = (GetSet_LoopTimeRange2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSet_LoopTimeRange2"], typeof(GetSet_LoopTimeRange2Delegate));
        
        GetSetEnvelopeState = (GetSetEnvelopeStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetEnvelopeState"], typeof(GetSetEnvelopeStateDelegate));
        
        GetSetEnvelopeState2 = (GetSetEnvelopeState2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetEnvelopeState2"], typeof(GetSetEnvelopeState2Delegate));
        
        GetSetItemState = (GetSetItemStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetItemState"], typeof(GetSetItemStateDelegate));
        
        GetSetItemState2 = (GetSetItemState2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetItemState2"], typeof(GetSetItemState2Delegate));
        
        GetSetMediaItemInfo = (GetSetMediaItemInfoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetMediaItemInfo"], typeof(GetSetMediaItemInfoDelegate));
        
        GetSetMediaItemTakeInfo = (GetSetMediaItemTakeInfoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetMediaItemTakeInfo"], typeof(GetSetMediaItemTakeInfoDelegate));
        
        GetSetMediaItemTakeInfo_String = (GetSetMediaItemTakeInfo_StringDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetMediaItemTakeInfo_String"], typeof(GetSetMediaItemTakeInfo_StringDelegate));
        
        GetSetMediaTrackInfo = (GetSetMediaTrackInfoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetMediaTrackInfo"], typeof(GetSetMediaTrackInfoDelegate));
        
        GetSetMediaTrackInfo_String = (GetSetMediaTrackInfo_StringDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetMediaTrackInfo_String"], typeof(GetSetMediaTrackInfo_StringDelegate));
        
        GetSetObjectState = (GetSetObjectStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetObjectState"], typeof(GetSetObjectStateDelegate));
        
        GetSetObjectState2 = (GetSetObjectState2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetObjectState2"], typeof(GetSetObjectState2Delegate));
        
        GetSetRepeat = (GetSetRepeatDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetRepeat"], typeof(GetSetRepeatDelegate));
        
        GetSetRepeatEx = (GetSetRepeatExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetRepeatEx"], typeof(GetSetRepeatExDelegate));
        
        GetSetTrackMIDISupportFile = (GetSetTrackMIDISupportFileDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetTrackMIDISupportFile"], typeof(GetSetTrackMIDISupportFileDelegate));
        
        GetSetTrackSendInfo = (GetSetTrackSendInfoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetTrackSendInfo"], typeof(GetSetTrackSendInfoDelegate));
        
        GetSetTrackState = (GetSetTrackStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetTrackState"], typeof(GetSetTrackStateDelegate));
        
        GetSetTrackState2 = (GetSetTrackState2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSetTrackState2"], typeof(GetSetTrackState2Delegate));
        
        GetSubProjectFromSource = (GetSubProjectFromSourceDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetSubProjectFromSource"], typeof(GetSubProjectFromSourceDelegate));
        
        GetTake = (GetTakeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTake"], typeof(GetTakeDelegate));
        
        GetTakeEnvelopeByName = (GetTakeEnvelopeByNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTakeEnvelopeByName"], typeof(GetTakeEnvelopeByNameDelegate));
        
        GetTakeName = (GetTakeNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTakeName"], typeof(GetTakeNameDelegate));
        
        GetTakeNumStretchMarkers = (GetTakeNumStretchMarkersDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTakeNumStretchMarkers"], typeof(GetTakeNumStretchMarkersDelegate));
        
        GetTakeStretchMarker = (GetTakeStretchMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTakeStretchMarker"], typeof(GetTakeStretchMarkerDelegate));
        
        GetTCPFXParm = (GetTCPFXParmDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTCPFXParm"], typeof(GetTCPFXParmDelegate));
        
        GetTempoMatchPlayRate = (GetTempoMatchPlayRateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTempoMatchPlayRate"], typeof(GetTempoMatchPlayRateDelegate));
        
        GetTempoTimeSigMarker = (GetTempoTimeSigMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTempoTimeSigMarker"], typeof(GetTempoTimeSigMarkerDelegate));
        
        GetToggleCommandState = (GetToggleCommandStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetToggleCommandState"], typeof(GetToggleCommandStateDelegate));
        
        GetToggleCommandState2 = (GetToggleCommandState2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetToggleCommandState2"], typeof(GetToggleCommandState2Delegate));
        
        GetToggleCommandStateThroughHooks = (GetToggleCommandStateThroughHooksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetToggleCommandStateThroughHooks"], typeof(GetToggleCommandStateThroughHooksDelegate));
        
        GetTooltipWindow = (GetTooltipWindowDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTooltipWindow"], typeof(GetTooltipWindowDelegate));
        
        GetTrack = (GetTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrack"], typeof(GetTrackDelegate));
        
        GetTrackAutomationMode = (GetTrackAutomationModeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackAutomationMode"], typeof(GetTrackAutomationModeDelegate));
        
        GetTrackColor = (GetTrackColorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackColor"], typeof(GetTrackColorDelegate));
        
        GetTrackEnvelope = (GetTrackEnvelopeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackEnvelope"], typeof(GetTrackEnvelopeDelegate));
        
        GetTrackEnvelopeByName = (GetTrackEnvelopeByNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackEnvelopeByName"], typeof(GetTrackEnvelopeByNameDelegate));
        
        GetTrackGUID = (GetTrackGUIDDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackGUID"], typeof(GetTrackGUIDDelegate));
        
        GetTrackInfo = (GetTrackInfoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackInfo"], typeof(GetTrackInfoDelegate));
        
        GetTrackMediaItem = (GetTrackMediaItemDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackMediaItem"], typeof(GetTrackMediaItemDelegate));
        
        GetTrackMIDINoteName = (GetTrackMIDINoteNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackMIDINoteName"], typeof(GetTrackMIDINoteNameDelegate));
        
        GetTrackMIDINoteNameEx = (GetTrackMIDINoteNameExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackMIDINoteNameEx"], typeof(GetTrackMIDINoteNameExDelegate));
        
        GetTrackMIDINoteRange = (GetTrackMIDINoteRangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackMIDINoteRange"], typeof(GetTrackMIDINoteRangeDelegate));
        
        GetTrackNumMediaItems = (GetTrackNumMediaItemsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackNumMediaItems"], typeof(GetTrackNumMediaItemsDelegate));
        
        GetTrackNumSends = (GetTrackNumSendsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackNumSends"], typeof(GetTrackNumSendsDelegate));
        
        GetTrackReceiveName = (GetTrackReceiveNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackReceiveName"], typeof(GetTrackReceiveNameDelegate));
        
        GetTrackReceiveUIMute = (GetTrackReceiveUIMuteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackReceiveUIMute"], typeof(GetTrackReceiveUIMuteDelegate));
        
        GetTrackReceiveUIVolPan = (GetTrackReceiveUIVolPanDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackReceiveUIVolPan"], typeof(GetTrackReceiveUIVolPanDelegate));
        
        GetTrackSendName = (GetTrackSendNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackSendName"], typeof(GetTrackSendNameDelegate));
        
        GetTrackSendUIMute = (GetTrackSendUIMuteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackSendUIMute"], typeof(GetTrackSendUIMuteDelegate));
        
        GetTrackSendUIVolPan = (GetTrackSendUIVolPanDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackSendUIVolPan"], typeof(GetTrackSendUIVolPanDelegate));
        
        GetTrackState = (GetTrackStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackState"], typeof(GetTrackStateDelegate));
        
        GetTrackUIMute = (GetTrackUIMuteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackUIMute"], typeof(GetTrackUIMuteDelegate));
        
        GetTrackUIPan = (GetTrackUIPanDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackUIPan"], typeof(GetTrackUIPanDelegate));
        
        GetTrackUIVolPan = (GetTrackUIVolPanDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetTrackUIVolPan"], typeof(GetTrackUIVolPanDelegate));
        
        GetUserFileNameForRead = (GetUserFileNameForReadDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetUserFileNameForRead"], typeof(GetUserFileNameForReadDelegate));
        
        GetUserInputs = (GetUserInputsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GetUserInputs"], typeof(GetUserInputsDelegate));
        
        GoToMarker = (GoToMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GoToMarker"], typeof(GoToMarkerDelegate));
        
        GoToRegion = (GoToRegionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GoToRegion"], typeof(GoToRegionDelegate));
        
        GR_SelectColor = (GR_SelectColorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GR_SelectColor"], typeof(GR_SelectColorDelegate));
        
        GSC_mainwnd = (GSC_mainwndDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["GSC_mainwnd"], typeof(GSC_mainwndDelegate));
        
        guidToString = (guidToStringDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["guidToString"], typeof(guidToStringDelegate));
        
        HasExtState = (HasExtStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["HasExtState"], typeof(HasExtStateDelegate));
        
        HasTrackMIDIPrograms = (HasTrackMIDIProgramsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["HasTrackMIDIPrograms"], typeof(HasTrackMIDIProgramsDelegate));
        
        HasTrackMIDIProgramsEx = (HasTrackMIDIProgramsExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["HasTrackMIDIProgramsEx"], typeof(HasTrackMIDIProgramsExDelegate));
        
        Help_Set = (Help_SetDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Help_Set"], typeof(Help_SetDelegate));
        
        HiresPeaksFromSource = (HiresPeaksFromSourceDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["HiresPeaksFromSource"], typeof(HiresPeaksFromSourceDelegate));
        
        image_resolve_fn = (image_resolve_fnDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["image_resolve_fn"], typeof(image_resolve_fnDelegate));
        
        InsertMedia = (InsertMediaDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["InsertMedia"], typeof(InsertMediaDelegate));
        
        InsertMediaSection = (InsertMediaSectionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["InsertMediaSection"], typeof(InsertMediaSectionDelegate));
        
        InsertTrackAtIndex = (InsertTrackAtIndexDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["InsertTrackAtIndex"], typeof(InsertTrackAtIndexDelegate));
        
        IsInRealTimeAudio = (IsInRealTimeAudioDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["IsInRealTimeAudio"], typeof(IsInRealTimeAudioDelegate));
        
        IsItemTakeActiveForPlayback = (IsItemTakeActiveForPlaybackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["IsItemTakeActiveForPlayback"], typeof(IsItemTakeActiveForPlaybackDelegate));
        
        IsMediaExtension = (IsMediaExtensionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["IsMediaExtension"], typeof(IsMediaExtensionDelegate));
        
        IsMediaItemSelected = (IsMediaItemSelectedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["IsMediaItemSelected"], typeof(IsMediaItemSelectedDelegate));
        
        IsTrackSelected = (IsTrackSelectedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["IsTrackSelected"], typeof(IsTrackSelectedDelegate));
        
        IsTrackVisible = (IsTrackVisibleDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["IsTrackVisible"], typeof(IsTrackVisibleDelegate));
        
        kbd_enumerateActions = (kbd_enumerateActionsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_enumerateActions"], typeof(kbd_enumerateActionsDelegate));
        
        kbd_formatKeyName = (kbd_formatKeyNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_formatKeyName"], typeof(kbd_formatKeyNameDelegate));
        
        kbd_getCommandName = (kbd_getCommandNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_getCommandName"], typeof(kbd_getCommandNameDelegate));
        
        kbd_getTextFromCmd = (kbd_getTextFromCmdDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_getTextFromCmd"], typeof(kbd_getTextFromCmdDelegate));
        
        KBD_OnMainActionEx = (KBD_OnMainActionExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["KBD_OnMainActionEx"], typeof(KBD_OnMainActionExDelegate));
        
        kbd_OnMidiEvent = (kbd_OnMidiEventDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_OnMidiEvent"], typeof(kbd_OnMidiEventDelegate));
        
        kbd_OnMidiList = (kbd_OnMidiListDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_OnMidiList"], typeof(kbd_OnMidiListDelegate));
        
        kbd_ProcessActionsMenu = (kbd_ProcessActionsMenuDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_ProcessActionsMenu"], typeof(kbd_ProcessActionsMenuDelegate));
        
        kbd_processMidiEventActionEx = (kbd_processMidiEventActionExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_processMidiEventActionEx"], typeof(kbd_processMidiEventActionExDelegate));
        
        kbd_reprocessMenu = (kbd_reprocessMenuDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_reprocessMenu"], typeof(kbd_reprocessMenuDelegate));
        
        kbd_RunCommandThroughHooks = (kbd_RunCommandThroughHooksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_RunCommandThroughHooks"], typeof(kbd_RunCommandThroughHooksDelegate));
        
        kbd_translateAccelerator = (kbd_translateAcceleratorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_translateAccelerator"], typeof(kbd_translateAcceleratorDelegate));
        
        kbd_translateMouse = (kbd_translateMouseDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["kbd_translateMouse"], typeof(kbd_translateMouseDelegate));
        
        Loop_OnArrow = (Loop_OnArrowDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Loop_OnArrow"], typeof(Loop_OnArrowDelegate));
        
        Main_OnCommand = (Main_OnCommandDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Main_OnCommand"], typeof(Main_OnCommandDelegate));
        
        Main_OnCommandEx = (Main_OnCommandExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Main_OnCommandEx"], typeof(Main_OnCommandExDelegate));
        
        Main_openProject = (Main_openProjectDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Main_openProject"], typeof(Main_openProjectDelegate));
        
        Main_UpdateLoopInfo = (Main_UpdateLoopInfoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Main_UpdateLoopInfo"], typeof(Main_UpdateLoopInfoDelegate));
        
        MarkProjectDirty = (MarkProjectDirtyDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MarkProjectDirty"], typeof(MarkProjectDirtyDelegate));
        
        MarkTrackItemsDirty = (MarkTrackItemsDirtyDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MarkTrackItemsDirty"], typeof(MarkTrackItemsDirtyDelegate));
        
        Master_GetPlayRate = (Master_GetPlayRateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Master_GetPlayRate"], typeof(Master_GetPlayRateDelegate));
        
        Master_GetPlayRateAtTime = (Master_GetPlayRateAtTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Master_GetPlayRateAtTime"], typeof(Master_GetPlayRateAtTimeDelegate));
        
        Master_GetTempo = (Master_GetTempoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Master_GetTempo"], typeof(Master_GetTempoDelegate));
        
        Master_NormalizePlayRate = (Master_NormalizePlayRateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Master_NormalizePlayRate"], typeof(Master_NormalizePlayRateDelegate));
        
        Master_NormalizeTempo = (Master_NormalizeTempoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Master_NormalizeTempo"], typeof(Master_NormalizeTempoDelegate));
        
        MB = (MBDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MB"], typeof(MBDelegate));
        
        MediaItemDescendsFromTrack = (MediaItemDescendsFromTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MediaItemDescendsFromTrack"], typeof(MediaItemDescendsFromTrackDelegate));
        
        MIDI_CountEvts = (MIDI_CountEvtsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_CountEvts"], typeof(MIDI_CountEvtsDelegate));
        
        MIDI_DeleteCC = (MIDI_DeleteCCDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_DeleteCC"], typeof(MIDI_DeleteCCDelegate));
        
        MIDI_DeleteEvt = (MIDI_DeleteEvtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_DeleteEvt"], typeof(MIDI_DeleteEvtDelegate));
        
        MIDI_DeleteNote = (MIDI_DeleteNoteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_DeleteNote"], typeof(MIDI_DeleteNoteDelegate));
        
        MIDI_DeleteTextSysexEvt = (MIDI_DeleteTextSysexEvtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_DeleteTextSysexEvt"], typeof(MIDI_DeleteTextSysexEvtDelegate));
        
        MIDI_EnumSelCC = (MIDI_EnumSelCCDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_EnumSelCC"], typeof(MIDI_EnumSelCCDelegate));
        
        MIDI_EnumSelEvts = (MIDI_EnumSelEvtsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_EnumSelEvts"], typeof(MIDI_EnumSelEvtsDelegate));
        
        MIDI_EnumSelNotes = (MIDI_EnumSelNotesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_EnumSelNotes"], typeof(MIDI_EnumSelNotesDelegate));
        
        MIDI_EnumSelTextSysexEvts = (MIDI_EnumSelTextSysexEvtsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_EnumSelTextSysexEvts"], typeof(MIDI_EnumSelTextSysexEvtsDelegate));
        
        MIDI_eventlist_Create = (MIDI_eventlist_CreateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_eventlist_Create"], typeof(MIDI_eventlist_CreateDelegate));
        
        MIDI_eventlist_Destroy = (MIDI_eventlist_DestroyDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_eventlist_Destroy"], typeof(MIDI_eventlist_DestroyDelegate));
        
        MIDI_GetCC = (MIDI_GetCCDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_GetCC"], typeof(MIDI_GetCCDelegate));
        
        MIDI_GetEvt = (MIDI_GetEvtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_GetEvt"], typeof(MIDI_GetEvtDelegate));
        
        MIDI_GetNote = (MIDI_GetNoteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_GetNote"], typeof(MIDI_GetNoteDelegate));
        
        MIDI_GetPPQPos_EndOfMeasure = (MIDI_GetPPQPos_EndOfMeasureDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_GetPPQPos_EndOfMeasure"], typeof(MIDI_GetPPQPos_EndOfMeasureDelegate));
        
        MIDI_GetPPQPos_StartOfMeasure = (MIDI_GetPPQPos_StartOfMeasureDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_GetPPQPos_StartOfMeasure"], typeof(MIDI_GetPPQPos_StartOfMeasureDelegate));
        
        MIDI_GetPPQPosFromProjTime = (MIDI_GetPPQPosFromProjTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_GetPPQPosFromProjTime"], typeof(MIDI_GetPPQPosFromProjTimeDelegate));
        
        MIDI_GetProjTimeFromPPQPos = (MIDI_GetProjTimeFromPPQPosDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_GetProjTimeFromPPQPos"], typeof(MIDI_GetProjTimeFromPPQPosDelegate));
        
        MIDI_GetTextSysexEvt = (MIDI_GetTextSysexEvtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_GetTextSysexEvt"], typeof(MIDI_GetTextSysexEvtDelegate));
        
        MIDI_InsertCC = (MIDI_InsertCCDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_InsertCC"], typeof(MIDI_InsertCCDelegate));
        
        MIDI_InsertEvt = (MIDI_InsertEvtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_InsertEvt"], typeof(MIDI_InsertEvtDelegate));
        
        MIDI_InsertNote = (MIDI_InsertNoteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_InsertNote"], typeof(MIDI_InsertNoteDelegate));
        
        MIDI_InsertTextSysexEvt = (MIDI_InsertTextSysexEvtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_InsertTextSysexEvt"], typeof(MIDI_InsertTextSysexEvtDelegate));
        
        midi_reinit = (midi_reinitDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["midi_reinit"], typeof(midi_reinitDelegate));
        
        MIDI_SetCC = (MIDI_SetCCDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_SetCC"], typeof(MIDI_SetCCDelegate));
        
        MIDI_SetEvt = (MIDI_SetEvtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_SetEvt"], typeof(MIDI_SetEvtDelegate));
        
        MIDI_SetNote = (MIDI_SetNoteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_SetNote"], typeof(MIDI_SetNoteDelegate));
        
        MIDI_SetTextSysexEvt = (MIDI_SetTextSysexEvtDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDI_SetTextSysexEvt"], typeof(MIDI_SetTextSysexEvtDelegate));
        
        MIDIEditor_GetActive = (MIDIEditor_GetActiveDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDIEditor_GetActive"], typeof(MIDIEditor_GetActiveDelegate));
        
        MIDIEditor_GetMode = (MIDIEditor_GetModeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDIEditor_GetMode"], typeof(MIDIEditor_GetModeDelegate));
        
        MIDIEditor_GetSetting_int = (MIDIEditor_GetSetting_intDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDIEditor_GetSetting_int"], typeof(MIDIEditor_GetSetting_intDelegate));
        
        MIDIEditor_GetSetting_str = (MIDIEditor_GetSetting_strDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDIEditor_GetSetting_str"], typeof(MIDIEditor_GetSetting_strDelegate));
        
        MIDIEditor_GetTake = (MIDIEditor_GetTakeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDIEditor_GetTake"], typeof(MIDIEditor_GetTakeDelegate));
        
        MIDIEditor_LastFocused_OnCommand = (MIDIEditor_LastFocused_OnCommandDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDIEditor_LastFocused_OnCommand"], typeof(MIDIEditor_LastFocused_OnCommandDelegate));
        
        MIDIEditor_OnCommand = (MIDIEditor_OnCommandDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MIDIEditor_OnCommand"], typeof(MIDIEditor_OnCommandDelegate));
        
        mkpanstr = (mkpanstrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["mkpanstr"], typeof(mkpanstrDelegate));
        
        mkvolpanstr = (mkvolpanstrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["mkvolpanstr"], typeof(mkvolpanstrDelegate));
        
        mkvolstr = (mkvolstrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["mkvolstr"], typeof(mkvolstrDelegate));
        
        MoveEditCursor = (MoveEditCursorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MoveEditCursor"], typeof(MoveEditCursorDelegate));
        
        MoveMediaItemToTrack = (MoveMediaItemToTrackDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MoveMediaItemToTrack"], typeof(MoveMediaItemToTrackDelegate));
        
        MuteAllTracks = (MuteAllTracksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["MuteAllTracks"], typeof(MuteAllTracksDelegate));
        
        my_getViewport = (my_getViewportDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["my_getViewport"], typeof(my_getViewportDelegate));
        
        NamedCommandLookup = (NamedCommandLookupDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["NamedCommandLookup"], typeof(NamedCommandLookupDelegate));
        
        OnPauseButton = (OnPauseButtonDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["OnPauseButton"], typeof(OnPauseButtonDelegate));
        
        OnPauseButtonEx = (OnPauseButtonExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["OnPauseButtonEx"], typeof(OnPauseButtonExDelegate));
        
        OnPlayButton = (OnPlayButtonDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["OnPlayButton"], typeof(OnPlayButtonDelegate));
        
        OnPlayButtonEx = (OnPlayButtonExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["OnPlayButtonEx"], typeof(OnPlayButtonExDelegate));
        
        OnStopButton = (OnStopButtonDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["OnStopButton"], typeof(OnStopButtonDelegate));
        
        OnStopButtonEx = (OnStopButtonExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["OnStopButtonEx"], typeof(OnStopButtonExDelegate));
        
        OscLocalMessageToHost = (OscLocalMessageToHostDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["OscLocalMessageToHost"], typeof(OscLocalMessageToHostDelegate));
        
        parse_timestr = (parse_timestrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["parse_timestr"], typeof(parse_timestrDelegate));
        
        parse_timestr_len = (parse_timestr_lenDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["parse_timestr_len"], typeof(parse_timestr_lenDelegate));
        
        parse_timestr_pos = (parse_timestr_posDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["parse_timestr_pos"], typeof(parse_timestr_posDelegate));
        
        parsepanstr = (parsepanstrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["parsepanstr"], typeof(parsepanstrDelegate));
        
        PCM_Sink_Create = (PCM_Sink_CreateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Sink_Create"], typeof(PCM_Sink_CreateDelegate));
        
        PCM_Sink_CreateEx = (PCM_Sink_CreateExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Sink_CreateEx"], typeof(PCM_Sink_CreateExDelegate));
        
        PCM_Sink_CreateMIDIFile = (PCM_Sink_CreateMIDIFileDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Sink_CreateMIDIFile"], typeof(PCM_Sink_CreateMIDIFileDelegate));
        
        PCM_Sink_CreateMIDIFileEx = (PCM_Sink_CreateMIDIFileExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Sink_CreateMIDIFileEx"], typeof(PCM_Sink_CreateMIDIFileExDelegate));
        
        PCM_Sink_Enum = (PCM_Sink_EnumDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Sink_Enum"], typeof(PCM_Sink_EnumDelegate));
        
        PCM_Sink_GetExtension = (PCM_Sink_GetExtensionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Sink_GetExtension"], typeof(PCM_Sink_GetExtensionDelegate));
        
        PCM_Sink_ShowConfig = (PCM_Sink_ShowConfigDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Sink_ShowConfig"], typeof(PCM_Sink_ShowConfigDelegate));
        
        PCM_Source_CreateFromFile = (PCM_Source_CreateFromFileDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Source_CreateFromFile"], typeof(PCM_Source_CreateFromFileDelegate));
        
        PCM_Source_CreateFromFileEx = (PCM_Source_CreateFromFileExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Source_CreateFromFileEx"], typeof(PCM_Source_CreateFromFileExDelegate));
        
        PCM_Source_CreateFromSimple = (PCM_Source_CreateFromSimpleDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Source_CreateFromSimple"], typeof(PCM_Source_CreateFromSimpleDelegate));
        
        PCM_Source_CreateFromType = (PCM_Source_CreateFromTypeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Source_CreateFromType"], typeof(PCM_Source_CreateFromTypeDelegate));
        
        PCM_Source_GetSectionInfo = (PCM_Source_GetSectionInfoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PCM_Source_GetSectionInfo"], typeof(PCM_Source_GetSectionInfoDelegate));
        
        PeakBuild_Create = (PeakBuild_CreateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PeakBuild_Create"], typeof(PeakBuild_CreateDelegate));
        
        PeakGet_Create = (PeakGet_CreateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PeakGet_Create"], typeof(PeakGet_CreateDelegate));
        
        PlayPreview = (PlayPreviewDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PlayPreview"], typeof(PlayPreviewDelegate));
        
        PlayPreviewEx = (PlayPreviewExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PlayPreviewEx"], typeof(PlayPreviewExDelegate));
        
        PlayTrackPreview = (PlayTrackPreviewDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PlayTrackPreview"], typeof(PlayTrackPreviewDelegate));
        
        PlayTrackPreview2 = (PlayTrackPreview2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PlayTrackPreview2"], typeof(PlayTrackPreview2Delegate));
        
        PlayTrackPreview2Ex = (PlayTrackPreview2ExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PlayTrackPreview2Ex"], typeof(PlayTrackPreview2ExDelegate));
        
        plugin_getapi = (plugin_getapiDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["plugin_getapi"], typeof(plugin_getapiDelegate));
        
        plugin_getFilterList = (plugin_getFilterListDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["plugin_getFilterList"], typeof(plugin_getFilterListDelegate));
        
        plugin_getImportableProjectFilterList = (plugin_getImportableProjectFilterListDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["plugin_getImportableProjectFilterList"], typeof(plugin_getImportableProjectFilterListDelegate));
        
        plugin_register = (plugin_registerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["plugin_register"], typeof(plugin_registerDelegate));
        
        PluginWantsAlwaysRunFx = (PluginWantsAlwaysRunFxDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PluginWantsAlwaysRunFx"], typeof(PluginWantsAlwaysRunFxDelegate));
        
        PreventUIRefresh = (PreventUIRefreshDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["PreventUIRefresh"], typeof(PreventUIRefreshDelegate));
        
        projectconfig_var_addr = (projectconfig_var_addrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["projectconfig_var_addr"], typeof(projectconfig_var_addrDelegate));
        
        projectconfig_var_getoffs = (projectconfig_var_getoffsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["projectconfig_var_getoffs"], typeof(projectconfig_var_getoffsDelegate));
        
        ReaperGetPitchShiftAPI = (ReaperGetPitchShiftAPIDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ReaperGetPitchShiftAPI"], typeof(ReaperGetPitchShiftAPIDelegate));
        
        ReaScriptError = (ReaScriptErrorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ReaScriptError"], typeof(ReaScriptErrorDelegate));
        
        RecursiveCreateDirectory = (RecursiveCreateDirectoryDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["RecursiveCreateDirectory"], typeof(RecursiveCreateDirectoryDelegate));
        
        RefreshToolbar = (RefreshToolbarDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["RefreshToolbar"], typeof(RefreshToolbarDelegate));
        
        relative_fn = (relative_fnDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["relative_fn"], typeof(relative_fnDelegate));
        
        RenderFileSection = (RenderFileSectionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["RenderFileSection"], typeof(RenderFileSectionDelegate));
        
        Resample_EnumModes = (Resample_EnumModesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Resample_EnumModes"], typeof(Resample_EnumModesDelegate));
        
        Resampler_Create = (Resampler_CreateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Resampler_Create"], typeof(Resampler_CreateDelegate));
        
        resolve_fn = (resolve_fnDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["resolve_fn"], typeof(resolve_fnDelegate));
        
        resolve_fn2 = (resolve_fn2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["resolve_fn2"], typeof(resolve_fn2Delegate));
        
        screenset_register = (screenset_registerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["screenset_register"], typeof(screenset_registerDelegate));
        
        screenset_registerNew = (screenset_registerNewDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["screenset_registerNew"], typeof(screenset_registerNewDelegate));
        
        screenset_unregister = (screenset_unregisterDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["screenset_unregister"], typeof(screenset_unregisterDelegate));
        
        screenset_unregisterByParam = (screenset_unregisterByParamDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["screenset_unregisterByParam"], typeof(screenset_unregisterByParamDelegate));
        
        SectionFromUniqueID = (SectionFromUniqueIDDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SectionFromUniqueID"], typeof(SectionFromUniqueIDDelegate));
        
        SelectAllMediaItems = (SelectAllMediaItemsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SelectAllMediaItems"], typeof(SelectAllMediaItemsDelegate));
        
        SelectProjectInstance = (SelectProjectInstanceDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SelectProjectInstance"], typeof(SelectProjectInstanceDelegate));
        
        SendLocalOscMessage = (SendLocalOscMessageDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SendLocalOscMessage"], typeof(SendLocalOscMessageDelegate));
        
        SetActiveTake = (SetActiveTakeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetActiveTake"], typeof(SetActiveTakeDelegate));
        
        SetAutomationMode = (SetAutomationModeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetAutomationMode"], typeof(SetAutomationModeDelegate));
        
        SetCurrentBPM = (SetCurrentBPMDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetCurrentBPM"], typeof(SetCurrentBPMDelegate));
        
        SetEditCurPos = (SetEditCurPosDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetEditCurPos"], typeof(SetEditCurPosDelegate));
        
        SetEditCurPos2 = (SetEditCurPos2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetEditCurPos2"], typeof(SetEditCurPos2Delegate));
        
        SetExtState = (SetExtStateDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetExtState"], typeof(SetExtStateDelegate));
        
        SetMasterTrackVisibility = (SetMasterTrackVisibilityDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMasterTrackVisibility"], typeof(SetMasterTrackVisibilityDelegate));
        
        SetMediaItemInfo_Value = (SetMediaItemInfo_ValueDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMediaItemInfo_Value"], typeof(SetMediaItemInfo_ValueDelegate));
        
        SetMediaItemLength = (SetMediaItemLengthDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMediaItemLength"], typeof(SetMediaItemLengthDelegate));
        
        SetMediaItemPosition = (SetMediaItemPositionDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMediaItemPosition"], typeof(SetMediaItemPositionDelegate));
        
        SetMediaItemSelected = (SetMediaItemSelectedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMediaItemSelected"], typeof(SetMediaItemSelectedDelegate));
        
        SetMediaItemTakeInfo_Value = (SetMediaItemTakeInfo_ValueDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMediaItemTakeInfo_Value"], typeof(SetMediaItemTakeInfo_ValueDelegate));
        
        SetMediaTrackInfo_Value = (SetMediaTrackInfo_ValueDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMediaTrackInfo_Value"], typeof(SetMediaTrackInfo_ValueDelegate));
        
        SetMixerScroll = (SetMixerScrollDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMixerScroll"], typeof(SetMixerScrollDelegate));
        
        SetMouseModifier = (SetMouseModifierDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetMouseModifier"], typeof(SetMouseModifierDelegate));
        
        SetOnlyTrackSelected = (SetOnlyTrackSelectedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetOnlyTrackSelected"], typeof(SetOnlyTrackSelectedDelegate));
        
        SetProjectMarker = (SetProjectMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetProjectMarker"], typeof(SetProjectMarkerDelegate));
        
        SetProjectMarker2 = (SetProjectMarker2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetProjectMarker2"], typeof(SetProjectMarker2Delegate));
        
        SetProjectMarker3 = (SetProjectMarker3Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetProjectMarker3"], typeof(SetProjectMarker3Delegate));
        
        SetProjectMarkerByIndex = (SetProjectMarkerByIndexDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetProjectMarkerByIndex"], typeof(SetProjectMarkerByIndexDelegate));
        
        SetRegionRenderMatrix = (SetRegionRenderMatrixDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetRegionRenderMatrix"], typeof(SetRegionRenderMatrixDelegate));
        
        SetRenderLastError = (SetRenderLastErrorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetRenderLastError"], typeof(SetRenderLastErrorDelegate));
        
        SetTakeStretchMarker = (SetTakeStretchMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTakeStretchMarker"], typeof(SetTakeStretchMarkerDelegate));
        
        SetTempoTimeSigMarker = (SetTempoTimeSigMarkerDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTempoTimeSigMarker"], typeof(SetTempoTimeSigMarkerDelegate));
        
        SetTrackAutomationMode = (SetTrackAutomationModeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTrackAutomationMode"], typeof(SetTrackAutomationModeDelegate));
        
        SetTrackColor = (SetTrackColorDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTrackColor"], typeof(SetTrackColorDelegate));
        
        SetTrackMIDINoteName = (SetTrackMIDINoteNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTrackMIDINoteName"], typeof(SetTrackMIDINoteNameDelegate));
        
        SetTrackMIDINoteNameEx = (SetTrackMIDINoteNameExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTrackMIDINoteNameEx"], typeof(SetTrackMIDINoteNameExDelegate));
        
        SetTrackSelected = (SetTrackSelectedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTrackSelected"], typeof(SetTrackSelectedDelegate));
        
        SetTrackSendUIPan = (SetTrackSendUIPanDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTrackSendUIPan"], typeof(SetTrackSendUIPanDelegate));
        
        SetTrackSendUIVol = (SetTrackSendUIVolDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SetTrackSendUIVol"], typeof(SetTrackSendUIVolDelegate));
        
        ShowActionList = (ShowActionListDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ShowActionList"], typeof(ShowActionListDelegate));
        
        ShowConsoleMsg = (ShowConsoleMsgDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ShowConsoleMsg"], typeof(ShowConsoleMsgDelegate));
        
        ShowMessageBox = (ShowMessageBoxDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ShowMessageBox"], typeof(ShowMessageBoxDelegate));
        
        SLIDER2DB = (SLIDER2DBDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SLIDER2DB"], typeof(SLIDER2DBDelegate));
        
        SnapToGrid = (SnapToGridDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SnapToGrid"], typeof(SnapToGridDelegate));
        
        SoloAllTracks = (SoloAllTracksDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SoloAllTracks"], typeof(SoloAllTracksDelegate));
        
        SplitMediaItem = (SplitMediaItemDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["SplitMediaItem"], typeof(SplitMediaItemDelegate));
        
        StopPreview = (StopPreviewDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["StopPreview"], typeof(StopPreviewDelegate));
        
        StopTrackPreview = (StopTrackPreviewDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["StopTrackPreview"], typeof(StopTrackPreviewDelegate));
        
        StopTrackPreview2 = (StopTrackPreview2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["StopTrackPreview2"], typeof(StopTrackPreview2Delegate));
        
        stringToGuid = (stringToGuidDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["stringToGuid"], typeof(stringToGuidDelegate));
        
        StuffMIDIMessage = (StuffMIDIMessageDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["StuffMIDIMessage"], typeof(StuffMIDIMessageDelegate));
        
        TimeMap2_beatsToTime = (TimeMap2_beatsToTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap2_beatsToTime"], typeof(TimeMap2_beatsToTimeDelegate));
        
        TimeMap2_GetDividedBpmAtTime = (TimeMap2_GetDividedBpmAtTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap2_GetDividedBpmAtTime"], typeof(TimeMap2_GetDividedBpmAtTimeDelegate));
        
        TimeMap2_GetNextChangeTime = (TimeMap2_GetNextChangeTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap2_GetNextChangeTime"], typeof(TimeMap2_GetNextChangeTimeDelegate));
        
        TimeMap2_QNToTime = (TimeMap2_QNToTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap2_QNToTime"], typeof(TimeMap2_QNToTimeDelegate));
        
        TimeMap2_timeToBeats = (TimeMap2_timeToBeatsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap2_timeToBeats"], typeof(TimeMap2_timeToBeatsDelegate));
        
        TimeMap2_timeToQN = (TimeMap2_timeToQNDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap2_timeToQN"], typeof(TimeMap2_timeToQNDelegate));
        
        TimeMap_GetDividedBpmAtTime = (TimeMap_GetDividedBpmAtTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap_GetDividedBpmAtTime"], typeof(TimeMap_GetDividedBpmAtTimeDelegate));
        
        TimeMap_GetTimeSigAtTime = (TimeMap_GetTimeSigAtTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap_GetTimeSigAtTime"], typeof(TimeMap_GetTimeSigAtTimeDelegate));
        
        TimeMap_QNToTime = (TimeMap_QNToTimeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap_QNToTime"], typeof(TimeMap_QNToTimeDelegate));
        
        TimeMap_QNToTime_abs = (TimeMap_QNToTime_absDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap_QNToTime_abs"], typeof(TimeMap_QNToTime_absDelegate));
        
        TimeMap_timeToQN = (TimeMap_timeToQNDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap_timeToQN"], typeof(TimeMap_timeToQNDelegate));
        
        TimeMap_timeToQN_abs = (TimeMap_timeToQN_absDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TimeMap_timeToQN_abs"], typeof(TimeMap_timeToQN_absDelegate));
        
        ToggleTrackSendUIMute = (ToggleTrackSendUIMuteDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ToggleTrackSendUIMute"], typeof(ToggleTrackSendUIMuteDelegate));
        
        Track_GetPeakHoldDB = (Track_GetPeakHoldDBDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Track_GetPeakHoldDB"], typeof(Track_GetPeakHoldDBDelegate));
        
        Track_GetPeakInfo = (Track_GetPeakInfoDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Track_GetPeakInfo"], typeof(Track_GetPeakInfoDelegate));
        
        TrackCtl_SetToolTip = (TrackCtl_SetToolTipDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackCtl_SetToolTip"], typeof(TrackCtl_SetToolTipDelegate));
        
        TrackFX_EndParamEdit = (TrackFX_EndParamEditDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_EndParamEdit"], typeof(TrackFX_EndParamEditDelegate));
        
        TrackFX_FormatParamValue = (TrackFX_FormatParamValueDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_FormatParamValue"], typeof(TrackFX_FormatParamValueDelegate));
        
        TrackFX_FormatParamValueNormalized = (TrackFX_FormatParamValueNormalizedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_FormatParamValueNormalized"], typeof(TrackFX_FormatParamValueNormalizedDelegate));
        
        TrackFX_GetByName = (TrackFX_GetByNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetByName"], typeof(TrackFX_GetByNameDelegate));
        
        TrackFX_GetChainVisible = (TrackFX_GetChainVisibleDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetChainVisible"], typeof(TrackFX_GetChainVisibleDelegate));
        
        TrackFX_GetCount = (TrackFX_GetCountDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetCount"], typeof(TrackFX_GetCountDelegate));
        
        TrackFX_GetEnabled = (TrackFX_GetEnabledDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetEnabled"], typeof(TrackFX_GetEnabledDelegate));
        
        TrackFX_GetEQ = (TrackFX_GetEQDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetEQ"], typeof(TrackFX_GetEQDelegate));
        
        TrackFX_GetEQBandEnabled = (TrackFX_GetEQBandEnabledDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetEQBandEnabled"], typeof(TrackFX_GetEQBandEnabledDelegate));
        
        TrackFX_GetEQParam = (TrackFX_GetEQParamDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetEQParam"], typeof(TrackFX_GetEQParamDelegate));
        
        TrackFX_GetFloatingWindow = (TrackFX_GetFloatingWindowDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetFloatingWindow"], typeof(TrackFX_GetFloatingWindowDelegate));
        
        TrackFX_GetFormattedParamValue = (TrackFX_GetFormattedParamValueDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetFormattedParamValue"], typeof(TrackFX_GetFormattedParamValueDelegate));
        
        TrackFX_GetFXGUID = (TrackFX_GetFXGUIDDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetFXGUID"], typeof(TrackFX_GetFXGUIDDelegate));
        
        TrackFX_GetFXName = (TrackFX_GetFXNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetFXName"], typeof(TrackFX_GetFXNameDelegate));
        
        TrackFX_GetInstrument = (TrackFX_GetInstrumentDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetInstrument"], typeof(TrackFX_GetInstrumentDelegate));
        
        TrackFX_GetNumParams = (TrackFX_GetNumParamsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetNumParams"], typeof(TrackFX_GetNumParamsDelegate));
        
        TrackFX_GetOpen = (TrackFX_GetOpenDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetOpen"], typeof(TrackFX_GetOpenDelegate));
        
        TrackFX_GetParam = (TrackFX_GetParamDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetParam"], typeof(TrackFX_GetParamDelegate));
        
        TrackFX_GetParameterStepSizes = (TrackFX_GetParameterStepSizesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetParameterStepSizes"], typeof(TrackFX_GetParameterStepSizesDelegate));
        
        TrackFX_GetParamEx = (TrackFX_GetParamExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetParamEx"], typeof(TrackFX_GetParamExDelegate));
        
        TrackFX_GetParamName = (TrackFX_GetParamNameDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetParamName"], typeof(TrackFX_GetParamNameDelegate));
        
        TrackFX_GetParamNormalized = (TrackFX_GetParamNormalizedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetParamNormalized"], typeof(TrackFX_GetParamNormalizedDelegate));
        
        TrackFX_GetPreset = (TrackFX_GetPresetDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetPreset"], typeof(TrackFX_GetPresetDelegate));
        
        TrackFX_GetPresetIndex = (TrackFX_GetPresetIndexDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_GetPresetIndex"], typeof(TrackFX_GetPresetIndexDelegate));
        
        TrackFX_NavigatePresets = (TrackFX_NavigatePresetsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_NavigatePresets"], typeof(TrackFX_NavigatePresetsDelegate));
        
        TrackFX_SetEnabled = (TrackFX_SetEnabledDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_SetEnabled"], typeof(TrackFX_SetEnabledDelegate));
        
        TrackFX_SetEQBandEnabled = (TrackFX_SetEQBandEnabledDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_SetEQBandEnabled"], typeof(TrackFX_SetEQBandEnabledDelegate));
        
        TrackFX_SetEQParam = (TrackFX_SetEQParamDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_SetEQParam"], typeof(TrackFX_SetEQParamDelegate));
        
        TrackFX_SetOpen = (TrackFX_SetOpenDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_SetOpen"], typeof(TrackFX_SetOpenDelegate));
        
        TrackFX_SetParam = (TrackFX_SetParamDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_SetParam"], typeof(TrackFX_SetParamDelegate));
        
        TrackFX_SetParamNormalized = (TrackFX_SetParamNormalizedDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_SetParamNormalized"], typeof(TrackFX_SetParamNormalizedDelegate));
        
        TrackFX_SetPreset = (TrackFX_SetPresetDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_SetPreset"], typeof(TrackFX_SetPresetDelegate));
        
        TrackFX_SetPresetByIndex = (TrackFX_SetPresetByIndexDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_SetPresetByIndex"], typeof(TrackFX_SetPresetByIndexDelegate));
        
        TrackFX_Show = (TrackFX_ShowDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackFX_Show"], typeof(TrackFX_ShowDelegate));
        
        TrackList_AdjustWindows = (TrackList_AdjustWindowsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackList_AdjustWindows"], typeof(TrackList_AdjustWindowsDelegate));
        
        TrackList_UpdateAllExternalSurfaces = (TrackList_UpdateAllExternalSurfacesDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["TrackList_UpdateAllExternalSurfaces"], typeof(TrackList_UpdateAllExternalSurfacesDelegate));
        
        Undo_BeginBlock = (Undo_BeginBlockDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_BeginBlock"], typeof(Undo_BeginBlockDelegate));
        
        Undo_BeginBlock2 = (Undo_BeginBlock2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_BeginBlock2"], typeof(Undo_BeginBlock2Delegate));
        
        Undo_CanRedo2 = (Undo_CanRedo2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_CanRedo2"], typeof(Undo_CanRedo2Delegate));
        
        Undo_CanUndo2 = (Undo_CanUndo2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_CanUndo2"], typeof(Undo_CanUndo2Delegate));
        
        Undo_DoRedo2 = (Undo_DoRedo2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_DoRedo2"], typeof(Undo_DoRedo2Delegate));
        
        Undo_DoUndo2 = (Undo_DoUndo2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_DoUndo2"], typeof(Undo_DoUndo2Delegate));
        
        Undo_EndBlock = (Undo_EndBlockDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_EndBlock"], typeof(Undo_EndBlockDelegate));
        
        Undo_EndBlock2 = (Undo_EndBlock2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_EndBlock2"], typeof(Undo_EndBlock2Delegate));
        
        Undo_OnStateChange = (Undo_OnStateChangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_OnStateChange"], typeof(Undo_OnStateChangeDelegate));
        
        Undo_OnStateChange2 = (Undo_OnStateChange2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_OnStateChange2"], typeof(Undo_OnStateChange2Delegate));
        
        Undo_OnStateChange_Item = (Undo_OnStateChange_ItemDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_OnStateChange_Item"], typeof(Undo_OnStateChange_ItemDelegate));
        
        Undo_OnStateChangeEx = (Undo_OnStateChangeExDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_OnStateChangeEx"], typeof(Undo_OnStateChangeExDelegate));
        
        Undo_OnStateChangeEx2 = (Undo_OnStateChangeEx2Delegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["Undo_OnStateChangeEx2"], typeof(Undo_OnStateChangeEx2Delegate));
        
        UpdateArrange = (UpdateArrangeDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["UpdateArrange"], typeof(UpdateArrangeDelegate));
        
        UpdateItemInProject = (UpdateItemInProjectDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["UpdateItemInProject"], typeof(UpdateItemInProjectDelegate));
        
        UpdateTimeline = (UpdateTimelineDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["UpdateTimeline"], typeof(UpdateTimelineDelegate));
        
        ValidatePtr = (ValidatePtrDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ValidatePtr"], typeof(ValidatePtrDelegate));
        
        ViewPrefs = (ViewPrefsDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["ViewPrefs"], typeof(ViewPrefsDelegate));
        
        WDL_VirtualWnd_ScaledBlitBG = (WDL_VirtualWnd_ScaledBlitBGDelegate)Marshal.GetDelegateForFunctionPointer(
            reaperFunctions["WDL_VirtualWnd_ScaledBlitBG"], typeof(WDL_VirtualWnd_ScaledBlitBGDelegate));
        
    }   
    
    }
}