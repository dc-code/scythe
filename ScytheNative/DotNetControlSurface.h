#include "reaper_plugin.h"

class DotNetControlSurface : public IReaperControlSurface
{
public:

    struct DotNetControlSurfaceCInterface;
    
    explicit DotNetControlSurface(const DotNetControlSurfaceCInterface& controlSurface);

    virtual const char *GetTypeString() { return ""; } // simple unique string with only A-Z, 0-9, no spaces or other chars
    virtual const char *GetDescString() { return ""; } // human readable description (can include instance specific info)
    virtual const char *GetConfigString() { return ""; } // string of configuration data

    virtual void SetPlayState(bool play, bool pause, bool rec);

    typedef void (*SetPlayStateFunction)(bool, bool, bool);

    struct DotNetControlSurfaceCInterface
    {
        SetPlayStateFunction setPlaystate;

        DotNetControlSurface* self;
    };

    static void Create(DotNetControlSurfaceCInterface* controlSurface);
    static void Destroy(DotNetControlSurfaceCInterface* controlSurface);

private:
    DotNetControlSurfaceCInterface mControlSurface;
};