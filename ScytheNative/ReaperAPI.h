#ifndef _REAPER_API_H_
#define _REAPER_API_H_
#include "reaper_plugin.h"

#include <map>
#include <string>

namespace Scythe
{
    void init(reaper_plugin_info_t *pluginInfo);
    typedef std::map<std::string, void*> FunctionMap;
    FunctionMap& getFunctionMap();

    class AudioAccessor;
    class LICE_IBitmap;
    class WDL_VirtualWnd_BGCfg;

    #define REAPERAPI_DECL extern
    #include "reaper_functions.inc"
    #undef REAPERAPI_DECL
    
}

#endif // _REAPER_API_H_