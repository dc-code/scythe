#ifndef MIDI_H
#define MIDI_H

class MediaItem_Take;

#define DELETED 0x01
#define NEWNOTE 0x02
#define END_PPQ_MODIFIED 0x04
#define VELOCITY_MODIFIED 0x08
#define SELECTED_MODIFIED 0x10
#define MUTED_MODIFIED 0x20

struct MidiNote 
{ 
    double startPPQOriginal;
    double startPPQ;
    double endPPQ;
    unsigned char channelOriginal;
    unsigned char pitchOriginal;
    unsigned char channel;
    unsigned char pitch;
    unsigned char velocity;
    unsigned char muted;
    unsigned char selected;
    unsigned char modifiedFlags;
};

class ReaperMidiNotes
{
public:
    ReaperMidiNotes(MediaItem_Take* take);

    void add(MidiNote* note);
    
    void save();

    const std::list<MidiNote*>& notes() const;

private:

    std::list<MidiNote*>::iterator insert(
        std::list<MidiNote*>::reverse_iterator begin, 
        std::list<MidiNote*>::reverse_iterator end, 
        MidiNote* note);

    std::list<MidiNote*>::iterator insert(
        std::list<MidiNote*>::iterator begin, 
        std::list<MidiNote*>::iterator end, 
        MidiNote* note);

    bool update(MidiNote* note, int index);

    // wrapper over Reaper MIDI functions. Virtual so can be tested
    // outside Reaper.
    virtual bool setNote(int noteidx, const bool* selectedInOptional, 
        const bool* mutedInOptional, const double* startppqposInOptional, 
        const double* endppqposInOptional, const int* chanInOptional, 
        const int* pitchInOptional, const int* velInOptional);
    
    virtual bool insertNote(bool selected, bool muted, double startppqpos, 
        double endppqpos, int chan, int pitch, int vel);
    
    virtual bool deleteNote(int noteidx);

    std::list<MidiNote*> mNotes;
    MediaItem_Take* mTake;
};

#endif /* MIDI_H */
