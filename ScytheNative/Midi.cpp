#include <list>
#include "Midi.h"

namespace 
{

int compareMidiNotes(const MidiNote& lhs, const MidiNote& rhs)
{
    if (lhs.startPPQOriginal < rhs.startPPQOriginal)
    {
        return 1;
    }
    if (lhs.startPPQOriginal == rhs.startPPQOriginal)
    {
        if (lhs.pitchOriginal == rhs.pitchOriginal)
        {
            // can't have equal midi notes so don't check for equality of channels
            return lhs.channelOriginal < rhs.channelOriginal ? 1 : -1;
        }

        return lhs.pitchOriginal < rhs.pitchOriginal ? 1 : -1;
    }
    return -1;
}

}

ReaperMidiNotes::ReaperMidiNotes(MediaItem_Take* take)
: mTake(take)
{
}

const std::list<MidiNote*>& ReaperMidiNotes::notes() const
{
    return mNotes;
}

bool ReaperMidiNotes::setNote(int noteidx, const bool* selectedInOptional, 
    const bool* mutedInOptional, const double* startppqposInOptional, 
    const double* endppqposInOptional, const int* chanInOptional, 
    const int* pitchInOptional, const int* velInOptional)
{
    return true;
}
    
bool ReaperMidiNotes::insertNote(bool selected, bool muted, double startppqpos, 
    double endppqpos, int chan, int pitch, int vel)
{
    return true;
}

bool ReaperMidiNotes::deleteNote(int noteidx)
{
    return true;
}

void ReaperMidiNotes::add(MidiNote* note)
{
    if (mNotes.empty() || compareMidiNotes(*mNotes.back(), *note) > 0)
    {
        mNotes.push_back(note);
        return;
    }

    if (compareMidiNotes(*note, *mNotes.front()) > 0)
    {
        mNotes.push_front(note);
        return;
    }

    insert(mNotes.rbegin(), mNotes.rend(), note);
}

std::list<MidiNote*>::iterator ReaperMidiNotes::insert(
    std::list<MidiNote*>::iterator begin, 
    std::list<MidiNote*>::iterator end, 
    MidiNote* note)
{
    for (; begin != end; ++begin)
    {
        if (compareMidiNotes(**begin, *note) < 0)
        {
            return mNotes.insert(begin, note);
        }
    }
    mNotes.push_back(note);

    return end;
}

std::list<MidiNote*>::iterator ReaperMidiNotes::insert(
    std::list<MidiNote*>::reverse_iterator begin, 
    std::list<MidiNote*>::reverse_iterator end, 
    MidiNote* note)
{
    for (; begin != end; ++begin)
    {
        if (compareMidiNotes(**begin, *note) > 0)
        {
            return mNotes.insert(begin.base(), note);
        }
    }
    mNotes.push_front(note);

    return end.base();
}

bool ReaperMidiNotes::update(MidiNote* note, int index)
{
    if (note->startPPQ == note->startPPQOriginal && note->channel == note->channelOriginal
        && note->pitch == note->pitchOriginal && note->modifiedFlags == 0)
    {
        return false;
    }
       
    int channel = note->channel;
    int velocity = note->velocity;
    int pitch = note->pitch;
    bool selected = note->selected > 0;
    bool muted = note->muted > 0;

    double* startPPQOptional = note->startPPQ != note->startPPQOriginal ? &note->startPPQ : 0;
    int *channelOptional = note->channelOriginal != note->channel ? &channel: 0;
    int *pitchOptional = note->pitchOriginal != note->pitch? &pitch: 0;

    note->startPPQOriginal = note->startPPQ;
    note->channelOriginal = note->channel;
    note->pitchOriginal = note->pitch;
    note->modifiedFlags = 0;

    double* endPPQOptional = note->modifiedFlags & END_PPQ_MODIFIED ? &note->endPPQ : 0;
    int* velocityOptional = note->modifiedFlags & VELOCITY_MODIFIED ? &velocity : 0;
    bool *selectedOptional = note->modifiedFlags & SELECTED_MODIFIED ? &selected : 0;
    bool *mutedOptional = note->modifiedFlags & MUTED_MODIFIED ? &muted: 0;

    return setNote(index, selectedOptional, mutedOptional, startPPQOptional, endPPQOptional,
        channelOptional, pitchOptional, velocityOptional);
}


void ReaperMidiNotes::save()
{
    int index = 0;
    std::list<MidiNote*>::iterator iter = mNotes.begin();
    while (iter != mNotes.end())
    {
        MidiNote* note = *iter;
        if (note->modifiedFlags & DELETED)
        {
            deleteNote(index);
            iter = mNotes.erase(iter);
            continue;
        }

        if (note->modifiedFlags & NEWNOTE)
        {
            insertNote(note->selected > 0, note->muted > 0, 
                note->startPPQ, note->endPPQ, note->channel, 
                note->pitch, note->velocity);
            ++iter;
            continue;
        }

        if (!update(note, index))
        {
            ++iter;
            ++index;
            continue;
        }

        if (index > 0)
        {
            --iter;
            MidiNote* previousNote = *iter;
            ++iter;

            if (compareMidiNotes(*previousNote, *note) < 0)
            {
                // erase and move note down the list
                iter = mNotes.erase(iter);
                std::list<MidiNote*>::reverse_iterator start = 
                    iter == mNotes.end() ? mNotes.rbegin() : std::list<MidiNote*>::reverse_iterator(iter); 
                insert(start, mNotes.rend(), note);
                continue;
            }
        }
        
        ++iter;
        if (iter == mNotes.end()) 
        {
            break;
        }

        MidiNote* nextNote = *iter;
        iter--;

        if (compareMidiNotes(*note, *nextNote) < 0)
        {
            // erase and move note up the list
            iter = mNotes.erase(iter);
            insert(iter, mNotes.end(), note);
            continue;
        }

        ++iter;
        ++index;
    }
}






