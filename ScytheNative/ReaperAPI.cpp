#include "ReaperAPI.h"

static Scythe::FunctionMap functionMap;

namespace
{
    void registerFunction(reaper_plugin_info_t *pluginInfo, const char* functionName, void** functionPtr)
    {
        *functionPtr = pluginInfo->GetFunc(functionName);
        ::functionMap[functionName] = *functionPtr; 
    }    
}

namespace Scythe
{
    #define REAPERAPI_DECL
    #include "reaper_functions.inc"
    #undef REAPERAPI_DECL

    void init(reaper_plugin_info_t *pluginInfo)
    {
        #define REGISTER_FUNCTION(x) (registerFunction(pluginInfo, #x, (void **)&Scythe::x));
        #include "reaper_function_register.inc"
        #undef REGISTER_FUNCTION
    }

    FunctionMap& getFunctionMap()
    {
        return ::functionMap;
    }

}


