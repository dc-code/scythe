#include "DotNetControlSurface.h"
#include "ReaperAPI.h"

DotNetControlSurface::DotNetControlSurface(const DotNetControlSurfaceCInterface& controlSurface) : 
    mControlSurface(controlSurface)
{
}

void DotNetControlSurface::SetPlayState(bool play, bool pause, bool rec)
{
    if (mControlSurface.setPlaystate)
    {
        mControlSurface.setPlaystate(play, pause, rec);
    }
}

void DotNetControlSurface::Create(DotNetControlSurfaceCInterface* controlSurface)
{
    DotNetControlSurface* dotNetControlSurface = new DotNetControlSurface(*controlSurface);
    controlSurface->self = dotNetControlSurface;
    Scythe::plugin_register("csurf_inst", dotNetControlSurface);
}

void DotNetControlSurface::Destroy(DotNetControlSurfaceCInterface* controlSurface)
{
    delete controlSurface->self;
    controlSurface->self = NULL;
}
