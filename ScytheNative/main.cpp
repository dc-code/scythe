#using <mscorlib.dll>
#include "windows.h"
#include "ReaperAPI.h"

//-----------------------------------------------------------------------------

using namespace System;
using namespace System::Reflection;
using namespace System::Collections::Generic;
using namespace System::IO;
using namespace System::Windows::Forms;

//-----------------------------------------------------------------------------

namespace
{

Dictionary<String^, IntPtr>^ marshalFunctionMap()
{
    Dictionary<String^, IntPtr>^ reaperFunctions = gcnew Dictionary<String^, IntPtr>();
    for (Scythe::FunctionMap::iterator i = Scythe::getFunctionMap().begin();
         i != Scythe::getFunctionMap().end(); ++i)
    {
        reaperFunctions[gcnew String(i->first.c_str())] = IntPtr(i->second);
    }
    return reaperFunctions;
}

String^ getScytheDotNetPath()
{
    System::String^ dllPath = Assembly::GetExecutingAssembly()->Location;
    String^ pluginPath = Path::GetDirectoryName(dllPath);
    String^ pluginBaseFilename = Path::GetFileNameWithoutExtension(dllPath);
    return Path::Combine(pluginPath, pluginBaseFilename, "ScytheDotNet.dll");
}

void invokeInitOnDotNetDll(String^ dotNetAssemblyPath, 
                              Dictionary<String^, IntPtr>^ reaperFunctions)
{
    Assembly^ assembly = Assembly::LoadFile(dotNetAssemblyPath);
    Type^ reaperAPI = assembly->GetType("Scythe.ReaperAPI");
    MethodInfo^ initMethod = reaperAPI->GetMethod("Init");
    initMethod->Invoke(nullptr, gcnew array<System::Object^>{ reaperFunctions });
}

} // anonymous

//-----------------------------------------------------------------------------

extern "C"
{

REAPER_PLUGIN_DLL_EXPORT int ReaperPluginEntry(void* pluginInstance,
    reaper_plugin_info_t *pluginInfo)
{
    if (pluginInfo == NULL) {
        return 1;
    }

    Scythe::init(pluginInfo);

    Dictionary<String^, IntPtr>^ reaperFunctions = marshalFunctionMap();    
    String^ dotNetAssemblyPath = getScytheDotNetPath();

    if (!File::Exists(dotNetAssemblyPath)) 
    {
        return 0;
    }

    invokeInitOnDotNetDll(dotNetAssemblyPath, reaperFunctions);
    
    return 1;
}

};
